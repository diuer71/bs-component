const path = require('path');

function resolve (dir) {
  return path.join(__dirname, '.', dir);
}

module.exports = function override(config) {
  config.resolve.alias = {
    ...config.resolve.alias,
    '@': resolve('src'),
    '@components': resolve('src/components'),
    '@routes': resolve('src/routes'),
    '@constants': resolve('src/constants'),
    '@hooks': resolve('src/hooks'),
    '@layouts': resolve('src/layouts'),
    '@pages': resolve('src/pages'),
    '@sections': resolve('src/sections'),
    // '@': resolve('src/'),
    // '@': resolve('src/'),
    // '@': resolve('src/'),
  };
  return config;
}

