import React from 'react';
import { useLocation } from 'react-router-dom'; // @mui

import { styled, useTheme } from '@mui/material/styles';
import { Box, Button, AppBar, Toolbar, Container } from '@mui/material'; // hooks

import useOffSetTop from '../../hooks/useOffSetTop';
import useResponsive from '../../hooks/useResponsive'; // utils

import cssStyles from '../../utils/cssStyles'; // config

import { HEADER } from '../../config'; // components

import Logo from '../../components/Logo';
import Label from '../../components/Label'; //

import MenuDesktop from './MenuDesktop';
import MenuMobile from './MenuMobile';
import navConfig from './MenuConfig'; // ----------------------------------------------------------------------

const ToolbarStyle = styled(Toolbar)(({
  theme
}) => ({
  height: HEADER.MOBILE_HEIGHT,
  transition: theme.transitions.create(['height', 'background-color'], {
    easing: theme.transitions.easing.easeInOut,
    duration: theme.transitions.duration.shorter
  }),
  [theme.breakpoints.up('md')]: {
    height: HEADER.MAIN_DESKTOP_HEIGHT
  }
}));
const ToolbarShadowStyle = styled('div')(({
  theme
}) => ({
  left: 0,
  right: 0,
  bottom: 0,
  height: 24,
  zIndex: -1,
  margin: 'auto',
  borderRadius: '50%',
  position: 'absolute',
  width: `calc(100% - 48px)`,
  boxShadow: theme.customShadows.z8
})); // ----------------------------------------------------------------------

export default function MainHeader() {
  const isOffset = useOffSetTop(HEADER.MAIN_DESKTOP_HEIGHT);
  const theme = useTheme();
  const {
    pathname
  } = useLocation();
  const isDesktop = useResponsive('up', 'md');
  const isHome = pathname === '/';
  return /*#__PURE__*/React.createElement(AppBar, {
    sx: {
      boxShadow: 0,
      bgcolor: 'transparent'
    }
  }, /*#__PURE__*/React.createElement(ToolbarStyle, {
    disableGutters: true,
    sx: { ...(isOffset && { ...cssStyles(theme).bgBlur(),
        height: {
          md: HEADER.MAIN_DESKTOP_HEIGHT - 16
        }
      })
    }
  }, /*#__PURE__*/React.createElement(Container, {
    sx: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'space-between'
    }
  }, /*#__PURE__*/React.createElement(Logo, null), /*#__PURE__*/React.createElement(Label, {
    color: "info",
    sx: {
      ml: 1
    }
  }, "v3.0.0"), /*#__PURE__*/React.createElement(Box, {
    sx: {
      flexGrow: 1
    }
  }), isDesktop && /*#__PURE__*/React.createElement(MenuDesktop, {
    isOffset: isOffset,
    isHome: isHome,
    navConfig: navConfig
  }), /*#__PURE__*/React.createElement(Button, {
    variant: "contained",
    target: "_blank",
    rel: "noopener",
    href: "https://material-ui.com/store/items/minimal-dashboard/"
  }, "Purchase Now"), !isDesktop && /*#__PURE__*/React.createElement(MenuMobile, {
    isOffset: isOffset,
    isHome: isHome,
    navConfig: navConfig
  }))), isOffset && /*#__PURE__*/React.createElement(ToolbarShadowStyle, null));
}