import React from 'react';
import { Link as RouterLink } from 'react-router-dom'; // @mui

import { styled } from '@mui/material/styles';
import { Grid, Link, Divider, Container, Typography, Stack } from '@mui/material'; // routes

import { PATH_PAGE } from '../../routes/paths'; // components

import Logo from '../../components/Logo';
import SocialsButton from '../../components/SocialsButton'; // ----------------------------------------------------------------------

const LINKS = [{
  headline: 'Minimal',
  children: [{
    name: 'About us',
    href: PATH_PAGE.about
  }, {
    name: 'Contact us',
    href: PATH_PAGE.contact
  }, {
    name: 'FAQs',
    href: PATH_PAGE.faqs
  }]
}, {
  headline: 'Legal',
  children: [{
    name: 'Terms and Condition',
    href: '#'
  }, {
    name: 'Privacy Policy',
    href: '#'
  }]
}, {
  headline: 'Contact',
  children: [{
    name: 'support@minimals.cc',
    href: '#'
  }, {
    name: 'Los Angeles, 359  Hidden Valley Road',
    href: '#'
  }]
}];
const RootStyle = styled('div')(({
  theme
}) => ({
  position: 'relative',
  backgroundColor: theme.palette.background.default
})); // ----------------------------------------------------------------------

export default function MainFooter() {
  return /*#__PURE__*/React.createElement(RootStyle, null, /*#__PURE__*/React.createElement(Divider, null), /*#__PURE__*/React.createElement(Container, {
    sx: {
      pt: 10
    }
  }, /*#__PURE__*/React.createElement(Grid, {
    container: true,
    justifyContent: {
      xs: 'center',
      md: 'space-between'
    },
    sx: {
      textAlign: {
        xs: 'center',
        md: 'left'
      }
    }
  }, /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: 12,
    sx: {
      mb: 3
    }
  }, /*#__PURE__*/React.createElement(Logo, {
    sx: {
      mx: {
        xs: 'auto',
        md: 'inherit'
      }
    }
  })), /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: 8,
    md: 3
  }, /*#__PURE__*/React.createElement(Typography, {
    variant: "body2",
    sx: {
      pr: {
        md: 5
      }
    }
  }, "The starting point for your next project with Minimal UI Kit, built on the newest version of Material-UI \xA9, ready to be customized to your style."), /*#__PURE__*/React.createElement(Stack, {
    direction: "row",
    justifyContent: {
      xs: 'center',
      md: 'flex-start'
    },
    sx: {
      mt: 5,
      mb: {
        xs: 5,
        md: 0
      }
    }
  }, /*#__PURE__*/React.createElement(SocialsButton, {
    sx: {
      mx: 0.5
    }
  }))), /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: 12,
    md: 7
  }, /*#__PURE__*/React.createElement(Stack, {
    spacing: 5,
    direction: {
      xs: 'column',
      md: 'row'
    },
    justifyContent: "space-between"
  }, LINKS.map(list => /*#__PURE__*/React.createElement(Stack, {
    key: list.headline,
    spacing: 2
  }, /*#__PURE__*/React.createElement(Typography, {
    component: "p",
    variant: "overline"
  }, list.headline), list.children.map(link => /*#__PURE__*/React.createElement(Link, {
    to: link.href,
    key: link.name,
    color: "inherit",
    variant: "body2",
    component: RouterLink,
    sx: {
      display: 'block'
    }
  }, link.name))))))), /*#__PURE__*/React.createElement(Typography, {
    component: "p",
    variant: "body2",
    sx: {
      mt: 10,
      pb: 5,
      fontSize: 13,
      textAlign: {
        xs: 'center',
        md: 'left'
      }
    }
  }, "\xA9 2021. All rights reserved")));
}