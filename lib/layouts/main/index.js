import { useLocation, Outlet } from 'react-router-dom'; // @mui

import { Box, Link, Container, Typography, Stack } from '@mui/material'; // components

import Logo from '../../components/Logo'; //

import MainFooter from './MainFooter';
import MainHeader from './MainHeader'; // ----------------------------------------------------------------------

export default function MainLayout() {
  const {
    pathname
  } = useLocation();
  const isHome = pathname === '/';
  return /*#__PURE__*/React.createElement(Stack, {
    sx: {
      minHeight: 1
    }
  }, /*#__PURE__*/React.createElement(MainHeader, null), /*#__PURE__*/React.createElement(Outlet, null), /*#__PURE__*/React.createElement(Box, {
    sx: {
      flexGrow: 1
    }
  }), !isHome ? /*#__PURE__*/React.createElement(MainFooter, null) : /*#__PURE__*/React.createElement(Box, {
    sx: {
      py: 5,
      textAlign: 'center',
      position: 'relative',
      bgcolor: 'background.default'
    }
  }, /*#__PURE__*/React.createElement(Container, null, /*#__PURE__*/React.createElement(Logo, {
    sx: {
      mb: 1,
      mx: 'auto'
    }
  }), /*#__PURE__*/React.createElement(Typography, {
    variant: "caption",
    component: "p"
  }, "\xA9 All rights reserved", /*#__PURE__*/React.createElement("br", null), " made by \xA0", /*#__PURE__*/React.createElement(Link, {
    href: "https://minimals.cc/"
  }, "minimals.cc")))));
}