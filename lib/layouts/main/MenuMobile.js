import React from 'react';
import PropTypes from 'prop-types';
import { useState, useEffect } from 'react';
import { NavLink as RouterLink, useLocation } from 'react-router-dom'; // @mui

import { alpha, styled } from '@mui/material/styles';
import { Box, List, Link, Drawer, Collapse, ListItemText, ListItemIcon, ListItemButton } from '@mui/material'; // config

import { NAVBAR } from '../../config'; // components

import Logo from '../../components/Logo';
import Iconify from '../../components/Iconify';
import Scrollbar from '../../components/Scrollbar';
import { IconButtonAnimate } from '../../components/animate';
import { NavSectionVertical } from '../../components/nav-section'; // ----------------------------------------------------------------------

const ListItemStyle = styled(ListItemButton)(({
  theme
}) => ({ ...theme.typography.body2,
  height: NAVBAR.DASHBOARD_ITEM_ROOT_HEIGHT,
  textTransform: 'capitalize',
  color: theme.palette.text.secondary
})); // ----------------------------------------------------------------------

MenuMobile.propTypes = {
  isOffset: PropTypes.bool,
  isHome: PropTypes.bool,
  navConfig: PropTypes.array
};
export default function MenuMobile({
  isOffset,
  isHome,
  navConfig
}) {
  const {
    pathname
  } = useLocation();
  const [open, setOpen] = useState(false);
  const [drawerOpen, setDrawerOpen] = useState(false);
  useEffect(() => {
    if (drawerOpen) {
      handleDrawerClose();
    } // eslint-disable-next-line react-hooks/exhaustive-deps

  }, [pathname]);

  const handleOpen = () => {
    setOpen(!open);
  };

  const handleDrawerOpen = () => {
    setDrawerOpen(true);
  };

  const handleDrawerClose = () => {
    setDrawerOpen(false);
  };

  return /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement(IconButtonAnimate, {
    onClick: handleDrawerOpen,
    sx: {
      ml: 1,
      ...(isHome && {
        color: 'common.white'
      }),
      ...(isOffset && {
        color: 'text.primary'
      })
    }
  }, /*#__PURE__*/React.createElement(Iconify, {
    icon: 'eva:menu-2-fill'
  })), /*#__PURE__*/React.createElement(Drawer, {
    open: drawerOpen,
    onClose: handleDrawerClose,
    ModalProps: {
      keepMounted: true
    },
    PaperProps: {
      sx: {
        pb: 5,
        width: 260
      }
    }
  }, /*#__PURE__*/React.createElement(Scrollbar, null, /*#__PURE__*/React.createElement(Logo, {
    sx: {
      mx: 2.5,
      my: 3
    }
  }), /*#__PURE__*/React.createElement(List, {
    disablePadding: true
  }, navConfig.map(link => /*#__PURE__*/React.createElement(MenuMobileItem, {
    key: link.title,
    item: link,
    isOpen: open,
    onOpen: handleOpen
  }))))));
} // ----------------------------------------------------------------------

MenuMobileItem.propTypes = {
  isOpen: PropTypes.bool,
  item: PropTypes.shape({
    children: PropTypes.array,
    icon: PropTypes.any,
    path: PropTypes.string,
    title: PropTypes.string
  }),
  onOpen: PropTypes.func
};

function MenuMobileItem({
  item,
  isOpen,
  onOpen
}) {
  const {
    title,
    path,
    icon,
    children
  } = item;

  if (children) {
    return /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement(ListItemStyle, {
      onClick: onOpen
    }, /*#__PURE__*/React.createElement(ListItemIcon, null, icon), /*#__PURE__*/React.createElement(ListItemText, {
      disableTypography: true,
      primary: title
    }), /*#__PURE__*/React.createElement(Iconify, {
      icon: isOpen ? 'eva:arrow-ios-downward-fill' : 'eva:arrow-ios-forward-fill',
      sx: {
        width: 16,
        height: 16,
        ml: 1
      }
    })), /*#__PURE__*/React.createElement(Collapse, {
      in: isOpen,
      timeout: "auto",
      unmountOnExit: true
    }, /*#__PURE__*/React.createElement(Box, {
      sx: {
        display: 'flex',
        flexDirection: 'column-reverse'
      }
    }, /*#__PURE__*/React.createElement(NavSectionVertical, {
      navConfig: children,
      sx: {
        '& .MuiList-root:last-of-type .MuiListItemButton-root': {
          height: 200,
          backgroundSize: 'cover',
          backgroundPosition: 'center',
          bgcolor: 'background.neutral',
          backgroundRepeat: 'no-repeat',
          backgroundImage: 'url(https://minimal-assets-api.vercel.app/assets/illustrations/illustration_dashboard.png)',
          '& > *:not(.MuiTouchRipple-root)': {
            display: 'none'
          }
        }
      }
    }))));
  }

  if (title === 'Documentation') {
    return /*#__PURE__*/React.createElement(ListItemStyle, {
      href: path,
      target: "_blank",
      rel: "noopener",
      component: Link
    }, /*#__PURE__*/React.createElement(ListItemIcon, null, icon), /*#__PURE__*/React.createElement(ListItemText, {
      disableTypography: true,
      primary: title
    }));
  }

  return /*#__PURE__*/React.createElement(ListItemStyle, {
    to: path,
    component: RouterLink,
    end: path === '/',
    sx: {
      '&.active': {
        color: 'primary.main',
        fontWeight: 'fontWeightMedium',
        bgcolor: theme => alpha(theme.palette.primary.main, theme.palette.action.selectedOpacity)
      }
    }
  }, /*#__PURE__*/React.createElement(ListItemIcon, null, icon), /*#__PURE__*/React.createElement(ListItemText, {
    disableTypography: true,
    primary: title
  }));
}