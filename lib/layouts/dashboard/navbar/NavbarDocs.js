import React from 'react'; // @mui

import { Stack, Button, Typography } from '@mui/material'; // hooks

import useAuth from '../../../hooks/useAuth'; // routes

import { PATH_DOCS } from '../../../routes/paths'; // assets

import { DocIllustration } from '../../../assets'; // ----------------------------------------------------------------------

export default function NavbarDocs() {
  const {
    user
  } = useAuth();
  return /*#__PURE__*/React.createElement(Stack, {
    spacing: 3,
    sx: {
      px: 5,
      pb: 5,
      mt: 10,
      width: 1,
      textAlign: 'center',
      display: 'block'
    }
  }, /*#__PURE__*/React.createElement(DocIllustration, {
    sx: {
      width: 1
    }
  }), /*#__PURE__*/React.createElement("div", null, /*#__PURE__*/React.createElement(Typography, {
    gutterBottom: true,
    variant: "subtitle1"
  }, "Hi, ", user?.displayName), /*#__PURE__*/React.createElement(Typography, {
    variant: "body2",
    sx: {
      color: 'text.secondary'
    }
  }, "Need help?", /*#__PURE__*/React.createElement("br", null), " Please check our docs")), /*#__PURE__*/React.createElement(Button, {
    href: PATH_DOCS,
    target: "_blank",
    rel: "noopener",
    variant: "contained"
  }, "Documentation"));
}