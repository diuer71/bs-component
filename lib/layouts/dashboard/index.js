import { useState } from 'react';
import { Outlet } from 'react-router-dom'; // @mui

import { styled } from '@mui/material/styles';
import { Box } from '@mui/material'; // hooks

import useSettings from '../../hooks/useSettings';
import useResponsive from '../../hooks/useResponsive';
import useCollapseDrawer from '../../hooks/useCollapseDrawer'; // config

import { HEADER, NAVBAR } from '../../config'; //

import DashboardHeader from './header';
import NavbarVertical from './navbar/NavbarVertical';
import NavbarHorizontal from './navbar/NavbarHorizontal'; // ----------------------------------------------------------------------

const MainStyle = styled('main', {
  shouldForwardProp: prop => prop !== 'collapseClick'
})(({
  collapseClick,
  theme
}) => ({
  flexGrow: 1,
  paddingTop: HEADER.MOBILE_HEIGHT + 24,
  paddingBottom: HEADER.MOBILE_HEIGHT + 24,
  [theme.breakpoints.up('lg')]: {
    paddingLeft: 16,
    paddingRight: 16,
    paddingTop: HEADER.DASHBOARD_DESKTOP_HEIGHT + 24,
    paddingBottom: HEADER.DASHBOARD_DESKTOP_HEIGHT + 24,
    width: `calc(100% - ${NAVBAR.DASHBOARD_WIDTH}px)`,
    transition: theme.transitions.create('margin-left', {
      duration: theme.transitions.duration.shorter
    }),
    ...(collapseClick && {
      marginLeft: NAVBAR.DASHBOARD_COLLAPSE_WIDTH
    })
  }
})); // ----------------------------------------------------------------------

export default function DashboardLayout() {
  const {
    collapseClick,
    isCollapse
  } = useCollapseDrawer();
  const {
    themeLayout
  } = useSettings();
  const isDesktop = useResponsive('up', 'lg');
  const [open, setOpen] = useState(false);
  const verticalLayout = themeLayout === 'vertical';

  if (verticalLayout) {
    return /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement(DashboardHeader, {
      onOpenSidebar: () => setOpen(true),
      verticalLayout: verticalLayout
    }), isDesktop ? /*#__PURE__*/React.createElement(NavbarHorizontal, null) : /*#__PURE__*/React.createElement(NavbarVertical, {
      isOpenSidebar: open,
      onCloseSidebar: () => setOpen(false)
    }), /*#__PURE__*/React.createElement(Box, {
      component: "main",
      sx: {
        px: {
          lg: 2
        },
        pt: {
          xs: `${HEADER.MOBILE_HEIGHT + 24}px`,
          lg: `${HEADER.DASHBOARD_DESKTOP_HEIGHT + 80}px`
        },
        pb: {
          xs: `${HEADER.MOBILE_HEIGHT + 24}px`,
          lg: `${HEADER.DASHBOARD_DESKTOP_HEIGHT + 24}px`
        }
      }
    }, /*#__PURE__*/React.createElement(Outlet, null)));
  }

  return /*#__PURE__*/React.createElement(Box, {
    sx: {
      display: {
        lg: 'flex'
      },
      minHeight: {
        lg: 1
      }
    }
  }, /*#__PURE__*/React.createElement(DashboardHeader, {
    isCollapse: isCollapse,
    onOpenSidebar: () => setOpen(true)
  }), /*#__PURE__*/React.createElement(NavbarVertical, {
    isOpenSidebar: open,
    onCloseSidebar: () => setOpen(false)
  }), /*#__PURE__*/React.createElement(MainStyle, {
    collapseClick: collapseClick
  }, /*#__PURE__*/React.createElement(Outlet, null)));
}