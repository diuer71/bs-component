import React from 'react';
import { useState } from 'react'; // @mui

import { styled } from '@mui/material/styles';
import { Input, Slide, Button, InputAdornment, ClickAwayListener } from '@mui/material'; // utils

import cssStyles from '../../../utils/cssStyles'; // components

import Iconify from '../../../components/Iconify';
import { IconButtonAnimate } from '../../../components/animate'; // ----------------------------------------------------------------------

const APPBAR_MOBILE = 64;
const APPBAR_DESKTOP = 92;
const SearchbarStyle = styled('div')(({
  theme
}) => ({ ...cssStyles(theme).bgBlur(),
  top: 0,
  left: 0,
  zIndex: 99,
  width: '100%',
  display: 'flex',
  position: 'absolute',
  alignItems: 'center',
  height: APPBAR_MOBILE,
  padding: theme.spacing(0, 3),
  boxShadow: theme.customShadows.z8,
  [theme.breakpoints.up('md')]: {
    height: APPBAR_DESKTOP,
    padding: theme.spacing(0, 5)
  }
})); // ----------------------------------------------------------------------

export default function Searchbar() {
  const [isOpen, setOpen] = useState(false);

  const handleOpen = () => {
    setOpen(prev => !prev);
  };

  const handleClose = () => {
    setOpen(false);
  };

  return /*#__PURE__*/React.createElement(ClickAwayListener, {
    onClickAway: handleClose
  }, /*#__PURE__*/React.createElement("div", null, !isOpen && /*#__PURE__*/React.createElement(IconButtonAnimate, {
    onClick: handleOpen
  }, /*#__PURE__*/React.createElement(Iconify, {
    icon: 'eva:search-fill',
    width: 20,
    height: 20
  })), /*#__PURE__*/React.createElement(Slide, {
    direction: "down",
    in: isOpen,
    mountOnEnter: true,
    unmountOnExit: true
  }, /*#__PURE__*/React.createElement(SearchbarStyle, null, /*#__PURE__*/React.createElement(Input, {
    autoFocus: true,
    fullWidth: true,
    disableUnderline: true,
    placeholder: "Search\u2026",
    startAdornment: /*#__PURE__*/React.createElement(InputAdornment, {
      position: "start"
    }, /*#__PURE__*/React.createElement(Iconify, {
      icon: 'eva:search-fill',
      sx: {
        color: 'text.disabled',
        width: 20,
        height: 20
      }
    })),
    sx: {
      mr: 1,
      fontWeight: 'fontWeightBold'
    }
  }), /*#__PURE__*/React.createElement(Button, {
    variant: "contained",
    onClick: handleClose
  }, "Search")))));
}