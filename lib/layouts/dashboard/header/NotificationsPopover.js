import React from 'react';
import PropTypes from 'prop-types';
import { noCase } from 'change-case';
import { useState } from 'react'; // @mui

import { Box, List, Badge, Button, Avatar, Tooltip, Divider, Typography, ListItemText, ListSubheader, ListItemAvatar, ListItemButton } from '@mui/material'; // utils

import { fToNow } from '../../../utils/formatTime'; // _mock_

import { _notifications } from '../../../_mock'; // components

import Iconify from '../../../components/Iconify';
import Scrollbar from '../../../components/Scrollbar';
import MenuPopover from '../../../components/MenuPopover';
import { IconButtonAnimate } from '../../../components/animate'; // ----------------------------------------------------------------------

export default function NotificationsPopover() {
  const [notifications, setNotifications] = useState(_notifications);
  const totalUnRead = notifications.filter(item => item.isUnRead === true).length;
  const [open, setOpen] = useState(null);

  const handleOpen = event => {
    setOpen(event.currentTarget);
  };

  const handleClose = () => {
    setOpen(null);
  };

  const handleMarkAllAsRead = () => {
    setNotifications(notifications.map(notification => ({ ...notification,
      isUnRead: false
    })));
  };

  return /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement(IconButtonAnimate, {
    color: open ? 'primary' : 'default',
    onClick: handleOpen,
    sx: {
      width: 40,
      height: 40
    }
  }, /*#__PURE__*/React.createElement(Badge, {
    badgeContent: totalUnRead,
    color: "error"
  }, /*#__PURE__*/React.createElement(Iconify, {
    icon: "eva:bell-fill",
    width: 20,
    height: 20
  }))), /*#__PURE__*/React.createElement(MenuPopover, {
    open: Boolean(open),
    anchorEl: open,
    onClose: handleClose,
    sx: {
      width: 360,
      p: 0,
      mt: 1.5,
      ml: 0.75
    }
  }, /*#__PURE__*/React.createElement(Box, {
    sx: {
      display: 'flex',
      alignItems: 'center',
      py: 2,
      px: 2.5
    }
  }, /*#__PURE__*/React.createElement(Box, {
    sx: {
      flexGrow: 1
    }
  }, /*#__PURE__*/React.createElement(Typography, {
    variant: "subtitle1"
  }, "Notifications"), /*#__PURE__*/React.createElement(Typography, {
    variant: "body2",
    sx: {
      color: 'text.secondary'
    }
  }, "You have ", totalUnRead, " unread messages")), totalUnRead > 0 && /*#__PURE__*/React.createElement(Tooltip, {
    title: " Mark all as read"
  }, /*#__PURE__*/React.createElement(IconButtonAnimate, {
    color: "primary",
    onClick: handleMarkAllAsRead
  }, /*#__PURE__*/React.createElement(Iconify, {
    icon: "eva:done-all-fill",
    width: 20,
    height: 20
  })))), /*#__PURE__*/React.createElement(Divider, {
    sx: {
      borderStyle: 'dashed'
    }
  }), /*#__PURE__*/React.createElement(Scrollbar, {
    sx: {
      height: {
        xs: 340,
        sm: 'auto'
      }
    }
  }, /*#__PURE__*/React.createElement(List, {
    disablePadding: true,
    subheader: /*#__PURE__*/React.createElement(ListSubheader, {
      disableSticky: true,
      sx: {
        py: 1,
        px: 2.5,
        typography: 'overline'
      }
    }, "New")
  }, notifications.slice(0, 2).map(notification => /*#__PURE__*/React.createElement(NotificationItem, {
    key: notification.id,
    notification: notification
  }))), /*#__PURE__*/React.createElement(List, {
    disablePadding: true,
    subheader: /*#__PURE__*/React.createElement(ListSubheader, {
      disableSticky: true,
      sx: {
        py: 1,
        px: 2.5,
        typography: 'overline'
      }
    }, "Before that")
  }, notifications.slice(2, 5).map(notification => /*#__PURE__*/React.createElement(NotificationItem, {
    key: notification.id,
    notification: notification
  })))), /*#__PURE__*/React.createElement(Divider, {
    sx: {
      borderStyle: 'dashed'
    }
  }), /*#__PURE__*/React.createElement(Box, {
    sx: {
      p: 1
    }
  }, /*#__PURE__*/React.createElement(Button, {
    fullWidth: true,
    disableRipple: true
  }, "View All"))));
} // ----------------------------------------------------------------------

NotificationItem.propTypes = {
  notification: PropTypes.shape({
    createdAt: PropTypes.instanceOf(Date),
    id: PropTypes.string,
    isUnRead: PropTypes.bool,
    title: PropTypes.string,
    description: PropTypes.string,
    type: PropTypes.string,
    avatar: PropTypes.any
  })
};

function NotificationItem({
  notification
}) {
  const {
    avatar,
    title
  } = renderContent(notification);
  return /*#__PURE__*/React.createElement(ListItemButton, {
    sx: {
      py: 1.5,
      px: 2.5,
      mt: '1px',
      ...(notification.isUnRead && {
        bgcolor: 'action.selected'
      })
    }
  }, /*#__PURE__*/React.createElement(ListItemAvatar, null, /*#__PURE__*/React.createElement(Avatar, {
    sx: {
      bgcolor: 'background.neutral'
    }
  }, avatar)), /*#__PURE__*/React.createElement(ListItemText, {
    primary: title,
    secondary: /*#__PURE__*/React.createElement(Typography, {
      variant: "caption",
      sx: {
        mt: 0.5,
        display: 'flex',
        alignItems: 'center',
        color: 'text.disabled'
      }
    }, /*#__PURE__*/React.createElement(Iconify, {
      icon: "eva:clock-outline",
      sx: {
        mr: 0.5,
        width: 16,
        height: 16
      }
    }), fToNow(notification.createdAt))
  }));
} // ----------------------------------------------------------------------


function renderContent(notification) {
  const title = /*#__PURE__*/React.createElement(Typography, {
    variant: "subtitle2"
  }, notification.title, /*#__PURE__*/React.createElement(Typography, {
    component: "span",
    variant: "body2",
    sx: {
      color: 'text.secondary'
    }
  }, "\xA0 ", noCase(notification.description)));

  if (notification.type === 'order_placed') {
    return {
      avatar: /*#__PURE__*/React.createElement("img", {
        alt: notification.title,
        src: "https://minimal-assets-api.vercel.app/assets/icons/ic_notification_package.svg"
      }),
      title
    };
  }

  if (notification.type === 'order_shipped') {
    return {
      avatar: /*#__PURE__*/React.createElement("img", {
        alt: notification.title,
        src: "https://minimal-assets-api.vercel.app/assets/icons/ic_notification_shipping.svg"
      }),
      title
    };
  }

  if (notification.type === 'mail') {
    return {
      avatar: /*#__PURE__*/React.createElement("img", {
        alt: notification.title,
        src: "https://minimal-assets-api.vercel.app/assets/icons/ic_notification_mail.svg"
      }),
      title
    };
  }

  if (notification.type === 'chat_message') {
    return {
      avatar: /*#__PURE__*/React.createElement("img", {
        alt: notification.title,
        src: "https://minimal-assets-api.vercel.app/assets/icons/ic_notification_chat.svg"
      }),
      title
    };
  }

  return {
    avatar: notification.avatar ? /*#__PURE__*/React.createElement("img", {
      alt: notification.title,
      src: notification.avatar
    }) : null,
    title
  };
}