import React from 'react';
import { useState } from 'react'; // @mui

import { alpha } from '@mui/material/styles';
import { Avatar, Typography, ListItemText, ListItemAvatar, MenuItem } from '@mui/material'; // utils

import { fToNow } from '../../../utils/formatTime'; // _mock_

import { _contacts } from '../../../_mock'; // components

import Iconify from '../../../components/Iconify';
import Scrollbar from '../../../components/Scrollbar';
import MenuPopover from '../../../components/MenuPopover';
import BadgeStatus from '../../../components/BadgeStatus';
import { IconButtonAnimate } from '../../../components/animate'; // ----------------------------------------------------------------------

const ITEM_HEIGHT = 64; // ----------------------------------------------------------------------

export default function ContactsPopover() {
  const [open, setOpen] = useState(null);

  const handleOpen = event => {
    setOpen(event.currentTarget);
  };

  const handleClose = () => {
    setOpen(null);
  };

  return /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement(IconButtonAnimate, {
    color: open ? 'primary' : 'default',
    onClick: handleOpen,
    sx: {
      width: 40,
      height: 40,
      ...(open && {
        bgcolor: theme => alpha(theme.palette.primary.main, theme.palette.action.focusOpacity)
      })
    }
  }, /*#__PURE__*/React.createElement(Iconify, {
    icon: 'eva:people-fill',
    width: 20,
    height: 20
  })), /*#__PURE__*/React.createElement(MenuPopover, {
    open: Boolean(open),
    anchorEl: open,
    onClose: handleClose,
    sx: {
      mt: 1.5,
      ml: 0.75,
      width: 320,
      '& .MuiMenuItem-root': {
        px: 1.5,
        height: ITEM_HEIGHT,
        borderRadius: 0.75
      }
    }
  }, /*#__PURE__*/React.createElement(Typography, {
    variant: "h6",
    sx: {
      p: 1.5
    }
  }, "Contacts ", /*#__PURE__*/React.createElement(Typography, {
    component: "span"
  }, "(", _contacts.length, ")")), /*#__PURE__*/React.createElement(Scrollbar, {
    sx: {
      height: ITEM_HEIGHT * 6
    }
  }, _contacts.map(contact => /*#__PURE__*/React.createElement(MenuItem, {
    key: contact.id
  }, /*#__PURE__*/React.createElement(ListItemAvatar, {
    sx: {
      position: 'relative'
    }
  }, /*#__PURE__*/React.createElement(Avatar, {
    src: contact.avatar
  }), /*#__PURE__*/React.createElement(BadgeStatus, {
    status: contact.status,
    sx: {
      position: 'absolute',
      right: 1,
      bottom: 1
    }
  })), /*#__PURE__*/React.createElement(ListItemText, {
    primaryTypographyProps: {
      typography: 'subtitle2',
      mb: 0.25
    },
    secondaryTypographyProps: {
      typography: 'caption'
    },
    primary: contact.name,
    secondary: contact.status === 'offline' && fToNow(contact.lastActivity)
  }))))));
}