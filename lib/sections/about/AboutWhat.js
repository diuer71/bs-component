import PropTypes from 'prop-types'; // @mui

import { alpha, useTheme, styled } from '@mui/material/styles';
import { Box, Grid, Button, Container, Typography, LinearProgress } from '@mui/material'; // hooks

import useResponsive from '../../hooks/useResponsive'; // utils

import { fPercent } from '../../utils/formatNumber'; // _mock_

import { _skills } from '../../_mock'; // components

import Image from '../../components/Image';
import Iconify from '../../components/Iconify';
import { MotionInView, varFade } from '../../components/animate'; // ----------------------------------------------------------------------

const RootStyle = styled('div')(({
  theme
}) => ({
  textAlign: 'center',
  paddingTop: theme.spacing(20),
  paddingBottom: theme.spacing(10),
  [theme.breakpoints.up('md')]: {
    textAlign: 'left'
  }
})); // ----------------------------------------------------------------------

export default function AboutWhat() {
  const theme = useTheme();
  const isDesktop = useResponsive('up', 'md');
  const isLight = theme.palette.mode === 'light';
  const shadow = `-40px 40px 80px ${alpha(isLight ? theme.palette.grey[500] : theme.palette.common.black, 0.48)}`;
  return /*#__PURE__*/React.createElement(RootStyle, null, /*#__PURE__*/React.createElement(Container, null, /*#__PURE__*/React.createElement(Grid, {
    container: true,
    spacing: 3
  }, isDesktop && /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: 12,
    md: 6,
    lg: 7,
    sx: {
      pr: {
        md: 7
      }
    }
  }, /*#__PURE__*/React.createElement(Grid, {
    container: true,
    spacing: 3,
    alignItems: "flex-end"
  }, /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: 6
  }, /*#__PURE__*/React.createElement(MotionInView, {
    variants: varFade().inUp
  }, /*#__PURE__*/React.createElement(Image, {
    src: "https://minimal-assets-api.vercel.app/assets/images/about/what-1.jpg",
    ratio: "3/4",
    sx: {
      borderRadius: 2,
      boxShadow: shadow
    }
  }))), /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: 6
  }, /*#__PURE__*/React.createElement(MotionInView, {
    variants: varFade().inUp
  }, /*#__PURE__*/React.createElement(Image, {
    src: "https://minimal-assets-api.vercel.app/assets/images/about/what-2.jpg",
    ratio: "1/1",
    sx: {
      borderRadius: 2
    }
  }))))), /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: 12,
    md: 6,
    lg: 5
  }, /*#__PURE__*/React.createElement(MotionInView, {
    variants: varFade().inRight
  }, /*#__PURE__*/React.createElement(Typography, {
    variant: "h2",
    sx: {
      mb: 3
    }
  }, "What is minimal?")), /*#__PURE__*/React.createElement(MotionInView, {
    variants: varFade().inRight
  }, /*#__PURE__*/React.createElement(Typography, {
    sx: {
      color: theme => theme.palette.mode === 'light' ? 'text.secondary' : 'common.white'
    }
  }, "Our theme is the most advanced and user-friendly theme you will find on the market, we have documentation and video to help set your site really easily, pre-installed demos you can import in one click and everything from the theme options to page content can be edited from the front-end. This is the theme you are looking for.")), /*#__PURE__*/React.createElement(Box, {
    sx: {
      my: 5
    }
  }, _skills.map(progress => /*#__PURE__*/React.createElement(MotionInView, {
    key: progress.label,
    variants: varFade().inRight
  }, /*#__PURE__*/React.createElement(ProgressItem, {
    progress: progress
  })))), /*#__PURE__*/React.createElement(MotionInView, {
    variants: varFade().inRight
  }, /*#__PURE__*/React.createElement(Button, {
    variant: "outlined",
    color: "inherit",
    size: "large",
    endIcon: /*#__PURE__*/React.createElement(Iconify, {
      icon: 'ic:round-arrow-right-alt',
      width: 24,
      height: 24
    })
  }, "Check out our work"))))));
} // ----------------------------------------------------------------------

ProgressItem.propTypes = {
  progress: PropTypes.shape({
    label: PropTypes.string,
    value: PropTypes.number
  })
};

function ProgressItem({
  progress
}) {
  const {
    label,
    value
  } = progress;
  return /*#__PURE__*/React.createElement(Box, {
    sx: {
      mt: 3
    }
  }, /*#__PURE__*/React.createElement(Box, {
    sx: {
      mb: 1.5,
      display: 'flex',
      alignItems: 'center'
    }
  }, /*#__PURE__*/React.createElement(Typography, {
    variant: "subtitle2"
  }, label, "\xA0-\xA0"), /*#__PURE__*/React.createElement(Typography, {
    variant: "body2",
    sx: {
      color: 'text.secondary'
    }
  }, fPercent(value))), /*#__PURE__*/React.createElement(LinearProgress, {
    variant: "determinate",
    value: value,
    sx: {
      '& .MuiLinearProgress-bar': {
        bgcolor: 'grey.700'
      },
      '&.MuiLinearProgress-determinate': {
        bgcolor: 'divider'
      }
    }
  }));
}