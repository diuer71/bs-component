// @mui
import { Box, Container, Typography, Grid } from '@mui/material'; // components

import Image from '../../components/Image';
import { MotionInView, varFade } from '../../components/animate'; // ----------------------------------------------------------------------

export default function AboutVision() {
  return /*#__PURE__*/React.createElement(Container, {
    sx: {
      mt: 10
    }
  }, /*#__PURE__*/React.createElement(Box, {
    sx: {
      mb: 10,
      position: 'relative',
      borderRadius: 2,
      overflow: 'hidden'
    }
  }, /*#__PURE__*/React.createElement(Image, {
    src: "https://minimal-assets-api.vercel.app/assets/images/about/vision.jpg",
    alt: "about-vision",
    effect: "black-and-white"
  }), /*#__PURE__*/React.createElement(Box, {
    sx: {
      bottom: {
        xs: 24,
        md: 80
      },
      width: '100%',
      display: 'flex',
      flexWrap: 'wrap',
      alignItems: 'center',
      position: 'absolute',
      justifyContent: 'center'
    }
  }, ['logo_amazon', 'logo_hbo', 'logo_ibm', 'logo_lya', 'logo_spotify', 'logo_netflix'].map(logo => /*#__PURE__*/React.createElement(MotionInView, {
    key: logo,
    variants: varFade().in
  }, /*#__PURE__*/React.createElement(Image, {
    alt: logo,
    src: `https://minimal-assets-api.vercel.app/assets/images/logos/${logo}.svg`,
    sx: {
      m: {
        xs: 1.5,
        md: 3
      },
      height: {
        xs: 24,
        md: 32
      }
    }
  }))))), /*#__PURE__*/React.createElement(Grid, {
    container: true,
    justifyContent: "center"
  }, /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: 12,
    sm: 8
  }, /*#__PURE__*/React.createElement(MotionInView, {
    variants: varFade().inUp
  }, /*#__PURE__*/React.createElement(Typography, {
    variant: "h3",
    sx: {
      textAlign: 'center'
    }
  }, "Our vision offering the best product nulla vehicula tortor scelerisque ultrices malesuada.")))));
}