import PropTypes from 'prop-types'; // @mui

import { alpha, styled, useTheme } from '@mui/material/styles';
import { Box, Grid, Link, Paper, Rating, Container, Typography } from '@mui/material'; // hooks

import useResponsive from '../../hooks/useResponsive'; // utils

import cssStyles from '../../utils/cssStyles'; // components

import Iconify from '../../components/Iconify';
import { MotionInView, varFade } from '../../components/animate'; // ----------------------------------------------------------------------

const TESTIMONIALS = [{
  name: 'Jenny Wilson',
  rating: 5,
  dateCreate: 'April 19, 2021',
  content: `Excellent Work! Thanks a lot!`
}, {
  name: 'Cody Fisher',
  rating: 5,
  dateCreate: 'April 19, 2021',
  content: `It's a very good dashboard and we are really liking the product . We've done some things, like migrate to TS and implementing a react useContext api, to fit our job methodology but the product is one of the best in terms of design and application architecture. The team did a really good job.`
}, {
  name: 'Marvin McKinney',
  rating: 5,
  dateCreate: 'April 19, 2021',
  content: `Customer support is realy fast and helpful the desgin of this theme is looks amazing also the code is very clean and readble realy good job !`
}, {
  name: 'Darrell Steward',
  rating: 5,
  dateCreate: 'April 19, 2021',
  content: `Amazing, really good code quality and gives you a lot of examples for implementations.`
}, {
  name: 'Jacob Jones',
  rating: 5,
  dateCreate: 'April 19, 2021',
  content: `Got a few questions after purchasing the product. The owner responded very fast and very helpfull. Overall the code is excellent and works very good. 5/5 stars!`
}, {
  name: 'Bessie Cooper',
  rating: 5,
  dateCreate: 'April 19, 2021',
  content: `CEO of Codealy.io here. We’ve built a developer assessment platform that makes sense - tasks are based on git repositories and run in virtual machines. We automate the pain points - storing candidates code, running it and sharing test results with the whole team, remotely. Bought this template as we need to provide an awesome dashboard for our early customers. I am super happy with purchase. The code is just as good as the design. Thanks!`
}];
const RootStyle = styled('div')(({
  theme
}) => ({
  textAlign: 'center',
  padding: theme.spacing(10, 0),
  backgroundSize: 'cover',
  backgroundImage: `linear-gradient(to right, ${alpha(theme.palette.grey[900], 0.8)} , ${alpha(theme.palette.grey[900], 0.8)}), url(https://minimal-assets-api.vercel.app/assets/images/about/testimonials.jpg)`,
  [theme.breakpoints.up('md')]: {
    textAlign: 'left',
    padding: 0,
    height: 840,
    overflow: 'hidden'
  }
})); // ----------------------------------------------------------------------

export default function AboutTestimonials() {
  const isDesktop = useResponsive('up', 'md');
  return /*#__PURE__*/React.createElement(RootStyle, null, /*#__PURE__*/React.createElement(Container, {
    sx: {
      position: 'relative',
      height: '100%'
    }
  }, /*#__PURE__*/React.createElement(Grid, {
    container: true,
    spacing: 3,
    alignItems: "center",
    justifyContent: {
      xs: 'center',
      md: 'space-between'
    },
    sx: {
      height: '100%'
    }
  }, /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: 10,
    md: 4
  }, /*#__PURE__*/React.createElement(Box, {
    sx: {
      maxWidth: {
        md: 360
      }
    }
  }, /*#__PURE__*/React.createElement(MotionInView, {
    variants: varFade().inUp
  }, /*#__PURE__*/React.createElement(Typography, {
    component: "p",
    variant: "overline",
    sx: {
      mb: 2,
      color: 'text.secondary'
    }
  }, "Testimonials")), /*#__PURE__*/React.createElement(MotionInView, {
    variants: varFade().inUp
  }, /*#__PURE__*/React.createElement(Typography, {
    variant: "h2",
    sx: {
      mb: 3,
      color: 'common.white'
    }
  }, "Who love ", /*#__PURE__*/React.createElement("br", null), "my work")), /*#__PURE__*/React.createElement(MotionInView, {
    variants: varFade().inUp
  }, /*#__PURE__*/React.createElement(Typography, {
    sx: {
      color: 'common.white'
    }
  }, "Our goal is to create a product and service that you\u2019re satisfied with and use it every day. This is why we\u2019re constantly working on our services to make it better every day and really listen to what our users has to say.")), !isDesktop && /*#__PURE__*/React.createElement(Box, {
    sx: {
      mt: 3,
      display: 'flex',
      justifyContent: 'center'
    }
  }, /*#__PURE__*/React.createElement(MotionInView, {
    variants: varFade().inUp
  }, /*#__PURE__*/React.createElement(TestimonialLink, null))))), /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: 12,
    md: 7,
    lg: 6,
    sx: {
      right: {
        md: 24
      },
      position: {
        md: 'absolute'
      }
    }
  }, /*#__PURE__*/React.createElement(Grid, {
    container: true,
    spacing: isDesktop ? 3 : 0,
    alignItems: "center"
  }, /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: 12,
    md: 6
  }, TESTIMONIALS.slice(0, 3).map(testimonial => /*#__PURE__*/React.createElement(MotionInView, {
    key: testimonial.name,
    variants: varFade().inUp
  }, /*#__PURE__*/React.createElement(TestimonialCard, {
    testimonial: testimonial
  })))), /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: 12,
    md: 6
  }, TESTIMONIALS.slice(3, 6).map(testimonial => /*#__PURE__*/React.createElement(MotionInView, {
    key: testimonial.name,
    variants: varFade().inUp
  }, /*#__PURE__*/React.createElement(TestimonialCard, {
    testimonial: testimonial
  }))))))), isDesktop && /*#__PURE__*/React.createElement(Box, {
    sx: {
      bottom: 60,
      position: 'absolute'
    }
  }, /*#__PURE__*/React.createElement(MotionInView, {
    variants: varFade().inLeft
  }, /*#__PURE__*/React.createElement(TestimonialLink, null)))));
} // ----------------------------------------------------------------------

function TestimonialLink() {
  return /*#__PURE__*/React.createElement(Link, {
    href: "#",
    variant: "subtitle2",
    sx: {
      display: 'flex',
      alignItems: 'center'
    }
  }, "Read more testimonials", /*#__PURE__*/React.createElement(Iconify, {
    icon: 'ic:round-arrow-right-alt',
    sx: {
      ml: 1,
      width: 20,
      height: 20
    }
  }));
}

TestimonialCard.propTypes = {
  testimonial: PropTypes.shape({
    content: PropTypes.string,
    dateCreate: PropTypes.string,
    name: PropTypes.string,
    rating: PropTypes.number
  })
};

function TestimonialCard({
  testimonial
}) {
  const theme = useTheme();
  const {
    name,
    rating,
    dateCreate,
    content
  } = testimonial;
  return /*#__PURE__*/React.createElement(Paper, {
    sx: {
      mt: 3,
      p: 3,
      color: 'common.white',
      ...cssStyles().bgBlur({
        color: theme.palette.common.white,
        opacity: 0.04
      })
    }
  }, /*#__PURE__*/React.createElement(Typography, {
    variant: "subtitle1",
    gutterBottom: true
  }, name), /*#__PURE__*/React.createElement(Typography, {
    gutterBottom: true,
    component: "p",
    variant: "caption",
    sx: {
      color: 'grey.500'
    }
  }, dateCreate), /*#__PURE__*/React.createElement(Rating, {
    value: rating,
    readOnly: true,
    size: "small"
  }), /*#__PURE__*/React.createElement(Typography, {
    variant: "body2",
    sx: {
      mt: 1.5
    }
  }, content));
}