import { m } from 'framer-motion'; // @mui

import { styled } from '@mui/material/styles';
import { Box, Container, Typography } from '@mui/material'; // components

import { MotionContainer, TextAnimate, varFade } from '../../components/animate'; // ----------------------------------------------------------------------

const RootStyle = styled('div')(({
  theme
}) => ({
  backgroundSize: 'cover',
  backgroundPosition: 'center',
  backgroundImage: 'url(https://minimal-assets-api.vercel.app/assets/overlay.svg), url(https://minimal-assets-api.vercel.app/assets/images/about/hero.jpg)',
  padding: theme.spacing(10, 0),
  [theme.breakpoints.up('md')]: {
    height: 560,
    padding: 0
  }
}));
const ContentStyle = styled('div')(({
  theme
}) => ({
  textAlign: 'center',
  [theme.breakpoints.up('md')]: {
    textAlign: 'left',
    position: 'absolute',
    bottom: theme.spacing(10)
  }
})); // ----------------------------------------------------------------------

export default function AboutHero() {
  return /*#__PURE__*/React.createElement(RootStyle, null, /*#__PURE__*/React.createElement(Container, {
    component: MotionContainer,
    sx: {
      position: 'relative',
      height: '100%'
    }
  }, /*#__PURE__*/React.createElement(ContentStyle, null, /*#__PURE__*/React.createElement(TextAnimate, {
    text: "Who",
    sx: {
      color: 'primary.main'
    },
    variants: varFade().inRight
  }), /*#__PURE__*/React.createElement("br", null), /*#__PURE__*/React.createElement(Box, {
    sx: {
      display: 'inline-flex',
      color: 'common.white'
    }
  }, /*#__PURE__*/React.createElement(TextAnimate, {
    text: "we",
    sx: {
      mr: 2
    }
  }), /*#__PURE__*/React.createElement(TextAnimate, {
    text: "are?"
  })), /*#__PURE__*/React.createElement(m.div, {
    variants: varFade().inRight
  }, /*#__PURE__*/React.createElement(Typography, {
    variant: "h4",
    sx: {
      mt: 5,
      color: 'common.white',
      fontWeight: 'fontWeightMedium'
    }
  }, "Let's work together and", /*#__PURE__*/React.createElement("br", null), " make awesome site easily")))));
}