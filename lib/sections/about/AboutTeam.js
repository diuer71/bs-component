function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

import PropTypes from 'prop-types';
import { useRef } from 'react';
import Slider from 'react-slick'; // @mui

import { useTheme } from '@mui/material/styles';
import { Box, Stack, Card, Button, Container, Typography } from '@mui/material'; // _mock_

import { _carouselsMembers } from '../../_mock'; // components

import Image from '../../components/Image';
import Iconify from '../../components/Iconify';
import { CarouselArrows } from '../../components/carousel';
import SocialsButton from '../../components/SocialsButton';
import { MotionInView, varFade } from '../../components/animate'; // ----------------------------------------------------------------------

export default function AboutTeam() {
  const carouselRef = useRef(null);
  const theme = useTheme();
  const settings = {
    arrows: false,
    slidesToShow: 4,
    centerMode: true,
    centerPadding: '0px',
    rtl: Boolean(theme.direction === 'rtl'),
    responsive: [{
      breakpoint: 1279,
      settings: {
        slidesToShow: 3
      }
    }, {
      breakpoint: 959,
      settings: {
        slidesToShow: 2
      }
    }, {
      breakpoint: 600,
      settings: {
        slidesToShow: 1
      }
    }]
  };

  const handlePrevious = () => {
    carouselRef.current.slickPrev();
  };

  const handleNext = () => {
    carouselRef.current.slickNext();
  };

  return /*#__PURE__*/React.createElement(Container, {
    sx: {
      pb: 10,
      textAlign: 'center'
    }
  }, /*#__PURE__*/React.createElement(MotionInView, {
    variants: varFade().inDown
  }, /*#__PURE__*/React.createElement(Typography, {
    component: "p",
    variant: "overline",
    sx: {
      mb: 2,
      color: 'text.secondary'
    }
  }, "Dream team")), /*#__PURE__*/React.createElement(MotionInView, {
    variants: varFade().inUp
  }, /*#__PURE__*/React.createElement(Typography, {
    variant: "h2",
    sx: {
      mb: 3
    }
  }, "Great team is the key")), /*#__PURE__*/React.createElement(MotionInView, {
    variants: varFade().inUp
  }, /*#__PURE__*/React.createElement(Typography, {
    sx: {
      mx: 'auto',
      maxWidth: 630,
      color: theme => theme.palette.mode === 'light' ? 'text.secondary' : 'common.white'
    }
  }, "Minimal will provide you support if you have any problems, our support team will reply within a day and we also have detailed documentation.")), /*#__PURE__*/React.createElement(Box, {
    sx: {
      position: 'relative'
    }
  }, /*#__PURE__*/React.createElement(CarouselArrows, {
    filled: true,
    onNext: handleNext,
    onPrevious: handlePrevious
  }, /*#__PURE__*/React.createElement(Slider, _extends({
    ref: carouselRef
  }, settings), _carouselsMembers.map(member => /*#__PURE__*/React.createElement(MotionInView, {
    key: member.id,
    variants: varFade().in,
    sx: {
      px: 1.5,
      py: 10
    }
  }, /*#__PURE__*/React.createElement(MemberCard, {
    member: member
  })))))), /*#__PURE__*/React.createElement(Button, {
    variant: "outlined",
    color: "inherit",
    size: "large",
    endIcon: /*#__PURE__*/React.createElement(Iconify, {
      icon: 'ic:round-arrow-right-alt',
      width: 24,
      height: 24
    }),
    sx: {
      mx: 'auto'
    }
  }, "View all team members"));
} // ----------------------------------------------------------------------

MemberCard.propTypes = {
  member: PropTypes.shape({
    avatar: PropTypes.string,
    name: PropTypes.string,
    role: PropTypes.string
  })
};

function MemberCard({
  member
}) {
  const {
    name,
    role,
    avatar
  } = member;
  return /*#__PURE__*/React.createElement(Card, {
    key: name,
    sx: {
      p: 1
    }
  }, /*#__PURE__*/React.createElement(Typography, {
    variant: "subtitle1",
    sx: {
      mt: 2,
      mb: 0.5
    }
  }, name), /*#__PURE__*/React.createElement(Typography, {
    variant: "body2",
    sx: {
      mb: 2,
      color: 'text.secondary'
    }
  }, role), /*#__PURE__*/React.createElement(Image, {
    src: avatar,
    ratio: "1/1",
    sx: {
      borderRadius: 1.5
    }
  }), /*#__PURE__*/React.createElement(Stack, {
    alignItems: "center",
    sx: {
      mt: 2,
      mb: 1
    }
  }, /*#__PURE__*/React.createElement(SocialsButton, {
    sx: {
      color: 'action.active'
    }
  })));
}