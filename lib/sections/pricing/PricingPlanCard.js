import PropTypes from 'prop-types'; // @mui

import { styled } from '@mui/material/styles';
import { Card, Button, Typography, Box, Stack } from '@mui/material'; // components

import Label from '../../components/Label';
import Iconify from '../../components/Iconify'; // ----------------------------------------------------------------------

const RootStyle = styled(Card)(({
  theme
}) => ({
  maxWidth: 480,
  margin: 'auto',
  display: 'flex',
  position: 'relative',
  alignItems: 'center',
  flexDirection: 'column',
  padding: theme.spacing(3),
  [theme.breakpoints.up(414)]: {
    padding: theme.spacing(5)
  }
})); // ----------------------------------------------------------------------

PricingPlanCard.propTypes = {
  index: PropTypes.number,
  card: PropTypes.object
};
export default function PricingPlanCard({
  card,
  index
}) {
  const {
    subscription,
    icon,
    price,
    caption,
    lists,
    labelAction
  } = card;
  return /*#__PURE__*/React.createElement(RootStyle, null, index === 1 && /*#__PURE__*/React.createElement(Label, {
    color: "info",
    sx: {
      top: 16,
      right: 16,
      position: 'absolute'
    }
  }, "POPULAR"), /*#__PURE__*/React.createElement(Typography, {
    variant: "overline",
    sx: {
      color: 'text.secondary'
    }
  }, subscription), /*#__PURE__*/React.createElement(Box, {
    sx: {
      display: 'flex',
      justifyContent: 'flex-end',
      my: 2
    }
  }, index === 1 || index === 2 ? /*#__PURE__*/React.createElement(Typography, {
    variant: "subtitle1",
    sx: {
      color: 'text.secondary'
    }
  }, "$") : '', /*#__PURE__*/React.createElement(Typography, {
    variant: "h2",
    sx: {
      mx: 1
    }
  }, price === 0 ? 'Free' : price), index === 1 || index === 2 ? /*#__PURE__*/React.createElement(Typography, {
    gutterBottom: true,
    component: "span",
    variant: "subtitle2",
    sx: {
      alignSelf: 'flex-end',
      color: 'text.secondary'
    }
  }, "/mo") : ''), /*#__PURE__*/React.createElement(Typography, {
    variant: "caption",
    sx: {
      color: 'primary.main',
      textTransform: 'capitalize'
    }
  }, caption), /*#__PURE__*/React.createElement(Box, {
    sx: {
      width: 80,
      height: 80,
      mt: 3
    }
  }, icon), /*#__PURE__*/React.createElement(Stack, {
    component: "ul",
    spacing: 2,
    sx: {
      my: 5,
      width: 1
    }
  }, lists.map(item => /*#__PURE__*/React.createElement(Stack, {
    key: item.text,
    component: "li",
    direction: "row",
    alignItems: "center",
    spacing: 1.5,
    sx: {
      typography: 'body2',
      color: item.isAvailable ? 'text.primary' : 'text.disabled'
    }
  }, /*#__PURE__*/React.createElement(Iconify, {
    icon: 'eva:checkmark-fill',
    sx: {
      width: 20,
      height: 20
    }
  }), /*#__PURE__*/React.createElement(Typography, {
    variant: "body2"
  }, item.text)))), /*#__PURE__*/React.createElement(Button, {
    fullWidth: true,
    size: "large",
    variant: "contained",
    disabled: index === 0
  }, labelAction));
}