// @mui
import { Button, Typography, TextField, Stack } from '@mui/material'; // components

import { MotionInView, varFade } from '../../components/animate'; // ----------------------------------------------------------------------

export default function ContactForm() {
  return /*#__PURE__*/React.createElement(Stack, {
    spacing: 5
  }, /*#__PURE__*/React.createElement(MotionInView, {
    variants: varFade().inUp
  }, /*#__PURE__*/React.createElement(Typography, {
    variant: "h3"
  }, "Feel free to contact us. ", /*#__PURE__*/React.createElement("br", null), "We'll be glad to hear from you, buddy.")), /*#__PURE__*/React.createElement(Stack, {
    spacing: 3
  }, /*#__PURE__*/React.createElement(MotionInView, {
    variants: varFade().inUp
  }, /*#__PURE__*/React.createElement(TextField, {
    fullWidth: true,
    label: "Name"
  })), /*#__PURE__*/React.createElement(MotionInView, {
    variants: varFade().inUp
  }, /*#__PURE__*/React.createElement(TextField, {
    fullWidth: true,
    label: "Email"
  })), /*#__PURE__*/React.createElement(MotionInView, {
    variants: varFade().inUp
  }, /*#__PURE__*/React.createElement(TextField, {
    fullWidth: true,
    label: "Subject"
  })), /*#__PURE__*/React.createElement(MotionInView, {
    variants: varFade().inUp
  }, /*#__PURE__*/React.createElement(TextField, {
    fullWidth: true,
    label: "Enter your message here.",
    multiline: true,
    rows: 4
  }))), /*#__PURE__*/React.createElement(MotionInView, {
    variants: varFade().inUp
  }, /*#__PURE__*/React.createElement(Button, {
    size: "large",
    variant: "contained"
  }, "Submit Now")));
}