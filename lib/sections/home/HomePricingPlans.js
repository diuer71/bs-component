import PropTypes from 'prop-types'; // @mui

import { useTheme, styled } from '@mui/material/styles';
import { Box, Grid, Card, Link, Stack, Button, Divider, Container, Typography } from '@mui/material'; // _mock_

import { _homePlans } from '../../_mock'; // components

import Image from '../../components/Image';
import Iconify from '../../components/Iconify';
import { MotionInView, varFade } from '../../components/animate'; // ----------------------------------------------------------------------

const RootStyle = styled('div')(({
  theme
}) => ({
  padding: theme.spacing(10, 0),
  backgroundColor: theme.palette.background.neutral,
  [theme.breakpoints.up('md')]: {
    padding: theme.spacing(15, 0)
  }
})); // ----------------------------------------------------------------------

export default function HomePricingPlans() {
  const theme = useTheme();
  const isLight = theme.palette.mode === 'light';
  return /*#__PURE__*/React.createElement(RootStyle, null, /*#__PURE__*/React.createElement(Container, null, /*#__PURE__*/React.createElement(Box, {
    sx: {
      mb: 10,
      textAlign: 'center'
    }
  }, /*#__PURE__*/React.createElement(MotionInView, {
    variants: varFade().inUp
  }, /*#__PURE__*/React.createElement(Typography, {
    component: "div",
    variant: "overline",
    sx: {
      mb: 2,
      color: 'text.disabled'
    }
  }, "pricing plans")), /*#__PURE__*/React.createElement(MotionInView, {
    variants: varFade().inDown
  }, /*#__PURE__*/React.createElement(Typography, {
    variant: "h2",
    sx: {
      mb: 3
    }
  }, "The right plan for your business")), /*#__PURE__*/React.createElement(MotionInView, {
    variants: varFade().inDown
  }, /*#__PURE__*/React.createElement(Typography, {
    sx: {
      color: isLight ? 'text.secondary' : 'text.primary'
    }
  }, "Choose the perfect plan for your needs. Always flexible to grow"))), /*#__PURE__*/React.createElement(Grid, {
    container: true,
    spacing: 5
  }, _homePlans.map(plan => /*#__PURE__*/React.createElement(Grid, {
    key: plan.license,
    item: true,
    xs: 12,
    md: 4
  }, /*#__PURE__*/React.createElement(MotionInView, {
    variants: plan.license === 'Standard Plus' ? varFade().inDown : varFade().inUp
  }, /*#__PURE__*/React.createElement(PlanCard, {
    plan: plan
  }))))), /*#__PURE__*/React.createElement(MotionInView, {
    variants: varFade().in
  }, /*#__PURE__*/React.createElement(Box, {
    sx: {
      p: 5,
      mt: 10,
      textAlign: 'center'
    }
  }, /*#__PURE__*/React.createElement(MotionInView, {
    variants: varFade().inDown
  }, /*#__PURE__*/React.createElement(Typography, {
    variant: "h3"
  }, "Still have questions?")), /*#__PURE__*/React.createElement(MotionInView, {
    variants: varFade().inDown
  }, /*#__PURE__*/React.createElement(Typography, {
    sx: {
      mt: 3,
      mb: 5,
      color: 'text.secondary'
    }
  }, "Please describe your case to receive the most accurate advice.")), /*#__PURE__*/React.createElement(MotionInView, {
    variants: varFade().inUp
  }, /*#__PURE__*/React.createElement(Button, {
    size: "large",
    variant: "contained",
    href: "mailto:support@minimals.cc?subject=[Feedback] from Customer"
  }, "Contact us"))))));
} // ----------------------------------------------------------------------

PlanCard.propTypes = {
  plan: PropTypes.shape({
    license: PropTypes.string,
    commons: PropTypes.arrayOf(PropTypes.string),
    icons: PropTypes.arrayOf(PropTypes.string),
    options: PropTypes.arrayOf(PropTypes.string)
  })
};

function PlanCard({
  plan
}) {
  const {
    license,
    commons,
    options,
    icons
  } = plan;
  const standard = license === 'Standard';
  const plus = license === 'Standard Plus';
  return /*#__PURE__*/React.createElement(Card, {
    sx: {
      p: 5,
      boxShadow: 0,
      ...(plus && {
        boxShadow: theme => theme.customShadows.z24
      })
    }
  }, /*#__PURE__*/React.createElement(Stack, {
    spacing: 5
  }, /*#__PURE__*/React.createElement("div", null, /*#__PURE__*/React.createElement(Typography, {
    variant: "overline",
    component: "div",
    sx: {
      mb: 2,
      color: 'text.disabled'
    }
  }, "LICENSE"), /*#__PURE__*/React.createElement(Typography, {
    variant: "h4"
  }, license)), standard ? /*#__PURE__*/React.createElement(Image, {
    src: icons[2],
    sx: {
      width: 40,
      height: 40
    }
  }) : /*#__PURE__*/React.createElement(Stack, {
    direction: "row",
    spacing: 1
  }, icons.map(icon => /*#__PURE__*/React.createElement(Image, {
    key: icon,
    src: icon,
    sx: {
      width: 40,
      height: 40
    }
  }))), /*#__PURE__*/React.createElement(Stack, {
    spacing: 2.5
  }, commons.map(option => /*#__PURE__*/React.createElement(Stack, {
    key: option,
    spacing: 1.5,
    direction: "row",
    alignItems: "center"
  }, /*#__PURE__*/React.createElement(Iconify, {
    icon: 'eva:checkmark-fill',
    sx: {
      color: 'primary.main',
      width: 20,
      height: 20
    }
  }), /*#__PURE__*/React.createElement(Typography, {
    variant: "body2"
  }, option))), /*#__PURE__*/React.createElement(Divider, {
    sx: {
      borderStyle: 'dashed'
    }
  }), options.map((option, optionIndex) => {
    const disabledLine = standard && optionIndex === 1 || standard && optionIndex === 2 || standard && optionIndex === 3 || plus && optionIndex === 3;
    return /*#__PURE__*/React.createElement(Stack, {
      spacing: 1.5,
      direction: "row",
      alignItems: "center",
      sx: { ...(disabledLine && {
          color: 'text.disabled'
        })
      },
      key: option
    }, /*#__PURE__*/React.createElement(Iconify, {
      icon: 'eva:checkmark-fill',
      sx: {
        width: 20,
        height: 20,
        color: 'primary.main',
        ...(disabledLine && {
          color: 'text.disabled'
        })
      }
    }), /*#__PURE__*/React.createElement(Typography, {
      variant: "body2"
    }, option));
  })), /*#__PURE__*/React.createElement(Stack, {
    direction: "row",
    justifyContent: "flex-end"
  }, /*#__PURE__*/React.createElement(Link, {
    color: "text.secondary",
    underline: "always",
    target: "_blank",
    rel: "noopener",
    href: "https://material-ui.com/store/license/#i-standard-license",
    sx: {
      typography: 'body2',
      display: 'flex',
      alignItems: 'center'
    }
  }, "Learn more ", /*#__PURE__*/React.createElement(Iconify, {
    icon: 'eva:chevron-right-fill',
    width: 20,
    height: 20
  }))), /*#__PURE__*/React.createElement(Button, {
    size: "large",
    fullWidth: true,
    variant: plus ? 'contained' : 'outlined',
    target: "_blank",
    rel: "noopener",
    href: "https://material-ui.com/store/items/minimal-dashboard/"
  }, "Choose Plan")));
}