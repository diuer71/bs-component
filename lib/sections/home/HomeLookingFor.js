// @mui
import { styled } from '@mui/material/styles';
import { Button, Container, Typography, Grid } from '@mui/material'; // components

import Image from '../../components/Image';
import Iconify from '../../components/Iconify';
import { MotionInView, varFade } from '../../components/animate'; // ----------------------------------------------------------------------

const RootStyle = styled('div')(({
  theme
}) => ({
  padding: theme.spacing(10, 0),
  [theme.breakpoints.up('md')]: {
    padding: theme.spacing(15, 0)
  }
})); // ----------------------------------------------------------------------

export default function HomeLookingFor() {
  return /*#__PURE__*/React.createElement(RootStyle, null, /*#__PURE__*/React.createElement(Container, null, /*#__PURE__*/React.createElement(Grid, {
    container: true,
    alignItems: "center",
    justifyContent: "space-between",
    spacing: {
      xs: 8,
      md: 3
    }
  }, /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: 12,
    md: 4,
    sx: {
      textAlign: {
        xs: 'center',
        md: 'left'
      }
    }
  }, /*#__PURE__*/React.createElement(MotionInView, {
    variants: varFade().inDown
  }, /*#__PURE__*/React.createElement(Typography, {
    variant: "overline",
    component: "div",
    sx: {
      color: 'text.disabled'
    }
  }, "Looking For a")), /*#__PURE__*/React.createElement(MotionInView, {
    variants: varFade().inDown
  }, /*#__PURE__*/React.createElement(Typography, {
    variant: "h2",
    sx: {
      mt: 2,
      mb: 5
    }
  }, "Landing Page Template?")), /*#__PURE__*/React.createElement(MotionInView, {
    variants: varFade().inDown
  }, /*#__PURE__*/React.createElement(Button, {
    color: "inherit",
    size: "large",
    variant: "outlined",
    target: "_blank",
    rel: "noopener",
    href: "https://material-ui.com/store/items/zone-landing-page/",
    endIcon: /*#__PURE__*/React.createElement(Iconify, {
      icon: 'ic:round-arrow-right-alt'
    })
  }, "Visit Zone Landing"))), /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: 12,
    md: 7
  }, /*#__PURE__*/React.createElement(MotionInView, {
    variants: varFade().inUp,
    sx: {
      mb: {
        xs: 3,
        md: 0
      }
    }
  }, /*#__PURE__*/React.createElement(Image, {
    disabledEffect: true,
    alt: "rocket",
    src: "https://minimal-assets-api.vercel.app/assets/images/home/zone_screen.png"
  }))))));
}