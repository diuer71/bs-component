function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

import { m } from 'framer-motion';
import { Link as RouterLink } from 'react-router-dom'; // @mui

import { styled } from '@mui/material/styles';
import { Button, Box, Link, Container, Typography, Stack } from '@mui/material'; // routes

import { PATH_DASHBOARD } from '../../routes/paths'; // components

import Image from '../../components/Image';
import Iconify from '../../components/Iconify';
import TextIconLabel from '../../components/TextIconLabel';
import { MotionContainer, varFade } from '../../components/animate'; // ----------------------------------------------------------------------

const RootStyle = styled(m.div)(({
  theme
}) => ({
  position: 'relative',
  backgroundColor: theme.palette.grey[400],
  [theme.breakpoints.up('md')]: {
    top: 0,
    left: 0,
    width: '100%',
    height: '100vh',
    display: 'flex',
    position: 'fixed',
    alignItems: 'center'
  }
}));
const ContentStyle = styled(props => /*#__PURE__*/React.createElement(Stack, _extends({
  spacing: 5
}, props)))(({
  theme
}) => ({
  zIndex: 10,
  maxWidth: 520,
  margin: 'auto',
  textAlign: 'center',
  position: 'relative',
  paddingTop: theme.spacing(15),
  paddingBottom: theme.spacing(15),
  [theme.breakpoints.up('md')]: {
    margin: 'unset',
    textAlign: 'left'
  }
}));
const HeroOverlayStyle = styled(m.img)({
  zIndex: 9,
  width: '100%',
  height: '100%',
  objectFit: 'cover',
  position: 'absolute'
});
const HeroImgStyle = styled(m.img)(({
  theme
}) => ({
  top: 0,
  right: 0,
  bottom: 0,
  zIndex: 8,
  width: '100%',
  margin: 'auto',
  position: 'absolute',
  [theme.breakpoints.up('lg')]: {
    right: '8%',
    width: 'auto',
    height: '48vh'
  }
})); // ----------------------------------------------------------------------

export default function HomeHero() {
  return /*#__PURE__*/React.createElement(MotionContainer, null, /*#__PURE__*/React.createElement(RootStyle, null, /*#__PURE__*/React.createElement(HeroOverlayStyle, {
    alt: "overlay",
    src: "https://minimal-assets-api.vercel.app/assets/overlay.svg",
    variants: varFade().in
  }), /*#__PURE__*/React.createElement(HeroImgStyle, {
    alt: "hero",
    src: "https://minimal-assets-api.vercel.app/assets/images/home/hero.png",
    variants: varFade().inUp
  }), /*#__PURE__*/React.createElement(Container, null, /*#__PURE__*/React.createElement(ContentStyle, null, /*#__PURE__*/React.createElement(m.div, {
    variants: varFade().inRight
  }, /*#__PURE__*/React.createElement(Typography, {
    variant: "h1",
    sx: {
      color: 'common.white'
    }
  }, "Start a ", /*#__PURE__*/React.createElement("br", null), "new project ", /*#__PURE__*/React.createElement("br", null), " with", /*#__PURE__*/React.createElement(Typography, {
    component: "span",
    variant: "h1",
    sx: {
      color: 'primary.main'
    }
  }, "\xA0Minimal"))), /*#__PURE__*/React.createElement(m.div, {
    variants: varFade().inRight
  }, /*#__PURE__*/React.createElement(Typography, {
    sx: {
      color: 'common.white'
    }
  }, "The starting point for your next project based on easy-to-customize MUI helps you build apps faster and better.")), /*#__PURE__*/React.createElement(Stack, {
    spacing: 2.5,
    alignItems: "center",
    direction: {
      xs: 'column',
      md: 'row'
    }
  }, /*#__PURE__*/React.createElement(m.div, {
    variants: varFade().inRight
  }, /*#__PURE__*/React.createElement(TextIconLabel, {
    icon: /*#__PURE__*/React.createElement(Image, {
      alt: "sketch icon",
      src: "https://minimal-assets-api.vercel.app/assets/images/home/ic_sketch_small.svg",
      sx: {
        width: 20,
        height: 20,
        mr: 1
      }
    }),
    value: /*#__PURE__*/React.createElement(Link, {
      href: "https://www.sketch.com/s/0fa4699d-a3ff-4cd5-a3a7-d851eb7e17f0",
      target: "_blank",
      rel: "noopener",
      color: "common.white",
      sx: {
        typography: 'body2'
      }
    }, "Preview Sketch")
  })), /*#__PURE__*/React.createElement(m.div, {
    variants: varFade().inRight
  }, /*#__PURE__*/React.createElement(TextIconLabel, {
    icon: /*#__PURE__*/React.createElement(Image, {
      alt: "sketch icon",
      src: "https://minimal-assets-api.vercel.app/assets/images/home/ic_figma_small.svg",
      sx: {
        width: 20,
        height: 20,
        mr: 1
      }
    }),
    value: /*#__PURE__*/React.createElement(Link, {
      href: "https://www.figma.com/file/x7earqGD0VGFjFdk5v2DgZ/%5BPreview%5D-Minimal-Web?node-id=866%3A55474",
      target: "_blank",
      rel: "noopener",
      color: "common.white",
      sx: {
        typography: 'body2'
      }
    }, "Preview Figma")
  }))), /*#__PURE__*/React.createElement(m.div, {
    variants: varFade().inRight
  }, /*#__PURE__*/React.createElement(Button, {
    size: "large",
    variant: "contained",
    component: RouterLink,
    to: PATH_DASHBOARD.root,
    startIcon: /*#__PURE__*/React.createElement(Iconify, {
      icon: 'eva:flash-fill',
      width: 20,
      height: 20
    })
  }, "Live Preview")), /*#__PURE__*/React.createElement(Stack, {
    spacing: 2.5
  }, /*#__PURE__*/React.createElement(m.div, {
    variants: varFade().inRight
  }, /*#__PURE__*/React.createElement(Typography, {
    variant: "overline",
    sx: {
      color: 'primary.light'
    }
  }, "Available For")), /*#__PURE__*/React.createElement(Stack, {
    direction: "row",
    spacing: 1.5,
    justifyContent: {
      xs: 'center',
      md: 'flex-start'
    }
  }, ['ic_sketch', 'ic_figma', 'ic_js', 'ic_ts', 'ic_nextjs'].map(resource => /*#__PURE__*/React.createElement(m.img, {
    key: resource,
    variants: varFade().inRight,
    src: `https://minimal-assets-api.vercel.app/assets/images/home/${resource}.svg`
  }))))))), /*#__PURE__*/React.createElement(Box, {
    sx: {
      height: {
        md: '100vh'
      }
    }
  }));
}