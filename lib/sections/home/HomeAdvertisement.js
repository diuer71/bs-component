import { m } from 'framer-motion'; // @mui

import { styled } from '@mui/material/styles';
import { Button, Box, Container, Typography } from '@mui/material'; // components

import Image from '../../components/Image';
import { MotionInView, varFade } from '../../components/animate'; // ----------------------------------------------------------------------

const ContentStyle = styled('div')(({
  theme
}) => ({
  maxWidth: 456,
  margin: 'auto',
  overflow: 'hidden',
  paddingBottom: theme.spacing(10),
  borderRadius: Number(theme.shape.borderRadius) * 2,
  backgroundImage: `linear-gradient(135deg,
    ${theme.palette.primary.main} 0%,
    ${theme.palette.primary.dark} 100%)`,
  [theme.breakpoints.up('md')]: {
    display: 'flex',
    maxWidth: '100%',
    paddingBottom: 0,
    alignItems: 'center'
  }
})); // ----------------------------------------------------------------------

export default function HomeAdvertisement() {
  return /*#__PURE__*/React.createElement(Container, null, /*#__PURE__*/React.createElement(ContentStyle, null, /*#__PURE__*/React.createElement(MotionInView, {
    variants: varFade().inUp,
    sx: {
      mb: {
        xs: 3,
        md: 0
      }
    }
  }, /*#__PURE__*/React.createElement(m.div, {
    animate: {
      y: [-20, 0, -20]
    },
    transition: {
      duration: 4,
      repeat: Infinity
    }
  }, /*#__PURE__*/React.createElement(Image, {
    visibleByDefault: true,
    alt: "rocket",
    src: "https://minimal-assets-api.vercel.app/assets/images/home/rocket.png",
    disabledEffect: true,
    sx: {
      maxWidth: 460
    }
  }))), /*#__PURE__*/React.createElement(Box, {
    sx: {
      pl: {
        md: 10
      },
      textAlign: {
        xs: 'center',
        md: 'left'
      }
    }
  }, /*#__PURE__*/React.createElement(MotionInView, {
    variants: varFade().inDown,
    sx: {
      color: 'common.white',
      mb: 5
    }
  }, /*#__PURE__*/React.createElement(Typography, {
    variant: "h2"
  }, "Get started with", /*#__PURE__*/React.createElement("br", null), " minimal kit today")), /*#__PURE__*/React.createElement(MotionInView, {
    variants: varFade().inDown
  }, /*#__PURE__*/React.createElement(Button, {
    size: "large",
    variant: "contained",
    target: "_blank",
    rel: "noopener",
    href: "https://material-ui.com/store/items/minimal-dashboard/",
    sx: {
      whiteSpace: 'nowrap',
      boxShadow: theme => theme.customShadows.z8,
      color: theme => theme.palette.getContrastText(theme.palette.common.white),
      bgcolor: 'common.white',
      '&:hover': {
        bgcolor: 'grey.300'
      }
    }
  }, "Purchase Now")))));
}