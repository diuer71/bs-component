// @mui
import { styled } from '@mui/material/styles';
import { Grid, Container, Typography } from '@mui/material'; // components

import Image from '../../components/Image';
import { MotionInView, varFade } from '../../components/animate'; // ----------------------------------------------------------------------

const RootStyle = styled('div')(({
  theme
}) => ({
  padding: theme.spacing(28, 0),
  backgroundColor: theme.palette.grey[900]
}));
const ContentStyle = styled('div')(({
  theme
}) => ({
  textAlign: 'center',
  position: 'relative',
  marginBottom: theme.spacing(10),
  [theme.breakpoints.up('md')]: {
    height: '100%',
    marginBottom: 0,
    textAlign: 'left',
    display: 'inline-flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'flex-start'
  }
})); // ----------------------------------------------------------------------

export default function HomeDarkMode() {
  return /*#__PURE__*/React.createElement(RootStyle, null, /*#__PURE__*/React.createElement(Container, {
    sx: {
      position: 'relative'
    }
  }, /*#__PURE__*/React.createElement(Image, {
    visibleByDefault: true,
    disabledEffect: true,
    alt: "image shape",
    src: "https://minimal-assets-api.vercel.app/assets/images/home/shape.svg",
    sx: {
      top: 0,
      right: 0,
      bottom: 0,
      width: 720,
      height: 720,
      opacity: 0.48,
      my: 'auto',
      position: 'absolute',
      display: {
        xs: 'none',
        md: 'block'
      }
    }
  }), /*#__PURE__*/React.createElement(Grid, {
    container: true,
    spacing: 5,
    direction: "row-reverse",
    justifyContent: "space-between"
  }, /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: 12,
    md: 4
  }, /*#__PURE__*/React.createElement(ContentStyle, null, /*#__PURE__*/React.createElement(MotionInView, {
    variants: varFade().inUp
  }, /*#__PURE__*/React.createElement(Typography, {
    component: "div",
    variant: "overline",
    sx: {
      mb: 2,
      color: 'text.disabled'
    }
  }, "Easy switch between styles.")), /*#__PURE__*/React.createElement(MotionInView, {
    variants: varFade().inUp
  }, /*#__PURE__*/React.createElement(Typography, {
    variant: "h2",
    sx: {
      mb: 3,
      color: 'common.white'
    }
  }, "Dark mode")), /*#__PURE__*/React.createElement(MotionInView, {
    variants: varFade().inUp
  }, /*#__PURE__*/React.createElement(Typography, {
    sx: {
      color: 'common.white',
      mb: 5
    }
  }, "A dark theme that feels easier on the eyes.")))), /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: 12,
    md: 7,
    sx: {
      position: 'relative'
    }
  }, /*#__PURE__*/React.createElement(MotionInView, {
    threshold: 0.5,
    variants: varFade().inUp
  }, /*#__PURE__*/React.createElement(Image, {
    disabledEffect: true,
    alt: "light mode",
    src: "https://minimal-assets-api.vercel.app/assets/images/home/lightmode.png"
  })), /*#__PURE__*/React.createElement(MotionInView, {
    threshold: 0.5,
    variants: varFade().inDown,
    sx: {
      top: 0,
      left: 0,
      position: 'absolute'
    }
  }, /*#__PURE__*/React.createElement(Image, {
    disabledEffect: true,
    alt: "dark mode",
    src: "https://minimal-assets-api.vercel.app/assets/images/home/darkmode.png"
  }))))));
}