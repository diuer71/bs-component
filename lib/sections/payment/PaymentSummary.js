// @mui
import { styled } from '@mui/material/styles';
import { Switch, Divider, Typography, Stack } from '@mui/material';
import { LoadingButton } from '@mui/lab'; // components

import Label from '../../components/Label';
import Iconify from '../../components/Iconify'; // ----------------------------------------------------------------------

const RootStyle = styled('div')(({
  theme
}) => ({
  padding: theme.spacing(5),
  backgroundColor: theme.palette.background.neutral,
  borderRadius: Number(theme.shape.borderRadius) * 2
})); // ----------------------------------------------------------------------

export default function PaymentSummary() {
  return /*#__PURE__*/React.createElement(RootStyle, null, /*#__PURE__*/React.createElement(Typography, {
    variant: "subtitle1",
    sx: {
      mb: 5
    }
  }, "Summary"), /*#__PURE__*/React.createElement(Stack, {
    spacing: 2.5
  }, /*#__PURE__*/React.createElement(Stack, {
    direction: "row",
    justifyContent: "space-between"
  }, /*#__PURE__*/React.createElement(Typography, {
    variant: "subtitle2",
    component: "p",
    sx: {
      color: 'text.secondary'
    }
  }, "Subscription"), /*#__PURE__*/React.createElement(Label, {
    color: "error",
    variant: "filled"
  }, "PREMIUM")), /*#__PURE__*/React.createElement(Stack, {
    direction: "row",
    justifyContent: "space-between"
  }, /*#__PURE__*/React.createElement(Typography, {
    component: "p",
    variant: "subtitle2",
    sx: {
      color: 'text.secondary'
    }
  }, "Billed Monthly"), /*#__PURE__*/React.createElement(Switch, {
    defaultChecked: true
  })), /*#__PURE__*/React.createElement(Stack, {
    direction: "row",
    justifyContent: "flex-end"
  }, /*#__PURE__*/React.createElement(Typography, {
    sx: {
      color: 'text.secondary'
    }
  }, "$"), /*#__PURE__*/React.createElement(Typography, {
    variant: "h2",
    sx: {
      mx: 1
    }
  }, "9.99"), /*#__PURE__*/React.createElement(Typography, {
    component: "span",
    variant: "body2",
    sx: {
      mb: 1,
      alignSelf: 'flex-end',
      color: 'text.secondary'
    }
  }, "/mo")), /*#__PURE__*/React.createElement(Divider, {
    sx: {
      borderStyle: 'dashed'
    }
  }), /*#__PURE__*/React.createElement(Stack, {
    direction: "row",
    alignItems: "center",
    justifyContent: "space-between"
  }, /*#__PURE__*/React.createElement(Typography, {
    variant: "h6",
    component: "p"
  }, "Total Billed"), /*#__PURE__*/React.createElement(Typography, {
    variant: "h6",
    component: "p"
  }, "$9.99*")), /*#__PURE__*/React.createElement(Divider, {
    sx: {
      borderStyle: 'dashed',
      mb: 1
    }
  })), /*#__PURE__*/React.createElement(Typography, {
    variant: "caption",
    sx: {
      color: 'text.secondary',
      mt: 1
    }
  }, "* Plus applicable taxes"), /*#__PURE__*/React.createElement(LoadingButton, {
    fullWidth: true,
    size: "large",
    type: "submit",
    variant: "contained",
    sx: {
      mt: 5,
      mb: 3
    }
  }, "Upgrade My Plan"), /*#__PURE__*/React.createElement(Stack, {
    alignItems: "center",
    spacing: 1
  }, /*#__PURE__*/React.createElement(Stack, {
    direction: "row",
    alignItems: "center",
    spacing: 1.5
  }, /*#__PURE__*/React.createElement(Iconify, {
    icon: 'eva:shield-fill',
    sx: {
      width: 20,
      height: 20,
      color: 'primary.main'
    }
  }), /*#__PURE__*/React.createElement(Typography, {
    variant: "subtitle2"
  }, "Secure credit card payment")), /*#__PURE__*/React.createElement(Typography, {
    variant: "caption",
    sx: {
      color: 'text.secondary',
      textAlign: 'center'
    }
  }, "This is a secure 128-bit SSL encrypted payment")));
}