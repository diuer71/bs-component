import PropTypes from 'prop-types';
import { useState } from 'react'; // @mui

import { Paper, Stack, Button, Popover, TextField, Typography, IconButton, InputAdornment } from '@mui/material'; // components

import Iconify from '../../components/Iconify'; // ----------------------------------------------------------------------

PaymentNewCardForm.propTypes = {
  onCancel: PropTypes.func
};
export default function PaymentNewCardForm({
  onCancel
}) {
  const [isOpen, setIsOpen] = useState(null);
  return /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement(Paper, {
    sx: {
      p: 2.5,
      mb: 2.5,
      bgcolor: 'background.neutral'
    }
  }, /*#__PURE__*/React.createElement(Stack, {
    spacing: 2
  }, /*#__PURE__*/React.createElement(Typography, {
    variant: "subtitle1"
  }, "Add new card"), /*#__PURE__*/React.createElement(TextField, {
    fullWidth: true,
    size: "small",
    label: "Name on card"
  }), /*#__PURE__*/React.createElement(TextField, {
    fullWidth: true,
    size: "small",
    label: "Card number"
  }), /*#__PURE__*/React.createElement(Stack, {
    direction: {
      xs: 'column',
      sm: 'row'
    },
    spacing: 2
  }, /*#__PURE__*/React.createElement(TextField, {
    size: "small",
    label: "MM/YY"
  }), /*#__PURE__*/React.createElement(TextField, {
    size: "small",
    label: "CVV",
    InputProps: {
      endAdornment: /*#__PURE__*/React.createElement(InputAdornment, {
        position: "end"
      }, /*#__PURE__*/React.createElement(IconButton, {
        size: "small",
        edge: "end",
        onClick: event => setIsOpen(event.currentTarget)
      }, /*#__PURE__*/React.createElement(Iconify, {
        icon: 'eva:info-fill'
      })))
    }
  })), /*#__PURE__*/React.createElement(Stack, {
    direction: "row",
    spacing: 2
  }, /*#__PURE__*/React.createElement(Button, {
    fullWidth: true,
    onClick: onCancel
  }, "Cancel"), /*#__PURE__*/React.createElement(Button, {
    fullWidth: true,
    variant: "contained",
    onClick: onCancel
  }, "Create")))), /*#__PURE__*/React.createElement(Popover, {
    open: Boolean(isOpen),
    anchorEl: isOpen,
    onClose: () => setIsOpen(null),
    anchorOrigin: {
      vertical: 'center',
      horizontal: 'center'
    },
    transformOrigin: {
      vertical: 'center',
      horizontal: 'center'
    },
    PaperProps: {
      sx: {
        p: 1,
        maxWidth: 200
      }
    }
  }, /*#__PURE__*/React.createElement(Typography, {
    variant: "body2",
    align: "center"
  }, "Three-digit number on the back of your VISA card")));
}