// @mui
import { Typography, TextField, Stack } from '@mui/material'; // ----------------------------------------------------------------------

export default function PaymentBillingAddress() {
  return /*#__PURE__*/React.createElement("div", null, /*#__PURE__*/React.createElement(Typography, {
    variant: "subtitle1"
  }, "Billing Address"), /*#__PURE__*/React.createElement(Stack, {
    spacing: 3,
    mt: 5
  }, /*#__PURE__*/React.createElement(TextField, {
    fullWidth: true,
    label: "Person name"
  }), /*#__PURE__*/React.createElement(TextField, {
    fullWidth: true,
    label: "Phone number"
  }), /*#__PURE__*/React.createElement(TextField, {
    fullWidth: true,
    label: "Email"
  }), /*#__PURE__*/React.createElement(TextField, {
    fullWidth: true,
    label: "Address"
  })));
}