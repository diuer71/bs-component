function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

import PropTypes from 'prop-types';
import { useState, useEffect } from 'react'; // @mui

import { useTheme, styled } from '@mui/material/styles';
import { Box, Drawer, Divider, IconButton } from '@mui/material'; // hooks

import useResponsive from '../../../hooks/useResponsive'; // components

import Iconify from '../../../components/Iconify'; //

import ChatRoomAttachment from './ChatRoomAttachment';
import ChatRoomOneParticipant from './ChatRoomOneParticipant';
import ChatRoomGroupParticipant from './ChatRoomGroupParticipant'; // ----------------------------------------------------------------------

const ToggleButtonStyle = styled(props => /*#__PURE__*/React.createElement(IconButton, _extends({
  disableRipple: true
}, props)))(({
  theme
}) => ({
  right: 0,
  zIndex: 9,
  width: 32,
  height: 32,
  position: 'absolute',
  top: theme.spacing(1),
  boxShadow: theme.customShadows.z8,
  backgroundColor: theme.palette.background.paper,
  border: `solid 1px ${theme.palette.divider}`,
  borderRight: 0,
  borderRadius: `12px 0 0 12px`,
  transition: theme.transitions.create('all'),
  '&:hover': {
    backgroundColor: theme.palette.background.neutral
  }
})); // ----------------------------------------------------------------------

const SIDEBAR_WIDTH = 240;
ChatRoom.propTypes = {
  conversation: PropTypes.object.isRequired,
  participants: PropTypes.array.isRequired
};
export default function ChatRoom({
  conversation,
  participants
}) {
  const theme = useTheme();
  const [openSidebar, setOpenSidebar] = useState(true);
  const [showInfo, setShowInfo] = useState(true);
  const [selectUser, setSelectUser] = useState(null);
  const [showAttachment, setShowAttachment] = useState(true);
  const [showParticipants, setShowParticipants] = useState(true);
  const isDesktop = useResponsive('up', 'lg');
  const isGroup = participants.length > 1;
  useEffect(() => {
    if (!isDesktop) {
      return handleCloseSidebar();
    }

    return handleOpenSidebar();
  }, [isDesktop]);

  const handleOpenSidebar = () => {
    setOpenSidebar(true);
  };

  const handleCloseSidebar = () => {
    setOpenSidebar(false);
  };

  const handleToggleSidebar = () => {
    setOpenSidebar(prev => !prev);
  };

  const renderContent = /*#__PURE__*/React.createElement(React.Fragment, null, isGroup ? /*#__PURE__*/React.createElement(ChatRoomGroupParticipant, {
    selectUserId: selectUser,
    participants: participants,
    isCollapse: showParticipants,
    onShowPopupUserInfo: participantId => setSelectUser(participantId),
    onCollapse: () => setShowParticipants(prev => !prev)
  }) : /*#__PURE__*/React.createElement("div", null, /*#__PURE__*/React.createElement(ChatRoomOneParticipant, {
    participants: participants,
    isCollapse: showInfo,
    onCollapse: () => setShowInfo(prev => !prev)
  })), /*#__PURE__*/React.createElement(Divider, null), /*#__PURE__*/React.createElement(ChatRoomAttachment, {
    conversation: conversation,
    isCollapse: showAttachment,
    onCollapse: () => setShowAttachment(prev => !prev)
  }));
  return /*#__PURE__*/React.createElement(Box, {
    sx: {
      position: 'relative'
    }
  }, /*#__PURE__*/React.createElement(ToggleButtonStyle, {
    onClick: handleToggleSidebar,
    sx: { ...(openSidebar && isDesktop && {
        right: SIDEBAR_WIDTH
      })
    }
  }, /*#__PURE__*/React.createElement(Iconify, {
    width: 16,
    height: 16,
    icon: openSidebar ? 'eva:arrow-ios-forward-fill' : 'eva:arrow-ios-back-fill'
  })), isDesktop ? /*#__PURE__*/React.createElement(Drawer, {
    open: openSidebar,
    anchor: "right",
    variant: "persistent",
    sx: {
      height: 1,
      width: SIDEBAR_WIDTH,
      transition: theme.transitions.create('width'),
      ...(!openSidebar && {
        width: '0px'
      }),
      '& .MuiDrawer-paper': {
        position: 'static',
        width: SIDEBAR_WIDTH
      }
    }
  }, renderContent) : /*#__PURE__*/React.createElement(Drawer, {
    anchor: "right",
    ModalProps: {
      keepMounted: true
    },
    open: openSidebar,
    onClose: handleCloseSidebar,
    sx: {
      '& .MuiDrawer-paper': {
        width: SIDEBAR_WIDTH
      }
    }
  }, renderContent));
}