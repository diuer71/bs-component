import PropTypes from 'prop-types'; // @mui

import { List, Avatar, Typography, ListItemText, ListItemAvatar, ListItemButton } from '@mui/material'; //

import SearchNotFound from '../../../components/SearchNotFound'; // ----------------------------------------------------------------------

ChatSearchResults.propTypes = {
  query: PropTypes.string,
  results: PropTypes.array,
  onSelectContact: PropTypes.func
};
export default function ChatSearchResults({
  query,
  results,
  onSelectContact
}) {
  const isFound = results.length > 0;
  return /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement(Typography, {
    paragraph: true,
    variant: "subtitle1",
    sx: {
      px: 3,
      color: 'text.secondary'
    }
  }, "Contacts"), /*#__PURE__*/React.createElement(List, {
    disablePadding: true
  }, results.map(result => /*#__PURE__*/React.createElement(ListItemButton, {
    key: result.id,
    onClick: () => onSelectContact(result),
    sx: {
      py: 1.5,
      px: 3
    }
  }, /*#__PURE__*/React.createElement(ListItemAvatar, null, /*#__PURE__*/React.createElement(Avatar, {
    alt: result.name,
    src: result.avatar
  })), /*#__PURE__*/React.createElement(ListItemText, {
    primary: result.name,
    primaryTypographyProps: {
      noWrap: true,
      variant: 'subtitle2'
    }
  })))), !isFound && /*#__PURE__*/React.createElement(SearchNotFound, {
    searchQuery: query,
    sx: {
      p: 3,
      mx: 'auto',
      width: `calc(100% - 48px)`,
      bgcolor: 'background.neutral'
    }
  }));
}