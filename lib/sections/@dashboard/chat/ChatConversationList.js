function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

import PropTypes from 'prop-types';
import { useNavigate } from 'react-router-dom'; // @mui

import { List } from '@mui/material'; // routes

import { PATH_DASHBOARD } from '../../../routes/paths'; // components

import { SkeletonConversationItem } from '../../../components/skeleton'; //

import ChatConversationItem from './ChatConversationItem'; // ----------------------------------------------------------------------

ChatConversationList.propTypes = {
  conversations: PropTypes.object,
  isOpenSidebar: PropTypes.bool,
  activeConversationId: PropTypes.string,
  sx: PropTypes.object
};
export default function ChatConversationList({
  conversations,
  isOpenSidebar,
  activeConversationId,
  sx,
  ...other
}) {
  const navigate = useNavigate();

  const handleSelectConversation = conversationId => {
    let conversationKey = '';
    const conversation = conversations.byId[conversationId];

    if (conversation.type === 'GROUP') {
      conversationKey = conversation.id;
    } else {
      const otherParticipant = conversation.participants.find(participant => participant.id !== '8864c717-587d-472a-929a-8e5f298024da-0');

      if (otherParticipant?.username) {
        conversationKey = otherParticipant?.username;
      }
    }

    navigate(`${PATH_DASHBOARD.chat.root}/${conversationKey}`);
  };

  const loading = !conversations.allIds.length;
  return /*#__PURE__*/React.createElement(List, _extends({
    disablePadding: true,
    sx: sx
  }, other), (loading ? [...Array(12)] : conversations.allIds).map((conversationId, index) => conversationId ? /*#__PURE__*/React.createElement(ChatConversationItem, {
    key: conversationId,
    isOpenSidebar: isOpenSidebar,
    conversation: conversations.byId[conversationId],
    isSelected: activeConversationId === conversationId,
    onSelectConversation: () => handleSelectConversation(conversationId)
  }) : /*#__PURE__*/React.createElement(SkeletonConversationItem, {
    key: index
  })));
}