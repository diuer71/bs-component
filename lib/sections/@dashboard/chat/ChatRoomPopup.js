import PropTypes from 'prop-types'; // @mui

import { styled } from '@mui/material/styles';
import { Avatar, Typography, DialogContent } from '@mui/material'; // components

import Iconify from '../../../components/Iconify';
import { DialogAnimate } from '../../../components/animate'; // ----------------------------------------------------------------------

const RowStyle = styled('div')(({
  theme
}) => ({
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
  marginTop: theme.spacing(1.5)
})); // ----------------------------------------------------------------------

ChatRoomPopup.propTypes = {
  participant: PropTypes.object,
  isOpen: PropTypes.bool,
  onClose: PropTypes.func
};
export default function ChatRoomPopup({
  participant,
  isOpen,
  onClose
}) {
  const {
    name,
    avatar,
    position,
    address,
    phone,
    email
  } = participant;
  return /*#__PURE__*/React.createElement(DialogAnimate, {
    fullWidth: true,
    maxWidth: "xs",
    open: isOpen,
    onClose: onClose
  }, /*#__PURE__*/React.createElement(DialogContent, {
    sx: {
      pb: 5,
      textAlign: 'center'
    }
  }, /*#__PURE__*/React.createElement(Avatar, {
    alt: name,
    src: avatar,
    sx: {
      mt: 5,
      mb: 2,
      mx: 'auto',
      width: 96,
      height: 96
    }
  }), /*#__PURE__*/React.createElement(Typography, {
    variant: "h6"
  }, name), /*#__PURE__*/React.createElement(Typography, {
    variant: "body2",
    paragraph: true,
    sx: {
      color: 'text.secondary'
    }
  }, position), /*#__PURE__*/React.createElement(RowStyle, null, /*#__PURE__*/React.createElement(Iconify, {
    icon: 'eva:pin-fill',
    sx: {
      mr: 1,
      width: 16,
      height: 16,
      color: 'text.disabled'
    }
  }), /*#__PURE__*/React.createElement(Typography, {
    variant: "body2"
  }, address)), /*#__PURE__*/React.createElement(RowStyle, null, /*#__PURE__*/React.createElement(Iconify, {
    icon: 'eva:phone-fill',
    sx: {
      mr: 1,
      width: 16,
      height: 16,
      color: 'text.disabled'
    }
  }), /*#__PURE__*/React.createElement(Typography, {
    variant: "body2"
  }, phone)), /*#__PURE__*/React.createElement(RowStyle, null, /*#__PURE__*/React.createElement(Iconify, {
    icon: 'eva:email-fill',
    sx: {
      mr: 1,
      width: 16,
      height: 16,
      color: 'text.disabled'
    }
  }), /*#__PURE__*/React.createElement(Typography, {
    variant: "body2"
  }, email))));
}