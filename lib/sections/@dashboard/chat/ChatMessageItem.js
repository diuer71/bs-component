import PropTypes from 'prop-types';
import { formatDistanceToNowStrict } from 'date-fns'; // @mui

import { styled } from '@mui/material/styles';
import { Avatar, Box, Typography } from '@mui/material'; // components

import Image from '../../../components/Image'; // ----------------------------------------------------------------------

const RootStyle = styled('div')(({
  theme
}) => ({
  display: 'flex',
  marginBottom: theme.spacing(3)
}));
const ContentStyle = styled('div')(({
  theme
}) => ({
  maxWidth: 320,
  padding: theme.spacing(1.5),
  marginTop: theme.spacing(0.5),
  borderRadius: theme.shape.borderRadius,
  backgroundColor: theme.palette.background.neutral
}));
const InfoStyle = styled(Typography)(({
  theme
}) => ({
  display: 'flex',
  marginBottom: theme.spacing(0.75),
  color: theme.palette.text.secondary
})); // ----------------------------------------------------------------------

ChatMessageItem.propTypes = {
  message: PropTypes.object.isRequired,
  conversation: PropTypes.object.isRequired,
  onOpenLightbox: PropTypes.func
};
export default function ChatMessageItem({
  message,
  conversation,
  onOpenLightbox
}) {
  const sender = conversation.participants.find(participant => participant.id === message.senderId);
  const senderDetails = message.senderId === '8864c717-587d-472a-929a-8e5f298024da-0' ? {
    type: 'me'
  } : {
    avatar: sender?.avatar,
    name: sender?.name
  };
  const isMe = senderDetails.type === 'me';
  const isImage = message.contentType === 'image';
  const firstName = senderDetails.name && senderDetails.name.split(' ')[0];
  return /*#__PURE__*/React.createElement(RootStyle, null, /*#__PURE__*/React.createElement(Box, {
    sx: {
      display: 'flex',
      ...(isMe && {
        ml: 'auto'
      })
    }
  }, senderDetails.type !== 'me' && /*#__PURE__*/React.createElement(Avatar, {
    alt: senderDetails.name,
    src: senderDetails.avatar,
    sx: {
      width: 32,
      height: 32,
      mr: 2
    }
  }), /*#__PURE__*/React.createElement("div", null, /*#__PURE__*/React.createElement(InfoStyle, {
    variant: "caption",
    sx: { ...(isMe && {
        justifyContent: 'flex-end'
      })
    }
  }, !isMe && `${firstName},`, "\xA0", formatDistanceToNowStrict(new Date(message.createdAt), {
    addSuffix: true
  })), /*#__PURE__*/React.createElement(ContentStyle, {
    sx: { ...(isMe && {
        color: 'grey.800',
        bgcolor: 'primary.lighter'
      }),
      ...(isImage && {
        p: 0
      })
    }
  }, isImage ? /*#__PURE__*/React.createElement(Image, {
    alt: "attachment",
    src: message.body,
    onClick: () => onOpenLightbox(message.body),
    sx: {
      borderRadius: 1,
      cursor: 'pointer',
      '&:hover': {
        opacity: 0.8
      }
    }
  }) : /*#__PURE__*/React.createElement(Typography, {
    variant: "body2"
  }, message.body)))));
}