import PropTypes from 'prop-types';
import { useRef, useState } from 'react'; // @mui

import { styled } from '@mui/material/styles';
import { Stack, Input, Divider, IconButton, InputAdornment } from '@mui/material'; // utils

import uuidv4 from '../../../utils/uuidv4'; // components

import Iconify from '../../../components/Iconify';
import EmojiPicker from '../../../components/EmojiPicker'; // ----------------------------------------------------------------------

const RootStyle = styled('div')(({
  theme
}) => ({
  minHeight: 56,
  display: 'flex',
  position: 'relative',
  alignItems: 'center',
  paddingLeft: theme.spacing(2)
})); // ----------------------------------------------------------------------

ChatMessageInput.propTypes = {
  disabled: PropTypes.bool,
  conversationId: PropTypes.string,
  onSend: PropTypes.func
};
export default function ChatMessageInput({
  disabled,
  conversationId,
  onSend
}) {
  const fileInputRef = useRef(null);
  const [message, setMessage] = useState('');

  const handleAttach = () => {
    fileInputRef.current?.click();
  };

  const handleKeyUp = event => {
    if (event.key === 'Enter') {
      handleSend();
    }
  };

  const handleSend = () => {
    if (!message) {
      return '';
    }

    if (onSend && conversationId) {
      onSend({
        conversationId,
        messageId: uuidv4(),
        message,
        contentType: 'text',
        attachments: [],
        createdAt: new Date(),
        senderId: '8864c717-587d-472a-929a-8e5f298024da-0'
      });
    }

    return setMessage('');
  };

  return /*#__PURE__*/React.createElement(RootStyle, null, /*#__PURE__*/React.createElement(Input, {
    disabled: disabled,
    fullWidth: true,
    value: message,
    disableUnderline: true,
    onKeyUp: handleKeyUp,
    onChange: event => setMessage(event.target.value),
    placeholder: "Type a message",
    startAdornment: /*#__PURE__*/React.createElement(InputAdornment, {
      position: "start"
    }, /*#__PURE__*/React.createElement(EmojiPicker, {
      disabled: disabled,
      value: message,
      setValue: setMessage
    })),
    endAdornment: /*#__PURE__*/React.createElement(Stack, {
      direction: "row",
      spacing: 1,
      sx: {
        flexShrink: 0,
        mr: 1.5
      }
    }, /*#__PURE__*/React.createElement(IconButton, {
      disabled: disabled,
      size: "small",
      onClick: handleAttach
    }, /*#__PURE__*/React.createElement(Iconify, {
      icon: "ic:round-add-photo-alternate",
      width: 22,
      height: 22
    })), /*#__PURE__*/React.createElement(IconButton, {
      disabled: disabled,
      size: "small",
      onClick: handleAttach
    }, /*#__PURE__*/React.createElement(Iconify, {
      icon: "eva:attach-2-fill",
      width: 22,
      height: 22
    })), /*#__PURE__*/React.createElement(IconButton, {
      disabled: disabled,
      size: "small"
    }, /*#__PURE__*/React.createElement(Iconify, {
      icon: "eva:mic-fill",
      width: 22,
      height: 22
    })))
  }), /*#__PURE__*/React.createElement(Divider, {
    orientation: "vertical",
    flexItem: true
  }), /*#__PURE__*/React.createElement(IconButton, {
    color: "primary",
    disabled: !message,
    onClick: handleSend,
    sx: {
      mx: 1
    }
  }, /*#__PURE__*/React.createElement(Iconify, {
    icon: "ic:round-send",
    width: 22,
    height: 22
  })), /*#__PURE__*/React.createElement("input", {
    type: "file",
    ref: fileInputRef,
    style: {
      display: 'none'
    }
  }));
}