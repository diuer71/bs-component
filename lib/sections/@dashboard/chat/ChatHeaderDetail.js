import PropTypes from 'prop-types';
import { capitalCase } from 'change-case'; // @mui

import { styled } from '@mui/material/styles';
import { Box, Link, Avatar, Typography, AvatarGroup, IconButton } from '@mui/material'; // utils

import { fToNow } from '../../../utils/formatTime'; // components

import Iconify from '../../../components/Iconify';
import BadgeStatus from '../../../components/BadgeStatus'; // ----------------------------------------------------------------------

const RootStyle = styled('div')(({
  theme
}) => ({
  flexShrink: 0,
  minHeight: 92,
  display: 'flex',
  alignItems: 'center',
  padding: theme.spacing(0, 3)
})); // ----------------------------------------------------------------------

ChatHeaderDetail.propTypes = {
  participants: PropTypes.array.isRequired
};
export default function ChatHeaderDetail({
  participants
}) {
  const isGroup = participants.length > 1;
  return /*#__PURE__*/React.createElement(RootStyle, null, isGroup ? /*#__PURE__*/React.createElement(GroupAvatar, {
    participants: participants
  }) : /*#__PURE__*/React.createElement(OneAvatar, {
    participants: participants
  }), /*#__PURE__*/React.createElement(Box, {
    sx: {
      flexGrow: 1
    }
  }), /*#__PURE__*/React.createElement(IconButton, null, /*#__PURE__*/React.createElement(Iconify, {
    icon: "eva:phone-fill",
    width: 20,
    height: 20
  })), /*#__PURE__*/React.createElement(IconButton, null, /*#__PURE__*/React.createElement(Iconify, {
    icon: "eva:video-fill",
    width: 20,
    height: 20
  })), /*#__PURE__*/React.createElement(IconButton, null, /*#__PURE__*/React.createElement(Iconify, {
    icon: "eva:more-vertical-fill",
    width: 20,
    height: 20
  })));
} // ----------------------------------------------------------------------

OneAvatar.propTypes = {
  participants: PropTypes.array.isRequired
};

function OneAvatar({
  participants
}) {
  const participant = [...participants][0];

  if (participant === undefined || !participant.status) {
    return null;
  }

  return /*#__PURE__*/React.createElement(Box, {
    sx: {
      display: 'flex',
      alignItems: 'center'
    }
  }, /*#__PURE__*/React.createElement(Box, {
    sx: {
      position: 'relative'
    }
  }, /*#__PURE__*/React.createElement(Avatar, {
    src: participant.avatar,
    alt: participant.name
  }), /*#__PURE__*/React.createElement(BadgeStatus, {
    status: participant.status,
    sx: {
      position: 'absolute',
      right: 2,
      bottom: 2
    }
  })), /*#__PURE__*/React.createElement(Box, {
    sx: {
      ml: 2
    }
  }, /*#__PURE__*/React.createElement(Typography, {
    variant: "subtitle2"
  }, participant.name), /*#__PURE__*/React.createElement(Typography, {
    variant: "body2",
    sx: {
      color: 'text.secondary'
    }
  }, participant.status !== 'offline' ? capitalCase(participant.status) : fToNow(participant.lastActivity || ''))));
} // ----------------------------------------------------------------------


GroupAvatar.propTypes = {
  participants: PropTypes.array.isRequired
};

function GroupAvatar({
  participants
}) {
  return /*#__PURE__*/React.createElement("div", null, /*#__PURE__*/React.createElement(AvatarGroup, {
    max: 3,
    sx: {
      mb: 0.5,
      '& .MuiAvatar-root': {
        width: 32,
        height: 32
      }
    }
  }, participants.map(participant => /*#__PURE__*/React.createElement(Avatar, {
    key: participant.id,
    alt: participant.name,
    src: participant.avatar
  }))), /*#__PURE__*/React.createElement(Link, {
    variant: "body2",
    underline: "none",
    component: "button",
    color: "text.secondary",
    onClick: () => {}
  }, /*#__PURE__*/React.createElement(Box, {
    sx: {
      display: 'flex',
      alignItems: 'center'
    }
  }, participants.length, " persons", /*#__PURE__*/React.createElement(Iconify, {
    icon: "eva:arrow-ios-forward-fill"
  }))));
}