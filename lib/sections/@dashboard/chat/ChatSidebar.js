function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

import { useState, useEffect } from 'react';
import { useNavigate, useLocation } from 'react-router-dom'; // @mui

import { useTheme, styled } from '@mui/material/styles';
import { Box, Stack, Drawer, IconButton } from '@mui/material'; // redux

import { useSelector } from '../../../redux/store'; // hooks

import useResponsive from '../../../hooks/useResponsive'; // utils

import axios from '../../../utils/axios'; // routes

import { PATH_DASHBOARD } from '../../../routes/paths'; // components

import Iconify from '../../../components/Iconify';
import Scrollbar from '../../../components/Scrollbar'; //

import ChatAccount from './ChatAccount';
import ChatSearchResults from './ChatSearchResults';
import ChatContactSearch from './ChatContactSearch';
import ChatConversationList from './ChatConversationList'; // ----------------------------------------------------------------------

const ToggleButtonStyle = styled(props => /*#__PURE__*/React.createElement(IconButton, _extends({
  disableRipple: true
}, props)))(({
  theme
}) => ({
  left: 0,
  zIndex: 9,
  width: 32,
  height: 32,
  position: 'absolute',
  top: theme.spacing(13),
  borderRadius: `0 12px 12px 0`,
  color: theme.palette.primary.contrastText,
  backgroundColor: theme.palette.primary.main,
  boxShadow: theme.customShadows.primary,
  '&:hover': {
    backgroundColor: theme.palette.primary.darker
  }
})); // ----------------------------------------------------------------------

const SIDEBAR_WIDTH = 320;
const SIDEBAR_COLLAPSE_WIDTH = 96;
export default function ChatSidebar() {
  const theme = useTheme();
  const navigate = useNavigate();
  const {
    pathname
  } = useLocation();
  const [openSidebar, setOpenSidebar] = useState(true);
  const [searchQuery, setSearchQuery] = useState('');
  const [searchResults, setSearchResults] = useState([]);
  const [isSearchFocused, setSearchFocused] = useState(false);
  const {
    conversations,
    activeConversationId
  } = useSelector(state => state.chat);
  const isDesktop = useResponsive('up', 'md');
  const displayResults = searchQuery && isSearchFocused;
  const isCollapse = isDesktop && !openSidebar;
  useEffect(() => {
    if (!isDesktop) {
      return handleCloseSidebar();
    }

    return handleOpenSidebar();
  }, [isDesktop, pathname]); // eslint-disable-next-line consistent-return

  useEffect(() => {
    if (!openSidebar) {
      return setSearchFocused(false);
    }
  }, [openSidebar]);

  const handleOpenSidebar = () => {
    setOpenSidebar(true);
  };

  const handleCloseSidebar = () => {
    setOpenSidebar(false);
  };

  const handleToggleSidebar = () => {
    setOpenSidebar(prev => !prev);
  };

  const handleClickAwaySearch = () => {
    setSearchFocused(false);
    setSearchQuery('');
  };

  const handleChangeSearch = async event => {
    try {
      const {
        value
      } = event.target;
      setSearchQuery(value);

      if (value) {
        const response = await axios.get('/api/chat/search', {
          params: {
            query: value
          }
        });
        setSearchResults(response.data.results);
      } else {
        setSearchResults([]);
      }
    } catch (error) {
      console.error(error);
    }
  };

  const handleSearchFocus = () => {
    setSearchFocused(true);
  };

  const handleSearchSelect = username => {
    setSearchFocused(false);
    setSearchQuery('');
    navigate(`${PATH_DASHBOARD.chat.root}/${username}`);
  };

  const handleSelectContact = result => {
    if (handleSearchSelect) {
      handleSearchSelect(result.username);
    }
  };

  const renderContent = /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement(Box, {
    sx: {
      py: 2,
      px: 3
    }
  }, /*#__PURE__*/React.createElement(Stack, {
    direction: "row",
    alignItems: "center",
    justifyContent: "center"
  }, !isCollapse && /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement(ChatAccount, null), /*#__PURE__*/React.createElement(Box, {
    sx: {
      flexGrow: 1
    }
  })), /*#__PURE__*/React.createElement(IconButton, {
    onClick: handleToggleSidebar
  }, /*#__PURE__*/React.createElement(Iconify, {
    width: 20,
    height: 20,
    icon: openSidebar ? 'eva:arrow-ios-back-fill' : 'eva:arrow-ios-forward-fill'
  })), !isCollapse && /*#__PURE__*/React.createElement(IconButton, {
    onClick: () => navigate(PATH_DASHBOARD.chat.new)
  }, /*#__PURE__*/React.createElement(Iconify, {
    icon: 'eva:edit-fill',
    width: 20,
    height: 20
  }))), !isCollapse && /*#__PURE__*/React.createElement(ChatContactSearch, {
    query: searchQuery,
    onFocus: handleSearchFocus,
    onChange: handleChangeSearch,
    onClickAway: handleClickAwaySearch
  })), /*#__PURE__*/React.createElement(Scrollbar, null, !displayResults ? /*#__PURE__*/React.createElement(ChatConversationList, {
    conversations: conversations,
    isOpenSidebar: openSidebar,
    activeConversationId: activeConversationId,
    sx: { ...(isSearchFocused && {
        display: 'none'
      })
    }
  }) : /*#__PURE__*/React.createElement(ChatSearchResults, {
    query: searchQuery,
    results: searchResults,
    onSelectContact: handleSelectContact
  })));
  return /*#__PURE__*/React.createElement(React.Fragment, null, !isDesktop && /*#__PURE__*/React.createElement(ToggleButtonStyle, {
    onClick: handleToggleSidebar
  }, /*#__PURE__*/React.createElement(Iconify, {
    width: 16,
    height: 16,
    icon: 'eva:people-fill'
  })), isDesktop ? /*#__PURE__*/React.createElement(Drawer, {
    open: openSidebar,
    variant: "persistent",
    sx: {
      width: SIDEBAR_WIDTH,
      transition: theme.transitions.create('width'),
      '& .MuiDrawer-paper': {
        position: 'static',
        width: SIDEBAR_WIDTH
      },
      ...(isCollapse && {
        width: SIDEBAR_COLLAPSE_WIDTH,
        '& .MuiDrawer-paper': {
          width: SIDEBAR_COLLAPSE_WIDTH,
          position: 'static',
          transform: 'none !important',
          visibility: 'visible !important'
        }
      })
    }
  }, renderContent) : /*#__PURE__*/React.createElement(Drawer, {
    ModalProps: {
      keepMounted: true
    },
    open: openSidebar,
    onClose: handleCloseSidebar,
    sx: {
      '& .MuiDrawer-paper': {
        width: SIDEBAR_WIDTH
      }
    }
  }, renderContent));
}