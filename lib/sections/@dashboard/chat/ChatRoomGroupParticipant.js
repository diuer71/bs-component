import PropTypes from 'prop-types'; // @mui

import { styled } from '@mui/material/styles';
import { Box, List, Avatar, Button, Collapse, ListItemText, ListItemAvatar, ListItemButton } from '@mui/material'; // components

import Iconify from '../../../components/Iconify';
import Scrollbar from '../../../components/Scrollbar';
import BadgeStatus from '../../../components/BadgeStatus'; //

import ChatRoomPopup from './ChatRoomPopup'; // ----------------------------------------------------------------------

const HEIGHT = 64;
const CollapseButtonStyle = styled(Button)(({
  theme
}) => ({ ...theme.typography.overline,
  height: 40,
  borderRadius: 0,
  padding: theme.spacing(1, 2),
  justifyContent: 'space-between',
  color: theme.palette.text.disabled
})); // ----------------------------------------------------------------------

ChatRoomGroupParticipant.propTypes = {
  participants: PropTypes.array.isRequired,
  selectUserId: PropTypes.string,
  onShowPopupUserInfo: PropTypes.func,
  isCollapse: PropTypes.bool,
  onCollapse: PropTypes.func
};
export default function ChatRoomGroupParticipant({
  participants,
  selectUserId,
  onShowPopupUserInfo,
  isCollapse,
  onCollapse
}) {
  return /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement(CollapseButtonStyle, {
    fullWidth: true,
    disableRipple: true,
    color: "inherit",
    onClick: onCollapse,
    endIcon: /*#__PURE__*/React.createElement(Iconify, {
      icon: isCollapse ? 'eva:arrow-ios-downward-fill' : 'eva:arrow-ios-forward-fill',
      width: 16,
      height: 16
    })
  }, "In room (", participants.length, ")"), /*#__PURE__*/React.createElement(Box, {
    sx: {
      height: isCollapse ? HEIGHT * 4 : 0
    }
  }, /*#__PURE__*/React.createElement(Scrollbar, null, /*#__PURE__*/React.createElement(Collapse, {
    in: isCollapse,
    sx: {
      height: isCollapse ? HEIGHT * 4 : 0
    }
  }, /*#__PURE__*/React.createElement(List, {
    disablePadding: true
  }, participants.map(participant => /*#__PURE__*/React.createElement(Participant, {
    key: participant.id,
    participant: participant,
    isOpen: selectUserId === participant.id,
    onShowPopup: () => onShowPopupUserInfo(participant.id),
    onClosePopup: () => onShowPopupUserInfo(null)
  })))))));
} // ----------------------------------------------------------------------

Participant.propTypes = {
  participant: PropTypes.object.isRequired,
  isOpen: PropTypes.bool,
  onClosePopup: PropTypes.func,
  onShowPopup: PropTypes.func
};

function Participant({
  participant,
  isOpen,
  onClosePopup,
  onShowPopup
}) {
  const {
    name,
    avatar,
    status,
    position
  } = participant;
  return /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement(ListItemButton, {
    onClick: onShowPopup,
    sx: {
      height: HEIGHT,
      px: 2.5
    }
  }, /*#__PURE__*/React.createElement(ListItemAvatar, null, /*#__PURE__*/React.createElement(Box, {
    sx: {
      position: 'relative',
      width: 40,
      height: 40
    }
  }, /*#__PURE__*/React.createElement(Avatar, {
    alt: name,
    src: avatar
  }), /*#__PURE__*/React.createElement(BadgeStatus, {
    status: status,
    sx: {
      right: 0,
      bottom: 0,
      position: 'absolute'
    }
  }))), /*#__PURE__*/React.createElement(ListItemText, {
    primary: name,
    secondary: position,
    primaryTypographyProps: {
      variant: 'subtitle2',
      noWrap: true
    },
    secondaryTypographyProps: {
      noWrap: true
    }
  })), /*#__PURE__*/React.createElement(ChatRoomPopup, {
    participant: participant,
    isOpen: isOpen,
    onClose: onClosePopup
  }));
}