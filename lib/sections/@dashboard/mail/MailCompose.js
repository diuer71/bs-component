import PropTypes from 'prop-types';
import { useState } from 'react'; // @mui

import { styled } from '@mui/material/styles';
import { Box, Input, Portal, Button, Divider, Backdrop, IconButton, Typography } from '@mui/material'; // hooks

import useResponsive from '../../../hooks/useResponsive'; // components

import Iconify from '../../../components/Iconify';
import Editor from '../../../components/editor'; // ----------------------------------------------------------------------

const RootStyle = styled('div')(({
  theme
}) => ({
  right: 0,
  bottom: 0,
  zIndex: 1999,
  minHeight: 440,
  outline: 'none',
  display: 'flex',
  position: 'fixed',
  overflow: 'hidden',
  flexDirection: 'column',
  margin: theme.spacing(3),
  boxShadow: theme.customShadows.z20,
  borderRadius: Number(theme.shape.borderRadius) * 2,
  backgroundColor: theme.palette.background.paper
}));
const InputStyle = styled(Input)(({
  theme
}) => ({
  padding: theme.spacing(0.5, 3),
  borderBottom: `solid 1px ${theme.palette.divider}`
})); // ----------------------------------------------------------------------

MailCompose.propTypes = {
  isOpenCompose: PropTypes.bool,
  onCloseCompose: PropTypes.func
};
export default function MailCompose({
  isOpenCompose,
  onCloseCompose
}) {
  const [fullScreen, setFullScreen] = useState(false);
  const [message, setMessage] = useState('');
  const isDesktop = useResponsive('up', 'sm');

  const handleChangeMessage = value => {
    setMessage(value);
  };

  const handleExitFullScreen = () => {
    setFullScreen(false);
  };

  const handleEnterFullScreen = () => {
    setFullScreen(true);
  };

  const handleClose = () => {
    onCloseCompose();
    setFullScreen(false);
  };

  if (!isOpenCompose) {
    return null;
  }

  return /*#__PURE__*/React.createElement(Portal, null, /*#__PURE__*/React.createElement(Backdrop, {
    open: fullScreen || !isDesktop,
    sx: {
      zIndex: 1998
    }
  }), /*#__PURE__*/React.createElement(RootStyle, {
    sx: { ...(fullScreen && {
        top: 0,
        left: 0,
        zIndex: 1999,
        margin: 'auto',
        width: {
          xs: `calc(100% - 24px)`,
          md: `calc(100% - 80px)`
        },
        height: {
          xs: `calc(100% - 24px)`,
          md: `calc(100% - 80px)`
        }
      })
    }
  }, /*#__PURE__*/React.createElement(Box, {
    sx: {
      pl: 3,
      pr: 1,
      height: 60,
      display: 'flex',
      alignItems: 'center'
    }
  }, /*#__PURE__*/React.createElement(Typography, {
    variant: "h6"
  }, "New Message"), /*#__PURE__*/React.createElement(Box, {
    sx: {
      flexGrow: 1
    }
  }), /*#__PURE__*/React.createElement(IconButton, {
    onClick: fullScreen ? handleExitFullScreen : handleEnterFullScreen
  }, /*#__PURE__*/React.createElement(Iconify, {
    icon: fullScreen ? 'eva:collapse-fill' : 'eva:expand-fill',
    width: 20,
    height: 20
  })), /*#__PURE__*/React.createElement(IconButton, {
    onClick: handleClose
  }, /*#__PURE__*/React.createElement(Iconify, {
    icon: 'eva:close-fill',
    width: 20,
    height: 20
  }))), /*#__PURE__*/React.createElement(Divider, null), /*#__PURE__*/React.createElement(InputStyle, {
    disableUnderline: true,
    placeholder: "To"
  }), /*#__PURE__*/React.createElement(InputStyle, {
    disableUnderline: true,
    placeholder: "Subject"
  }), /*#__PURE__*/React.createElement(Editor, {
    simple: true,
    id: "compose-mail",
    value: message,
    onChange: handleChangeMessage,
    placeholder: "Type a message",
    sx: {
      borderColor: 'transparent',
      flexGrow: 1
    }
  }), /*#__PURE__*/React.createElement(Divider, null), /*#__PURE__*/React.createElement(Box, {
    sx: {
      py: 2,
      px: 3,
      display: 'flex',
      alignItems: 'center'
    }
  }, /*#__PURE__*/React.createElement(Button, {
    variant: "contained"
  }, "Send"), /*#__PURE__*/React.createElement(IconButton, {
    size: "small",
    sx: {
      ml: 2,
      mr: 1
    }
  }, /*#__PURE__*/React.createElement(Iconify, {
    icon: 'ic:round-add-photo-alternate',
    width: 24,
    height: 24
  })), /*#__PURE__*/React.createElement(IconButton, {
    size: "small"
  }, /*#__PURE__*/React.createElement(Iconify, {
    icon: 'eva:attach-2-fill',
    width: 24,
    height: 24
  })))));
}