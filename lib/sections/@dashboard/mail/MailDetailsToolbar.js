import PropTypes from 'prop-types';
import { useNavigate, useParams } from 'react-router-dom'; // @mui

import { styled } from '@mui/material/styles';
import { Box, Link, Tooltip, Typography, IconButton } from '@mui/material'; // routes

import { PATH_DASHBOARD } from '../../../routes/paths'; // hooks

import useResponsive from '../../../hooks/useResponsive'; // utils

import createAvatar from '../../../utils/createAvatar';
import { fDateTimeSuffix } from '../../../utils/formatTime'; // components

import Avatar from '../../../components/Avatar';
import Iconify from '../../../components/Iconify'; // ----------------------------------------------------------------------

const RootStyle = styled('div')(({
  theme
}) => ({
  height: 84,
  flexShrink: 0,
  display: 'flex',
  alignItems: 'center',
  padding: theme.spacing(0, 2),
  justifyContent: 'space-between'
})); // ----------------------------------------------------------------------

MailDetailsToolbar.propTypes = {
  mail: PropTypes.object
};
export default function MailDetailsToolbar({
  mail,
  ...other
}) {
  const navigate = useNavigate();
  const {
    systemLabel,
    customLabel
  } = useParams();
  const isDesktop = useResponsive('up', 'sm');
  const baseUrl = PATH_DASHBOARD.mail.root;

  const handleBack = () => {
    if (systemLabel) {
      return navigate(`${baseUrl}/${systemLabel}`);
    }

    if (customLabel) {
      return navigate(`${baseUrl}/label/${customLabel}`);
    }

    return navigate(`${baseUrl}/inbox`);
  };

  return /*#__PURE__*/React.createElement(RootStyle, other, /*#__PURE__*/React.createElement(Box, {
    sx: {
      display: 'flex',
      alignItems: 'center'
    }
  }, /*#__PURE__*/React.createElement(Tooltip, {
    title: "Back"
  }, /*#__PURE__*/React.createElement(IconButton, {
    onClick: handleBack
  }, /*#__PURE__*/React.createElement(Iconify, {
    icon: 'eva:arrow-ios-back-fill',
    width: 20,
    height: 20
  }))), /*#__PURE__*/React.createElement(Avatar, {
    alt: mail.from.name,
    src: mail.from.avatar || '',
    color: createAvatar(mail.from.name).color
  }, createAvatar(mail.from.name).name), /*#__PURE__*/React.createElement(Box, {
    sx: {
      ml: 2
    }
  }, /*#__PURE__*/React.createElement(Typography, {
    display: "inline",
    variant: "subtitle2"
  }, mail.from.name), /*#__PURE__*/React.createElement(Link, {
    variant: "caption",
    sx: {
      color: 'text.secondary'
    }
  }, "\xA0 ", `<${mail.from.email}>`), /*#__PURE__*/React.createElement(Typography, {
    variant: "caption",
    sx: {
      color: 'text.secondary',
      display: 'block'
    }
  }, "To:\xA0", mail.to.map(person => /*#__PURE__*/React.createElement(Link, {
    color: "inherit",
    key: person.email
  }, person.email))))), /*#__PURE__*/React.createElement(Box, {
    sx: {
      display: 'flex',
      alignItems: 'center'
    }
  }, isDesktop && /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement(Typography, {
    variant: "caption",
    sx: {
      color: 'text.secondary'
    }
  }, fDateTimeSuffix(mail.createdAt)), /*#__PURE__*/React.createElement(Tooltip, {
    title: "Reply"
  }, /*#__PURE__*/React.createElement(IconButton, null, /*#__PURE__*/React.createElement(Iconify, {
    icon: 'ic:round-reply',
    width: 20,
    height: 20
  })))), /*#__PURE__*/React.createElement(Tooltip, {
    title: "More options"
  }, /*#__PURE__*/React.createElement(IconButton, null, /*#__PURE__*/React.createElement(Iconify, {
    icon: 'eva:more-vertical-fill',
    width: 20,
    height: 20
  })))));
}