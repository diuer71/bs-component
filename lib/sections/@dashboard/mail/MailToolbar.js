import PropTypes from 'prop-types'; // @mui

import { styled } from '@mui/material/styles';
import { Box, Tooltip, Checkbox, Typography, IconButton, InputAdornment } from '@mui/material'; // hooks

import useResponsive from '../../../hooks/useResponsive'; // components

import Iconify from '../../../components/Iconify';
import InputStyle from '../../../components/InputStyle'; // ----------------------------------------------------------------------

const RootStyle = styled('div')(({
  theme
}) => ({
  height: 84,
  flexShrink: 0,
  display: 'flex',
  alignItems: 'center',
  padding: theme.spacing(0, 2)
})); // ----------------------------------------------------------------------

MailToolbar.propTypes = {
  mails: PropTypes.number.isRequired,
  selectedMails: PropTypes.number.isRequired,
  onOpenSidebar: PropTypes.func,
  onToggleDense: PropTypes.func,
  onSelectAll: PropTypes.func,
  onDeselectAll: PropTypes.func
};
export default function MailToolbar({
  mails,
  selectedMails,
  onOpenSidebar,
  onToggleDense,
  onSelectAll,
  onDeselectAll,
  ...other
}) {
  const smUp = useResponsive('up', 'sm');
  const mdUp = useResponsive('up', 'md');

  const handleSelectChange = checked => checked ? onSelectAll() : onDeselectAll();

  const selectedAllMails = selectedMails === mails && mails > 0;
  const selectedSomeMails = selectedMails > 0 && selectedMails < mails;
  return /*#__PURE__*/React.createElement(RootStyle, other, !mdUp && /*#__PURE__*/React.createElement(IconButton, {
    onClick: onOpenSidebar
  }, /*#__PURE__*/React.createElement(Iconify, {
    icon: 'eva:menu-fill'
  })), smUp && /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement(Checkbox, {
    checked: selectedAllMails,
    indeterminate: selectedSomeMails,
    onChange: event => handleSelectChange(event.target.checked)
  }), /*#__PURE__*/React.createElement(Tooltip, {
    title: "Refresh"
  }, /*#__PURE__*/React.createElement(IconButton, null, /*#__PURE__*/React.createElement(Iconify, {
    icon: 'eva:refresh-fill',
    width: 20,
    height: 20
  }))), /*#__PURE__*/React.createElement(Tooltip, {
    title: "Dense"
  }, /*#__PURE__*/React.createElement(IconButton, {
    onClick: onToggleDense
  }, /*#__PURE__*/React.createElement(Iconify, {
    icon: 'eva:collapse-fill',
    width: 20,
    height: 20
  }))), /*#__PURE__*/React.createElement(Tooltip, {
    title: "More"
  }, /*#__PURE__*/React.createElement(IconButton, null, /*#__PURE__*/React.createElement(Iconify, {
    icon: 'eva:more-vertical-fill',
    width: 20,
    height: 20
  })))), /*#__PURE__*/React.createElement(Box, {
    sx: {
      flexGrow: 1
    }
  }), /*#__PURE__*/React.createElement(InputStyle, {
    stretchStart: 180,
    size: "small",
    placeholder: "Search mail\u2026",
    InputProps: {
      startAdornment: /*#__PURE__*/React.createElement(InputAdornment, {
        position: "start"
      }, /*#__PURE__*/React.createElement(Iconify, {
        icon: 'eva:search-fill',
        sx: {
          color: 'text.disabled',
          width: 20,
          height: 20
        }
      }))
    }
  }), smUp && /*#__PURE__*/React.createElement(Box, {
    sx: {
      display: 'flex',
      alignItems: 'center',
      flexShrink: 0
    }
  }, /*#__PURE__*/React.createElement(Typography, {
    variant: "body2",
    sx: {
      mx: 2,
      color: 'text.secondary'
    }
  }, "1 - ", mails, " of ", mails), /*#__PURE__*/React.createElement(Tooltip, {
    title: "Next page"
  }, /*#__PURE__*/React.createElement(IconButton, null, /*#__PURE__*/React.createElement(Iconify, {
    icon: 'eva:arrow-ios-back-fill',
    width: 20,
    height: 20
  }))), /*#__PURE__*/React.createElement(Tooltip, {
    title: "Previous page"
  }, /*#__PURE__*/React.createElement(IconButton, null, /*#__PURE__*/React.createElement(Iconify, {
    icon: 'eva:arrow-ios-forward-fill',
    width: 20,
    height: 20
  })))));
}