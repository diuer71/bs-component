function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

import PropTypes from 'prop-types';
import { NavLink as RouterLink } from 'react-router-dom'; // @mui

import { Typography, ListItemText, ListItemIcon, ListItemButton } from '@mui/material'; // routes

import { PATH_DASHBOARD } from '../../../routes/paths'; // components

import Iconify from '../../../components/Iconify'; // ----------------------------------------------------------------------

const LABEL_ICONS = {
  all: 'eva:email-fill',
  inbox: 'eva:inbox-fill',
  trash: 'eva:trash-2-outline',
  drafts: 'eva:file-fill',
  spam: 'ic:round-report',
  sent: 'ic:round-send',
  starred: 'eva:star-fill',
  important: 'ic:round-label-important',
  id_social: 'eva:share-fill',
  id_promotions: 'ic:round-label',
  id_forums: 'ic:round-forum'
};

const linkTo = label => {
  const baseUrl = PATH_DASHBOARD.mail.root;

  if (label.type === 'system') {
    return `${baseUrl}/${label.id}`;
  }

  if (label.type === 'custom') {
    return `${baseUrl}/label/${label.name}`;
  }

  return baseUrl;
}; // ----------------------------------------------------------------------


MailSidebarItem.propTypes = {
  label: PropTypes.object.isRequired
};
export default function MailSidebarItem({
  label,
  ...other
}) {
  const isUnread = label.unreadCount > 0;
  return /*#__PURE__*/React.createElement(ListItemButton, _extends({
    to: linkTo(label),
    component: RouterLink,
    sx: {
      px: 3,
      height: 48,
      typography: 'body2',
      color: 'text.secondary',
      textTransform: 'capitalize',
      '&.active': {
        color: 'text.primary',
        fontWeight: 'fontWeightMedium',
        bgcolor: 'action.selected'
      }
    }
  }, other), /*#__PURE__*/React.createElement(ListItemIcon, null, /*#__PURE__*/React.createElement(Iconify, {
    icon: LABEL_ICONS[label.id],
    style: {
      color: label.color
    },
    width: 24,
    height: 24
  })), /*#__PURE__*/React.createElement(ListItemText, {
    disableTypography: true,
    primary: label.name
  }), isUnread && /*#__PURE__*/React.createElement(Typography, {
    variant: "caption"
  }, label.unreadCount));
}