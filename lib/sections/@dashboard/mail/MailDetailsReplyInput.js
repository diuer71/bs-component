import { useRef, useState } from 'react'; // @mui

import { Box, Button, TextField, IconButton } from '@mui/material'; // components

import Iconify from '../../../components/Iconify'; // ----------------------------------------------------------------------

export default function MailDetailsReplyInput() {
  const fileInputRef = useRef(null);
  const [message, setMessage] = useState('');

  const handleChangeMessage = event => {
    setMessage(event.target.value);
  };

  const handleAttach = () => {
    fileInputRef.current?.click();
  };

  return /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement(TextField, {
    fullWidth: true,
    multiline: true,
    minRows: 2,
    maxRows: 8,
    value: message,
    placeholder: "Type a message",
    onChange: handleChangeMessage,
    sx: {
      '& fieldset': {
        border: 'none !important'
      }
    }
  }), /*#__PURE__*/React.createElement(Box, {
    sx: {
      mr: 3,
      mb: 3,
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'flex-end'
    }
  }, /*#__PURE__*/React.createElement(IconButton, {
    size: "small",
    onClick: handleAttach
  }, /*#__PURE__*/React.createElement(Iconify, {
    icon: 'ic:round-add-photo-alternate',
    width: 24,
    height: 24
  })), /*#__PURE__*/React.createElement(IconButton, {
    size: "small",
    onClick: handleAttach,
    sx: {
      ml: 1,
      mr: 2
    }
  }, /*#__PURE__*/React.createElement(Iconify, {
    icon: 'eva:attach-2-fill',
    width: 24,
    height: 24
  })), /*#__PURE__*/React.createElement(Button, {
    variant: "contained"
  }, "Send")), /*#__PURE__*/React.createElement("input", {
    type: "file",
    ref: fileInputRef,
    style: {
      display: 'none'
    }
  }));
}