import { useState } from 'react'; // @mui

import { Button, MenuItem, Typography } from '@mui/material'; // redux

import { useDispatch, useSelector } from '../../../../redux/store';
import { sortByProducts } from '../../../../redux/slices/product'; // components

import Iconify from '../../../../components/Iconify';
import MenuPopover from '../../../../components/MenuPopover'; // ----------------------------------------------------------------------

const SORT_BY_OPTIONS = [{
  value: 'featured',
  label: 'Featured'
}, {
  value: 'newest',
  label: 'Newest'
}, {
  value: 'priceDesc',
  label: 'Price: High-Low'
}, {
  value: 'priceAsc',
  label: 'Price: Low-High'
}];

function renderLabel(label) {
  if (label === 'featured') {
    return 'Featured';
  }

  if (label === 'newest') {
    return 'Newest';
  }

  if (label === 'priceDesc') {
    return 'Price: High-Low';
  }

  return 'Price: Low-High';
} // ----------------------------------------------------------------------


export default function ShopProductSort() {
  const dispatch = useDispatch();
  const {
    sortBy
  } = useSelector(state => state.product);
  const [open, setOpen] = useState(null);

  const handleOpen = currentTarget => {
    setOpen(currentTarget);
  };

  const handleClose = () => {
    setOpen(null);
  };

  const handleSortBy = value => {
    handleClose();
    dispatch(sortByProducts(value));
  };

  return /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement(Button, {
    color: "inherit",
    disableRipple: true,
    onClick: event => handleOpen(event.currentTarget),
    endIcon: /*#__PURE__*/React.createElement(Iconify, {
      icon: open ? 'eva:chevron-up-fill' : 'eva:chevron-down-fill'
    })
  }, "Sort By:\xA0", /*#__PURE__*/React.createElement(Typography, {
    component: "span",
    variant: "subtitle2",
    sx: {
      color: 'text.secondary'
    }
  }, renderLabel(sortBy))), /*#__PURE__*/React.createElement(MenuPopover, {
    anchorEl: open,
    open: Boolean(open),
    onClose: handleClose,
    sx: {
      width: 'auto',
      '& .MuiMenuItem-root': {
        typography: 'body2',
        borderRadius: 0.75
      }
    }
  }, SORT_BY_OPTIONS.map(option => /*#__PURE__*/React.createElement(MenuItem, {
    key: option.value,
    selected: option.value === sortBy,
    onClick: () => handleSortBy(option.value)
  }, option.label))));
}