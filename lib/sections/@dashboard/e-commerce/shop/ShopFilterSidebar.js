import PropTypes from 'prop-types'; // form

import { Controller, useFormContext } from 'react-hook-form'; // @mui

import { Box, Radio, Stack, Button, Drawer, Rating, Divider, IconButton, Typography, RadioGroup, FormControlLabel } from '@mui/material'; // @types

import { NAVBAR } from '../../../../config'; // components

import Iconify from '../../../../components/Iconify';
import Scrollbar from '../../../../components/Scrollbar';
import { ColorManyPicker } from '../../../../components/color-utils';
import { RHFMultiCheckbox, RHFRadioGroup } from '../../../../components/hook-form'; // ----------------------------------------------------------------------

export const SORT_BY_OPTIONS = [{
  value: 'featured',
  label: 'Featured'
}, {
  value: 'newest',
  label: 'Newest'
}, {
  value: 'priceDesc',
  label: 'Price: High-Low'
}, {
  value: 'priceAsc',
  label: 'Price: Low-High'
}];
export const FILTER_GENDER_OPTIONS = ['Men', 'Women', 'Kids'];
export const FILTER_CATEGORY_OPTIONS = ['All', 'Shose', 'Apparel', 'Accessories'];
export const FILTER_RATING_OPTIONS = ['up4Star', 'up3Star', 'up2Star', 'up1Star'];
export const FILTER_PRICE_OPTIONS = [{
  value: 'below',
  label: 'Below $25'
}, {
  value: 'between',
  label: 'Between $25 - $75'
}, {
  value: 'above',
  label: 'Above $75'
}];
export const FILTER_COLOR_OPTIONS = ['#00AB55', '#000000', '#FFFFFF', '#FFC0CB', '#FF4842', '#1890FF', '#94D82D', '#FFC107']; // ----------------------------------------------------------------------

const onSelected = (selected, item) => selected.includes(item) ? selected.filter(value => value !== item) : [...selected, item];

ShopFilterSidebar.propTypes = {
  isOpen: PropTypes.bool,
  onResetAll: PropTypes.func,
  onOpen: PropTypes.func,
  onClose: PropTypes.func
};
export default function ShopFilterSidebar({
  isOpen,
  onResetAll,
  onOpen,
  onClose
}) {
  const {
    control
  } = useFormContext();
  return /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement(Button, {
    disableRipple: true,
    color: "inherit",
    endIcon: /*#__PURE__*/React.createElement(Iconify, {
      icon: 'ic:round-filter-list'
    }),
    onClick: onOpen
  }, "Filters"), /*#__PURE__*/React.createElement(Drawer, {
    anchor: "right",
    open: isOpen,
    onClose: onClose,
    PaperProps: {
      sx: {
        width: NAVBAR.BASE_WIDTH
      }
    }
  }, /*#__PURE__*/React.createElement(Stack, {
    direction: "row",
    alignItems: "center",
    justifyContent: "space-between",
    sx: {
      px: 1,
      py: 2
    }
  }, /*#__PURE__*/React.createElement(Typography, {
    variant: "subtitle1",
    sx: {
      ml: 1
    }
  }, "Filters"), /*#__PURE__*/React.createElement(IconButton, {
    onClick: onClose
  }, /*#__PURE__*/React.createElement(Iconify, {
    icon: 'eva:close-fill',
    width: 20,
    height: 20
  }))), /*#__PURE__*/React.createElement(Divider, null), /*#__PURE__*/React.createElement(Scrollbar, null, /*#__PURE__*/React.createElement(Stack, {
    spacing: 3,
    sx: {
      p: 3
    }
  }, /*#__PURE__*/React.createElement(Stack, {
    spacing: 1
  }, /*#__PURE__*/React.createElement(Typography, {
    variant: "subtitle1"
  }, "Gender"), /*#__PURE__*/React.createElement(RHFMultiCheckbox, {
    name: "gender",
    options: FILTER_GENDER_OPTIONS,
    sx: {
      width: 1
    }
  })), /*#__PURE__*/React.createElement(Stack, {
    spacing: 1
  }, /*#__PURE__*/React.createElement(Typography, {
    variant: "subtitle1"
  }, "Category"), /*#__PURE__*/React.createElement(RHFRadioGroup, {
    name: "category",
    options: FILTER_CATEGORY_OPTIONS,
    row: false
  })), /*#__PURE__*/React.createElement(Stack, {
    spacing: 1
  }, /*#__PURE__*/React.createElement(Typography, {
    variant: "subtitle1"
  }, "Colour"), /*#__PURE__*/React.createElement(Controller, {
    name: "colors",
    control: control,
    render: ({
      field
    }) => /*#__PURE__*/React.createElement(ColorManyPicker, {
      colors: FILTER_COLOR_OPTIONS,
      onChangeColor: color => field.onChange(onSelected(field.value, color)),
      sx: {
        maxWidth: 36 * 4
      }
    })
  })), /*#__PURE__*/React.createElement(Stack, {
    spacing: 1
  }, /*#__PURE__*/React.createElement(Typography, {
    variant: "subtitle1"
  }, "Price"), /*#__PURE__*/React.createElement(RHFRadioGroup, {
    name: "priceRange",
    options: FILTER_PRICE_OPTIONS.map(item => item.value),
    getOptionLabel: FILTER_PRICE_OPTIONS.map(item => item.label)
  })), /*#__PURE__*/React.createElement(Stack, {
    spacing: 1
  }, /*#__PURE__*/React.createElement(Typography, {
    variant: "subtitle1"
  }, "Rating"), /*#__PURE__*/React.createElement(Controller, {
    name: "rating",
    control: control,
    render: ({
      field
    }) => /*#__PURE__*/React.createElement(RadioGroup, field, FILTER_RATING_OPTIONS.map((item, index) => /*#__PURE__*/React.createElement(FormControlLabel, {
      key: item,
      value: item,
      control: /*#__PURE__*/React.createElement(Radio, {
        disableRipple: true,
        color: "default",
        icon: /*#__PURE__*/React.createElement(Rating, {
          readOnly: true,
          value: 4 - index
        }),
        checkedIcon: /*#__PURE__*/React.createElement(Rating, {
          readOnly: true,
          value: 4 - index
        }),
        sx: {
          '&:hover': {
            bgcolor: 'transparent'
          }
        }
      }),
      label: "& Up",
      sx: {
        my: 0.5,
        borderRadius: 1,
        '&:hover': {
          opacity: 0.48
        },
        ...(field.value.includes(item) && {
          bgcolor: 'action.selected'
        })
      }
    })))
  })))), /*#__PURE__*/React.createElement(Box, {
    sx: {
      p: 3
    }
  }, /*#__PURE__*/React.createElement(Button, {
    fullWidth: true,
    size: "large",
    type: "submit",
    color: "inherit",
    variant: "outlined",
    onClick: onResetAll,
    startIcon: /*#__PURE__*/React.createElement(Iconify, {
      icon: 'ic:round-clear-all'
    })
  }, "Clear All"))));
}