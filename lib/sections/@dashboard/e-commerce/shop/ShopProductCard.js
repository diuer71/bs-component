import PropTypes from 'prop-types';
import { paramCase } from 'change-case';
import { Link as RouterLink } from 'react-router-dom'; // @mui

import { Box, Card, Link, Typography, Stack } from '@mui/material'; // routes

import { PATH_DASHBOARD } from '../../../../routes/paths'; // utils

import { fCurrency } from '../../../../utils/formatNumber'; // components

import Label from '../../../../components/Label';
import Image from '../../../../components/Image';
import { ColorPreview } from '../../../../components/color-utils'; // ----------------------------------------------------------------------

ShopProductCard.propTypes = {
  product: PropTypes.object
};
export default function ShopProductCard({
  product
}) {
  const {
    name,
    cover,
    price,
    colors,
    status,
    priceSale
  } = product;
  const linkTo = `${PATH_DASHBOARD.eCommerce.root}/product/${paramCase(name)}`;
  return /*#__PURE__*/React.createElement(Card, null, /*#__PURE__*/React.createElement(Box, {
    sx: {
      position: 'relative'
    }
  }, status && /*#__PURE__*/React.createElement(Label, {
    variant: "filled",
    color: status === 'sale' && 'error' || 'info',
    sx: {
      top: 16,
      right: 16,
      zIndex: 9,
      position: 'absolute',
      textTransform: 'uppercase'
    }
  }, status), /*#__PURE__*/React.createElement(Image, {
    alt: name,
    src: cover,
    ratio: "1/1"
  })), /*#__PURE__*/React.createElement(Stack, {
    spacing: 2,
    sx: {
      p: 3
    }
  }, /*#__PURE__*/React.createElement(Link, {
    to: linkTo,
    color: "inherit",
    component: RouterLink
  }, /*#__PURE__*/React.createElement(Typography, {
    variant: "subtitle2",
    noWrap: true
  }, name)), /*#__PURE__*/React.createElement(Stack, {
    direction: "row",
    alignItems: "center",
    justifyContent: "space-between"
  }, /*#__PURE__*/React.createElement(ColorPreview, {
    colors: colors
  }), /*#__PURE__*/React.createElement(Stack, {
    direction: "row",
    spacing: 0.5
  }, priceSale && /*#__PURE__*/React.createElement(Typography, {
    component: "span",
    sx: {
      color: 'text.disabled',
      textDecoration: 'line-through'
    }
  }, fCurrency(priceSale)), /*#__PURE__*/React.createElement(Typography, {
    variant: "subtitle1"
  }, fCurrency(price))))));
}