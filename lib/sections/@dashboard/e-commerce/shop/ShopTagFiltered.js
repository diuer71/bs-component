function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

import PropTypes from 'prop-types';
import { sentenceCase } from 'change-case'; // @mui

import { useTheme, styled } from '@mui/material/styles';
import { Chip, Typography, Stack, Button } from '@mui/material'; // utils

import getColorName from '../../../../utils/getColorName'; // components

import Iconify from '../../../../components/Iconify'; // ----------------------------------------------------------------------

const RootStyle = styled('div')({
  flexGrow: 1,
  display: 'flex',
  flexWrap: 'wrap',
  alignItems: 'center'
});
const WrapperStyle = styled('div')(({
  theme
}) => ({
  display: 'flex',
  overflow: 'hidden',
  alignItems: 'stretch',
  margin: theme.spacing(0.5),
  borderRadius: theme.shape.borderRadius,
  border: `solid 1px ${theme.palette.divider}`
}));
const LabelStyle = styled(props => /*#__PURE__*/React.createElement(Typography, _extends({
  component: "span",
  variant: "subtitle2"
}, props)))(({
  theme
}) => ({
  display: 'flex',
  alignItems: 'center',
  padding: theme.spacing(0, 1),
  color: theme.palette.text.secondary,
  backgroundColor: theme.palette.background.neutral,
  borderRight: `solid 1px ${theme.palette.divider}`
})); // ----------------------------------------------------------------------

function labelPriceRange(range) {
  if (range === 'below') {
    return 'Below $25';
  }

  if (range === 'between') {
    return 'Between $25 - $75';
  }

  return 'Above $75';
}

ShopTagFiltered.propTypes = {
  filters: PropTypes.object,
  isShowReset: PropTypes.bool,
  onRemoveGender: PropTypes.func,
  onRemoveCategory: PropTypes.func,
  onRemoveColor: PropTypes.func,
  onRemovePrice: PropTypes.func,
  onRemoveRating: PropTypes.func,
  onResetAll: PropTypes.func
};
export default function ShopTagFiltered({
  filters,
  isShowReset,
  onRemoveGender,
  onRemoveCategory,
  onRemoveColor,
  onRemovePrice,
  onRemoveRating,
  onResetAll
}) {
  const theme = useTheme();
  const {
    gender,
    category,
    colors,
    priceRange,
    rating
  } = filters;
  return /*#__PURE__*/React.createElement(RootStyle, null, gender.length > 0 && /*#__PURE__*/React.createElement(WrapperStyle, null, /*#__PURE__*/React.createElement(LabelStyle, null, "Gender:"), /*#__PURE__*/React.createElement(Stack, {
    direction: "row",
    flexWrap: "wrap",
    sx: {
      p: 0.75
    }
  }, gender.map(_gender => /*#__PURE__*/React.createElement(Chip, {
    key: _gender,
    label: _gender,
    size: "small",
    onDelete: () => onRemoveGender(_gender),
    sx: {
      m: 0.5
    }
  })))), category !== 'All' && /*#__PURE__*/React.createElement(WrapperStyle, null, /*#__PURE__*/React.createElement(LabelStyle, null, "Category:"), /*#__PURE__*/React.createElement(Stack, {
    direction: "row",
    flexWrap: "wrap",
    sx: {
      p: 0.75
    }
  }, /*#__PURE__*/React.createElement(Chip, {
    size: "small",
    label: category,
    onDelete: onRemoveCategory,
    sx: {
      m: 0.5
    }
  }))), colors.length > 0 && /*#__PURE__*/React.createElement(WrapperStyle, null, /*#__PURE__*/React.createElement(LabelStyle, null, "Colors:"), /*#__PURE__*/React.createElement(Stack, {
    direction: "row",
    flexWrap: "wrap",
    sx: {
      p: 0.75
    }
  }, colors.map(color => /*#__PURE__*/React.createElement(Chip, {
    key: color,
    label: getColorName(color),
    size: "small",
    onDelete: () => onRemoveColor(color),
    sx: {
      m: 0.5,
      bgcolor: color,
      color: theme.palette.getContrastText(color),
      ...((color === '#FFFFFF' || color === '#000000') && {
        border: `solid 1px ${theme.palette.grey[500_32]}`,
        '& .MuiChip-deleteIcon': {
          color: 'text.disabled'
        }
      })
    }
  })))), priceRange && /*#__PURE__*/React.createElement(WrapperStyle, null, /*#__PURE__*/React.createElement(LabelStyle, null, "Price:"), /*#__PURE__*/React.createElement(Stack, {
    direction: "row",
    flexWrap: "wrap",
    sx: {
      p: 0.75
    }
  }, /*#__PURE__*/React.createElement(Chip, {
    size: "small",
    label: labelPriceRange(priceRange),
    onDelete: onRemovePrice,
    sx: {
      m: 0.5
    }
  }))), rating && /*#__PURE__*/React.createElement(WrapperStyle, null, /*#__PURE__*/React.createElement(LabelStyle, null, "Rating:"), /*#__PURE__*/React.createElement(Stack, {
    direction: "row",
    flexWrap: "wrap",
    sx: {
      p: 0.75
    }
  }, /*#__PURE__*/React.createElement(Chip, {
    size: "small",
    label: sentenceCase(rating),
    onDelete: onRemoveRating,
    sx: {
      m: 0.5
    }
  }))), isShowReset && /*#__PURE__*/React.createElement(Button, {
    color: "error",
    size: "small",
    onClick: onResetAll,
    startIcon: /*#__PURE__*/React.createElement(Iconify, {
      icon: 'ic:round-clear-all'
    })
  }, "Clear All"));
}