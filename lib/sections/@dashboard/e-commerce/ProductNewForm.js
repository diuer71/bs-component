function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

import PropTypes from 'prop-types';
import * as Yup from 'yup';
import { useSnackbar } from 'notistack';
import { useNavigate } from 'react-router-dom';
import { useCallback, useEffect, useMemo } from 'react'; // form

import { useForm, Controller } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup'; // @mui

import { styled } from '@mui/material/styles';
import { LoadingButton } from '@mui/lab';
import { Card, Chip, Grid, Stack, TextField, Typography, Autocomplete, InputAdornment } from '@mui/material'; // routes

import { PATH_DASHBOARD } from '../../../routes/paths'; // components

import { FormProvider, RHFSwitch, RHFSelect, RHFEditor, RHFTextField, RHFRadioGroup, RHFUploadMultiFile } from '../../../components/hook-form'; // ----------------------------------------------------------------------

const GENDER_OPTION = ['Men', 'Women', 'Kids'];
const CATEGORY_OPTION = [{
  group: 'Clothing',
  classify: ['Shirts', 'T-shirts', 'Jeans', 'Leather']
}, {
  group: 'Tailored',
  classify: ['Suits', 'Blazers', 'Trousers', 'Waistcoats']
}, {
  group: 'Accessories',
  classify: ['Shoes', 'Backpacks and bags', 'Bracelets', 'Face masks']
}];
const TAGS_OPTION = ['Toy Story 3', 'Logan', 'Full Metal Jacket', 'Dangal', 'The Sting', '2001: A Space Odyssey', "Singin' in the Rain", 'Toy Story', 'Bicycle Thieves', 'The Kid', 'Inglourious Basterds', 'Snatch', '3 Idiots'];
const LabelStyle = styled(Typography)(({
  theme
}) => ({ ...theme.typography.subtitle2,
  color: theme.palette.text.secondary,
  marginBottom: theme.spacing(1)
})); // ----------------------------------------------------------------------

ProductNewForm.propTypes = {
  isEdit: PropTypes.bool,
  currentProduct: PropTypes.object
};
export default function ProductNewForm({
  isEdit,
  currentProduct
}) {
  const navigate = useNavigate();
  const {
    enqueueSnackbar
  } = useSnackbar();
  const NewProductSchema = Yup.object().shape({
    name: Yup.string().required('Name is required'),
    description: Yup.string().required('Description is required'),
    images: Yup.array().min(1, 'Images is required'),
    price: Yup.number().moreThan(0, 'Price should not be $0.00')
  });
  const defaultValues = useMemo(() => ({
    name: currentProduct?.name || '',
    description: currentProduct?.description || '',
    images: currentProduct?.images || [],
    code: currentProduct?.code || '',
    sku: currentProduct?.sku || '',
    price: currentProduct?.price || 0,
    priceSale: currentProduct?.priceSale || 0,
    tags: currentProduct?.tags || [TAGS_OPTION[0]],
    inStock: true,
    taxes: true,
    gender: currentProduct?.gender || GENDER_OPTION[2],
    category: currentProduct?.category || CATEGORY_OPTION[0].classify[1]
  }), // eslint-disable-next-line react-hooks/exhaustive-deps
  [currentProduct]);
  const methods = useForm({
    resolver: yupResolver(NewProductSchema),
    defaultValues
  });
  const {
    reset,
    watch,
    control,
    setValue,
    getValues,
    handleSubmit,
    formState: {
      isSubmitting
    }
  } = methods;
  const values = watch();
  useEffect(() => {
    if (isEdit && currentProduct) {
      reset(defaultValues);
    }

    if (!isEdit) {
      reset(defaultValues);
    } // eslint-disable-next-line react-hooks/exhaustive-deps

  }, [isEdit, currentProduct]);

  const onSubmit = async () => {
    try {
      await new Promise(resolve => setTimeout(resolve, 500));
      reset();
      enqueueSnackbar(!isEdit ? 'Create success!' : 'Update success!');
      navigate(PATH_DASHBOARD.eCommerce.list);
    } catch (error) {
      console.error(error);
    }
  };

  const handleDrop = useCallback(acceptedFiles => {
    setValue('images', acceptedFiles.map(file => Object.assign(file, {
      preview: URL.createObjectURL(file)
    })));
  }, [setValue]);

  const handleRemoveAll = () => {
    setValue('images', []);
  };

  const handleRemove = file => {
    const filteredItems = values.images?.filter(_file => _file !== file);
    setValue('images', filteredItems);
  };

  return /*#__PURE__*/React.createElement(FormProvider, {
    methods: methods,
    onSubmit: handleSubmit(onSubmit)
  }, /*#__PURE__*/React.createElement(Grid, {
    container: true,
    spacing: 3
  }, /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: 12,
    md: 8
  }, /*#__PURE__*/React.createElement(Card, {
    sx: {
      p: 3
    }
  }, /*#__PURE__*/React.createElement(Stack, {
    spacing: 3
  }, /*#__PURE__*/React.createElement(RHFTextField, {
    name: "name",
    label: "Product Name"
  }), /*#__PURE__*/React.createElement("div", null, /*#__PURE__*/React.createElement(LabelStyle, null, "Description"), /*#__PURE__*/React.createElement(RHFEditor, {
    simple: true,
    name: "description"
  })), /*#__PURE__*/React.createElement("div", null, /*#__PURE__*/React.createElement(LabelStyle, null, "Images"), /*#__PURE__*/React.createElement(RHFUploadMultiFile, {
    name: "images",
    showPreview: true,
    accept: "image/*",
    maxSize: 3145728,
    onDrop: handleDrop,
    onRemove: handleRemove,
    onRemoveAll: handleRemoveAll
  }))))), /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: 12,
    md: 4
  }, /*#__PURE__*/React.createElement(Stack, {
    spacing: 3
  }, /*#__PURE__*/React.createElement(Card, {
    sx: {
      p: 3
    }
  }, /*#__PURE__*/React.createElement(RHFSwitch, {
    name: "inStock",
    label: "In stock"
  }), /*#__PURE__*/React.createElement(Stack, {
    spacing: 3,
    mt: 2
  }, /*#__PURE__*/React.createElement(RHFTextField, {
    name: "code",
    label: "Product Code"
  }), /*#__PURE__*/React.createElement(RHFTextField, {
    name: "sku",
    label: "Product SKU"
  }), /*#__PURE__*/React.createElement("div", null, /*#__PURE__*/React.createElement(LabelStyle, null, "Gender"), /*#__PURE__*/React.createElement(RHFRadioGroup, {
    name: "gender",
    options: GENDER_OPTION,
    sx: {
      '& .MuiFormControlLabel-root': {
        mr: 4
      }
    }
  })), /*#__PURE__*/React.createElement(RHFSelect, {
    name: "category",
    label: "Category"
  }, CATEGORY_OPTION.map(category => /*#__PURE__*/React.createElement("optgroup", {
    key: category.group,
    label: category.group
  }, category.classify.map(classify => /*#__PURE__*/React.createElement("option", {
    key: classify,
    value: classify
  }, classify))))), /*#__PURE__*/React.createElement(Controller, {
    name: "tags",
    control: control,
    render: ({
      field
    }) => /*#__PURE__*/React.createElement(Autocomplete, _extends({}, field, {
      multiple: true,
      freeSolo: true,
      onChange: (event, newValue) => field.onChange(newValue),
      options: TAGS_OPTION.map(option => option),
      renderTags: (value, getTagProps) => value.map((option, index) => /*#__PURE__*/React.createElement(Chip, _extends({}, getTagProps({
        index
      }), {
        key: option,
        size: "small",
        label: option
      }))),
      renderInput: params => /*#__PURE__*/React.createElement(TextField, _extends({
        label: "Tags"
      }, params))
    }))
  }))), /*#__PURE__*/React.createElement(Card, {
    sx: {
      p: 3
    }
  }, /*#__PURE__*/React.createElement(Stack, {
    spacing: 3,
    mb: 2
  }, /*#__PURE__*/React.createElement(RHFTextField, {
    name: "price",
    label: "Regular Price",
    placeholder: "0.00",
    value: getValues('price') === 0 ? '' : getValues('price'),
    onChange: event => setValue('price', Number(event.target.value)),
    InputLabelProps: {
      shrink: true
    },
    InputProps: {
      startAdornment: /*#__PURE__*/React.createElement(InputAdornment, {
        position: "start"
      }, "$"),
      type: 'number'
    }
  }), /*#__PURE__*/React.createElement(RHFTextField, {
    name: "priceSale",
    label: "Sale Price",
    placeholder: "0.00",
    value: getValues('priceSale') === 0 ? '' : getValues('priceSale'),
    onChange: event => setValue('price', Number(event.target.value)),
    InputLabelProps: {
      shrink: true
    },
    InputProps: {
      startAdornment: /*#__PURE__*/React.createElement(InputAdornment, {
        position: "start"
      }, "$"),
      type: 'number'
    }
  })), /*#__PURE__*/React.createElement(RHFSwitch, {
    name: "taxes",
    label: "Price includes taxes"
  })), /*#__PURE__*/React.createElement(LoadingButton, {
    type: "submit",
    variant: "contained",
    size: "large",
    loading: isSubmitting
  }, !isEdit ? 'Create Product' : 'Save Changes')))));
}