import PropTypes from 'prop-types';
import { useState } from 'react'; // @mui

import { Box, List, Button, Rating, Avatar, ListItem, Pagination, Typography } from '@mui/material'; // utils

import { fDate } from '../../../../utils/formatTime';
import { fShortenNumber } from '../../../../utils/formatNumber'; // components

import Iconify from '../../../../components/Iconify'; // ----------------------------------------------------------------------

ProductDetailsReviewList.propTypes = {
  product: PropTypes.object
};
export default function ProductDetailsReviewList({
  product
}) {
  const {
    reviews
  } = product;
  return /*#__PURE__*/React.createElement(Box, {
    sx: {
      pt: 3,
      px: 2,
      pb: 5
    }
  }, /*#__PURE__*/React.createElement(List, {
    disablePadding: true
  }, reviews.map(review => /*#__PURE__*/React.createElement(ReviewItem, {
    key: review.id,
    review: review
  }))), /*#__PURE__*/React.createElement(Box, {
    sx: {
      display: 'flex',
      justifyContent: 'flex-end'
    }
  }, /*#__PURE__*/React.createElement(Pagination, {
    count: 10,
    color: "primary"
  })));
} // ----------------------------------------------------------------------

ReviewItem.propTypes = {
  review: PropTypes.object
};

function ReviewItem({
  review
}) {
  const [isHelpful, setHelpfuls] = useState(false);
  const {
    name,
    rating,
    comment,
    helpful,
    postedAt,
    avatarUrl,
    isPurchased
  } = review;

  const handleClickHelpful = () => {
    setHelpfuls(prev => !prev);
  };

  return /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement(ListItem, {
    disableGutters: true,
    sx: {
      mb: 5,
      alignItems: 'flex-start',
      flexDirection: {
        xs: 'column',
        sm: 'row'
      }
    }
  }, /*#__PURE__*/React.createElement(Box, {
    sx: {
      mr: 2,
      display: 'flex',
      alignItems: 'center',
      mb: {
        xs: 2,
        sm: 0
      },
      minWidth: {
        xs: 160,
        md: 240
      },
      textAlign: {
        sm: 'center'
      },
      flexDirection: {
        sm: 'column'
      }
    }
  }, /*#__PURE__*/React.createElement(Avatar, {
    src: avatarUrl,
    sx: {
      mr: {
        xs: 2,
        sm: 0
      },
      mb: {
        sm: 2
      },
      width: {
        md: 64
      },
      height: {
        md: 64
      }
    }
  }), /*#__PURE__*/React.createElement("div", null, /*#__PURE__*/React.createElement(Typography, {
    variant: "subtitle2",
    noWrap: true
  }, name), /*#__PURE__*/React.createElement(Typography, {
    variant: "caption",
    sx: {
      color: 'text.secondary'
    },
    noWrap: true
  }, fDate(postedAt)))), /*#__PURE__*/React.createElement("div", null, /*#__PURE__*/React.createElement(Rating, {
    size: "small",
    value: rating,
    precision: 0.1,
    readOnly: true
  }), isPurchased && /*#__PURE__*/React.createElement(Typography, {
    variant: "caption",
    sx: {
      my: 1,
      display: 'flex',
      alignItems: 'center',
      color: 'primary.main'
    }
  }, /*#__PURE__*/React.createElement(Iconify, {
    icon: 'ic:round-verified',
    width: 16,
    height: 16
  }), "\xA0Verified purchase"), /*#__PURE__*/React.createElement(Typography, {
    variant: "body2"
  }, comment), /*#__PURE__*/React.createElement(Box, {
    sx: {
      mt: 1,
      display: 'flex',
      flexWrap: 'wrap',
      alignItems: 'center'
    }
  }, !isHelpful && /*#__PURE__*/React.createElement(Typography, {
    variant: "body2",
    sx: {
      mr: 1
    }
  }, "Was this review helpful to you?"), /*#__PURE__*/React.createElement(Button, {
    size: "small",
    color: "inherit",
    startIcon: /*#__PURE__*/React.createElement(Iconify, {
      icon: !isHelpful ? 'ic:round-thumb-up' : 'eva:checkmark-fill'
    }),
    onClick: handleClickHelpful
  }, isHelpful ? 'Helpful' : 'Thank', "(", fShortenNumber(!isHelpful ? helpful : helpful + 1), ")")))));
}