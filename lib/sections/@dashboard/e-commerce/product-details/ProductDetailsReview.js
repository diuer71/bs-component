import PropTypes from 'prop-types';
import { useState } from 'react'; // @mui

import { Divider, Collapse } from '@mui/material'; //

import ProductDetailsReviewForm from './ProductDetailsReviewForm';
import ProductDetailsReviewList from './ProductDetailsReviewList';
import ProductDetailsReviewOverview from './ProductDetailsReviewOverview'; // ----------------------------------------------------------------------

ProductDetailsReview.propTypes = {
  product: PropTypes.object
};
export default function ProductDetailsReview({
  product
}) {
  const [reviewBox, setReviewBox] = useState(false);

  const handleOpenReviewBox = () => {
    setReviewBox(prev => !prev);
  };

  const handleCloseReviewBox = () => {
    setReviewBox(false);
  };

  return /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement(ProductDetailsReviewOverview, {
    product: product,
    onOpen: handleOpenReviewBox
  }), /*#__PURE__*/React.createElement(Divider, null), /*#__PURE__*/React.createElement(Collapse, {
    in: reviewBox
  }, /*#__PURE__*/React.createElement(ProductDetailsReviewForm, {
    onClose: handleCloseReviewBox,
    id: "move_add_review"
  }), /*#__PURE__*/React.createElement(Divider, null)), /*#__PURE__*/React.createElement(ProductDetailsReviewList, {
    product: product
  }));
}