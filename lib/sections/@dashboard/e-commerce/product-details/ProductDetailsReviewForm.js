function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

import * as Yup from 'yup';
import PropTypes from 'prop-types'; // form

import { useForm, Controller } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup'; // @mui

import { styled } from '@mui/material/styles';
import { Button, Stack, Rating, Typography, FormHelperText } from '@mui/material';
import { LoadingButton } from '@mui/lab'; // components

import { FormProvider, RHFTextField } from '../../../../components/hook-form'; // ----------------------------------------------------------------------

const RootStyle = styled('div')(({
  theme
}) => ({
  margin: theme.spacing(3),
  padding: theme.spacing(3),
  borderRadius: Number(theme.shape.borderRadius) * 2,
  backgroundColor: theme.palette.background.neutral
})); // ----------------------------------------------------------------------

ProductDetailsReviewForm.propTypes = {
  onClose: PropTypes.func,
  id: PropTypes.string
};
export default function ProductDetailsReviewForm({
  onClose,
  id,
  ...other
}) {
  const ReviewSchema = Yup.object().shape({
    rating: Yup.mixed().required('Rating is required'),
    review: Yup.string().required('Review is required'),
    name: Yup.string().required('Name is required'),
    email: Yup.string().email('Email must be a valid email address').required('Email is required')
  });
  const defaultValues = {
    rating: null,
    review: '',
    name: '',
    email: ''
  };
  const methods = useForm({
    resolver: yupResolver(ReviewSchema),
    defaultValues
  });
  const {
    reset,
    control,
    handleSubmit,
    formState: {
      errors,
      isSubmitting
    }
  } = methods;

  const onSubmit = async () => {
    try {
      await new Promise(resolve => setTimeout(resolve, 500));
      reset();
      onClose();
    } catch (error) {
      console.error(error);
    }
  };

  const onCancel = () => {
    onClose();
    reset();
  };

  return /*#__PURE__*/React.createElement(RootStyle, _extends({}, other, {
    id: id
  }), /*#__PURE__*/React.createElement(Typography, {
    variant: "subtitle1",
    gutterBottom: true
  }, "Add Review"), /*#__PURE__*/React.createElement(FormProvider, {
    methods: methods,
    onSubmit: handleSubmit(onSubmit)
  }, /*#__PURE__*/React.createElement(Stack, {
    spacing: 3
  }, /*#__PURE__*/React.createElement("div", null, /*#__PURE__*/React.createElement(Stack, {
    direction: "row",
    flexWrap: "wrap",
    alignItems: "center",
    spacing: 1.5
  }, /*#__PURE__*/React.createElement(Typography, {
    variant: "body2"
  }, "Your review about this product:"), /*#__PURE__*/React.createElement(Controller, {
    name: "rating",
    control: control,
    render: ({
      field
    }) => /*#__PURE__*/React.createElement(Rating, _extends({}, field, {
      value: Number(field.value)
    }))
  })), !!errors.rating && /*#__PURE__*/React.createElement(FormHelperText, {
    error: true
  }, " ", errors.rating?.message)), /*#__PURE__*/React.createElement(RHFTextField, {
    name: "review",
    label: "Review *",
    multiline: true,
    rows: 3
  }), /*#__PURE__*/React.createElement(RHFTextField, {
    name: "name",
    label: "Name *"
  }), /*#__PURE__*/React.createElement(RHFTextField, {
    name: "email",
    label: "Email *"
  }), /*#__PURE__*/React.createElement(Stack, {
    direction: "row",
    justifyContent: "flex-end",
    spacing: 1.5
  }, /*#__PURE__*/React.createElement(Button, {
    color: "inherit",
    variant: "outlined",
    onClick: onCancel
  }, "Cancel"), /*#__PURE__*/React.createElement(LoadingButton, {
    type: "submit",
    variant: "contained",
    loading: isSubmitting
  }, "Post review")))));
}