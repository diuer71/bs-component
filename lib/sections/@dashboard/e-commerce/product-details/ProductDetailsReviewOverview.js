import PropTypes from 'prop-types';
import sumBy from 'lodash/sumBy'; // @mui

import { styled } from '@mui/material/styles';
import { Grid, Rating, Button, Typography, LinearProgress, Stack, Link } from '@mui/material'; // utils

import { fShortenNumber } from '../../../../utils/formatNumber'; // components

import Iconify from '../../../../components/Iconify'; // ----------------------------------------------------------------------

const RatingStyle = styled(Rating)(({
  theme
}) => ({
  marginBottom: theme.spacing(1)
}));
const GridStyle = styled(Grid)(({
  theme
}) => ({
  padding: theme.spacing(3),
  display: 'flex',
  alignItems: 'center',
  flexDirection: 'column',
  justifyContent: 'center',
  '&:nth-of-type(2)': {
    [theme.breakpoints.up('md')]: {
      borderLeft: `solid 1px ${theme.palette.divider}`,
      borderRight: `solid 1px ${theme.palette.divider}`
    }
  }
})); // ----------------------------------------------------------------------

ProductDetailsReviewOverview.propTypes = {
  product: PropTypes.object,
  onOpen: PropTypes.func
};
export default function ProductDetailsReviewOverview({
  product,
  onOpen
}) {
  const {
    totalRating,
    totalReview,
    ratings
  } = product;
  const total = sumBy(ratings, star => star.starCount);
  return /*#__PURE__*/React.createElement(Grid, {
    container: true
  }, /*#__PURE__*/React.createElement(GridStyle, {
    item: true,
    xs: 12,
    md: 4
  }, /*#__PURE__*/React.createElement(Typography, {
    variant: "subtitle1",
    gutterBottom: true
  }, "Average rating"), /*#__PURE__*/React.createElement(Typography, {
    variant: "h2",
    gutterBottom: true,
    sx: {
      color: 'error.main'
    }
  }, totalRating, "/5"), /*#__PURE__*/React.createElement(RatingStyle, {
    readOnly: true,
    value: totalRating,
    precision: 0.1
  }), /*#__PURE__*/React.createElement(Typography, {
    variant: "body2",
    sx: {
      color: 'text.secondary'
    }
  }, "(", fShortenNumber(totalReview), "\xA0reviews)")), /*#__PURE__*/React.createElement(GridStyle, {
    item: true,
    xs: 12,
    md: 4
  }, /*#__PURE__*/React.createElement(Stack, {
    spacing: 1.5,
    sx: {
      width: 1
    }
  }, ratings.slice(0).reverse().map(rating => /*#__PURE__*/React.createElement(ProgressItem, {
    key: rating.name,
    star: rating,
    total: total
  })))), /*#__PURE__*/React.createElement(GridStyle, {
    item: true,
    xs: 12,
    md: 4
  }, /*#__PURE__*/React.createElement(Link, {
    href: "#move_add_review",
    underline: "none"
  }, /*#__PURE__*/React.createElement(Button, {
    size: "large",
    onClick: onOpen,
    variant: "outlined",
    startIcon: /*#__PURE__*/React.createElement(Iconify, {
      icon: 'eva:edit-2-fill'
    })
  }, "Write your review"))));
} // ----------------------------------------------------------------------

ProgressItem.propTypes = {
  star: PropTypes.object,
  total: PropTypes.number
};

function ProgressItem({
  star,
  total
}) {
  const {
    name,
    starCount,
    reviewCount
  } = star;
  return /*#__PURE__*/React.createElement(Stack, {
    direction: "row",
    alignItems: "center",
    spacing: 1.5
  }, /*#__PURE__*/React.createElement(Typography, {
    variant: "subtitle2"
  }, name), /*#__PURE__*/React.createElement(LinearProgress, {
    variant: "determinate",
    value: starCount / total * 100,
    sx: {
      mx: 2,
      flexGrow: 1,
      bgcolor: 'divider'
    }
  }), /*#__PURE__*/React.createElement(Typography, {
    variant: "body2",
    sx: {
      color: 'text.secondary',
      minWidth: 64,
      textAlign: 'right'
    }
  }, fShortenNumber(reviewCount)));
}