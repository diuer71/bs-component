import PropTypes from 'prop-types';
import { sentenceCase } from 'change-case';
import { useNavigate } from 'react-router-dom'; // form

import { Controller, useForm } from 'react-hook-form'; // @mui

import { useTheme, styled } from '@mui/material/styles';
import { Box, Link, Stack, Button, Rating, Divider, IconButton, Typography } from '@mui/material'; // routes

import { PATH_DASHBOARD } from '../../../../routes/paths'; // utils

import { fShortenNumber, fCurrency } from '../../../../utils/formatNumber'; // components

import Label from '../../../../components/Label';
import Iconify from '../../../../components/Iconify';
import SocialsButton from '../../../../components/SocialsButton';
import { ColorSinglePicker } from '../../../../components/color-utils';
import { FormProvider, RHFSelect } from '../../../../components/hook-form'; // ----------------------------------------------------------------------

const RootStyle = styled('div')(({
  theme
}) => ({
  padding: theme.spacing(3),
  [theme.breakpoints.up(1368)]: {
    padding: theme.spacing(5, 8)
  }
})); // ----------------------------------------------------------------------

ProductDetailsSummary.propTypes = {
  cart: PropTypes.array,
  onAddCart: PropTypes.func,
  onGotoStep: PropTypes.func,
  product: PropTypes.shape({
    available: PropTypes.number,
    colors: PropTypes.arrayOf(PropTypes.string),
    cover: PropTypes.string,
    id: PropTypes.string,
    inventoryType: PropTypes.string,
    name: PropTypes.string,
    price: PropTypes.number,
    priceSale: PropTypes.number,
    sizes: PropTypes.arrayOf(PropTypes.string),
    status: PropTypes.string,
    totalRating: PropTypes.number,
    totalReview: PropTypes.number
  })
};
export default function ProductDetailsSummary({
  cart,
  product,
  onAddCart,
  onGotoStep,
  ...other
}) {
  const theme = useTheme();
  const navigate = useNavigate();
  const {
    id,
    name,
    sizes,
    price,
    cover,
    status,
    colors,
    available,
    priceSale,
    totalRating,
    totalReview,
    inventoryType
  } = product;
  const alreadyProduct = cart.map(item => item.id).includes(id);
  const isMaxQuantity = cart.filter(item => item.id === id).map(item => item.quantity)[0] >= available;
  const defaultValues = {
    id,
    name,
    cover,
    available,
    price,
    color: colors[0],
    size: sizes[4],
    quantity: available < 1 ? 0 : 1
  };
  const methods = useForm({
    defaultValues
  });
  const {
    watch,
    control,
    setValue,
    handleSubmit
  } = methods;
  const values = watch();

  const onSubmit = async data => {
    try {
      if (!alreadyProduct) {
        onAddCart({ ...data,
          subtotal: data.price * data.quantity
        });
      }

      onGotoStep(0);
      navigate(PATH_DASHBOARD.eCommerce.checkout);
    } catch (error) {
      console.error(error);
    }
  };

  const handleAddCart = async () => {
    try {
      onAddCart({ ...values,
        subtotal: values.price * values.quantity
      });
    } catch (error) {
      console.error(error);
    }
  };

  return /*#__PURE__*/React.createElement(RootStyle, other, /*#__PURE__*/React.createElement(FormProvider, {
    methods: methods,
    onSubmit: handleSubmit(onSubmit)
  }, /*#__PURE__*/React.createElement(Label, {
    variant: theme.palette.mode === 'light' ? 'ghost' : 'filled',
    color: inventoryType === 'in_stock' ? 'success' : 'error',
    sx: {
      textTransform: 'uppercase'
    }
  }, sentenceCase(inventoryType || '')), /*#__PURE__*/React.createElement(Typography, {
    variant: "overline",
    sx: {
      mt: 2,
      mb: 1,
      display: 'block',
      color: status === 'sale' ? 'error.main' : 'info.main'
    }
  }, status), /*#__PURE__*/React.createElement(Typography, {
    variant: "h5",
    paragraph: true
  }, name), /*#__PURE__*/React.createElement(Stack, {
    direction: "row",
    alignItems: "center",
    spacing: 1,
    sx: {
      mb: 2
    }
  }, /*#__PURE__*/React.createElement(Rating, {
    value: totalRating,
    precision: 0.1,
    readOnly: true
  }), /*#__PURE__*/React.createElement(Typography, {
    variant: "body2",
    sx: {
      color: 'text.secondary'
    }
  }, "(", fShortenNumber(totalReview), "reviews)")), /*#__PURE__*/React.createElement(Typography, {
    variant: "h4",
    sx: {
      mb: 3
    }
  }, /*#__PURE__*/React.createElement(Box, {
    component: "span",
    sx: {
      color: 'text.disabled',
      textDecoration: 'line-through'
    }
  }, priceSale && fCurrency(priceSale)), "\xA0", fCurrency(price)), /*#__PURE__*/React.createElement(Divider, {
    sx: {
      borderStyle: 'dashed'
    }
  }), /*#__PURE__*/React.createElement(Stack, {
    direction: "row",
    alignItems: "center",
    justifyContent: "space-between",
    sx: {
      my: 3
    }
  }, /*#__PURE__*/React.createElement(Typography, {
    variant: "subtitle1",
    sx: {
      mt: 0.5
    }
  }, "Color"), /*#__PURE__*/React.createElement(Controller, {
    name: "color",
    control: control,
    render: ({
      field
    }) => /*#__PURE__*/React.createElement(ColorSinglePicker, {
      colors: colors,
      value: field.value,
      onChange: field.onChange,
      sx: { ...(colors.length > 4 && {
          maxWidth: 144,
          justifyContent: 'flex-end'
        })
      }
    })
  })), /*#__PURE__*/React.createElement(Stack, {
    direction: "row",
    justifyContent: "space-between",
    sx: {
      mb: 3
    }
  }, /*#__PURE__*/React.createElement(Typography, {
    variant: "subtitle1",
    sx: {
      mt: 0.5
    }
  }, "Size"), /*#__PURE__*/React.createElement(RHFSelect, {
    name: "size",
    size: "small",
    fullWidth: false,
    FormHelperTextProps: {
      sx: {
        textAlign: 'right',
        margin: 0,
        mt: 1
      }
    },
    helperText: /*#__PURE__*/React.createElement(Link, {
      underline: "always",
      color: "text.secondary"
    }, "Size Chart")
  }, sizes.map(size => /*#__PURE__*/React.createElement("option", {
    key: size,
    value: size
  }, size)))), /*#__PURE__*/React.createElement(Stack, {
    direction: "row",
    justifyContent: "space-between",
    sx: {
      mb: 3
    }
  }, /*#__PURE__*/React.createElement(Typography, {
    variant: "subtitle1",
    sx: {
      mt: 0.5
    }
  }, "Quantity"), /*#__PURE__*/React.createElement("div", null, /*#__PURE__*/React.createElement(Incrementer, {
    name: "quantity",
    quantity: values.quantity,
    available: available,
    onIncrementQuantity: () => setValue('quantity', values.quantity + 1),
    onDecrementQuantity: () => setValue('quantity', values.quantity - 1)
  }), /*#__PURE__*/React.createElement(Typography, {
    variant: "caption",
    component: "div",
    sx: {
      mt: 1,
      textAlign: 'right',
      color: 'text.secondary'
    }
  }, "Available: ", available))), /*#__PURE__*/React.createElement(Divider, {
    sx: {
      borderStyle: 'dashed'
    }
  }), /*#__PURE__*/React.createElement(Stack, {
    direction: "row",
    spacing: 2,
    sx: {
      mt: 5
    }
  }, /*#__PURE__*/React.createElement(Button, {
    fullWidth: true,
    disabled: isMaxQuantity,
    size: "large",
    color: "warning",
    variant: "contained",
    startIcon: /*#__PURE__*/React.createElement(Iconify, {
      icon: 'ic:round-add-shopping-cart'
    }),
    onClick: handleAddCart,
    sx: {
      whiteSpace: 'nowrap'
    }
  }, "Add to Cart"), /*#__PURE__*/React.createElement(Button, {
    fullWidth: true,
    size: "large",
    type: "submit",
    variant: "contained"
  }, "Buy Now")), /*#__PURE__*/React.createElement(Stack, {
    alignItems: "center",
    sx: {
      mt: 3
    }
  }, /*#__PURE__*/React.createElement(SocialsButton, {
    initialColor: true
  }))));
} // ----------------------------------------------------------------------

Incrementer.propTypes = {
  available: PropTypes.number,
  quantity: PropTypes.number,
  onIncrementQuantity: PropTypes.func,
  onDecrementQuantity: PropTypes.func
};

function Incrementer({
  available,
  quantity,
  onIncrementQuantity,
  onDecrementQuantity
}) {
  return /*#__PURE__*/React.createElement(Box, {
    sx: {
      py: 0.5,
      px: 0.75,
      border: 1,
      lineHeight: 0,
      borderRadius: 1,
      display: 'flex',
      alignItems: 'center',
      borderColor: 'grey.50032'
    }
  }, /*#__PURE__*/React.createElement(IconButton, {
    size: "small",
    color: "inherit",
    disabled: quantity <= 1,
    onClick: onDecrementQuantity
  }, /*#__PURE__*/React.createElement(Iconify, {
    icon: 'eva:minus-fill',
    width: 14,
    height: 14
  })), /*#__PURE__*/React.createElement(Typography, {
    variant: "body2",
    component: "span",
    sx: {
      width: 40,
      textAlign: 'center'
    }
  }, quantity), /*#__PURE__*/React.createElement(IconButton, {
    size: "small",
    color: "inherit",
    disabled: quantity >= available,
    onClick: onIncrementQuantity
  }, /*#__PURE__*/React.createElement(Iconify, {
    icon: 'eva:plus-fill',
    width: 14,
    height: 14
  })));
}