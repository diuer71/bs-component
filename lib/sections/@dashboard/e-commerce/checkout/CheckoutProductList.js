import PropTypes from 'prop-types'; // @mui

import { styled } from '@mui/material/styles';
import { Box, Table, Divider, TableRow, TableBody, TableCell, TableHead, Typography, IconButton, TableContainer } from '@mui/material'; // utils

import getColorName from '../../../../utils/getColorName';
import { fCurrency } from '../../../../utils/formatNumber'; // components

import Image from '../../../../components/Image';
import Iconify from '../../../../components/Iconify'; // ----------------------------------------------------------------------

const IncrementerStyle = styled('div')(({
  theme
}) => ({
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'space-between',
  marginBottom: theme.spacing(0.5),
  padding: theme.spacing(0.5, 0.75),
  borderRadius: theme.shape.borderRadius,
  border: `solid 1px ${theme.palette.grey[500_32]}`
})); // ----------------------------------------------------------------------

CheckoutProductList.propTypes = {
  products: PropTypes.array.isRequired,
  onDelete: PropTypes.func,
  onDecreaseQuantity: PropTypes.func,
  onIncreaseQuantity: PropTypes.func
};
export default function CheckoutProductList({
  products,
  onDelete,
  onIncreaseQuantity,
  onDecreaseQuantity
}) {
  return /*#__PURE__*/React.createElement(TableContainer, {
    sx: {
      minWidth: 720
    }
  }, /*#__PURE__*/React.createElement(Table, null, /*#__PURE__*/React.createElement(TableHead, null, /*#__PURE__*/React.createElement(TableRow, null, /*#__PURE__*/React.createElement(TableCell, null, "Product"), /*#__PURE__*/React.createElement(TableCell, {
    align: "left"
  }, "Price"), /*#__PURE__*/React.createElement(TableCell, {
    align: "left"
  }, "Quantity"), /*#__PURE__*/React.createElement(TableCell, {
    align: "right"
  }, "Total Price"), /*#__PURE__*/React.createElement(TableCell, {
    align: "right"
  }))), /*#__PURE__*/React.createElement(TableBody, null, products.map(product => {
    const {
      id,
      name,
      size,
      price,
      color,
      cover,
      quantity,
      available
    } = product;
    return /*#__PURE__*/React.createElement(TableRow, {
      key: id
    }, /*#__PURE__*/React.createElement(TableCell, null, /*#__PURE__*/React.createElement(Box, {
      sx: {
        display: 'flex',
        alignItems: 'center'
      }
    }, /*#__PURE__*/React.createElement(Image, {
      alt: "product image",
      src: cover,
      sx: {
        width: 64,
        height: 64,
        borderRadius: 1.5,
        mr: 2
      }
    }), /*#__PURE__*/React.createElement(Box, null, /*#__PURE__*/React.createElement(Typography, {
      noWrap: true,
      variant: "subtitle2",
      sx: {
        maxWidth: 240
      }
    }, name), /*#__PURE__*/React.createElement(Box, {
      sx: {
        display: 'flex',
        alignItems: 'center'
      }
    }, /*#__PURE__*/React.createElement(Typography, {
      variant: "body2"
    }, /*#__PURE__*/React.createElement(Typography, {
      component: "span",
      variant: "body2",
      sx: {
        color: 'text.secondary'
      }
    }, "size:\xA0"), size), /*#__PURE__*/React.createElement(Divider, {
      orientation: "vertical",
      sx: {
        mx: 1,
        height: 16
      }
    }), /*#__PURE__*/React.createElement(Typography, {
      variant: "body2"
    }, /*#__PURE__*/React.createElement(Typography, {
      component: "span",
      variant: "body2",
      sx: {
        color: 'text.secondary'
      }
    }, "color:\xA0"), getColorName(color)))))), /*#__PURE__*/React.createElement(TableCell, {
      align: "left"
    }, fCurrency(price)), /*#__PURE__*/React.createElement(TableCell, {
      align: "left"
    }, /*#__PURE__*/React.createElement(Incrementer, {
      quantity: quantity,
      available: available,
      onDecrease: () => onDecreaseQuantity(id),
      onIncrease: () => onIncreaseQuantity(id)
    })), /*#__PURE__*/React.createElement(TableCell, {
      align: "right"
    }, fCurrency(price * quantity)), /*#__PURE__*/React.createElement(TableCell, {
      align: "right"
    }, /*#__PURE__*/React.createElement(IconButton, {
      onClick: () => onDelete(id)
    }, /*#__PURE__*/React.createElement(Iconify, {
      icon: 'eva:trash-2-outline',
      width: 20,
      height: 20
    }))));
  }))));
} // ----------------------------------------------------------------------

Incrementer.propTypes = {
  available: PropTypes.number,
  quantity: PropTypes.number,
  onIncrease: PropTypes.func,
  onDecrease: PropTypes.func
};

function Incrementer({
  available,
  quantity,
  onIncrease,
  onDecrease
}) {
  return /*#__PURE__*/React.createElement(Box, {
    sx: {
      width: 96,
      textAlign: 'right'
    }
  }, /*#__PURE__*/React.createElement(IncrementerStyle, null, /*#__PURE__*/React.createElement(IconButton, {
    size: "small",
    color: "inherit",
    onClick: onDecrease,
    disabled: quantity <= 1
  }, /*#__PURE__*/React.createElement(Iconify, {
    icon: 'eva:minus-fill',
    width: 16,
    height: 16
  })), quantity, /*#__PURE__*/React.createElement(IconButton, {
    size: "small",
    color: "inherit",
    onClick: onIncrease,
    disabled: quantity >= available
  }, /*#__PURE__*/React.createElement(Iconify, {
    icon: 'eva:plus-fill',
    width: 16,
    height: 16
  }))), /*#__PURE__*/React.createElement(Typography, {
    variant: "caption",
    sx: {
      color: 'text.secondary'
    }
  }, "available: ", available));
}