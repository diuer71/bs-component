import PropTypes from 'prop-types'; // @mui

import { Card, Button, Typography, CardHeader, CardContent } from '@mui/material'; // redux

import { useSelector } from '../../../../redux/store'; // components

import Iconify from '../../../../components/Iconify'; // ----------------------------------------------------------------------

CheckoutBillingInfo.propTypes = {
  onBackStep: PropTypes.func
};
export default function CheckoutBillingInfo({
  onBackStep
}) {
  const {
    checkout
  } = useSelector(state => state.product);
  const {
    billing
  } = checkout;
  return /*#__PURE__*/React.createElement(Card, {
    sx: {
      mb: 3
    }
  }, /*#__PURE__*/React.createElement(CardHeader, {
    title: "Billing Address",
    action: /*#__PURE__*/React.createElement(Button, {
      size: "small",
      startIcon: /*#__PURE__*/React.createElement(Iconify, {
        icon: 'eva:edit-fill'
      }),
      onClick: onBackStep
    }, "Edit")
  }), /*#__PURE__*/React.createElement(CardContent, null, /*#__PURE__*/React.createElement(Typography, {
    variant: "subtitle2",
    gutterBottom: true
  }, billing?.receiver, "\xA0", /*#__PURE__*/React.createElement(Typography, {
    component: "span",
    variant: "body2",
    sx: {
      color: 'text.secondary'
    }
  }, "(", billing?.addressType, ")")), /*#__PURE__*/React.createElement(Typography, {
    variant: "body2",
    gutterBottom: true
  }, billing?.fullAddress), /*#__PURE__*/React.createElement(Typography, {
    variant: "body2",
    sx: {
      color: 'text.secondary'
    }
  }, billing?.phone)));
}