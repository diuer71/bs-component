function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

import PropTypes from 'prop-types'; // form

import { Controller, useFormContext } from 'react-hook-form'; // @mui

import { styled } from '@mui/material/styles';
import { Box, Card, Radio, Stack, Typography, RadioGroup, CardHeader, CardContent, FormControlLabel } from '@mui/material'; // components

import Iconify from '../../../../components/Iconify'; // ----------------------------------------------------------------------

const OptionStyle = styled('div')(({
  theme
}) => ({
  width: '100%',
  display: 'flex',
  alignItems: 'center',
  padding: theme.spacing(0, 2.5),
  justifyContent: 'space-between',
  transition: theme.transitions.create('all'),
  border: `solid 1px ${theme.palette.divider}`,
  borderRadius: Number(theme.shape.borderRadius) * 1.5
})); // ----------------------------------------------------------------------

CheckoutDelivery.propTypes = {
  deliveryOptions: PropTypes.array,
  onApplyShipping: PropTypes.func
};
export default function CheckoutDelivery({
  deliveryOptions,
  onApplyShipping
}) {
  const {
    control
  } = useFormContext();
  return /*#__PURE__*/React.createElement(Card, null, /*#__PURE__*/React.createElement(CardHeader, {
    title: "Delivery options"
  }), /*#__PURE__*/React.createElement(CardContent, null, /*#__PURE__*/React.createElement(Controller, {
    name: "delivery",
    control: control,
    render: ({
      field
    }) => /*#__PURE__*/React.createElement(RadioGroup, _extends({}, field, {
      onChange: event => {
        const {
          value
        } = event.target;
        field.onChange(Number(value));
        onApplyShipping(Number(value));
      }
    }), /*#__PURE__*/React.createElement(Stack, {
      spacing: 2,
      alignItems: "center",
      direction: {
        xs: 'column',
        md: 'row'
      }
    }, deliveryOptions.map(delivery => {
      const selected = field.value === delivery.value;
      return /*#__PURE__*/React.createElement(OptionStyle, {
        key: delivery.value,
        sx: { ...(selected && {
            boxShadow: theme => theme.customShadows.z20
          })
        }
      }, /*#__PURE__*/React.createElement(FormControlLabel, {
        value: delivery.value,
        control: /*#__PURE__*/React.createElement(Radio, {
          checkedIcon: /*#__PURE__*/React.createElement(Iconify, {
            icon: 'eva:checkmark-circle-2-fill'
          })
        }),
        label: /*#__PURE__*/React.createElement(Box, {
          sx: {
            ml: 1
          }
        }, /*#__PURE__*/React.createElement(Typography, {
          variant: "subtitle2"
        }, delivery.title), /*#__PURE__*/React.createElement(Typography, {
          variant: "body2",
          sx: {
            color: 'text.secondary'
          }
        }, delivery.description)),
        sx: {
          py: 3,
          flexGrow: 1,
          mr: 0
        }
      }));
    })))
  })));
}