import PropTypes from 'prop-types'; // @mui

import { Box, Card, Stack, Button, Divider, TextField, CardHeader, Typography, CardContent, InputAdornment } from '@mui/material'; // utils

import { fCurrency } from '../../../../utils/formatNumber'; // components

import Iconify from '../../../../components/Iconify'; // ----------------------------------------------------------------------

CheckoutSummary.propTypes = {
  total: PropTypes.number,
  discount: PropTypes.number,
  subtotal: PropTypes.number,
  shipping: PropTypes.number,
  onEdit: PropTypes.func,
  enableEdit: PropTypes.bool,
  onApplyDiscount: PropTypes.func,
  enableDiscount: PropTypes.bool
};
export default function CheckoutSummary({
  total,
  onEdit,
  discount,
  subtotal,
  shipping,
  onApplyDiscount,
  enableEdit = false,
  enableDiscount = false
}) {
  const displayShipping = shipping !== null ? 'Free' : '-';
  return /*#__PURE__*/React.createElement(Card, {
    sx: {
      mb: 3
    }
  }, /*#__PURE__*/React.createElement(CardHeader, {
    title: "Order Summary",
    action: enableEdit && /*#__PURE__*/React.createElement(Button, {
      size: "small",
      onClick: onEdit,
      startIcon: /*#__PURE__*/React.createElement(Iconify, {
        icon: 'eva:edit-fill'
      })
    }, "Edit")
  }), /*#__PURE__*/React.createElement(CardContent, null, /*#__PURE__*/React.createElement(Stack, {
    spacing: 2
  }, /*#__PURE__*/React.createElement(Stack, {
    direction: "row",
    justifyContent: "space-between"
  }, /*#__PURE__*/React.createElement(Typography, {
    variant: "body2",
    sx: {
      color: 'text.secondary'
    }
  }, "Sub Total"), /*#__PURE__*/React.createElement(Typography, {
    variant: "subtitle2"
  }, fCurrency(subtotal))), /*#__PURE__*/React.createElement(Stack, {
    direction: "row",
    justifyContent: "space-between"
  }, /*#__PURE__*/React.createElement(Typography, {
    variant: "body2",
    sx: {
      color: 'text.secondary'
    }
  }, "Discount"), /*#__PURE__*/React.createElement(Typography, {
    variant: "subtitle2"
  }, discount ? fCurrency(-discount) : '-')), /*#__PURE__*/React.createElement(Stack, {
    direction: "row",
    justifyContent: "space-between"
  }, /*#__PURE__*/React.createElement(Typography, {
    variant: "body2",
    sx: {
      color: 'text.secondary'
    }
  }, "Shipping"), /*#__PURE__*/React.createElement(Typography, {
    variant: "subtitle2"
  }, shipping ? fCurrency(shipping) : displayShipping)), /*#__PURE__*/React.createElement(Divider, null), /*#__PURE__*/React.createElement(Stack, {
    direction: "row",
    justifyContent: "space-between"
  }, /*#__PURE__*/React.createElement(Typography, {
    variant: "subtitle1"
  }, "Total"), /*#__PURE__*/React.createElement(Box, {
    sx: {
      textAlign: 'right'
    }
  }, /*#__PURE__*/React.createElement(Typography, {
    variant: "subtitle1",
    sx: {
      color: 'error.main'
    }
  }, fCurrency(total)), /*#__PURE__*/React.createElement(Typography, {
    variant: "caption",
    sx: {
      fontStyle: 'italic'
    }
  }, "(VAT included if applicable)"))), enableDiscount && onApplyDiscount && /*#__PURE__*/React.createElement(TextField, {
    fullWidth: true,
    placeholder: "Discount codes / Gifts",
    value: "DISCOUNT5",
    InputProps: {
      endAdornment: /*#__PURE__*/React.createElement(InputAdornment, {
        position: "end"
      }, /*#__PURE__*/React.createElement(Button, {
        onClick: () => onApplyDiscount(5),
        sx: {
          mr: -0.5
        }
      }, "Apply"))
    }
  }))));
}