function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

import PropTypes from 'prop-types'; // form

import { Controller, useFormContext } from 'react-hook-form'; // @mui

import { styled } from '@mui/material/styles';
import { Box, Card, Radio, Stack, Button, Collapse, TextField, Typography, RadioGroup, CardHeader, CardContent, FormHelperText, FormControlLabel } from '@mui/material'; // hooks

import useResponsive from '../../../../hooks/useResponsive'; // components

import Image from '../../../../components/Image';
import Iconify from '../../../../components/Iconify'; // ----------------------------------------------------------------------

const OptionStyle = styled('div')(({
  theme
}) => ({
  display: 'flex',
  alignItems: 'center',
  padding: theme.spacing(0, 2.5),
  justifyContent: 'space-between',
  transition: theme.transitions.create('all'),
  border: `solid 1px ${theme.palette.divider}`,
  borderRadius: Number(theme.shape.borderRadius) * 1.5
})); // ----------------------------------------------------------------------

CheckoutPaymentMethods.propTypes = {
  paymentOptions: PropTypes.array,
  cardOptions: PropTypes.array
};
export default function CheckoutPaymentMethods({
  paymentOptions,
  cardOptions
}) {
  const {
    control
  } = useFormContext();
  const isDesktop = useResponsive('up', 'sm');
  return /*#__PURE__*/React.createElement(Card, {
    sx: {
      my: 3
    }
  }, /*#__PURE__*/React.createElement(CardHeader, {
    title: "Payment options"
  }), /*#__PURE__*/React.createElement(CardContent, null, /*#__PURE__*/React.createElement(Controller, {
    name: "payment",
    control: control,
    render: ({
      field,
      fieldState: {
        error
      }
    }) => /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement(RadioGroup, _extends({
      row: true
    }, field), /*#__PURE__*/React.createElement(Stack, {
      spacing: 2
    }, paymentOptions.map(method => {
      const {
        value,
        title,
        icons,
        description
      } = method;
      const hasChildren = value === 'credit_card';
      const selected = field.value === value;
      return /*#__PURE__*/React.createElement(OptionStyle, {
        key: title,
        sx: { ...(selected && {
            boxShadow: theme => theme.customShadows.z20
          }),
          ...(hasChildren && {
            flexWrap: 'wrap'
          })
        }
      }, /*#__PURE__*/React.createElement(FormControlLabel, {
        value: value,
        control: /*#__PURE__*/React.createElement(Radio, {
          checkedIcon: /*#__PURE__*/React.createElement(Iconify, {
            icon: 'eva:checkmark-circle-2-fill'
          })
        }),
        label: /*#__PURE__*/React.createElement(Box, {
          sx: {
            ml: 1
          }
        }, /*#__PURE__*/React.createElement(Typography, {
          variant: "subtitle2"
        }, title), /*#__PURE__*/React.createElement(Typography, {
          variant: "body2",
          sx: {
            color: 'text.secondary'
          }
        }, description)),
        sx: {
          flexGrow: 1,
          py: 3
        }
      }), isDesktop && /*#__PURE__*/React.createElement(Stack, {
        direction: "row",
        spacing: 1,
        flexShrink: 0
      }, icons.map(icon => /*#__PURE__*/React.createElement(Image, {
        key: icon,
        alt: "logo card",
        src: icon
      }))), hasChildren && /*#__PURE__*/React.createElement(Collapse, {
        in: field.value === 'credit_card',
        sx: {
          width: 1
        }
      }, /*#__PURE__*/React.createElement(TextField, {
        select: true,
        fullWidth: true,
        label: "Cards",
        SelectProps: {
          native: true
        }
      }, cardOptions.map(option => /*#__PURE__*/React.createElement("option", {
        key: option.value,
        value: option.value
      }, option.label))), /*#__PURE__*/React.createElement(Button, {
        size: "small",
        startIcon: /*#__PURE__*/React.createElement(Iconify, {
          icon: 'eva:plus-fill',
          width: 20,
          height: 20
        }),
        sx: {
          my: 3
        }
      }, "Add new card")));
    }))), !!error && /*#__PURE__*/React.createElement(FormHelperText, {
      error: true,
      sx: {
        pt: 1,
        px: 2
      }
    }, error.message))
  })));
}