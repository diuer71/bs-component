import PropTypes from 'prop-types';
import { useState } from 'react'; // @mui

import { Box, Grid, Card, Button, Typography } from '@mui/material'; // redux

import { useDispatch, useSelector } from '../../../../redux/store';
import { onBackStep, onNextStep, createBilling } from '../../../../redux/slices/product'; // _mock_

import { _addressBooks } from '../../../../_mock'; // components

import Label from '../../../../components/Label';
import Iconify from '../../../../components/Iconify'; //

import CheckoutSummary from './CheckoutSummary';
import CheckoutNewAddressForm from './CheckoutNewAddressForm'; // ----------------------------------------------------------------------

export default function CheckoutBillingAddress() {
  //
  const dispatch = useDispatch();
  const {
    checkout
  } = useSelector(state => state.product);
  const {
    total,
    discount,
    subtotal
  } = checkout; //

  const [open, setOpen] = useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleNextStep = () => {
    dispatch(onNextStep());
  };

  const handleBackStep = () => {
    dispatch(onBackStep());
  };

  const handleCreateBilling = value => {
    dispatch(createBilling(value));
  };

  return /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement(Grid, {
    container: true,
    spacing: 3
  }, /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: 12,
    md: 8
  }, _addressBooks.map((address, index) => /*#__PURE__*/React.createElement(AddressItem, {
    key: index,
    address: address,
    onNextStep: handleNextStep,
    onCreateBilling: handleCreateBilling
  })), /*#__PURE__*/React.createElement(Box, {
    sx: {
      display: 'flex',
      justifyContent: 'space-between'
    }
  }, /*#__PURE__*/React.createElement(Button, {
    size: "small",
    color: "inherit",
    onClick: handleBackStep,
    startIcon: /*#__PURE__*/React.createElement(Iconify, {
      icon: 'eva:arrow-ios-back-fill'
    })
  }, "Back"), /*#__PURE__*/React.createElement(Button, {
    size: "small",
    onClick: handleClickOpen,
    startIcon: /*#__PURE__*/React.createElement(Iconify, {
      icon: 'eva:plus-fill'
    })
  }, "Add new address"))), /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: 12,
    md: 4
  }, /*#__PURE__*/React.createElement(CheckoutSummary, {
    subtotal: subtotal,
    total: total,
    discount: discount
  }))), /*#__PURE__*/React.createElement(CheckoutNewAddressForm, {
    open: open,
    onClose: handleClose,
    onNextStep: handleNextStep,
    onCreateBilling: handleCreateBilling
  }));
} // ----------------------------------------------------------------------

AddressItem.propTypes = {
  address: PropTypes.object,
  onNextStep: PropTypes.func,
  onCreateBilling: PropTypes.func
};

function AddressItem({
  address,
  onNextStep,
  onCreateBilling
}) {
  const {
    receiver,
    fullAddress,
    addressType,
    phone,
    isDefault
  } = address;

  const handleCreateBilling = () => {
    onCreateBilling(address);
    onNextStep();
  };

  return /*#__PURE__*/React.createElement(Card, {
    sx: {
      p: 3,
      mb: 3,
      position: 'relative'
    }
  }, /*#__PURE__*/React.createElement(Box, {
    sx: {
      mb: 1,
      display: 'flex',
      alignItems: 'center'
    }
  }, /*#__PURE__*/React.createElement(Typography, {
    variant: "subtitle1"
  }, receiver), /*#__PURE__*/React.createElement(Typography, {
    variant: "body2",
    sx: {
      color: 'text.secondary'
    }
  }, "\xA0(", addressType, ")"), isDefault && /*#__PURE__*/React.createElement(Label, {
    color: "info",
    sx: {
      ml: 1
    }
  }, "Default")), /*#__PURE__*/React.createElement(Typography, {
    variant: "body2",
    gutterBottom: true
  }, fullAddress), /*#__PURE__*/React.createElement(Typography, {
    variant: "body2",
    sx: {
      color: 'text.secondary'
    }
  }, phone), /*#__PURE__*/React.createElement(Box, {
    sx: {
      mt: 3,
      display: 'flex',
      position: {
        sm: 'absolute'
      },
      right: {
        sm: 24
      },
      bottom: {
        sm: 24
      }
    }
  }, !isDefault && /*#__PURE__*/React.createElement(Button, {
    variant: "outlined",
    size: "small",
    color: "inherit"
  }, "Delete"), /*#__PURE__*/React.createElement(Box, {
    sx: {
      mx: 0.5
    }
  }), /*#__PURE__*/React.createElement(Button, {
    variant: "outlined",
    size: "small",
    onClick: handleCreateBilling
  }, "Deliver to this Address")));
}