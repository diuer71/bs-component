import PropTypes from 'prop-types';
import * as Yup from 'yup'; // form

import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup'; // @mui

import { Box, Stack, Dialog, Button, Divider, DialogTitle, DialogContent, DialogActions } from '@mui/material';
import { LoadingButton } from '@mui/lab'; // _mock

import { countries } from '../../../../_mock';
import { FormProvider, RHFCheckbox, RHFSelect, RHFTextField, RHFRadioGroup } from '../../../../components/hook-form'; // ----------------------------------------------------------------------

CheckoutNewAddressForm.propTypes = {
  open: PropTypes.bool,
  onClose: PropTypes.func,
  onNextStep: PropTypes.func,
  onCreateBilling: PropTypes.func
};
export default function CheckoutNewAddressForm({
  open,
  onClose,
  onNextStep,
  onCreateBilling
}) {
  const NewAddressSchema = Yup.object().shape({
    receiver: Yup.string().required('Fullname is required'),
    phone: Yup.string().required('Phone is required'),
    address: Yup.string().required('Address is required'),
    city: Yup.string().required('City is required'),
    state: Yup.string().required('State is required')
  });
  const defaultValues = {
    addressType: 'Home',
    receiver: '',
    phone: '',
    address: '',
    city: '',
    state: '',
    country: countries[0].label,
    zipcode: '',
    isDefault: true
  };
  const methods = useForm({
    resolver: yupResolver(NewAddressSchema),
    defaultValues
  });
  const {
    handleSubmit,
    formState: {
      isSubmitting
    }
  } = methods;

  const onSubmit = async data => {
    try {
      onNextStep();
      onCreateBilling({
        receiver: data.receiver,
        phone: data.phone,
        fullAddress: `${data.address}, ${data.city}, ${data.state}, ${data.country}, ${data.zipcode}`,
        addressType: data.addressType,
        isDefault: data.isDefault
      });
    } catch (error) {
      console.error(error);
    }
  };

  return /*#__PURE__*/React.createElement(Dialog, {
    fullWidth: true,
    maxWidth: "sm",
    open: open,
    onClose: onClose
  }, /*#__PURE__*/React.createElement(DialogTitle, null, "Add new address"), /*#__PURE__*/React.createElement(FormProvider, {
    methods: methods,
    onSubmit: handleSubmit(onSubmit)
  }, /*#__PURE__*/React.createElement(DialogContent, null, /*#__PURE__*/React.createElement(Stack, {
    spacing: 3
  }, /*#__PURE__*/React.createElement(RHFRadioGroup, {
    name: "addressType",
    options: ['Home', 'Office']
  }), /*#__PURE__*/React.createElement(Box, {
    sx: {
      display: 'grid',
      rowGap: 3,
      columnGap: 2,
      gridTemplateColumns: {
        xs: 'repeat(1, 1fr)',
        sm: 'repeat(2, 1fr)'
      }
    }
  }, /*#__PURE__*/React.createElement(RHFTextField, {
    name: "receiver",
    label: "Full Name"
  }), /*#__PURE__*/React.createElement(RHFTextField, {
    name: "phone",
    label: "Phone Number"
  })), /*#__PURE__*/React.createElement(RHFTextField, {
    name: "address",
    label: "Address"
  }), /*#__PURE__*/React.createElement(Box, {
    sx: {
      display: 'grid',
      rowGap: 3,
      columnGap: 2,
      gridTemplateColumns: {
        xs: 'repeat(1, 1fr)',
        sm: 'repeat(3, 1fr)'
      }
    }
  }, /*#__PURE__*/React.createElement(RHFTextField, {
    name: "city",
    label: "Town / City"
  }), /*#__PURE__*/React.createElement(RHFTextField, {
    name: "state",
    label: "State"
  }), /*#__PURE__*/React.createElement(RHFTextField, {
    name: "zipcode",
    label: "Zip / Postal Code"
  })), /*#__PURE__*/React.createElement(RHFSelect, {
    name: "country",
    label: "Country"
  }, countries.map(option => /*#__PURE__*/React.createElement("option", {
    key: option.code,
    value: option.label
  }, option.label))), /*#__PURE__*/React.createElement(RHFCheckbox, {
    name: "isDefault",
    label: "Use this address as default.",
    sx: {
      mt: 3
    }
  }))), /*#__PURE__*/React.createElement(Divider, null), /*#__PURE__*/React.createElement(DialogActions, null, /*#__PURE__*/React.createElement(LoadingButton, {
    type: "submit",
    variant: "contained",
    loading: isSubmitting
  }, "Deliver to this Address"), /*#__PURE__*/React.createElement(Button, {
    color: "inherit",
    variant: "outlined",
    onClick: onClose
  }, "Cancel"))));
}