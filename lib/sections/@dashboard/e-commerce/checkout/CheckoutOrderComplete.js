function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

import { useNavigate } from 'react-router-dom'; // @mui

import { styled } from '@mui/material/styles';
import { Box, Link, Button, Divider, Typography, Stack } from '@mui/material'; // redux

import { useDispatch } from '../../../../redux/store';
import { resetCart } from '../../../../redux/slices/product'; // routes

import { PATH_DASHBOARD } from '../../../../routes/paths'; // components

import Iconify from '../../../../components/Iconify';
import { DialogAnimate } from '../../../../components/animate'; // assets

import { OrderCompleteIllustration } from '../../../../assets'; // ----------------------------------------------------------------------

const DialogStyle = styled(DialogAnimate)(({
  theme
}) => ({
  '& .MuiDialog-paper': {
    margin: 0,
    [theme.breakpoints.up('md')]: {
      maxWidth: 'calc(100% - 48px)',
      maxHeight: 'calc(100% - 48px)'
    }
  }
})); // ----------------------------------------------------------------------

export default function CheckoutOrderComplete({ ...other
}) {
  const navigate = useNavigate();
  const dispatch = useDispatch();

  const handleResetStep = () => {
    dispatch(resetCart());
    navigate(PATH_DASHBOARD.eCommerce.shop);
  };

  return /*#__PURE__*/React.createElement(DialogStyle, _extends({
    fullScreen: true
  }, other), /*#__PURE__*/React.createElement(Box, {
    sx: {
      p: 4,
      maxWidth: 480,
      margin: 'auto'
    }
  }, /*#__PURE__*/React.createElement(Box, {
    sx: {
      textAlign: 'center'
    }
  }, /*#__PURE__*/React.createElement(Typography, {
    variant: "h4",
    paragraph: true
  }, "Thank you for your purchase!"), /*#__PURE__*/React.createElement(OrderCompleteIllustration, {
    sx: {
      height: 260,
      my: 10
    }
  }), /*#__PURE__*/React.createElement(Typography, {
    align: "left",
    paragraph: true
  }, "Thanks for placing order \xA0", /*#__PURE__*/React.createElement(Link, {
    href: "#"
  }, "01dc1370-3df6-11eb-b378-0242ac130002")), /*#__PURE__*/React.createElement(Typography, {
    align: "left",
    sx: {
      color: 'text.secondary'
    }
  }, "We will send you a notification within 5 days when it ships.", /*#__PURE__*/React.createElement("br", null), " ", /*#__PURE__*/React.createElement("br", null), " If you have any question or queries then fell to get in contact us. ", /*#__PURE__*/React.createElement("br", null), " ", /*#__PURE__*/React.createElement("br", null), " All the best,")), /*#__PURE__*/React.createElement(Divider, {
    sx: {
      my: 3
    }
  }), /*#__PURE__*/React.createElement(Stack, {
    direction: {
      xs: 'column-reverse',
      sm: 'row'
    },
    justifyContent: "space-between",
    spacing: 2
  }, /*#__PURE__*/React.createElement(Button, {
    color: "inherit",
    onClick: handleResetStep,
    startIcon: /*#__PURE__*/React.createElement(Iconify, {
      icon: 'eva:arrow-ios-back-fill'
    })
  }, "Continue Shopping"), /*#__PURE__*/React.createElement(Button, {
    variant: "contained",
    startIcon: /*#__PURE__*/React.createElement(Iconify, {
      icon: 'ant-design:file-pdf-filled'
    }),
    onClick: handleResetStep
  }, "Download as PDF"))));
}