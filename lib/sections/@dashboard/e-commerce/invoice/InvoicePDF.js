import PropTypes from 'prop-types';
import sum from 'lodash/sum';
import { Page, View, Text, Image, Document } from '@react-pdf/renderer'; // utils

import { fCurrency } from '../../../../utils/formatNumber'; //

import styles from './InvoiceStyle'; // ----------------------------------------------------------------------

InvoicePDF.propTypes = {
  invoice: PropTypes.object.isRequired
};
export default function InvoicePDF({
  invoice
}) {
  const {
    id,
    items,
    taxes,
    status,
    discount,
    invoiceTo,
    invoiceFrom
  } = invoice;
  const subTotal = sum(items.map(item => item.price * item.qty));
  const total = subTotal - discount + taxes;
  return /*#__PURE__*/React.createElement(Document, null, /*#__PURE__*/React.createElement(Page, {
    size: "A4",
    style: styles.page
  }, /*#__PURE__*/React.createElement(View, {
    style: [styles.gridContainer, styles.mb40]
  }, /*#__PURE__*/React.createElement(Image, {
    source: "/logo/logo_full.jpg",
    style: {
      height: 32
    }
  }), /*#__PURE__*/React.createElement(View, {
    style: {
      alignItems: 'flex-end',
      flexDirection: 'column'
    }
  }, /*#__PURE__*/React.createElement(Text, {
    style: styles.h3
  }, status), /*#__PURE__*/React.createElement(Text, null, "INV-", id))), /*#__PURE__*/React.createElement(View, {
    style: [styles.gridContainer, styles.mb40]
  }, /*#__PURE__*/React.createElement(View, {
    style: styles.col6
  }, /*#__PURE__*/React.createElement(Text, {
    style: [styles.overline, styles.mb8]
  }, "Invoice from"), /*#__PURE__*/React.createElement(Text, {
    style: styles.body1
  }, invoiceFrom.name), /*#__PURE__*/React.createElement(Text, {
    style: styles.body1
  }, invoiceFrom.address), /*#__PURE__*/React.createElement(Text, {
    style: styles.body1
  }, invoiceFrom.phone)), /*#__PURE__*/React.createElement(View, {
    style: styles.col6
  }, /*#__PURE__*/React.createElement(Text, {
    style: [styles.overline, styles.mb8]
  }, "Invoice to"), /*#__PURE__*/React.createElement(Text, {
    style: styles.body1
  }, invoiceTo.name), /*#__PURE__*/React.createElement(Text, {
    style: styles.body1
  }, invoiceTo.address), /*#__PURE__*/React.createElement(Text, {
    style: styles.body1
  }, invoiceTo.phone))), /*#__PURE__*/React.createElement(Text, {
    style: [styles.overline, styles.mb8]
  }, "Invoice Details"), /*#__PURE__*/React.createElement(View, {
    style: styles.table
  }, /*#__PURE__*/React.createElement(View, {
    style: styles.tableHeader
  }, /*#__PURE__*/React.createElement(View, {
    style: styles.tableRow
  }, /*#__PURE__*/React.createElement(View, {
    style: styles.tableCell_1
  }, /*#__PURE__*/React.createElement(Text, {
    style: styles.subtitle2
  }, "#")), /*#__PURE__*/React.createElement(View, {
    style: styles.tableCell_2
  }, /*#__PURE__*/React.createElement(Text, {
    style: styles.subtitle2
  }, "Description")), /*#__PURE__*/React.createElement(View, {
    style: styles.tableCell_3
  }, /*#__PURE__*/React.createElement(Text, {
    style: styles.subtitle2
  }, "Qty")), /*#__PURE__*/React.createElement(View, {
    style: styles.tableCell_3
  }, /*#__PURE__*/React.createElement(Text, {
    style: styles.subtitle2
  }, "Unit price")), /*#__PURE__*/React.createElement(View, {
    style: [styles.tableCell_3, styles.alignRight]
  }, /*#__PURE__*/React.createElement(Text, {
    style: styles.subtitle2
  }, "Total")))), /*#__PURE__*/React.createElement(View, {
    style: styles.tableBody
  }, items.map((item, index) => /*#__PURE__*/React.createElement(View, {
    style: styles.tableRow,
    key: item.id
  }, /*#__PURE__*/React.createElement(View, {
    style: styles.tableCell_1
  }, /*#__PURE__*/React.createElement(Text, null, index + 1)), /*#__PURE__*/React.createElement(View, {
    style: styles.tableCell_2
  }, /*#__PURE__*/React.createElement(Text, {
    style: styles.subtitle2
  }, item.title), /*#__PURE__*/React.createElement(Text, null, item.description)), /*#__PURE__*/React.createElement(View, {
    style: styles.tableCell_3
  }, /*#__PURE__*/React.createElement(Text, null, item.qty)), /*#__PURE__*/React.createElement(View, {
    style: styles.tableCell_3
  }, /*#__PURE__*/React.createElement(Text, null, item.price)), /*#__PURE__*/React.createElement(View, {
    style: [styles.tableCell_3, styles.alignRight]
  }, /*#__PURE__*/React.createElement(Text, null, fCurrency(item.price * item.qty))))), /*#__PURE__*/React.createElement(View, {
    style: [styles.tableRow, styles.noBorder]
  }, /*#__PURE__*/React.createElement(View, {
    style: styles.tableCell_1
  }), /*#__PURE__*/React.createElement(View, {
    style: styles.tableCell_2
  }), /*#__PURE__*/React.createElement(View, {
    style: styles.tableCell_3
  }), /*#__PURE__*/React.createElement(View, {
    style: styles.tableCell_3
  }, /*#__PURE__*/React.createElement(Text, null, "Subtotal")), /*#__PURE__*/React.createElement(View, {
    style: [styles.tableCell_3, styles.alignRight]
  }, /*#__PURE__*/React.createElement(Text, null, fCurrency(subTotal)))), /*#__PURE__*/React.createElement(View, {
    style: [styles.tableRow, styles.noBorder]
  }, /*#__PURE__*/React.createElement(View, {
    style: styles.tableCell_1
  }), /*#__PURE__*/React.createElement(View, {
    style: styles.tableCell_2
  }), /*#__PURE__*/React.createElement(View, {
    style: styles.tableCell_3
  }), /*#__PURE__*/React.createElement(View, {
    style: styles.tableCell_3
  }, /*#__PURE__*/React.createElement(Text, null, "Discount")), /*#__PURE__*/React.createElement(View, {
    style: [styles.tableCell_3, styles.alignRight]
  }, /*#__PURE__*/React.createElement(Text, null, fCurrency(-discount)))), /*#__PURE__*/React.createElement(View, {
    style: [styles.tableRow, styles.noBorder]
  }, /*#__PURE__*/React.createElement(View, {
    style: styles.tableCell_1
  }), /*#__PURE__*/React.createElement(View, {
    style: styles.tableCell_2
  }), /*#__PURE__*/React.createElement(View, {
    style: styles.tableCell_3
  }), /*#__PURE__*/React.createElement(View, {
    style: styles.tableCell_3
  }, /*#__PURE__*/React.createElement(Text, null, "Taxes")), /*#__PURE__*/React.createElement(View, {
    style: [styles.tableCell_3, styles.alignRight]
  }, /*#__PURE__*/React.createElement(Text, null, fCurrency(taxes)))), /*#__PURE__*/React.createElement(View, {
    style: [styles.tableRow, styles.noBorder]
  }, /*#__PURE__*/React.createElement(View, {
    style: styles.tableCell_1
  }), /*#__PURE__*/React.createElement(View, {
    style: styles.tableCell_2
  }), /*#__PURE__*/React.createElement(View, {
    style: styles.tableCell_3
  }), /*#__PURE__*/React.createElement(View, {
    style: styles.tableCell_3
  }, /*#__PURE__*/React.createElement(Text, {
    style: styles.h4
  }, "Total")), /*#__PURE__*/React.createElement(View, {
    style: [styles.tableCell_3, styles.alignRight]
  }, /*#__PURE__*/React.createElement(Text, {
    style: styles.h4
  }, fCurrency(total)))))), /*#__PURE__*/React.createElement(View, {
    style: [styles.gridContainer, styles.footer]
  }, /*#__PURE__*/React.createElement(View, {
    style: styles.col8
  }, /*#__PURE__*/React.createElement(Text, {
    style: styles.subtitle2
  }, "NOTES"), /*#__PURE__*/React.createElement(Text, null, "We appreciate your business. Should you need us to add VAT or extra notes let us know!")), /*#__PURE__*/React.createElement(View, {
    style: [styles.col4, styles.alignRight]
  }, /*#__PURE__*/React.createElement(Text, {
    style: styles.subtitle2
  }, "Have a Question?"), /*#__PURE__*/React.createElement(Text, null, "support@abcapp.com")))));
}