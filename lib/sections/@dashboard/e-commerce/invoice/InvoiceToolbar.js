import PropTypes from 'prop-types';
import { useState } from 'react';
import { PDFDownloadLink, PDFViewer } from '@react-pdf/renderer'; // @mui

import { styled } from '@mui/material/styles';
import { Box, Tooltip, IconButton, DialogActions, Button } from '@mui/material';
import { LoadingButton } from '@mui/lab'; // components

import Iconify from '../../../../components/Iconify';
import { DialogAnimate } from '../../../../components/animate'; //

import InvoicePDF from './InvoicePDF'; // ----------------------------------------------------------------------

const RootStyle = styled('div')(({
  theme
}) => ({
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'flex-end',
  marginBottom: theme.spacing(5)
})); // ----------------------------------------------------------------------

InvoiceToolbar.propTypes = {
  invoice: PropTypes.object.isRequired
};
export default function InvoiceToolbar({
  invoice,
  ...other
}) {
  const [openPDF, setOpenPDF] = useState(false);

  const handleOpenPreview = () => {
    setOpenPDF(true);
  };

  const handleClosePreview = () => {
    setOpenPDF(false);
  };

  return /*#__PURE__*/React.createElement(RootStyle, other, /*#__PURE__*/React.createElement(Button, {
    color: "error",
    size: "small",
    variant: "contained",
    endIcon: /*#__PURE__*/React.createElement(Iconify, {
      icon: 'eva:share-fill'
    })
  }, "Share"), /*#__PURE__*/React.createElement(Button, {
    color: "info",
    size: "small",
    variant: "contained",
    onClick: handleOpenPreview,
    endIcon: /*#__PURE__*/React.createElement(Iconify, {
      icon: 'eva:eye-fill'
    }),
    sx: {
      mx: 1
    }
  }, "Preview"), /*#__PURE__*/React.createElement(PDFDownloadLink, {
    document: /*#__PURE__*/React.createElement(InvoicePDF, {
      invoice: invoice
    }),
    fileName: `INVOICE-${invoice.id}`,
    style: {
      textDecoration: 'none'
    }
  }, ({
    loading
  }) => /*#__PURE__*/React.createElement(LoadingButton, {
    size: "small",
    loading: loading,
    variant: "contained",
    loadingPosition: "end",
    endIcon: /*#__PURE__*/React.createElement(Iconify, {
      icon: 'eva:download-fill'
    })
  }, "Download")), /*#__PURE__*/React.createElement(DialogAnimate, {
    fullScreen: true,
    open: openPDF
  }, /*#__PURE__*/React.createElement(Box, {
    sx: {
      height: '100%',
      display: 'flex',
      flexDirection: 'column'
    }
  }, /*#__PURE__*/React.createElement(DialogActions, {
    sx: {
      zIndex: 9,
      padding: '12px !important',
      boxShadow: theme => theme.customShadows.z8
    }
  }, /*#__PURE__*/React.createElement(Tooltip, {
    title: "Close"
  }, /*#__PURE__*/React.createElement(IconButton, {
    color: "inherit",
    onClick: handleClosePreview
  }, /*#__PURE__*/React.createElement(Iconify, {
    icon: 'eva:close-fill'
  })))), /*#__PURE__*/React.createElement(Box, {
    sx: {
      flexGrow: 1,
      height: '100%',
      overflow: 'hidden'
    }
  }, /*#__PURE__*/React.createElement(PDFViewer, {
    width: "100%",
    height: "100%",
    style: {
      border: 'none'
    }
  }, /*#__PURE__*/React.createElement(InvoicePDF, {
    invoice: invoice
  }))))));
}