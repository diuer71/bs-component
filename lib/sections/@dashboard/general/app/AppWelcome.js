import PropTypes from 'prop-types';
import { Link as RouterLink } from 'react-router-dom'; // @mui

import { styled } from '@mui/material/styles';
import { Typography, Button, Card, CardContent } from '@mui/material';
import { SeoIllustration } from '../../../../assets'; // ----------------------------------------------------------------------

const RootStyle = styled(Card)(({
  theme
}) => ({
  boxShadow: 'none',
  textAlign: 'center',
  backgroundColor: theme.palette.primary.lighter,
  [theme.breakpoints.up('md')]: {
    height: '100%',
    display: 'flex',
    textAlign: 'left',
    alignItems: 'center',
    justifyContent: 'space-between'
  }
})); // ----------------------------------------------------------------------

AppWelcome.propTypes = {
  displayName: PropTypes.string
};
export default function AppWelcome({
  displayName
}) {
  return /*#__PURE__*/React.createElement(RootStyle, null, /*#__PURE__*/React.createElement(CardContent, {
    sx: {
      p: {
        md: 0
      },
      pl: {
        md: 5
      },
      color: 'grey.800'
    }
  }, /*#__PURE__*/React.createElement(Typography, {
    gutterBottom: true,
    variant: "h4"
  }, "Welcome back,", /*#__PURE__*/React.createElement("br", null), " ", !displayName ? '...' : displayName, "!"), /*#__PURE__*/React.createElement(Typography, {
    variant: "body2",
    sx: {
      pb: {
        xs: 3,
        xl: 5
      },
      maxWidth: 480,
      mx: 'auto'
    }
  }, "If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything"), /*#__PURE__*/React.createElement(Button, {
    variant: "contained",
    to: "#",
    component: RouterLink
  }, "Go Now")), /*#__PURE__*/React.createElement(SeoIllustration, {
    sx: {
      p: 3,
      width: 360,
      margin: {
        xs: 'auto',
        md: 'inherit'
      }
    }
  }));
}