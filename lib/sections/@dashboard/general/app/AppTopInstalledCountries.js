function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

import PropTypes from 'prop-types'; // @mui

import { styled } from '@mui/material/styles';
import { Card, CardHeader, Typography, Stack } from '@mui/material'; // utils

import { fShortenNumber } from '../../../../utils/formatNumber'; // _mock_

import { _appInstalled } from '../../../../_mock'; // components

import Image from '../../../../components/Image';
import Iconify from '../../../../components/Iconify';
import Scrollbar from '../../../../components/Scrollbar'; // ----------------------------------------------------------------------

const ItemBlockStyle = styled(props => /*#__PURE__*/React.createElement(Stack, _extends({
  direction: "row",
  alignItems: "center"
}, props)))({
  minWidth: 72,
  flex: '1 1'
});
const ItemIconStyle = styled(Iconify)(({
  theme
}) => ({
  width: 16,
  height: 16,
  marginRight: theme.spacing(0.5),
  color: theme.palette.text.disabled
})); // ----------------------------------------------------------------------

export default function AppTopInstalledCountries() {
  return /*#__PURE__*/React.createElement(Card, null, /*#__PURE__*/React.createElement(CardHeader, {
    title: "Top Installed Countries"
  }), /*#__PURE__*/React.createElement(Scrollbar, null, /*#__PURE__*/React.createElement(Stack, {
    spacing: 3,
    sx: {
      p: 3
    }
  }, _appInstalled.map(country => /*#__PURE__*/React.createElement(CountryItem, {
    key: country.id,
    country: country
  })))));
} // ----------------------------------------------------------------------

CountryItem.propTypes = {
  country: PropTypes.shape({
    android: PropTypes.number,
    flag: PropTypes.string,
    name: PropTypes.string,
    windows: PropTypes.number
  })
};

function CountryItem({
  country
}) {
  return /*#__PURE__*/React.createElement(Stack, {
    direction: "row",
    alignItems: "center",
    spacing: 2
  }, /*#__PURE__*/React.createElement(ItemBlockStyle, {
    sx: {
      minWidth: 120
    }
  }, /*#__PURE__*/React.createElement(Image, {
    disabledEffect: true,
    alt: country.name,
    src: country.flag,
    sx: {
      width: 28,
      mr: 1
    }
  }), /*#__PURE__*/React.createElement(Typography, {
    variant: "subtitle2"
  }, country.name)), /*#__PURE__*/React.createElement(ItemBlockStyle, null, /*#__PURE__*/React.createElement(ItemIconStyle, {
    icon: 'ant-design:android-filled'
  }), /*#__PURE__*/React.createElement(Typography, {
    variant: "body2"
  }, fShortenNumber(country.android))), /*#__PURE__*/React.createElement(ItemBlockStyle, null, /*#__PURE__*/React.createElement(ItemIconStyle, {
    icon: 'ant-design:windows-filled'
  }), /*#__PURE__*/React.createElement(Typography, {
    variant: "body2"
  }, fShortenNumber(country.windows))), /*#__PURE__*/React.createElement(ItemBlockStyle, {
    sx: {
      minWidth: 88
    }
  }, /*#__PURE__*/React.createElement(ItemIconStyle, {
    icon: 'ant-design:apple-filled'
  }), /*#__PURE__*/React.createElement(Typography, {
    variant: "body2"
  }, fShortenNumber(country.windows))));
}