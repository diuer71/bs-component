import { useState } from 'react';
import { sentenceCase } from 'change-case'; // @mui

import { useTheme } from '@mui/material/styles';
import { Box, Card, Table, Button, Divider, MenuItem, TableRow, TableBody, TableCell, TableHead, CardHeader, IconButton, TableContainer } from '@mui/material'; // utils

import { fCurrency } from '../../../../utils/formatNumber'; // _mock_

import { _appInvoices } from '../../../../_mock'; // components

import Label from '../../../../components/Label';
import Iconify from '../../../../components/Iconify';
import Scrollbar from '../../../../components/Scrollbar';
import MenuPopover from '../../../../components/MenuPopover'; // ----------------------------------------------------------------------

export default function AppNewInvoice() {
  const theme = useTheme();
  return /*#__PURE__*/React.createElement(Card, null, /*#__PURE__*/React.createElement(CardHeader, {
    title: "New Invoice",
    sx: {
      mb: 3
    }
  }), /*#__PURE__*/React.createElement(Scrollbar, null, /*#__PURE__*/React.createElement(TableContainer, {
    sx: {
      minWidth: 720
    }
  }, /*#__PURE__*/React.createElement(Table, null, /*#__PURE__*/React.createElement(TableHead, null, /*#__PURE__*/React.createElement(TableRow, null, /*#__PURE__*/React.createElement(TableCell, null, "Invoice ID"), /*#__PURE__*/React.createElement(TableCell, null, "Category"), /*#__PURE__*/React.createElement(TableCell, null, "Price"), /*#__PURE__*/React.createElement(TableCell, null, "Status"), /*#__PURE__*/React.createElement(TableCell, null))), /*#__PURE__*/React.createElement(TableBody, null, _appInvoices.map(row => /*#__PURE__*/React.createElement(TableRow, {
    key: row.id
  }, /*#__PURE__*/React.createElement(TableCell, null, `INV-${row.id}`), /*#__PURE__*/React.createElement(TableCell, null, row.category), /*#__PURE__*/React.createElement(TableCell, null, fCurrency(row.price)), /*#__PURE__*/React.createElement(TableCell, null, /*#__PURE__*/React.createElement(Label, {
    variant: theme.palette.mode === 'light' ? 'ghost' : 'filled',
    color: row.status === 'in_progress' && 'warning' || row.status === 'out_of_date' && 'error' || 'success'
  }, sentenceCase(row.status))), /*#__PURE__*/React.createElement(TableCell, {
    align: "right"
  }, /*#__PURE__*/React.createElement(MoreMenuButton, null)))))))), /*#__PURE__*/React.createElement(Divider, null), /*#__PURE__*/React.createElement(Box, {
    sx: {
      p: 2,
      textAlign: 'right'
    }
  }, /*#__PURE__*/React.createElement(Button, {
    size: "small",
    color: "inherit",
    endIcon: /*#__PURE__*/React.createElement(Iconify, {
      icon: 'eva:arrow-ios-forward-fill'
    })
  }, "View All")));
} // ----------------------------------------------------------------------

function MoreMenuButton() {
  const [open, setOpen] = useState(null);

  const handleOpen = event => {
    setOpen(event.currentTarget);
  };

  const handleClose = () => {
    setOpen(null);
  };

  const ICON = {
    mr: 2,
    width: 20,
    height: 20
  };
  return /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement(IconButton, {
    size: "large",
    onClick: handleOpen
  }, /*#__PURE__*/React.createElement(Iconify, {
    icon: 'eva:more-vertical-fill',
    width: 20,
    height: 20
  })), /*#__PURE__*/React.createElement(MenuPopover, {
    open: Boolean(open),
    anchorEl: open,
    onClose: handleClose,
    anchorOrigin: {
      vertical: 'top',
      horizontal: 'left'
    },
    transformOrigin: {
      vertical: 'top',
      horizontal: 'right'
    },
    arrow: "right-top",
    sx: {
      mt: -0.5,
      width: 160,
      '& .MuiMenuItem-root': {
        px: 1,
        typography: 'body2',
        borderRadius: 0.75
      }
    }
  }, /*#__PURE__*/React.createElement(MenuItem, null, /*#__PURE__*/React.createElement(Iconify, {
    icon: 'eva:download-fill',
    sx: { ...ICON
    }
  }), "Download"), /*#__PURE__*/React.createElement(MenuItem, null, /*#__PURE__*/React.createElement(Iconify, {
    icon: 'eva:printer-fill',
    sx: { ...ICON
    }
  }), "Print"), /*#__PURE__*/React.createElement(MenuItem, null, /*#__PURE__*/React.createElement(Iconify, {
    icon: 'eva:share-fill',
    sx: { ...ICON
    }
  }), "Share"), /*#__PURE__*/React.createElement(Divider, {
    sx: {
      borderStyle: 'dashed'
    }
  }), /*#__PURE__*/React.createElement(MenuItem, {
    sx: {
      color: 'error.main'
    }
  }, /*#__PURE__*/React.createElement(Iconify, {
    icon: 'eva:trash-2-outline',
    sx: { ...ICON
    }
  }), "Delete")));
}