function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

import PropTypes from 'prop-types';
import Slider from 'react-slick';
import { m } from 'framer-motion';
import { useState, useRef } from 'react';
import { Link as RouterLink } from 'react-router-dom'; // @mui

import { alpha, useTheme, styled } from '@mui/material/styles';
import { CardContent, Box, Card, Typography, Link } from '@mui/material'; // _mock_

import { _appFeatured } from '../../../../_mock'; // components

import Image from '../../../../components/Image';
import { MotionContainer, varFade } from '../../../../components/animate';
import { CarouselDots, CarouselArrows } from '../../../../components/carousel'; // ----------------------------------------------------------------------

const OverlayStyle = styled('div')(({
  theme
}) => ({
  top: 0,
  left: 0,
  right: 0,
  bottom: 0,
  zIndex: 8,
  position: 'absolute',
  backgroundColor: alpha(theme.palette.grey[900], 0.64)
})); // ----------------------------------------------------------------------

export default function AppFeatured() {
  const theme = useTheme();
  const carouselRef = useRef(null);
  const [currentIndex, setCurrentIndex] = useState(theme.direction === 'rtl' ? _appFeatured.length - 1 : 0);
  const settings = {
    speed: 800,
    dots: true,
    arrows: false,
    autoplay: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    rtl: Boolean(theme.direction === 'rtl'),
    beforeChange: (current, next) => setCurrentIndex(next),
    ...CarouselDots({
      zIndex: 9,
      top: 24,
      left: 24,
      position: 'absolute'
    })
  };

  const handlePrevious = () => {
    carouselRef.current.slickPrev();
  };

  const handleNext = () => {
    carouselRef.current.slickNext();
  };

  return /*#__PURE__*/React.createElement(Card, null, /*#__PURE__*/React.createElement(Slider, _extends({
    ref: carouselRef
  }, settings), _appFeatured.map((app, index) => /*#__PURE__*/React.createElement(CarouselItem, {
    key: app.id,
    item: app,
    isActive: index === currentIndex
  }))), /*#__PURE__*/React.createElement(CarouselArrows, {
    onNext: handleNext,
    onPrevious: handlePrevious,
    spacing: 0,
    sx: {
      top: 16,
      right: 16,
      position: 'absolute',
      '& .arrow': {
        p: 0,
        width: 32,
        height: 32,
        opacity: 0.48,
        color: 'common.white',
        '&:hover': {
          color: 'common.white',
          opacity: 1
        }
      }
    }
  }));
} // ----------------------------------------------------------------------

CarouselItem.propTypes = {
  isActive: PropTypes.bool,
  item: PropTypes.shape({
    description: PropTypes.string,
    image: PropTypes.string,
    title: PropTypes.string
  })
};

function CarouselItem({
  item,
  isActive
}) {
  const {
    image,
    title,
    description
  } = item;
  return /*#__PURE__*/React.createElement(Box, {
    sx: {
      position: 'relative'
    }
  }, /*#__PURE__*/React.createElement(CardContent, {
    component: MotionContainer,
    animate: isActive,
    action: true,
    sx: {
      bottom: 0,
      width: 1,
      zIndex: 9,
      textAlign: 'left',
      position: 'absolute',
      color: 'common.white'
    }
  }, /*#__PURE__*/React.createElement(m.div, {
    variants: varFade().inRight
  }, /*#__PURE__*/React.createElement(Typography, {
    variant: "overline",
    component: "div",
    sx: {
      mb: 1,
      opacity: 0.48
    }
  }, "Featured App")), /*#__PURE__*/React.createElement(m.div, {
    variants: varFade().inRight
  }, /*#__PURE__*/React.createElement(Link, {
    component: RouterLink,
    to: "#",
    color: "inherit",
    underline: "none"
  }, /*#__PURE__*/React.createElement(Typography, {
    variant: "h5",
    gutterBottom: true,
    noWrap: true
  }, title))), /*#__PURE__*/React.createElement(m.div, {
    variants: varFade().inRight
  }, /*#__PURE__*/React.createElement(Typography, {
    variant: "body2",
    noWrap: true
  }, description))), /*#__PURE__*/React.createElement(OverlayStyle, null), /*#__PURE__*/React.createElement(Image, {
    alt: title,
    src: image,
    sx: {
      height: {
        xs: 280,
        xl: 320
      }
    }
  }));
}