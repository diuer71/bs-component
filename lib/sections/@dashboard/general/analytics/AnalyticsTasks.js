import PropTypes from 'prop-types';
import { useState } from 'react'; // form

import { useForm, Controller } from 'react-hook-form'; // @mui

import { Card, Stack, Divider, Checkbox, MenuItem, IconButton, CardHeader, FormControlLabel } from '@mui/material'; // components

import Iconify from '../../../../components/Iconify';
import MenuPopover from '../../../../components/MenuPopover'; // ----------------------------------------------------------------------

const TASKS = ['Create FireStone Logo', 'Add SCSS and JS files if required', 'Stakeholder Meeting', 'Scoping & Estimations', 'Sprint Showcase']; // ----------------------------------------------------------------------

export default function AnalyticsTasks() {
  const {
    control
  } = useForm({
    defaultValues: {
      taskCompleted: ['Stakeholder Meeting']
    }
  });
  return /*#__PURE__*/React.createElement(Card, null, /*#__PURE__*/React.createElement(CardHeader, {
    title: "Tasks"
  }), /*#__PURE__*/React.createElement(Controller, {
    name: "taskCompleted",
    control: control,
    render: ({
      field
    }) => {
      const onSelected = task => field.value.includes(task) ? field.value.filter(value => value !== task) : [...field.value, task];

      return /*#__PURE__*/React.createElement(React.Fragment, null, TASKS.map(task => /*#__PURE__*/React.createElement(TaskItem, {
        key: task,
        task: task,
        checked: field.value.includes(task),
        onChange: () => field.onChange(onSelected(task))
      })));
    }
  }));
} // ----------------------------------------------------------------------

TaskItem.propTypes = {
  task: PropTypes.string,
  checked: PropTypes.bool,
  onChange: PropTypes.func
};

function TaskItem({
  task,
  checked,
  onChange
}) {
  return /*#__PURE__*/React.createElement(Stack, {
    direction: "row",
    sx: {
      px: 2,
      py: 0.75,
      ...(checked && {
        color: 'text.disabled',
        textDecoration: 'line-through'
      })
    }
  }, /*#__PURE__*/React.createElement(FormControlLabel, {
    control: /*#__PURE__*/React.createElement(Checkbox, {
      checked: checked,
      onChange: onChange
    }),
    label: task,
    sx: {
      flexGrow: 1,
      m: 0
    }
  }), /*#__PURE__*/React.createElement(MoreMenuButton, null));
} // ----------------------------------------------------------------------


function MoreMenuButton() {
  const [open, setOpen] = useState(null);

  const handleOpen = event => {
    setOpen(event.currentTarget);
  };

  const handleClose = () => {
    setOpen(null);
  };

  const ICON = {
    mr: 2,
    width: 20,
    height: 20
  };
  return /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement(IconButton, {
    size: "large",
    onClick: handleOpen
  }, /*#__PURE__*/React.createElement(Iconify, {
    icon: 'eva:more-vertical-fill',
    width: 20,
    height: 20
  })), /*#__PURE__*/React.createElement(MenuPopover, {
    open: Boolean(open),
    anchorEl: open,
    onClose: handleClose,
    anchorOrigin: {
      vertical: 'top',
      horizontal: 'left'
    },
    transformOrigin: {
      vertical: 'top',
      horizontal: 'right'
    },
    arrow: "right-top",
    sx: {
      mt: -0.5,
      width: 'auto',
      '& .MuiMenuItem-root': {
        px: 1,
        typography: 'body2',
        borderRadius: 0.75
      }
    }
  }, /*#__PURE__*/React.createElement(MenuItem, null, /*#__PURE__*/React.createElement(Iconify, {
    icon: 'eva:checkmark-circle-2-fill',
    sx: { ...ICON
    }
  }), "Mark Complete"), /*#__PURE__*/React.createElement(MenuItem, null, /*#__PURE__*/React.createElement(Iconify, {
    icon: 'eva:edit-fill',
    sx: { ...ICON
    }
  }), "Edit"), /*#__PURE__*/React.createElement(MenuItem, null, /*#__PURE__*/React.createElement(Iconify, {
    icon: 'eva:share-fill',
    sx: { ...ICON
    }
  }), "Share"), /*#__PURE__*/React.createElement(Divider, {
    sx: {
      borderStyle: 'dashed'
    }
  }), /*#__PURE__*/React.createElement(MenuItem, {
    sx: {
      color: 'error.main'
    }
  }, /*#__PURE__*/React.createElement(Iconify, {
    icon: 'eva:trash-2-outline',
    sx: { ...ICON
    }
  }), "Delete")));
}