import PropTypes from 'prop-types'; // @mui

import { Box, Grid, Card, Paper, Typography, CardHeader, CardContent } from '@mui/material'; // utils

import { fShortenNumber } from '../../../../utils/formatNumber'; // _mock_

import { _analyticTraffic } from '../../../../_mock'; // ----------------------------------------------------------------------

export default function AnalyticsTrafficBySite() {
  return /*#__PURE__*/React.createElement(Card, null, /*#__PURE__*/React.createElement(CardHeader, {
    title: "Traffic by Site"
  }), /*#__PURE__*/React.createElement(CardContent, null, /*#__PURE__*/React.createElement(Grid, {
    container: true,
    spacing: 2
  }, _analyticTraffic.map(site => /*#__PURE__*/React.createElement(SiteItem, {
    key: site.name,
    site: site
  })))));
} // ----------------------------------------------------------------------

SiteItem.propTypes = {
  site: PropTypes.shape({
    icon: PropTypes.any,
    name: PropTypes.string,
    value: PropTypes.number
  })
};

function SiteItem({
  site
}) {
  const {
    icon,
    value,
    name
  } = site;
  return /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: 6
  }, /*#__PURE__*/React.createElement(Paper, {
    variant: "outlined",
    sx: {
      py: 2.5,
      textAlign: 'center'
    }
  }, /*#__PURE__*/React.createElement(Box, {
    sx: {
      mb: 0.5
    }
  }, icon), /*#__PURE__*/React.createElement(Typography, {
    variant: "h6"
  }, fShortenNumber(value)), /*#__PURE__*/React.createElement(Typography, {
    variant: "body2",
    sx: {
      color: 'text.secondary'
    }
  }, name)));
}