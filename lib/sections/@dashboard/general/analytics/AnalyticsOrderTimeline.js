import PropTypes from 'prop-types'; // @mui

import { Card, Typography, CardHeader, CardContent } from '@mui/material';
import { Timeline, TimelineDot, TimelineItem, TimelineContent, TimelineSeparator, TimelineConnector } from '@mui/lab'; // utils

import { fDateTime } from '../../../../utils/formatTime'; // _mock_

import { _analyticOrderTimeline } from '../../../../_mock'; // ----------------------------------------------------------------------

export default function AnalyticsOrderTimeline() {
  return /*#__PURE__*/React.createElement(Card, {
    sx: {
      '& .MuiTimelineItem-missingOppositeContent:before': {
        display: 'none'
      }
    }
  }, /*#__PURE__*/React.createElement(CardHeader, {
    title: "Order Timeline"
  }), /*#__PURE__*/React.createElement(CardContent, null, /*#__PURE__*/React.createElement(Timeline, null, _analyticOrderTimeline.map((item, index) => /*#__PURE__*/React.createElement(OrderItem, {
    key: item.id,
    item: item,
    isLast: index === _analyticOrderTimeline.length - 1
  })))));
} // ----------------------------------------------------------------------

OrderItem.propTypes = {
  isLast: PropTypes.bool,
  item: PropTypes.shape({
    time: PropTypes.instanceOf(Date),
    title: PropTypes.string,
    type: PropTypes.string
  })
};

function OrderItem({
  item,
  isLast
}) {
  const {
    type,
    title,
    time
  } = item;
  return /*#__PURE__*/React.createElement(TimelineItem, null, /*#__PURE__*/React.createElement(TimelineSeparator, null, /*#__PURE__*/React.createElement(TimelineDot, {
    color: type === 'order1' && 'primary' || type === 'order2' && 'success' || type === 'order3' && 'info' || type === 'order4' && 'warning' || 'error'
  }), isLast ? null : /*#__PURE__*/React.createElement(TimelineConnector, null)), /*#__PURE__*/React.createElement(TimelineContent, null, /*#__PURE__*/React.createElement(Typography, {
    variant: "subtitle2"
  }, title), /*#__PURE__*/React.createElement(Typography, {
    variant: "caption",
    sx: {
      color: 'text.secondary'
    }
  }, fDateTime(time))));
}