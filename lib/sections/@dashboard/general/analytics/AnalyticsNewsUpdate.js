import PropTypes from 'prop-types';
import { Link as RouterLink } from 'react-router-dom'; // @mui

import { Box, Stack, Link, Card, Button, Divider, Typography, CardHeader } from '@mui/material'; // utils

import { fToNow } from '../../../../utils/formatTime'; // _mock_

import { _analyticPost } from '../../../../_mock'; // components

import Image from '../../../../components/Image';
import Iconify from '../../../../components/Iconify';
import Scrollbar from '../../../../components/Scrollbar'; // ----------------------------------------------------------------------

export default function AnalyticsNewsUpdate() {
  return /*#__PURE__*/React.createElement(Card, null, /*#__PURE__*/React.createElement(CardHeader, {
    title: "News Update"
  }), /*#__PURE__*/React.createElement(Scrollbar, null, /*#__PURE__*/React.createElement(Stack, {
    spacing: 3,
    sx: {
      p: 3,
      pr: 0
    }
  }, _analyticPost.map(news => /*#__PURE__*/React.createElement(NewsItem, {
    key: news.id,
    news: news
  })))), /*#__PURE__*/React.createElement(Divider, null), /*#__PURE__*/React.createElement(Box, {
    sx: {
      p: 2,
      textAlign: 'right'
    }
  }, /*#__PURE__*/React.createElement(Button, {
    to: "#",
    size: "small",
    color: "inherit",
    component: RouterLink,
    endIcon: /*#__PURE__*/React.createElement(Iconify, {
      icon: 'eva:arrow-ios-forward-fill'
    })
  }, "View all")));
} // ----------------------------------------------------------------------

NewsItem.propTypes = {
  news: PropTypes.shape({
    description: PropTypes.string,
    image: PropTypes.string,
    postedAt: PropTypes.instanceOf(Date),
    title: PropTypes.string
  })
};

function NewsItem({
  news
}) {
  const {
    image,
    title,
    description,
    postedAt
  } = news;
  return /*#__PURE__*/React.createElement(Stack, {
    direction: "row",
    alignItems: "center",
    spacing: 2
  }, /*#__PURE__*/React.createElement(Image, {
    alt: title,
    src: image,
    sx: {
      width: 48,
      height: 48,
      borderRadius: 1.5,
      flexShrink: 0
    }
  }), /*#__PURE__*/React.createElement(Box, {
    sx: {
      minWidth: 240
    }
  }, /*#__PURE__*/React.createElement(Link, {
    component: RouterLink,
    to: "#",
    color: "inherit"
  }, /*#__PURE__*/React.createElement(Typography, {
    variant: "subtitle2",
    noWrap: true
  }, title)), /*#__PURE__*/React.createElement(Typography, {
    variant: "body2",
    sx: {
      color: 'text.secondary'
    },
    noWrap: true
  }, description)), /*#__PURE__*/React.createElement(Typography, {
    variant: "caption",
    sx: {
      pr: 3,
      flexShrink: 0,
      color: 'text.secondary'
    }
  }, fToNow(postedAt)));
}