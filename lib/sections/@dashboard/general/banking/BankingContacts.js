// @mui
import { Box, Card, Stack, Button, Avatar, Tooltip, Typography, CardHeader, IconButton } from '@mui/material'; // _mock_

import { _bankingContacts } from '../../../../_mock'; // components

import Iconify from '../../../../components/Iconify'; // ----------------------------------------------------------------------

export default function BankingContacts() {
  return /*#__PURE__*/React.createElement(Card, null, /*#__PURE__*/React.createElement(CardHeader, {
    title: "Contacts",
    subheader: "You have 122 contacts",
    action: /*#__PURE__*/React.createElement(Tooltip, {
      title: "Add Contact"
    }, /*#__PURE__*/React.createElement(IconButton, {
      color: "primary",
      size: "large"
    }, /*#__PURE__*/React.createElement(Iconify, {
      icon: 'eva:plus-fill',
      width: 20,
      height: 20
    })))
  }), /*#__PURE__*/React.createElement(Stack, {
    spacing: 3,
    sx: {
      p: 3
    }
  }, _bankingContacts.map(contact => /*#__PURE__*/React.createElement(Stack, {
    direction: "row",
    alignItems: "center",
    key: contact.id
  }, /*#__PURE__*/React.createElement(Avatar, {
    src: contact.avatar,
    sx: {
      width: 48,
      height: 48
    }
  }), /*#__PURE__*/React.createElement(Box, {
    sx: {
      flexGrow: 1,
      ml: 2,
      minWidth: 100
    }
  }, /*#__PURE__*/React.createElement(Typography, {
    variant: "subtitle2",
    sx: {
      mb: 0.5
    },
    noWrap: true
  }, contact.name), /*#__PURE__*/React.createElement(Typography, {
    variant: "body2",
    sx: {
      color: 'text.secondary'
    },
    noWrap: true
  }, contact.email)), /*#__PURE__*/React.createElement(Tooltip, {
    title: "Quick Transfer"
  }, /*#__PURE__*/React.createElement(IconButton, {
    size: "small"
  }, /*#__PURE__*/React.createElement(Iconify, {
    icon: 'eva:flash-fill',
    width: 22,
    height: 22
  }))))), /*#__PURE__*/React.createElement(Button, {
    variant: "outlined",
    size: "large",
    color: "inherit"
  }, "View All")));
}