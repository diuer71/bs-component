import PropTypes from 'prop-types';
import { useState } from 'react';
import { format } from 'date-fns';
import { sentenceCase } from 'change-case'; // @mui

import { useTheme } from '@mui/material/styles';
import { Box, Card, Table, Avatar, Button, Divider, MenuItem, TableRow, TableBody, TableCell, TableHead, CardHeader, Typography, IconButton, TableContainer } from '@mui/material'; // utils

import { fCurrency } from '../../../../utils/formatNumber'; // _mock

import { _bankingRecentTransitions } from '../../../../_mock'; // components

import Label from '../../../../components/Label';
import Iconify from '../../../../components/Iconify';
import Scrollbar from '../../../../components/Scrollbar';
import MenuPopover from '../../../../components/MenuPopover'; // ----------------------------------------------------------------------

export default function BankingRecentTransitions() {
  const theme = useTheme();
  const isLight = theme.palette.mode === 'light';
  return /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement(Card, null, /*#__PURE__*/React.createElement(CardHeader, {
    title: "Recent Transitions",
    sx: {
      mb: 3
    }
  }), /*#__PURE__*/React.createElement(Scrollbar, null, /*#__PURE__*/React.createElement(TableContainer, {
    sx: {
      minWidth: 720
    }
  }, /*#__PURE__*/React.createElement(Table, null, /*#__PURE__*/React.createElement(TableHead, null, /*#__PURE__*/React.createElement(TableRow, null, /*#__PURE__*/React.createElement(TableCell, null, "Description"), /*#__PURE__*/React.createElement(TableCell, null, "Date"), /*#__PURE__*/React.createElement(TableCell, null, "Amount"), /*#__PURE__*/React.createElement(TableCell, null, "Status"), /*#__PURE__*/React.createElement(TableCell, null))), /*#__PURE__*/React.createElement(TableBody, null, _bankingRecentTransitions.map(row => /*#__PURE__*/React.createElement(TableRow, {
    key: row.id
  }, /*#__PURE__*/React.createElement(TableCell, null, /*#__PURE__*/React.createElement(Box, {
    sx: {
      display: 'flex',
      alignItems: 'center'
    }
  }, /*#__PURE__*/React.createElement(Box, {
    sx: {
      position: 'relative'
    }
  }, renderAvatar(row.category, row.avatar), /*#__PURE__*/React.createElement(Box, {
    sx: {
      right: 0,
      bottom: 0,
      width: 18,
      height: 18,
      display: 'flex',
      borderRadius: '50%',
      position: 'absolute',
      alignItems: 'center',
      color: 'common.white',
      bgcolor: 'error.main',
      justifyContent: 'center',
      ...(row.type === 'Income' && {
        bgcolor: 'success.main'
      })
    }
  }, /*#__PURE__*/React.createElement(Iconify, {
    icon: row.type === 'Income' ? 'eva:diagonal-arrow-left-down-fill' : 'eva:diagonal-arrow-right-up-fill',
    width: 16,
    height: 16
  }))), /*#__PURE__*/React.createElement(Box, {
    sx: {
      ml: 2
    }
  }, /*#__PURE__*/React.createElement(Typography, {
    variant: "body2",
    sx: {
      color: 'text.secondary'
    }
  }, row.message), /*#__PURE__*/React.createElement(Typography, {
    variant: "subtitle2"
  }, " ", row.category)))), /*#__PURE__*/React.createElement(TableCell, null, /*#__PURE__*/React.createElement(Typography, {
    variant: "subtitle2"
  }, format(new Date(row.date), 'dd MMM yyyy')), /*#__PURE__*/React.createElement(Typography, {
    variant: "body2",
    sx: {
      color: 'text.secondary'
    }
  }, format(new Date(row.date), 'p'))), /*#__PURE__*/React.createElement(TableCell, null, fCurrency(row.amount)), /*#__PURE__*/React.createElement(TableCell, null, /*#__PURE__*/React.createElement(Label, {
    variant: isLight ? 'ghost' : 'filled',
    color: row.status === 'completed' && 'success' || row.status === 'in_progress' && 'warning' || 'error'
  }, sentenceCase(row.status))), /*#__PURE__*/React.createElement(TableCell, {
    align: "right"
  }, /*#__PURE__*/React.createElement(MoreMenuButton, null)))))))), /*#__PURE__*/React.createElement(Divider, null), /*#__PURE__*/React.createElement(Box, {
    sx: {
      p: 2,
      textAlign: 'right'
    }
  }, /*#__PURE__*/React.createElement(Button, {
    size: "small",
    color: "inherit",
    endIcon: /*#__PURE__*/React.createElement(Iconify, {
      icon: 'eva:arrow-ios-forward-fill'
    })
  }, "View All"))));
} // ----------------------------------------------------------------------

AvatarIcon.propTypes = {
  icon: PropTypes.string.isRequired
};

function AvatarIcon({
  icon
}) {
  return /*#__PURE__*/React.createElement(Avatar, {
    sx: {
      width: 48,
      height: 48,
      color: 'text.secondary',
      bgcolor: 'background.neutral'
    }
  }, /*#__PURE__*/React.createElement(Iconify, {
    icon: icon,
    width: 24,
    height: 24
  }));
} // ----------------------------------------------------------------------


function renderAvatar(category, avatar) {
  if (category === 'Books') {
    return /*#__PURE__*/React.createElement(AvatarIcon, {
      icon: 'eva:book-fill'
    });
  }

  if (category === 'Beauty & Health') {
    return /*#__PURE__*/React.createElement(AvatarIcon, {
      icon: 'eva:heart-fill'
    });
  }

  return avatar ? /*#__PURE__*/React.createElement(Avatar, {
    alt: category,
    src: avatar,
    sx: {
      width: 48,
      height: 48,
      boxShadow: theme => theme.customShadows.z8
    }
  }) : null;
} // ----------------------------------------------------------------------


function MoreMenuButton() {
  const [open, setOpen] = useState(null);

  const handleOpen = event => {
    setOpen(event.currentTarget);
  };

  const handleClose = () => {
    setOpen(null);
  };

  const ICON = {
    mr: 2,
    width: 20,
    height: 20
  };
  return /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement(IconButton, {
    size: "large",
    onClick: handleOpen
  }, /*#__PURE__*/React.createElement(Iconify, {
    icon: 'eva:more-vertical-fill',
    width: 20,
    height: 20
  })), /*#__PURE__*/React.createElement(MenuPopover, {
    open: Boolean(open),
    anchorEl: open,
    onClose: handleClose,
    anchorOrigin: {
      vertical: 'top',
      horizontal: 'left'
    },
    transformOrigin: {
      vertical: 'top',
      horizontal: 'right'
    },
    arrow: "right-top",
    sx: {
      mt: -0.5,
      width: 160,
      '& .MuiMenuItem-root': {
        px: 1,
        typography: 'body2',
        borderRadius: 0.75
      }
    }
  }, /*#__PURE__*/React.createElement(MenuItem, null, /*#__PURE__*/React.createElement(Iconify, {
    icon: 'eva:download-fill',
    sx: { ...ICON
    }
  }), "Download"), /*#__PURE__*/React.createElement(MenuItem, null, /*#__PURE__*/React.createElement(Iconify, {
    icon: 'eva:printer-fill',
    sx: { ...ICON
    }
  }), "Print"), /*#__PURE__*/React.createElement(MenuItem, null, /*#__PURE__*/React.createElement(Iconify, {
    icon: 'eva:share-fill',
    sx: { ...ICON
    }
  }), "Share"), /*#__PURE__*/React.createElement(Divider, {
    sx: {
      borderStyle: 'dashed'
    }
  }), /*#__PURE__*/React.createElement(MenuItem, {
    sx: {
      color: 'error.main'
    }
  }, /*#__PURE__*/React.createElement(Iconify, {
    icon: 'eva:trash-2-outline',
    sx: { ...ICON
    }
  }), "Delete")));
}