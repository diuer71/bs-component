// @mui
import { Card, CardHeader, Typography, Stack, LinearProgress, Box } from '@mui/material'; // utils

import { fShortenNumber } from '../../../../utils/formatNumber'; // _mock_

import { _bookingsOverview } from '../../../../_mock'; // ----------------------------------------------------------------------

export default function BookingBookedRoom() {
  return /*#__PURE__*/React.createElement(Card, null, /*#__PURE__*/React.createElement(CardHeader, {
    title: "Booked Room"
  }), /*#__PURE__*/React.createElement(Stack, {
    spacing: 3,
    sx: {
      px: 3,
      my: 5
    }
  }, _bookingsOverview.map(progress => /*#__PURE__*/React.createElement(LinearProgress, {
    variant: "determinate",
    key: progress.status,
    value: progress.value,
    color: progress.status === 'Pending' && 'warning' || progress.status === 'Cancel' && 'error' || 'success',
    sx: {
      height: 8,
      bgcolor: 'grey.50016'
    }
  }))), /*#__PURE__*/React.createElement(Stack, {
    direction: "row",
    justifyContent: "space-between",
    sx: {
      px: 3,
      pb: 3
    }
  }, _bookingsOverview.map(progress => /*#__PURE__*/React.createElement(Stack, {
    key: progress.status,
    alignItems: "center"
  }, /*#__PURE__*/React.createElement(Stack, {
    direction: "row",
    alignItems: "center",
    spacing: 1,
    sx: {
      mb: 1
    }
  }, /*#__PURE__*/React.createElement(Box, {
    sx: {
      width: 12,
      height: 12,
      borderRadius: 0.5,
      bgcolor: 'success.main',
      ...(progress.status === 'Pending' && {
        bgcolor: 'warning.main'
      }),
      ...(progress.status === 'Cancel' && {
        bgcolor: 'error.main'
      })
    }
  }), /*#__PURE__*/React.createElement(Typography, {
    variant: "subtitle2",
    sx: {
      color: 'text.secondary'
    }
  }, progress.status)), /*#__PURE__*/React.createElement(Typography, {
    variant: "h6"
  }, fShortenNumber(progress.quantity))))));
}