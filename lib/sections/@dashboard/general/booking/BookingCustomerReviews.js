function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

import PropTypes from 'prop-types';
import Slider from 'react-slick';
import { useRef } from 'react'; // @mui

import { useTheme } from '@mui/material/styles';
import { Card, Chip, Stack, Avatar, Rating, Button, CardHeader, Typography } from '@mui/material'; // utils

import { fDateTime } from '../../../../utils/formatTime'; // _mock_

import { _bookingReview } from '../../../../_mock'; // components

import Iconify from '../../../../components/Iconify';
import { CarouselArrows } from '../../../../components/carousel'; // ----------------------------------------------------------------------

export default function BookingCustomerReviews() {
  const theme = useTheme();
  const carouselRef = useRef(null);
  const settings = {
    dots: false,
    arrows: false,
    slidesToShow: 1,
    slidesToScroll: 1,
    rtl: Boolean(theme.direction === 'rtl'),
    adaptiveHeight: true
  };

  const handlePrevious = () => {
    carouselRef.current?.slickPrev();
  };

  const handleNext = () => {
    carouselRef.current?.slickNext();
  };

  return /*#__PURE__*/React.createElement(Card, null, /*#__PURE__*/React.createElement(CardHeader, {
    title: "Customer Reviews",
    subheader: `${_bookingReview.length} Reviews`,
    action: /*#__PURE__*/React.createElement(CarouselArrows, {
      customIcon: 'ic:round-keyboard-arrow-right',
      onNext: handleNext,
      onPrevious: handlePrevious,
      sx: {
        '& .arrow': {
          width: 28,
          height: 28,
          p: 0
        }
      }
    }),
    sx: {
      '& .MuiCardHeader-action': {
        alignSelf: 'center'
      }
    }
  }), /*#__PURE__*/React.createElement(Slider, _extends({
    ref: carouselRef
  }, settings), _bookingReview.map(item => /*#__PURE__*/React.createElement(ReviewItem, {
    key: item.id,
    item: item
  }))));
} // ----------------------------------------------------------------------

ReviewItem.propTypes = {
  item: PropTypes.shape({
    avatar: PropTypes.string,
    description: PropTypes.string,
    name: PropTypes.string,
    postedAt: PropTypes.instanceOf(Date),
    rating: PropTypes.number,
    tags: PropTypes.array
  })
};

function ReviewItem({
  item
}) {
  const {
    avatar,
    name,
    description,
    rating,
    postedAt,
    tags
  } = item;
  return /*#__PURE__*/React.createElement(Stack, {
    spacing: 2,
    sx: {
      minHeight: 402,
      position: 'relative',
      p: 3
    }
  }, /*#__PURE__*/React.createElement(Stack, {
    direction: "row",
    alignItems: "center",
    spacing: 2
  }, /*#__PURE__*/React.createElement(Avatar, {
    alt: name,
    src: avatar
  }), /*#__PURE__*/React.createElement("div", null, /*#__PURE__*/React.createElement(Typography, {
    variant: "subtitle2"
  }, name), /*#__PURE__*/React.createElement(Typography, {
    variant: "caption",
    sx: {
      color: 'text.secondary',
      mt: 0.5,
      display: 'block'
    }
  }, "Posted ", fDateTime(postedAt)))), /*#__PURE__*/React.createElement(Rating, {
    value: rating,
    size: "small",
    readOnly: true,
    precision: 0.5
  }), /*#__PURE__*/React.createElement(Typography, {
    variant: "body2"
  }, description), /*#__PURE__*/React.createElement(Stack, {
    direction: "row",
    flexWrap: "wrap"
  }, tags.map(tag => /*#__PURE__*/React.createElement(Chip, {
    size: "small",
    key: tag,
    label: tag,
    sx: {
      mr: 1,
      mb: 1,
      color: 'text.secondary'
    }
  }))), /*#__PURE__*/React.createElement(Stack, {
    direction: "row",
    spacing: 2,
    alignItems: "flex-end",
    sx: {
      flexGrow: 1
    }
  }, /*#__PURE__*/React.createElement(Button, {
    fullWidth: true,
    variant: "contained",
    endIcon: /*#__PURE__*/React.createElement(Iconify, {
      icon: 'eva:checkmark-circle-2-fill'
    })
  }, "Accept"), /*#__PURE__*/React.createElement(Button, {
    fullWidth: true,
    variant: "contained",
    color: "error",
    endIcon: /*#__PURE__*/React.createElement(Iconify, {
      icon: 'eva:close-circle-fill'
    })
  }, "Reject")));
}