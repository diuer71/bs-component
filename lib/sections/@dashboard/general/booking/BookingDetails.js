import { useState } from 'react';
import { format } from 'date-fns';
import { sentenceCase } from 'change-case'; // @mui

import { useTheme } from '@mui/material/styles';
import { Box, Card, Stack, Table, Avatar, Button, Divider, MenuItem, TableRow, TableBody, TableCell, IconButton, TableHead, CardHeader, Typography, TableContainer } from '@mui/material'; // _mock_

import { _bookings } from '../../../../_mock'; //

import Label from '../../../../components/Label';
import Iconify from '../../../../components/Iconify';
import Scrollbar from '../../../../components/Scrollbar';
import MenuPopover from '../../../../components/MenuPopover'; // ----------------------------------------------------------------------

export default function BookingDetails() {
  const theme = useTheme();
  const isLight = theme.palette.mode === 'light';
  return /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement(Card, null, /*#__PURE__*/React.createElement(CardHeader, {
    title: "Booking Details",
    sx: {
      mb: 3
    }
  }), /*#__PURE__*/React.createElement(Scrollbar, null, /*#__PURE__*/React.createElement(TableContainer, {
    sx: {
      minWidth: 720
    }
  }, /*#__PURE__*/React.createElement(Table, null, /*#__PURE__*/React.createElement(TableHead, null, /*#__PURE__*/React.createElement(TableRow, null, /*#__PURE__*/React.createElement(TableCell, {
    sx: {
      minWidth: 240
    }
  }, "Booker"), /*#__PURE__*/React.createElement(TableCell, {
    sx: {
      minWidth: 160
    }
  }, "Check In"), /*#__PURE__*/React.createElement(TableCell, {
    sx: {
      minWidth: 160
    }
  }, "Check Out"), /*#__PURE__*/React.createElement(TableCell, {
    sx: {
      minWidth: 120
    }
  }, "Status"), /*#__PURE__*/React.createElement(TableCell, {
    sx: {
      minWidth: 200
    }
  }, "Phone"), /*#__PURE__*/React.createElement(TableCell, {
    sx: {
      minWidth: 120
    }
  }, "Room Type"), /*#__PURE__*/React.createElement(TableCell, null))), /*#__PURE__*/React.createElement(TableBody, null, _bookings.map(row => /*#__PURE__*/React.createElement(TableRow, {
    key: row.id
  }, /*#__PURE__*/React.createElement(TableCell, null, /*#__PURE__*/React.createElement(Stack, {
    direction: "row",
    alignItems: "center",
    spacing: 2
  }, /*#__PURE__*/React.createElement(Avatar, {
    alt: row.name,
    src: row.avatar
  }), /*#__PURE__*/React.createElement(Typography, {
    variant: "subtitle2"
  }, row.name))), /*#__PURE__*/React.createElement(TableCell, null, format(new Date(row.checkIn), 'dd MMM yyyy')), /*#__PURE__*/React.createElement(TableCell, null, format(new Date(row.checkOut), 'dd MMM yyyy')), /*#__PURE__*/React.createElement(TableCell, null, /*#__PURE__*/React.createElement(Label, {
    variant: isLight ? 'ghost' : 'filled',
    color: row.status === 'paid' && 'success' || row.status === 'pending' && 'warning' || 'error'
  }, sentenceCase(row.status))), /*#__PURE__*/React.createElement(TableCell, null, row.phoneNumber), /*#__PURE__*/React.createElement(TableCell, {
    sx: {
      textTransform: 'capitalize'
    }
  }, row.roomType), /*#__PURE__*/React.createElement(TableCell, {
    align: "right"
  }, /*#__PURE__*/React.createElement(MoreMenuButton, null)))))))), /*#__PURE__*/React.createElement(Divider, null), /*#__PURE__*/React.createElement(Box, {
    sx: {
      p: 2,
      textAlign: 'right'
    }
  }, /*#__PURE__*/React.createElement(Button, {
    size: "small",
    color: "inherit",
    endIcon: /*#__PURE__*/React.createElement(Iconify, {
      icon: 'eva:arrow-ios-forward-fill'
    })
  }, "View All"))));
} // ----------------------------------------------------------------------

function MoreMenuButton() {
  const [open, setOpen] = useState(null);

  const handleOpen = event => {
    setOpen(event.currentTarget);
  };

  const handleClose = () => {
    setOpen(null);
  };

  const ICON = {
    mr: 2,
    width: 20,
    height: 20
  };
  return /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement(IconButton, {
    size: "large",
    onClick: handleOpen
  }, /*#__PURE__*/React.createElement(Iconify, {
    icon: 'eva:more-vertical-fill',
    width: 20,
    height: 20
  })), /*#__PURE__*/React.createElement(MenuPopover, {
    open: Boolean(open),
    anchorEl: open,
    onClose: handleClose,
    anchorOrigin: {
      vertical: 'top',
      horizontal: 'left'
    },
    transformOrigin: {
      vertical: 'top',
      horizontal: 'right'
    },
    arrow: "right-top",
    sx: {
      mt: -0.5,
      width: 160,
      '& .MuiMenuItem-root': {
        px: 1,
        typography: 'body2',
        borderRadius: 0.75
      }
    }
  }, /*#__PURE__*/React.createElement(MenuItem, null, /*#__PURE__*/React.createElement(Iconify, {
    icon: 'eva:download-fill',
    sx: { ...ICON
    }
  }), "Download"), /*#__PURE__*/React.createElement(MenuItem, null, /*#__PURE__*/React.createElement(Iconify, {
    icon: 'eva:printer-fill',
    sx: { ...ICON
    }
  }), "Print"), /*#__PURE__*/React.createElement(MenuItem, null, /*#__PURE__*/React.createElement(Iconify, {
    icon: 'eva:share-fill',
    sx: { ...ICON
    }
  }), "Share"), /*#__PURE__*/React.createElement(Divider, {
    sx: {
      borderStyle: 'dashed'
    }
  }), /*#__PURE__*/React.createElement(MenuItem, {
    sx: {
      color: 'error.main'
    }
  }, /*#__PURE__*/React.createElement(Iconify, {
    icon: 'eva:trash-2-outline',
    sx: { ...ICON
    }
  }), "Delete")));
}