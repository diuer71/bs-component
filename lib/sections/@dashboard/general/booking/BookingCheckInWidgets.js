function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

import merge from 'lodash/merge';
import ReactApexChart from 'react-apexcharts'; // @mui

import { useTheme } from '@mui/material/styles';
import { Card, Typography, Stack, Divider } from '@mui/material'; // hooks

import useResponsive from '../../../../hooks/useResponsive'; // utils

import { fNumber } from '../../../../utils/formatNumber'; //

import { BaseOptionChart } from '../../../../components/chart'; // ----------------------------------------------------------------------

const CHART_SIZE = {
  width: 106,
  height: 106
};
const TOTAL_CHECK_IN = 38566;
const TOTAL_CHECK_OUT = 18472;
const CHART_DATA_CHECK_IN = [72];
const CHART_DATA_CHECK_OUT = [64];
export default function BookingCheckInWidgets() {
  const theme = useTheme();
  const isDesktop = useResponsive('up', 'sm');
  const chartOptionsCheckIn = merge(BaseOptionChart(), {
    chart: {
      sparkline: {
        enabled: true
      }
    },
    grid: {
      padding: {
        top: -9,
        bottom: -9
      }
    },
    legend: {
      show: false
    },
    plotOptions: {
      radialBar: {
        hollow: {
          size: '64%'
        },
        track: {
          margin: 0
        },
        dataLabels: {
          name: {
            show: false
          },
          value: {
            offsetY: 6,
            fontSize: theme.typography.subtitle2.fontSize
          }
        }
      }
    }
  });
  const chartOptionsCheckOut = { ...chartOptionsCheckIn,
    colors: [theme.palette.chart.yellow[0]]
  };
  return /*#__PURE__*/React.createElement(Card, null, /*#__PURE__*/React.createElement(Stack, {
    direction: {
      xs: 'column',
      sm: 'row'
    },
    divider: /*#__PURE__*/React.createElement(Divider, {
      orientation: isDesktop ? 'vertical' : 'horizontal',
      flexItem: true,
      sx: {
        borderStyle: 'dashed'
      }
    })
  }, /*#__PURE__*/React.createElement(Stack, {
    direction: "row",
    alignItems: "center",
    justifyContent: "center",
    spacing: 3,
    sx: {
      width: 1,
      py: 5
    }
  }, /*#__PURE__*/React.createElement(ReactApexChart, _extends({
    type: "radialBar",
    series: CHART_DATA_CHECK_IN,
    options: chartOptionsCheckIn
  }, CHART_SIZE)), /*#__PURE__*/React.createElement("div", null, /*#__PURE__*/React.createElement(Typography, {
    variant: "h4",
    sx: {
      mb: 0.5
    }
  }, fNumber(TOTAL_CHECK_IN)), /*#__PURE__*/React.createElement(Typography, {
    variant: "body2",
    sx: {
      opacity: 0.72
    }
  }, "Check In"))), /*#__PURE__*/React.createElement(Stack, {
    direction: "row",
    alignItems: "center",
    justifyContent: "center",
    spacing: 3,
    sx: {
      width: 1,
      py: 5
    }
  }, /*#__PURE__*/React.createElement(ReactApexChart, _extends({
    type: "radialBar",
    series: CHART_DATA_CHECK_OUT,
    options: chartOptionsCheckOut
  }, CHART_SIZE)), /*#__PURE__*/React.createElement("div", null, /*#__PURE__*/React.createElement(Typography, {
    variant: "h4",
    sx: {
      mb: 0.5
    }
  }, fNumber(TOTAL_CHECK_OUT)), /*#__PURE__*/React.createElement(Typography, {
    variant: "body2",
    sx: {
      opacity: 0.72
    }
  }, "Check Out")))));
}