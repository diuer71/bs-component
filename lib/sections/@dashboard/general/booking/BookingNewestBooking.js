function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

import PropTypes from 'prop-types';
import Slider from 'react-slick';
import { useRef } from 'react'; // @mui

import { useTheme } from '@mui/material/styles';
import { Box, Stack, Avatar, Typography, Paper, CardHeader } from '@mui/material'; // utils

import { fDateTime } from '../../../../utils/formatTime'; // _mock_

import { _bookingNew } from '../../../../_mock'; // components

import Label from '../../../../components/Label';
import Image from '../../../../components/Image';
import Iconify from '../../../../components/Iconify';
import { CarouselArrows } from '../../../../components/carousel'; // ----------------------------------------------------------------------

export default function BookingNewestBooking() {
  const theme = useTheme();
  const carouselRef = useRef(null);
  const settings = {
    dots: false,
    arrows: false,
    slidesToShow: 4,
    slidesToScroll: 1,
    rtl: Boolean(theme.direction === 'rtl'),
    responsive: [{
      breakpoint: theme.breakpoints.values.lg,
      settings: {
        slidesToShow: 3
      }
    }, {
      breakpoint: theme.breakpoints.values.md,
      settings: {
        slidesToShow: 2
      }
    }, {
      breakpoint: theme.breakpoints.values.sm,
      settings: {
        slidesToShow: 1
      }
    }]
  };

  const handlePrevious = () => {
    carouselRef.current?.slickPrev();
  };

  const handleNext = () => {
    carouselRef.current?.slickNext();
  };

  return /*#__PURE__*/React.createElement(Box, {
    sx: {
      py: 2
    }
  }, /*#__PURE__*/React.createElement(CardHeader, {
    title: "Newest Booking",
    subheader: "12 Booking",
    action: /*#__PURE__*/React.createElement(CarouselArrows, {
      customIcon: 'ic:round-keyboard-arrow-right',
      onNext: handleNext,
      onPrevious: handlePrevious,
      sx: {
        '& .arrow': {
          width: 28,
          height: 28,
          p: 0
        }
      }
    }),
    sx: {
      p: 0,
      mb: 3,
      '& .MuiCardHeader-action': {
        alignSelf: 'center'
      }
    }
  }), /*#__PURE__*/React.createElement(Slider, _extends({
    ref: carouselRef
  }, settings), _bookingNew.map(item => /*#__PURE__*/React.createElement(BookingItem, {
    key: item.id,
    item: item
  }))));
} // ----------------------------------------------------------------------

BookingItem.propTypes = {
  item: PropTypes.shape({
    avatar: PropTypes.string,
    bookdAt: PropTypes.instanceOf(Date),
    cover: PropTypes.string,
    name: PropTypes.string,
    person: PropTypes.string,
    roomNumber: PropTypes.string,
    roomType: PropTypes.string
  })
};

function BookingItem({
  item
}) {
  const {
    avatar,
    name,
    roomNumber,
    bookdAt,
    person,
    cover,
    roomType
  } = item;
  return /*#__PURE__*/React.createElement(Paper, {
    sx: {
      mx: 1.5,
      borderRadius: 2,
      bgcolor: 'background.neutral'
    }
  }, /*#__PURE__*/React.createElement(Stack, {
    spacing: 2.5,
    sx: {
      p: 3,
      pb: 2.5
    }
  }, /*#__PURE__*/React.createElement(Stack, {
    direction: "row",
    alignItems: "center",
    spacing: 2
  }, /*#__PURE__*/React.createElement(Avatar, {
    alt: name,
    src: avatar
  }), /*#__PURE__*/React.createElement("div", null, /*#__PURE__*/React.createElement(Typography, {
    variant: "subtitle2"
  }, name), /*#__PURE__*/React.createElement(Typography, {
    variant: "caption",
    sx: {
      color: 'text.disabled',
      mt: 0.5,
      display: 'block'
    }
  }, fDateTime(bookdAt)))), /*#__PURE__*/React.createElement(Stack, {
    direction: "row",
    alignItems: "center",
    spacing: 3,
    sx: {
      color: 'text.secondary'
    }
  }, /*#__PURE__*/React.createElement(Stack, {
    direction: "row",
    alignItems: "center",
    spacing: 1
  }, /*#__PURE__*/React.createElement(Iconify, {
    icon: 'ic:round-vpn-key',
    width: 16,
    height: 16
  }), /*#__PURE__*/React.createElement(Typography, {
    variant: "caption"
  }, "Room ", roomNumber)), /*#__PURE__*/React.createElement(Stack, {
    direction: "row",
    alignItems: "center",
    spacing: 1
  }, /*#__PURE__*/React.createElement(Iconify, {
    icon: 'eva:people-fill',
    width: 16,
    height: 16
  }), /*#__PURE__*/React.createElement(Typography, {
    variant: "caption"
  }, person, " Person")))), /*#__PURE__*/React.createElement(Box, {
    sx: {
      p: 1,
      position: 'relative'
    }
  }, /*#__PURE__*/React.createElement(Label, {
    variant: "filled",
    color: roomType === 'king' && 'error' || roomType === 'double' && 'info' || 'warning',
    sx: {
      right: 16,
      zIndex: 9,
      bottom: 16,
      position: 'absolute',
      textTransform: 'capitalize'
    }
  }, roomType), /*#__PURE__*/React.createElement(Image, {
    src: cover,
    ratio: "1/1",
    sx: {
      borderRadius: 1.5
    }
  })));
}