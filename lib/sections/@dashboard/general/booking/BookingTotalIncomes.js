import merge from 'lodash/merge';
import ReactApexChart from 'react-apexcharts'; // @mui

import { styled } from '@mui/material/styles';
import { Card, Typography, Stack } from '@mui/material'; // utils

import { fCurrency, fPercent } from '../../../../utils/formatNumber'; // components

import Iconify from '../../../../components/Iconify';
import BaseOptionChart from '../../../../components/chart/BaseOptionChart'; // ----------------------------------------------------------------------

const RootStyle = styled(Card)(({
  theme
}) => ({
  boxShadow: 'none',
  padding: theme.spacing(3),
  color: theme.palette.primary.darker,
  backgroundColor: theme.palette.primary.lighter
})); // ----------------------------------------------------------------------

const TOTAL = 18765;
const PERCENT = 2.6;
const CHART_DATA = [{
  data: [111, 136, 76, 108, 74, 54, 57, 84]
}];
export default function BookingTotalIncomes() {
  const chartOptions = merge(BaseOptionChart(), {
    chart: {
      sparkline: {
        enabled: true
      }
    },
    xaxis: {
      labels: {
        show: false
      }
    },
    yaxis: {
      labels: {
        show: false
      }
    },
    stroke: {
      width: 4
    },
    legend: {
      show: false
    },
    grid: {
      show: false
    },
    tooltip: {
      marker: {
        show: false
      },
      y: {
        formatter: seriesName => fCurrency(seriesName),
        title: {
          formatter: () => ''
        }
      }
    },
    fill: {
      gradient: {
        opacityFrom: 0,
        opacityTo: 0
      }
    }
  });
  return /*#__PURE__*/React.createElement(RootStyle, null, /*#__PURE__*/React.createElement(Stack, {
    direction: "row",
    justifyContent: "space-between",
    sx: {
      mb: 3
    }
  }, /*#__PURE__*/React.createElement("div", null, /*#__PURE__*/React.createElement(Typography, {
    sx: {
      mb: 2,
      typography: 'subtitle2'
    }
  }, "Total Incomes"), /*#__PURE__*/React.createElement(Typography, {
    sx: {
      typography: 'h3'
    }
  }, fCurrency(TOTAL))), /*#__PURE__*/React.createElement("div", null, /*#__PURE__*/React.createElement(Stack, {
    direction: "row",
    alignItems: "center",
    justifyContent: "flex-end",
    sx: {
      mb: 0.6
    }
  }, /*#__PURE__*/React.createElement(Iconify, {
    width: 20,
    height: 20,
    icon: PERCENT >= 0 ? 'eva:trending-up-fill' : 'eva:trending-down-fill'
  }), /*#__PURE__*/React.createElement(Typography, {
    variant: "subtitle2",
    component: "span",
    sx: {
      ml: 0.5
    }
  }, PERCENT > 0 && '+', fPercent(PERCENT))), /*#__PURE__*/React.createElement(Typography, {
    variant: "body2",
    component: "span",
    sx: {
      opacity: 0.72
    }
  }, "\xA0than last month"))), /*#__PURE__*/React.createElement(ReactApexChart, {
    type: "area",
    series: CHART_DATA,
    options: chartOptions,
    height: 132
  }));
}