import PropTypes from 'prop-types';
import Slider from 'react-slick';
import { Link as RouterLink } from 'react-router-dom'; // @mui

import { alpha, useTheme, styled } from '@mui/material/styles';
import { Box, Card, Button, CardContent, Typography } from '@mui/material'; // _mock_

import { _ecommerceNewProducts } from '../../../../_mock'; // components

import Image from '../../../../components/Image';
import { CarouselDots } from '../../../../components/carousel'; // ----------------------------------------------------------------------

const OverlayStyle = styled('div')(({
  theme
}) => ({
  top: 0,
  left: 0,
  right: 0,
  bottom: 0,
  zIndex: 8,
  position: 'absolute',
  backgroundColor: alpha(theme.palette.grey[900], 0.64)
})); // ----------------------------------------------------------------------

export default function EcommerceNewProducts() {
  const theme = useTheme();
  const settings = {
    speed: 1000,
    dots: true,
    arrows: false,
    autoplay: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    rtl: Boolean(theme.direction === 'rtl'),
    ...CarouselDots({
      position: 'absolute',
      right: 24,
      bottom: 24
    })
  };
  return /*#__PURE__*/React.createElement(Card, null, /*#__PURE__*/React.createElement(Slider, settings, _ecommerceNewProducts.map(item => /*#__PURE__*/React.createElement(CarouselItem, {
    key: item.name,
    item: item
  }))));
} // ----------------------------------------------------------------------

CarouselItem.propTypes = {
  item: PropTypes.shape({
    image: PropTypes.string,
    name: PropTypes.string
  })
};

function CarouselItem({
  item
}) {
  const {
    image,
    name
  } = item;
  return /*#__PURE__*/React.createElement(Box, {
    sx: {
      position: 'relative'
    }
  }, /*#__PURE__*/React.createElement(CardContent, {
    sx: {
      left: 0,
      bottom: 0,
      zIndex: 9,
      maxWidth: '80%',
      position: 'absolute',
      color: 'common.white'
    }
  }, /*#__PURE__*/React.createElement(Typography, {
    variant: "overline",
    sx: {
      opacity: 0.48
    }
  }, "New"), /*#__PURE__*/React.createElement(Typography, {
    noWrap: true,
    variant: "h5",
    sx: {
      mt: 1,
      mb: 3
    }
  }, name), /*#__PURE__*/React.createElement(Button, {
    to: "#",
    variant: "contained",
    component: RouterLink
  }, "Buy Now")), /*#__PURE__*/React.createElement(OverlayStyle, null), /*#__PURE__*/React.createElement(Image, {
    alt: name,
    src: image,
    sx: {
      height: {
        xs: 280,
        xl: 320
      }
    }
  }));
}