import PropTypes from 'prop-types';
import { Link as RouterLink } from 'react-router-dom'; // @mui

import { Box, Link, Card, CardHeader, Typography, Stack } from '@mui/material'; // utils

import { fCurrency } from '../../../../utils/formatNumber'; // _mock_

import { _ecommerceLatestProducts } from '../../../../_mock'; //

import Image from '../../../../components/Image';
import Scrollbar from '../../../../components/Scrollbar';
import { ColorPreview } from '../../../../components/color-utils'; // ----------------------------------------------------------------------

export default function EcommerceLatestProducts() {
  return /*#__PURE__*/React.createElement(Card, null, /*#__PURE__*/React.createElement(CardHeader, {
    title: "Latest Products"
  }), /*#__PURE__*/React.createElement(Scrollbar, null, /*#__PURE__*/React.createElement(Stack, {
    spacing: 3,
    sx: {
      p: 3,
      pr: 0
    }
  }, _ecommerceLatestProducts.map(product => /*#__PURE__*/React.createElement(ProductItem, {
    key: product.id,
    product: product
  })))));
} // ----------------------------------------------------------------------

ProductItem.propTypes = {
  product: PropTypes.shape({
    colors: PropTypes.arrayOf(PropTypes.string),
    image: PropTypes.string,
    name: PropTypes.string,
    price: PropTypes.number,
    priceSale: PropTypes.number
  })
};

function ProductItem({
  product
}) {
  const {
    name,
    image,
    price,
    priceSale
  } = product;
  const hasSale = priceSale > 0;
  return /*#__PURE__*/React.createElement(Stack, {
    direction: "row",
    spacing: 2
  }, /*#__PURE__*/React.createElement(Image, {
    alt: name,
    src: image,
    sx: {
      width: 48,
      height: 48,
      borderRadius: 1.5,
      flexShrink: 0
    }
  }), /*#__PURE__*/React.createElement(Box, {
    sx: {
      flexGrow: 1,
      minWidth: 200
    }
  }, /*#__PURE__*/React.createElement(Link, {
    component: RouterLink,
    to: "#",
    sx: {
      color: 'text.primary',
      typography: 'subtitle2'
    }
  }, name), /*#__PURE__*/React.createElement(Stack, {
    direction: "row"
  }, hasSale && /*#__PURE__*/React.createElement(Typography, {
    variant: "body2",
    sx: {
      color: 'text.secondary',
      textDecoration: 'line-through'
    }
  }, fCurrency(priceSale)), "\xA0", /*#__PURE__*/React.createElement(Typography, {
    variant: "body2",
    sx: {
      color: priceSale ? 'error.main' : 'text.secondary'
    }
  }, fCurrency(price)))), /*#__PURE__*/React.createElement(ColorPreview, {
    limit: 3,
    colors: product.colors,
    sx: {
      minWidth: 72,
      pr: 3
    }
  }));
}