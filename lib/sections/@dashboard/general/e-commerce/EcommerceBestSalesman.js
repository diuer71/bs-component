// @mui
import { useTheme } from '@mui/material/styles';
import { Box, Card, Table, Avatar, TableRow, TableBody, TableCell, TableHead, CardHeader, Typography, TableContainer } from '@mui/material'; // utils

import { fCurrency } from '../../../../utils/formatNumber'; // _mock_

import { _ecommerceBestSalesman } from '../../../../_mock'; // components

import Label from '../../../../components/Label';
import Image from '../../../../components/Image';
import Scrollbar from '../../../../components/Scrollbar'; // ----------------------------------------------------------------------

export default function EcommerceBestSalesman() {
  const theme = useTheme();
  return /*#__PURE__*/React.createElement(Card, null, /*#__PURE__*/React.createElement(CardHeader, {
    title: "Best Salesman",
    sx: {
      mb: 3
    }
  }), /*#__PURE__*/React.createElement(Scrollbar, null, /*#__PURE__*/React.createElement(TableContainer, {
    sx: {
      minWidth: 720
    }
  }, /*#__PURE__*/React.createElement(Table, null, /*#__PURE__*/React.createElement(TableHead, null, /*#__PURE__*/React.createElement(TableRow, null, /*#__PURE__*/React.createElement(TableCell, null, "Seller"), /*#__PURE__*/React.createElement(TableCell, null, "Product"), /*#__PURE__*/React.createElement(TableCell, null, "Country"), /*#__PURE__*/React.createElement(TableCell, null, "Total"), /*#__PURE__*/React.createElement(TableCell, {
    align: "right"
  }, "Rank"))), /*#__PURE__*/React.createElement(TableBody, null, _ecommerceBestSalesman.map(row => /*#__PURE__*/React.createElement(TableRow, {
    key: row.name
  }, /*#__PURE__*/React.createElement(TableCell, null, /*#__PURE__*/React.createElement(Box, {
    sx: {
      display: 'flex',
      alignItems: 'center'
    }
  }, /*#__PURE__*/React.createElement(Avatar, {
    alt: row.name,
    src: row.avatar
  }), /*#__PURE__*/React.createElement(Box, {
    sx: {
      ml: 2
    }
  }, /*#__PURE__*/React.createElement(Typography, {
    variant: "subtitle2"
  }, " ", row.name), /*#__PURE__*/React.createElement(Typography, {
    variant: "body2",
    sx: {
      color: 'text.secondary'
    }
  }, row.email)))), /*#__PURE__*/React.createElement(TableCell, null, row.category), /*#__PURE__*/React.createElement(TableCell, null, /*#__PURE__*/React.createElement(Image, {
    src: row.flag,
    alt: "country flag",
    sx: {
      maxWidth: 28
    }
  })), /*#__PURE__*/React.createElement(TableCell, null, fCurrency(row.total)), /*#__PURE__*/React.createElement(TableCell, {
    align: "right"
  }, /*#__PURE__*/React.createElement(Label, {
    variant: theme.palette.mode === 'light' ? 'ghost' : 'filled',
    color: row.rank === 'Top 1' && 'primary' || row.rank === 'Top 2' && 'info' || row.rank === 'Top 3' && 'success' || row.rank === 'Top 4' && 'warning' || 'error'
  }, row.rank)))))))));
}