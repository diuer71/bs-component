// @mui
import { styled } from '@mui/material/styles';
import { Button, Card, Typography, Stack } from '@mui/material'; // utils

import { fCurrency } from '../../../../utils/formatNumber'; // ----------------------------------------------------------------------

const RowStyle = styled('div')({
  display: 'flex',
  justifyContent: 'space-between'
}); // ----------------------------------------------------------------------

export default function EcommerceCurrentBalance() {
  const currentBalance = 187650;
  const sentAmount = 25500;
  const totalAmount = currentBalance - sentAmount;
  return /*#__PURE__*/React.createElement(Card, {
    sx: {
      p: 3
    }
  }, /*#__PURE__*/React.createElement(Typography, {
    variant: "subtitle2",
    gutterBottom: true
  }, "Your Current Balance"), /*#__PURE__*/React.createElement(Stack, {
    spacing: 2
  }, /*#__PURE__*/React.createElement(Typography, {
    variant: "h3"
  }, fCurrency(totalAmount)), /*#__PURE__*/React.createElement(RowStyle, null, /*#__PURE__*/React.createElement(Typography, {
    variant: "body2",
    sx: {
      color: 'text.secondary'
    }
  }, "Your Current Balance"), /*#__PURE__*/React.createElement(Typography, {
    variant: "body2"
  }, fCurrency(currentBalance))), /*#__PURE__*/React.createElement(RowStyle, null, /*#__PURE__*/React.createElement(Typography, {
    variant: "body2",
    sx: {
      color: 'text.secondary'
    }
  }, "Sent Amount"), /*#__PURE__*/React.createElement(Typography, {
    variant: "body2"
  }, "- ", fCurrency(sentAmount))), /*#__PURE__*/React.createElement(RowStyle, null, /*#__PURE__*/React.createElement(Typography, {
    variant: "body2",
    sx: {
      color: 'text.secondary'
    }
  }, "Total Amount"), /*#__PURE__*/React.createElement(Typography, {
    variant: "subtitle1"
  }, fCurrency(totalAmount))), /*#__PURE__*/React.createElement(Stack, {
    direction: "row",
    spacing: 1.5
  }, /*#__PURE__*/React.createElement(Button, {
    fullWidth: true,
    variant: "contained",
    color: "warning"
  }, "Transfer"), /*#__PURE__*/React.createElement(Button, {
    fullWidth: true,
    variant: "contained"
  }, "Receive"))));
}