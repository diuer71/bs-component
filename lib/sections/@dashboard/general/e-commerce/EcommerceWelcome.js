// @mui
import { styled } from '@mui/material/styles';
import { Typography, Button, Card, CardContent } from '@mui/material'; //

import { MotivationIllustration } from '../../../../assets'; // ----------------------------------------------------------------------

const RootStyle = styled(Card)(({
  theme
}) => ({
  boxShadow: 'none',
  textAlign: 'center',
  backgroundColor: theme.palette.primary.lighter,
  [theme.breakpoints.up('md')]: {
    height: '100%',
    display: 'flex',
    textAlign: 'left',
    alignItems: 'center',
    justifyContent: 'space-between'
  }
})); // ----------------------------------------------------------------------

export default function EcommerceWelcome() {
  return /*#__PURE__*/React.createElement(RootStyle, null, /*#__PURE__*/React.createElement(CardContent, {
    sx: {
      color: 'grey.800',
      p: {
        md: 0
      },
      pl: {
        md: 5
      }
    }
  }, /*#__PURE__*/React.createElement(Typography, {
    gutterBottom: true,
    variant: "h4"
  }, "Congratulations,", /*#__PURE__*/React.createElement("br", null), " Fabiana Capmany!"), /*#__PURE__*/React.createElement(Typography, {
    variant: "body2",
    sx: {
      pb: {
        xs: 3,
        xl: 5
      },
      maxWidth: 480,
      mx: 'auto'
    }
  }, "Best seller of the month You have done 57.6% more sales today."), /*#__PURE__*/React.createElement(Button, {
    variant: "contained"
  }, "Go Now")), /*#__PURE__*/React.createElement(MotivationIllustration, {
    sx: {
      p: 3,
      width: 360,
      margin: {
        xs: 'auto',
        md: 'inherit'
      }
    }
  }));
}