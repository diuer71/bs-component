import PropTypes from 'prop-types'; // @mui

import { Card, CardHeader, Typography, Stack, LinearProgress } from '@mui/material'; // utils

import { fPercent, fCurrency } from '../../../../utils/formatNumber'; // _mock_

import { _ecommerceSalesOverview } from '../../../../_mock'; // ----------------------------------------------------------------------

export default function EcommerceSalesOverview() {
  return /*#__PURE__*/React.createElement(Card, null, /*#__PURE__*/React.createElement(CardHeader, {
    title: "Sales Overview"
  }), /*#__PURE__*/React.createElement(Stack, {
    spacing: 4,
    sx: {
      p: 3
    }
  }, _ecommerceSalesOverview.map(progress => /*#__PURE__*/React.createElement(ProgressItem, {
    key: progress.label,
    progress: progress
  }))));
} // ----------------------------------------------------------------------

ProgressItem.propTypes = {
  progress: PropTypes.shape({
    amount: PropTypes.number,
    label: PropTypes.string,
    value: PropTypes.number
  })
};

function ProgressItem({
  progress
}) {
  return /*#__PURE__*/React.createElement(Stack, {
    spacing: 1
  }, /*#__PURE__*/React.createElement(Stack, {
    direction: "row",
    alignItems: "center"
  }, /*#__PURE__*/React.createElement(Typography, {
    variant: "subtitle2",
    sx: {
      flexGrow: 1
    }
  }, progress.label), /*#__PURE__*/React.createElement(Typography, {
    variant: "subtitle2"
  }, fCurrency(progress.amount)), /*#__PURE__*/React.createElement(Typography, {
    variant: "body2",
    sx: {
      color: 'text.secondary'
    }
  }, "\xA0(", fPercent(progress.value), ")")), /*#__PURE__*/React.createElement(LinearProgress, {
    variant: "determinate",
    value: progress.value,
    color: progress.label === 'Total Income' && 'info' || progress.label === 'Total Expenses' && 'warning' || 'primary'
  }));
}