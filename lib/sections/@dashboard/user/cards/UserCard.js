import PropTypes from 'prop-types'; // @mui

import { styled } from '@mui/material/styles';
import { Box, Card, Avatar, Divider, Typography, Stack } from '@mui/material'; // utils

import cssStyles from '../../../../utils/cssStyles';
import { fShortenNumber } from '../../../../utils/formatNumber'; // components

import Image from '../../../../components/Image';
import SocialsButton from '../../../../components/SocialsButton';
import SvgIconStyle from '../../../../components/SvgIconStyle'; // ----------------------------------------------------------------------

const OverlayStyle = styled('div')(({
  theme
}) => ({ ...cssStyles().bgBlur({
    blur: 2,
    color: theme.palette.primary.darker
  }),
  top: 0,
  zIndex: 8,
  content: "''",
  width: '100%',
  height: '100%',
  position: 'absolute'
})); // ----------------------------------------------------------------------

UserCard.propTypes = {
  user: PropTypes.object.isRequired
};
export default function UserCard({
  user
}) {
  const {
    name,
    cover,
    position,
    follower,
    totalPost,
    avatarUrl,
    following
  } = user;
  return /*#__PURE__*/React.createElement(Card, {
    sx: {
      textAlign: 'center'
    }
  }, /*#__PURE__*/React.createElement(Box, {
    sx: {
      position: 'relative'
    }
  }, /*#__PURE__*/React.createElement(SvgIconStyle, {
    src: "https://minimal-assets-api.vercel.app/assets/icons/shape-avatar.svg",
    sx: {
      width: 144,
      height: 62,
      zIndex: 10,
      left: 0,
      right: 0,
      bottom: -26,
      mx: 'auto',
      position: 'absolute',
      color: 'background.paper'
    }
  }), /*#__PURE__*/React.createElement(Avatar, {
    alt: name,
    src: avatarUrl,
    sx: {
      width: 64,
      height: 64,
      zIndex: 11,
      left: 0,
      right: 0,
      bottom: -32,
      mx: 'auto',
      position: 'absolute'
    }
  }), /*#__PURE__*/React.createElement(OverlayStyle, null), /*#__PURE__*/React.createElement(Image, {
    src: cover,
    alt: cover,
    ratio: "16/9"
  })), /*#__PURE__*/React.createElement(Typography, {
    variant: "subtitle1",
    sx: {
      mt: 6
    }
  }, name), /*#__PURE__*/React.createElement(Typography, {
    variant: "body2",
    sx: {
      color: 'text.secondary'
    }
  }, position), /*#__PURE__*/React.createElement(Stack, {
    alignItems: "center"
  }, /*#__PURE__*/React.createElement(SocialsButton, {
    initialColor: true,
    sx: {
      my: 2.5
    }
  })), /*#__PURE__*/React.createElement(Divider, {
    sx: {
      borderStyle: 'dashed'
    }
  }), /*#__PURE__*/React.createElement(Box, {
    sx: {
      py: 3,
      display: 'grid',
      gridTemplateColumns: 'repeat(3, 1fr)'
    }
  }, /*#__PURE__*/React.createElement("div", null, /*#__PURE__*/React.createElement(Typography, {
    variant: "caption",
    component: "div",
    sx: {
      mb: 0.75,
      color: 'text.disabled'
    }
  }, "Follower"), /*#__PURE__*/React.createElement(Typography, {
    variant: "subtitle1"
  }, fShortenNumber(follower))), /*#__PURE__*/React.createElement("div", null, /*#__PURE__*/React.createElement(Typography, {
    variant: "caption",
    component: "div",
    sx: {
      mb: 0.75,
      color: 'text.disabled'
    }
  }, "Following"), /*#__PURE__*/React.createElement(Typography, {
    variant: "subtitle1"
  }, fShortenNumber(following))), /*#__PURE__*/React.createElement("div", null, /*#__PURE__*/React.createElement(Typography, {
    variant: "caption",
    component: "div",
    sx: {
      mb: 0.75,
      color: 'text.disabled'
    }
  }, "Total Post"), /*#__PURE__*/React.createElement(Typography, {
    variant: "subtitle1"
  }, fShortenNumber(totalPost)))));
}