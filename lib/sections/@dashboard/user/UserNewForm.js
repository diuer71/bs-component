function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

import PropTypes from 'prop-types';
import * as Yup from 'yup';
import { useCallback, useEffect, useMemo } from 'react';
import { useSnackbar } from 'notistack';
import { useNavigate } from 'react-router-dom'; // form

import { useForm, Controller } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup'; // @mui

import { LoadingButton } from '@mui/lab';
import { Box, Card, Grid, Stack, Switch, Typography, FormControlLabel } from '@mui/material'; // utils

import { fData } from '../../../utils/formatNumber'; // routes

import { PATH_DASHBOARD } from '../../../routes/paths'; // _mock

import { countries } from '../../../_mock'; // components

import Label from '../../../components/Label';
import { FormProvider, RHFSelect, RHFSwitch, RHFTextField, RHFUploadAvatar } from '../../../components/hook-form'; // ----------------------------------------------------------------------

UserNewForm.propTypes = {
  isEdit: PropTypes.bool,
  currentUser: PropTypes.object
};
export default function UserNewForm({
  isEdit,
  currentUser
}) {
  const navigate = useNavigate();
  const {
    enqueueSnackbar
  } = useSnackbar();
  const NewUserSchema = Yup.object().shape({
    name: Yup.string().required('Name is required'),
    email: Yup.string().required('Email is required').email(),
    phoneNumber: Yup.string().required('Phone number is required'),
    address: Yup.string().required('Address is required'),
    country: Yup.string().required('country is required'),
    company: Yup.string().required('Company is required'),
    state: Yup.string().required('State is required'),
    city: Yup.string().required('City is required'),
    role: Yup.string().required('Role Number is required'),
    avatarUrl: Yup.mixed().test('required', 'Avatar is required', value => value !== '')
  });
  const defaultValues = useMemo(() => ({
    name: currentUser?.name || '',
    email: currentUser?.email || '',
    phoneNumber: currentUser?.phoneNumber || '',
    address: currentUser?.address || '',
    country: currentUser?.country || '',
    state: currentUser?.state || '',
    city: currentUser?.city || '',
    zipCode: currentUser?.zipCode || '',
    avatarUrl: currentUser?.avatarUrl || '',
    isVerified: currentUser?.isVerified || true,
    status: currentUser?.status,
    company: currentUser?.company || '',
    role: currentUser?.role || ''
  }), // eslint-disable-next-line react-hooks/exhaustive-deps
  [currentUser]);
  const methods = useForm({
    resolver: yupResolver(NewUserSchema),
    defaultValues
  });
  const {
    reset,
    watch,
    control,
    setValue,
    handleSubmit,
    formState: {
      isSubmitting
    }
  } = methods;
  const values = watch();
  useEffect(() => {
    if (isEdit && currentUser) {
      reset(defaultValues);
    }

    if (!isEdit) {
      reset(defaultValues);
    } // eslint-disable-next-line react-hooks/exhaustive-deps

  }, [isEdit, currentUser]);

  const onSubmit = async () => {
    try {
      await new Promise(resolve => setTimeout(resolve, 500));
      reset();
      enqueueSnackbar(!isEdit ? 'Create success!' : 'Update success!');
      navigate(PATH_DASHBOARD.user.list);
    } catch (error) {
      console.error(error);
    }
  };

  const handleDrop = useCallback(acceptedFiles => {
    const file = acceptedFiles[0];

    if (file) {
      setValue('avatarUrl', Object.assign(file, {
        preview: URL.createObjectURL(file)
      }));
    }
  }, [setValue]);
  return /*#__PURE__*/React.createElement(FormProvider, {
    methods: methods,
    onSubmit: handleSubmit(onSubmit)
  }, /*#__PURE__*/React.createElement(Grid, {
    container: true,
    spacing: 3
  }, /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: 12,
    md: 4
  }, /*#__PURE__*/React.createElement(Card, {
    sx: {
      py: 10,
      px: 3
    }
  }, isEdit && /*#__PURE__*/React.createElement(Label, {
    color: values.status !== 'active' ? 'error' : 'success',
    sx: {
      textTransform: 'uppercase',
      position: 'absolute',
      top: 24,
      right: 24
    }
  }, values.status), /*#__PURE__*/React.createElement(Box, {
    sx: {
      mb: 5
    }
  }, /*#__PURE__*/React.createElement(RHFUploadAvatar, {
    name: "avatarUrl",
    accept: "image/*",
    maxSize: 3145728,
    onDrop: handleDrop,
    helperText: /*#__PURE__*/React.createElement(Typography, {
      variant: "caption",
      sx: {
        mt: 2,
        mx: 'auto',
        display: 'block',
        textAlign: 'center',
        color: 'text.secondary'
      }
    }, "Allowed *.jpeg, *.jpg, *.png, *.gif", /*#__PURE__*/React.createElement("br", null), " max size of ", fData(3145728))
  })), isEdit && /*#__PURE__*/React.createElement(FormControlLabel, {
    labelPlacement: "start",
    control: /*#__PURE__*/React.createElement(Controller, {
      name: "status",
      control: control,
      render: ({
        field
      }) => /*#__PURE__*/React.createElement(Switch, _extends({}, field, {
        checked: field.value !== 'active',
        onChange: event => field.onChange(event.target.checked ? 'banned' : 'active')
      }))
    }),
    label: /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement(Typography, {
      variant: "subtitle2",
      sx: {
        mb: 0.5
      }
    }, "Banned"), /*#__PURE__*/React.createElement(Typography, {
      variant: "body2",
      sx: {
        color: 'text.secondary'
      }
    }, "Apply disable account")),
    sx: {
      mx: 0,
      mb: 3,
      width: 1,
      justifyContent: 'space-between'
    }
  }), /*#__PURE__*/React.createElement(RHFSwitch, {
    name: "isVerified",
    labelPlacement: "start",
    label: /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement(Typography, {
      variant: "subtitle2",
      sx: {
        mb: 0.5
      }
    }, "Email Verified"), /*#__PURE__*/React.createElement(Typography, {
      variant: "body2",
      sx: {
        color: 'text.secondary'
      }
    }, "Disabling this will automatically send the user a verification email")),
    sx: {
      mx: 0,
      width: 1,
      justifyContent: 'space-between'
    }
  }))), /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: 12,
    md: 8
  }, /*#__PURE__*/React.createElement(Card, {
    sx: {
      p: 3
    }
  }, /*#__PURE__*/React.createElement(Box, {
    sx: {
      display: 'grid',
      columnGap: 2,
      rowGap: 3,
      gridTemplateColumns: {
        xs: 'repeat(1, 1fr)',
        sm: 'repeat(2, 1fr)'
      }
    }
  }, /*#__PURE__*/React.createElement(RHFTextField, {
    name: "name",
    label: "Full Name"
  }), /*#__PURE__*/React.createElement(RHFTextField, {
    name: "email",
    label: "Email Address"
  }), /*#__PURE__*/React.createElement(RHFTextField, {
    name: "phoneNumber",
    label: "Phone Number"
  }), /*#__PURE__*/React.createElement(RHFSelect, {
    name: "country",
    label: "Country",
    placeholder: "Country"
  }, /*#__PURE__*/React.createElement("option", {
    value: ""
  }), countries.map(option => /*#__PURE__*/React.createElement("option", {
    key: option.code,
    value: option.label
  }, option.label))), /*#__PURE__*/React.createElement(RHFTextField, {
    name: "state",
    label: "State/Region"
  }), /*#__PURE__*/React.createElement(RHFTextField, {
    name: "city",
    label: "City"
  }), /*#__PURE__*/React.createElement(RHFTextField, {
    name: "address",
    label: "Address"
  }), /*#__PURE__*/React.createElement(RHFTextField, {
    name: "zipCode",
    label: "Zip/Code"
  }), /*#__PURE__*/React.createElement(RHFTextField, {
    name: "company",
    label: "Company"
  }), /*#__PURE__*/React.createElement(RHFTextField, {
    name: "role",
    label: "Role"
  })), /*#__PURE__*/React.createElement(Stack, {
    alignItems: "flex-end",
    sx: {
      mt: 3
    }
  }, /*#__PURE__*/React.createElement(LoadingButton, {
    type: "submit",
    variant: "contained",
    loading: isSubmitting
  }, !isEdit ? 'Create User' : 'Save Changes'))))));
}