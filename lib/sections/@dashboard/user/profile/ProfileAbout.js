import PropTypes from 'prop-types'; // @mui

import { styled } from '@mui/material/styles';
import { Link, Card, Typography, CardHeader, Stack } from '@mui/material'; // components

import Iconify from '../../../../components/Iconify'; // ----------------------------------------------------------------------

const IconStyle = styled(Iconify)(({
  theme
}) => ({
  width: 20,
  height: 20,
  marginTop: 1,
  flexShrink: 0,
  marginRight: theme.spacing(2)
})); // ----------------------------------------------------------------------

ProfileAbout.propTypes = {
  profile: PropTypes.object
};
export default function ProfileAbout({
  profile
}) {
  const {
    quote,
    country,
    email,
    role,
    company,
    school
  } = profile;
  return /*#__PURE__*/React.createElement(Card, null, /*#__PURE__*/React.createElement(CardHeader, {
    title: "About"
  }), /*#__PURE__*/React.createElement(Stack, {
    spacing: 2,
    sx: {
      p: 3
    }
  }, /*#__PURE__*/React.createElement(Typography, {
    variant: "body2"
  }, quote), /*#__PURE__*/React.createElement(Stack, {
    direction: "row"
  }, /*#__PURE__*/React.createElement(IconStyle, {
    icon: 'eva:pin-fill'
  }), /*#__PURE__*/React.createElement(Typography, {
    variant: "body2"
  }, "Live at \xA0", /*#__PURE__*/React.createElement(Link, {
    component: "span",
    variant: "subtitle2",
    color: "text.primary"
  }, country))), /*#__PURE__*/React.createElement(Stack, {
    direction: "row"
  }, /*#__PURE__*/React.createElement(IconStyle, {
    icon: 'eva:email-fill'
  }), /*#__PURE__*/React.createElement(Typography, {
    variant: "body2"
  }, email)), /*#__PURE__*/React.createElement(Stack, {
    direction: "row"
  }, /*#__PURE__*/React.createElement(IconStyle, {
    icon: 'ic:round-business-center'
  }), /*#__PURE__*/React.createElement(Typography, {
    variant: "body2"
  }, role, " at \xA0", /*#__PURE__*/React.createElement(Link, {
    component: "span",
    variant: "subtitle2",
    color: "text.primary"
  }, company))), /*#__PURE__*/React.createElement(Stack, {
    direction: "row"
  }, /*#__PURE__*/React.createElement(IconStyle, {
    icon: 'ic:round-business-center'
  }), /*#__PURE__*/React.createElement(Typography, {
    variant: "body2"
  }, "Studied at \xA0", /*#__PURE__*/React.createElement(Link, {
    component: "span",
    variant: "subtitle2",
    color: "text.primary"
  }, school)))));
}