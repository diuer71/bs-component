import PropTypes from 'prop-types';
import { useState, useRef } from 'react';
import { Link as RouterLink } from 'react-router-dom'; // @mui

import { Box, Link, Card, Stack, Paper, Avatar, Checkbox, TextField, Typography, CardHeader, IconButton, AvatarGroup, InputAdornment, FormControlLabel } from '@mui/material'; // hooks

import useAuth from '../../../../hooks/useAuth'; // utils

import { fDate } from '../../../../utils/formatTime';
import { fShortenNumber } from '../../../../utils/formatNumber'; // components

import Image from '../../../../components/Image';
import Iconify from '../../../../components/Iconify';
import MyAvatar from '../../../../components/MyAvatar';
import EmojiPicker from '../../../../components/EmojiPicker'; // ----------------------------------------------------------------------

ProfilePostCard.propTypes = {
  post: PropTypes.object
};
export default function ProfilePostCard({
  post
}) {
  const {
    user
  } = useAuth();
  const commentInputRef = useRef(null);
  const fileInputRef = useRef(null);
  const [isLiked, setLiked] = useState(post.isLiked);
  const [likes, setLikes] = useState(post.personLikes.length);
  const [message, setMessage] = useState('');
  const hasComments = post.comments.length > 0;

  const handleLike = () => {
    setLiked(true);
    setLikes(prevLikes => prevLikes + 1);
  };

  const handleUnlike = () => {
    setLiked(false);
    setLikes(prevLikes => prevLikes - 1);
  };

  const handleChangeMessage = value => {
    setMessage(value);
  };

  const handleClickAttach = () => {
    fileInputRef.current?.click();
  };

  const handleClickComment = () => {
    commentInputRef.current?.focus();
  };

  return /*#__PURE__*/React.createElement(Card, null, /*#__PURE__*/React.createElement(CardHeader, {
    disableTypography: true,
    avatar: /*#__PURE__*/React.createElement(MyAvatar, null),
    title: /*#__PURE__*/React.createElement(Link, {
      to: "#",
      variant: "subtitle2",
      color: "text.primary",
      component: RouterLink
    }, user?.displayName),
    subheader: /*#__PURE__*/React.createElement(Typography, {
      variant: "caption",
      sx: {
        display: 'block',
        color: 'text.secondary'
      }
    }, fDate(post.createdAt)),
    action: /*#__PURE__*/React.createElement(IconButton, null, /*#__PURE__*/React.createElement(Iconify, {
      icon: 'eva:more-vertical-fill',
      width: 20,
      height: 20
    }))
  }), /*#__PURE__*/React.createElement(Stack, {
    spacing: 3,
    sx: {
      p: 3
    }
  }, /*#__PURE__*/React.createElement(Typography, null, post.message), /*#__PURE__*/React.createElement(Image, {
    alt: "post media",
    src: post.media,
    ratio: "16/9",
    sx: {
      borderRadius: 1
    }
  }), /*#__PURE__*/React.createElement(Stack, {
    direction: "row",
    alignItems: "center"
  }, /*#__PURE__*/React.createElement(FormControlLabel, {
    control: /*#__PURE__*/React.createElement(Checkbox, {
      size: "small",
      color: "error",
      checked: isLiked,
      icon: /*#__PURE__*/React.createElement(Iconify, {
        icon: 'eva:heart-fill'
      }),
      checkedIcon: /*#__PURE__*/React.createElement(Iconify, {
        icon: 'eva:heart-fill'
      }),
      onChange: isLiked ? handleUnlike : handleLike
    }),
    label: fShortenNumber(likes),
    sx: {
      minWidth: 72,
      mr: 0
    }
  }), /*#__PURE__*/React.createElement(AvatarGroup, {
    max: 4,
    sx: {
      '& .MuiAvatar-root': {
        width: 32,
        height: 32
      }
    }
  }, post.personLikes.map(person => /*#__PURE__*/React.createElement(Avatar, {
    key: person.name,
    alt: person.name,
    src: person.avatarUrl
  }))), /*#__PURE__*/React.createElement(Box, {
    sx: {
      flexGrow: 1
    }
  }), /*#__PURE__*/React.createElement(IconButton, {
    onClick: handleClickComment
  }, /*#__PURE__*/React.createElement(Iconify, {
    icon: 'eva:message-square-fill',
    width: 20,
    height: 20
  })), /*#__PURE__*/React.createElement(IconButton, null, /*#__PURE__*/React.createElement(Iconify, {
    icon: 'eva:share-fill',
    width: 20,
    height: 20
  }))), hasComments && /*#__PURE__*/React.createElement(Stack, {
    spacing: 1.5
  }, post.comments.map(comment => /*#__PURE__*/React.createElement(Stack, {
    key: comment.id,
    direction: "row",
    spacing: 2
  }, /*#__PURE__*/React.createElement(Avatar, {
    alt: comment.author.name,
    src: comment.author.avatarUrl
  }), /*#__PURE__*/React.createElement(Paper, {
    sx: {
      p: 1.5,
      flexGrow: 1,
      bgcolor: 'background.neutral'
    }
  }, /*#__PURE__*/React.createElement(Stack, {
    direction: {
      xs: 'column',
      sm: 'row'
    },
    alignItems: {
      sm: 'center'
    },
    justifyContent: "space-between",
    sx: {
      mb: 0.5
    }
  }, /*#__PURE__*/React.createElement(Typography, {
    variant: "subtitle2"
  }, comment.author.name), /*#__PURE__*/React.createElement(Typography, {
    variant: "caption",
    sx: {
      color: 'text.disabled'
    }
  }, fDate(comment.createdAt))), /*#__PURE__*/React.createElement(Typography, {
    variant: "body2",
    sx: {
      color: 'text.secondary'
    }
  }, comment.message))))), /*#__PURE__*/React.createElement(Stack, {
    direction: "row",
    alignItems: "center"
  }, /*#__PURE__*/React.createElement(MyAvatar, null), /*#__PURE__*/React.createElement(TextField, {
    fullWidth: true,
    size: "small",
    value: message,
    inputRef: commentInputRef,
    placeholder: "Write a comment\u2026",
    onChange: event => handleChangeMessage(event.target.value),
    InputProps: {
      endAdornment: /*#__PURE__*/React.createElement(InputAdornment, {
        position: "end"
      }, /*#__PURE__*/React.createElement(IconButton, {
        size: "small",
        onClick: handleClickAttach
      }, /*#__PURE__*/React.createElement(Iconify, {
        icon: 'ic:round-add-photo-alternate',
        width: 24,
        height: 24
      })), /*#__PURE__*/React.createElement(EmojiPicker, {
        alignRight: true,
        value: message,
        setValue: setMessage
      }))
    },
    sx: {
      ml: 2,
      mr: 1,
      '& fieldset': {
        borderWidth: `1px !important`,
        borderColor: theme => `${theme.palette.grey[500_32]} !important`
      }
    }
  }), /*#__PURE__*/React.createElement(IconButton, null, /*#__PURE__*/React.createElement(Iconify, {
    icon: 'ic:round-send',
    width: 24,
    height: 24
  })), /*#__PURE__*/React.createElement("input", {
    type: "file",
    ref: fileInputRef,
    style: {
      display: 'none'
    }
  }))));
}