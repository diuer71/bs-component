import PropTypes from 'prop-types';
import { useState } from 'react'; // @mui

import { Box, Grid, Card, Button, Avatar, Typography } from '@mui/material'; // components

import Iconify from '../../../../components/Iconify'; // ----------------------------------------------------------------------

ProfileFollowers.propTypes = {
  followers: PropTypes.array
};
export default function ProfileFollowers({
  followers
}) {
  return /*#__PURE__*/React.createElement(Box, {
    sx: {
      mt: 5
    }
  }, /*#__PURE__*/React.createElement(Typography, {
    variant: "h4",
    sx: {
      mb: 3
    }
  }, "Followers"), /*#__PURE__*/React.createElement(Grid, {
    container: true,
    spacing: 3
  }, followers.map(follower => /*#__PURE__*/React.createElement(Grid, {
    key: follower.id,
    item: true,
    xs: 12,
    md: 4
  }, /*#__PURE__*/React.createElement(FollowerCard, {
    follower: follower
  })))));
} // ----------------------------------------------------------------------

FollowerCard.propTypes = {
  follower: PropTypes.object
};

function FollowerCard({
  follower
}) {
  const {
    name,
    country,
    avatarUrl,
    isFollowed
  } = follower;
  const [toggle, setToogle] = useState(isFollowed);
  return /*#__PURE__*/React.createElement(Card, {
    sx: {
      display: 'flex',
      alignItems: 'center',
      p: 3
    }
  }, /*#__PURE__*/React.createElement(Avatar, {
    alt: name,
    src: avatarUrl,
    sx: {
      width: 48,
      height: 48
    }
  }), /*#__PURE__*/React.createElement(Box, {
    sx: {
      flexGrow: 1,
      minWidth: 0,
      pl: 2,
      pr: 1
    }
  }, /*#__PURE__*/React.createElement(Typography, {
    variant: "subtitle2",
    noWrap: true
  }, name), /*#__PURE__*/React.createElement(Box, {
    sx: {
      display: 'flex',
      alignItems: 'center'
    }
  }, /*#__PURE__*/React.createElement(Iconify, {
    icon: 'eva:pin-fill',
    sx: {
      width: 16,
      height: 16,
      mr: 0.5,
      flexShrink: 0
    }
  }), /*#__PURE__*/React.createElement(Typography, {
    variant: "body2",
    sx: {
      color: 'text.secondary'
    },
    noWrap: true
  }, country))), /*#__PURE__*/React.createElement(Button, {
    size: "small",
    onClick: () => setToogle(!toggle),
    variant: toggle ? 'text' : 'outlined',
    color: toggle ? 'primary' : 'inherit',
    startIcon: toggle && /*#__PURE__*/React.createElement(Iconify, {
      icon: 'eva:checkmark-fill'
    })
  }, toggle ? 'Followed' : 'Follow'));
}