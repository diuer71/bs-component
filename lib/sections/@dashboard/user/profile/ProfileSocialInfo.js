import PropTypes from 'prop-types'; // @mui

import { styled } from '@mui/material/styles';
import { Link, Card, CardHeader, Stack } from '@mui/material'; // components

import Iconify from '../../../../components/Iconify'; // ----------------------------------------------------------------------

const IconStyle = styled(Iconify)(({
  theme
}) => ({
  width: 20,
  height: 20,
  marginTop: 1,
  flexShrink: 0,
  marginRight: theme.spacing(2)
})); // ----------------------------------------------------------------------

ProfileSocialInfo.propTypes = {
  profile: PropTypes.object
};
export default function ProfileSocialInfo({
  profile
}) {
  const {
    facebookLink,
    instagramLink,
    linkedinLink,
    twitterLink
  } = profile;
  const SOCIALS = [{
    name: 'Linkedin',
    icon: /*#__PURE__*/React.createElement(IconStyle, {
      icon: 'eva:linkedin-fill',
      color: "#006097"
    }),
    href: linkedinLink
  }, {
    name: 'Twitter',
    icon: /*#__PURE__*/React.createElement(IconStyle, {
      icon: 'eva:twitter-fill',
      color: "#1C9CEA"
    }),
    href: twitterLink
  }, {
    name: 'Instagram',
    icon: /*#__PURE__*/React.createElement(IconStyle, {
      icon: 'ant-design:instagram-filled',
      color: "#D7336D"
    }),
    href: instagramLink
  }, {
    name: 'Facebook',
    icon: /*#__PURE__*/React.createElement(IconStyle, {
      icon: 'eva:facebook-fill',
      color: "#1877F2"
    }),
    href: facebookLink
  }];
  return /*#__PURE__*/React.createElement(Card, null, /*#__PURE__*/React.createElement(CardHeader, {
    title: "Social"
  }), /*#__PURE__*/React.createElement(Stack, {
    spacing: 2,
    sx: {
      p: 3
    }
  }, SOCIALS.map(link => /*#__PURE__*/React.createElement(Stack, {
    key: link.name,
    direction: "row",
    alignItems: "center"
  }, link.icon, /*#__PURE__*/React.createElement(Link, {
    component: "span",
    variant: "body2",
    color: "text.primary",
    noWrap: true
  }, link.href)))));
}