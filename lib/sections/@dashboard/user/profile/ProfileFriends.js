import PropTypes from 'prop-types'; // @mui

import { Box, Grid, Card, Link, Avatar, IconButton, Typography, InputAdornment } from '@mui/material'; // components

import Iconify from '../../../../components/Iconify';
import InputStyle from '../../../../components/InputStyle';
import SocialsButton from '../../../../components/SocialsButton';
import SearchNotFound from '../../../../components/SearchNotFound'; // ----------------------------------------------------------------------

ProfileFriends.propTypes = {
  friends: PropTypes.array,
  findFriends: PropTypes.string,
  onFindFriends: PropTypes.func
};
export default function ProfileFriends({
  friends,
  findFriends,
  onFindFriends
}) {
  const friendFiltered = applyFilter(friends, findFriends);
  const isNotFound = friendFiltered.length === 0;
  return /*#__PURE__*/React.createElement(Box, {
    sx: {
      mt: 5
    }
  }, /*#__PURE__*/React.createElement(Typography, {
    variant: "h4",
    sx: {
      mb: 3
    }
  }, "Friends"), /*#__PURE__*/React.createElement(InputStyle, {
    stretchStart: 240,
    value: findFriends,
    onChange: event => onFindFriends(event.target.value),
    placeholder: "Find friends...",
    InputProps: {
      startAdornment: /*#__PURE__*/React.createElement(InputAdornment, {
        position: "start"
      }, /*#__PURE__*/React.createElement(Iconify, {
        icon: 'eva:search-fill',
        sx: {
          color: 'text.disabled',
          width: 20,
          height: 20
        }
      }))
    },
    sx: {
      mb: 5
    }
  }), /*#__PURE__*/React.createElement(Grid, {
    container: true,
    spacing: 3
  }, friendFiltered.map(friend => /*#__PURE__*/React.createElement(Grid, {
    key: friend.id,
    item: true,
    xs: 12,
    md: 4
  }, /*#__PURE__*/React.createElement(FriendCard, {
    friend: friend
  })))), isNotFound && /*#__PURE__*/React.createElement(Box, {
    sx: {
      mt: 5
    }
  }, /*#__PURE__*/React.createElement(SearchNotFound, {
    searchQuery: findFriends
  })));
} // ----------------------------------------------------------------------

FriendCard.propTypes = {
  friend: PropTypes.object
};

function FriendCard({
  friend
}) {
  const {
    name,
    role,
    avatarUrl
  } = friend;
  return /*#__PURE__*/React.createElement(Card, {
    sx: {
      py: 5,
      display: 'flex',
      position: 'relative',
      alignItems: 'center',
      flexDirection: 'column'
    }
  }, /*#__PURE__*/React.createElement(Avatar, {
    alt: name,
    src: avatarUrl,
    sx: {
      width: 64,
      height: 64,
      mb: 3
    }
  }), /*#__PURE__*/React.createElement(Link, {
    variant: "subtitle1",
    color: "text.primary"
  }, name), /*#__PURE__*/React.createElement(Typography, {
    variant: "body2",
    sx: {
      color: 'text.secondary',
      mb: 1
    }
  }, role), /*#__PURE__*/React.createElement(SocialsButton, {
    initialColor: true
  }), /*#__PURE__*/React.createElement(IconButton, {
    sx: {
      top: 8,
      right: 8,
      position: 'absolute'
    }
  }, /*#__PURE__*/React.createElement(Iconify, {
    icon: 'eva:more-vertical-fill',
    width: 20,
    height: 20
  })));
} // ----------------------------------------------------------------------


function applyFilter(array, query) {
  if (query) {
    return array.filter(friend => friend.name.toLowerCase().indexOf(query.toLowerCase()) !== -1);
  }

  return array;
}