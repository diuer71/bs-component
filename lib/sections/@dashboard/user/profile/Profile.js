import PropTypes from 'prop-types'; // @mui

import { Grid, Stack } from '@mui/material'; //

import ProfileAbout from './ProfileAbout';
import ProfilePostCard from './ProfilePostCard';
import ProfilePostInput from './ProfilePostInput';
import ProfileFollowInfo from './ProfileFollowInfo';
import ProfileSocialInfo from './ProfileSocialInfo'; // ----------------------------------------------------------------------

Profile.propTypes = {
  myProfile: PropTypes.object,
  posts: PropTypes.array
};
export default function Profile({
  myProfile,
  posts
}) {
  return /*#__PURE__*/React.createElement(Grid, {
    container: true,
    spacing: 3
  }, /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: 12,
    md: 4
  }, /*#__PURE__*/React.createElement(Stack, {
    spacing: 3
  }, /*#__PURE__*/React.createElement(ProfileFollowInfo, {
    profile: myProfile
  }), /*#__PURE__*/React.createElement(ProfileAbout, {
    profile: myProfile
  }), /*#__PURE__*/React.createElement(ProfileSocialInfo, {
    profile: myProfile
  }))), /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: 12,
    md: 8
  }, /*#__PURE__*/React.createElement(Stack, {
    spacing: 3
  }, /*#__PURE__*/React.createElement(ProfilePostInput, null), posts.map(post => /*#__PURE__*/React.createElement(ProfilePostCard, {
    key: post.id,
    post: post
  })))));
}