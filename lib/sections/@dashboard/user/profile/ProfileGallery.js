import PropTypes from 'prop-types';
import { useState } from 'react'; // @mui

import { styled } from '@mui/material/styles';
import { Box, Card, IconButton, Typography, CardContent } from '@mui/material'; // utils

import { fDate } from '../../../../utils/formatTime';
import cssStyles from '../../../../utils/cssStyles'; // components

import Image from '../../../../components/Image';
import Iconify from '../../../../components/Iconify';
import LightboxModal from '../../../../components/LightboxModal'; // ----------------------------------------------------------------------

const CaptionStyle = styled(CardContent)(({
  theme
}) => ({ ...cssStyles().bgBlur({
    blur: 2,
    color: theme.palette.grey[900]
  }),
  bottom: 0,
  width: '100%',
  display: 'flex',
  alignItems: 'center',
  position: 'absolute',
  justifyContent: 'space-between',
  color: theme.palette.common.white
})); // ----------------------------------------------------------------------

ProfileGallery.propTypes = {
  gallery: PropTypes.array.isRequired
};
export default function ProfileGallery({
  gallery
}) {
  const [openLightbox, setOpenLightbox] = useState(false);
  const [selectedImage, setSelectedImage] = useState(0);
  const imagesLightbox = gallery.map(img => img.imageUrl);

  const handleOpenLightbox = url => {
    const selectedImage = imagesLightbox.findIndex(index => index === url);
    setOpenLightbox(true);
    setSelectedImage(selectedImage);
  };

  return /*#__PURE__*/React.createElement(Box, {
    sx: {
      mt: 5
    }
  }, /*#__PURE__*/React.createElement(Typography, {
    variant: "h4",
    sx: {
      mb: 3
    }
  }, "Gallery"), /*#__PURE__*/React.createElement(Card, {
    sx: {
      p: 3
    }
  }, /*#__PURE__*/React.createElement(Box, {
    sx: {
      display: 'grid',
      gap: 3,
      gridTemplateColumns: {
        xs: 'repeat(1, 1fr)',
        sm: 'repeat(2, 1fr)',
        md: 'repeat(3, 1fr)'
      }
    }
  }, gallery.map(image => /*#__PURE__*/React.createElement(GalleryItem, {
    key: image.id,
    image: image,
    onOpenLightbox: handleOpenLightbox
  }))), /*#__PURE__*/React.createElement(LightboxModal, {
    images: imagesLightbox,
    mainSrc: imagesLightbox[selectedImage],
    photoIndex: selectedImage,
    setPhotoIndex: setSelectedImage,
    isOpen: openLightbox,
    onCloseRequest: () => setOpenLightbox(false)
  })));
} // ----------------------------------------------------------------------

GalleryItem.propTypes = {
  image: PropTypes.object,
  onOpenLightbox: PropTypes.func
};

function GalleryItem({
  image,
  onOpenLightbox
}) {
  const {
    imageUrl,
    title,
    postAt
  } = image;
  return /*#__PURE__*/React.createElement(Card, {
    sx: {
      cursor: 'pointer',
      position: 'relative'
    }
  }, /*#__PURE__*/React.createElement(Image, {
    alt: "gallery image",
    ratio: "1/1",
    src: imageUrl,
    onClick: () => onOpenLightbox(imageUrl)
  }), /*#__PURE__*/React.createElement(CaptionStyle, null, /*#__PURE__*/React.createElement("div", null, /*#__PURE__*/React.createElement(Typography, {
    variant: "subtitle1"
  }, title), /*#__PURE__*/React.createElement(Typography, {
    variant: "body2",
    sx: {
      opacity: 0.72
    }
  }, fDate(postAt))), /*#__PURE__*/React.createElement(IconButton, {
    color: "inherit"
  }, /*#__PURE__*/React.createElement(Iconify, {
    icon: 'eva:more-vertical-fill',
    width: 20,
    height: 20
  }))));
}