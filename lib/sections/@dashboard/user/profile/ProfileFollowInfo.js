import PropTypes from 'prop-types'; // @mui

import { Card, Stack, Typography, Divider } from '@mui/material'; // utils

import { fNumber } from '../../../../utils/formatNumber'; // ----------------------------------------------------------------------

ProfileFollowInfo.propTypes = {
  profile: PropTypes.shape({
    follower: PropTypes.number,
    following: PropTypes.number
  })
};
export default function ProfileFollowInfo({
  profile
}) {
  const {
    follower,
    following
  } = profile;
  return /*#__PURE__*/React.createElement(Card, {
    sx: {
      py: 3
    }
  }, /*#__PURE__*/React.createElement(Stack, {
    direction: "row",
    divider: /*#__PURE__*/React.createElement(Divider, {
      orientation: "vertical",
      flexItem: true
    })
  }, /*#__PURE__*/React.createElement(Stack, {
    width: 1,
    textAlign: "center"
  }, /*#__PURE__*/React.createElement(Typography, {
    variant: "h4"
  }, fNumber(follower)), /*#__PURE__*/React.createElement(Typography, {
    variant: "body2",
    sx: {
      color: 'text.secondary'
    }
  }, "Follower")), /*#__PURE__*/React.createElement(Stack, {
    width: 1,
    textAlign: "center"
  }, /*#__PURE__*/React.createElement(Typography, {
    variant: "h4"
  }, fNumber(following)), /*#__PURE__*/React.createElement(Typography, {
    variant: "body2",
    sx: {
      color: 'text.secondary'
    }
  }, "Following"))));
}