import PropTypes from 'prop-types'; // @mui

import { Box, Checkbox, TableRow, TableCell, TableHead, TableSortLabel } from '@mui/material'; // ----------------------------------------------------------------------

const visuallyHidden = {
  border: 0,
  clip: 'rect(0 0 0 0)',
  height: '1px',
  margin: -1,
  overflow: 'hidden',
  padding: 0,
  position: 'absolute',
  whiteSpace: 'nowrap',
  width: '1px'
};
UserListHead.propTypes = {
  order: PropTypes.oneOf(['asc', 'desc']),
  orderBy: PropTypes.string,
  rowCount: PropTypes.number,
  headLabel: PropTypes.array,
  numSelected: PropTypes.number,
  onRequestSort: PropTypes.func,
  onSelectAllClick: PropTypes.func
};
export default function UserListHead({
  order,
  orderBy,
  rowCount,
  headLabel,
  numSelected,
  onRequestSort,
  onSelectAllClick
}) {
  const createSortHandler = property => event => {
    onRequestSort(event, property);
  };

  return /*#__PURE__*/React.createElement(TableHead, null, /*#__PURE__*/React.createElement(TableRow, null, /*#__PURE__*/React.createElement(TableCell, {
    padding: "checkbox"
  }, /*#__PURE__*/React.createElement(Checkbox, {
    indeterminate: numSelected > 0 && numSelected < rowCount,
    checked: rowCount > 0 && numSelected === rowCount,
    onChange: onSelectAllClick
  })), headLabel.map(headCell => /*#__PURE__*/React.createElement(TableCell, {
    key: headCell.id,
    align: headCell.alignRight ? 'right' : 'left',
    sortDirection: orderBy === headCell.id ? order : false
  }, /*#__PURE__*/React.createElement(TableSortLabel, {
    hideSortIcon: true,
    active: orderBy === headCell.id,
    direction: orderBy === headCell.id ? order : 'asc',
    onClick: createSortHandler(headCell.id)
  }, headCell.label, orderBy === headCell.id ? /*#__PURE__*/React.createElement(Box, {
    sx: { ...visuallyHidden
    }
  }, order === 'desc' ? 'sorted descending' : 'sorted ascending') : null)))));
}