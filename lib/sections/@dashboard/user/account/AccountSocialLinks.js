import PropTypes from 'prop-types';
import { useSnackbar } from 'notistack'; // form

import { useForm } from 'react-hook-form'; // @mui

import { Stack, Card, InputAdornment } from '@mui/material';
import { LoadingButton } from '@mui/lab'; // components

import Iconify from '../../../../components/Iconify';
import { FormProvider, RHFTextField } from '../../../../components/hook-form'; // ----------------------------------------------------------------------

const SOCIAL_LINKS = [{
  value: 'facebookLink',
  icon: /*#__PURE__*/React.createElement(Iconify, {
    icon: 'eva:facebook-fill',
    width: 24,
    height: 24
  })
}, {
  value: 'instagramLink',
  icon: /*#__PURE__*/React.createElement(Iconify, {
    icon: 'ant-design:instagram-filled',
    width: 24,
    height: 24
  })
}, {
  value: 'linkedinLink',
  icon: /*#__PURE__*/React.createElement(Iconify, {
    icon: 'eva:linkedin-fill',
    width: 24,
    height: 24
  })
}, {
  value: 'twitterLink',
  icon: /*#__PURE__*/React.createElement(Iconify, {
    icon: 'eva:twitter-fill',
    width: 24,
    height: 24
  })
}]; // ----------------------------------------------------------------------

AccountSocialLinks.propTypes = {
  myProfile: PropTypes.shape({
    facebookLink: PropTypes.string,
    instagramLink: PropTypes.string,
    linkedinLink: PropTypes.string,
    twitterLink: PropTypes.string
  })
};
export default function AccountSocialLinks({
  myProfile
}) {
  const {
    enqueueSnackbar
  } = useSnackbar();
  const defaultValues = {
    facebookLink: myProfile.facebookLink,
    instagramLink: myProfile.instagramLink,
    linkedinLink: myProfile.linkedinLink,
    twitterLink: myProfile.twitterLink
  };
  const methods = useForm({
    defaultValues
  });
  const {
    handleSubmit,
    formState: {
      isSubmitting
    }
  } = methods;

  const onSubmit = async () => {
    try {
      await new Promise(resolve => setTimeout(resolve, 500));
      enqueueSnackbar('Update success!');
    } catch (error) {
      console.error(error);
    }
  };

  return /*#__PURE__*/React.createElement(Card, {
    sx: {
      p: 3
    }
  }, /*#__PURE__*/React.createElement(FormProvider, {
    methods: methods,
    onSubmit: handleSubmit(onSubmit)
  }, /*#__PURE__*/React.createElement(Stack, {
    spacing: 3,
    alignItems: "flex-end"
  }, SOCIAL_LINKS.map(link => /*#__PURE__*/React.createElement(RHFTextField, {
    key: link.value,
    name: link.value,
    InputProps: {
      startAdornment: /*#__PURE__*/React.createElement(InputAdornment, {
        position: "start"
      }, link.icon)
    }
  })), /*#__PURE__*/React.createElement(LoadingButton, {
    type: "submit",
    variant: "contained",
    loading: isSubmitting
  }, "Save Changes"))));
}