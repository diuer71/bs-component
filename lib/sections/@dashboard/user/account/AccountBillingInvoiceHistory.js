import PropTypes from 'prop-types';
import { Link as RouterLink } from 'react-router-dom'; // @mui

import { Stack, Link, Button, Typography } from '@mui/material'; // utils

import { fDate } from '../../../../utils/formatTime';
import { fCurrency } from '../../../../utils/formatNumber'; // components

import Iconify from '../../../../components/Iconify'; // ----------------------------------------------------------------------

AccountBillingInvoiceHistory.propTypes = {
  invoices: PropTypes.array
};
export default function AccountBillingInvoiceHistory({
  invoices
}) {
  return /*#__PURE__*/React.createElement(Stack, {
    spacing: 3,
    alignItems: "flex-end"
  }, /*#__PURE__*/React.createElement(Typography, {
    variant: "subtitle1",
    sx: {
      width: 1
    }
  }, "Invoice History"), /*#__PURE__*/React.createElement(Stack, {
    spacing: 2,
    sx: {
      width: 1
    }
  }, invoices.map(invoice => /*#__PURE__*/React.createElement(Stack, {
    key: invoice.id,
    direction: "row",
    justifyContent: "space-between",
    sx: {
      width: 1
    }
  }, /*#__PURE__*/React.createElement(Typography, {
    variant: "body2",
    sx: {
      minWidth: 160
    }
  }, fDate(invoice.createdAt)), /*#__PURE__*/React.createElement(Typography, {
    variant: "body2"
  }, fCurrency(invoice.price)), /*#__PURE__*/React.createElement(Link, {
    component: RouterLink,
    to: "#"
  }, "PDF")))), /*#__PURE__*/React.createElement(Button, {
    size: "small",
    endIcon: /*#__PURE__*/React.createElement(Iconify, {
      icon: 'eva:arrow-ios-forward-fill'
    })
  }, "All invoices"));
}