import PropTypes from 'prop-types';
import { useState } from 'react'; // @mui

import { Box, Grid, Card, Button, Typography, Stack } from '@mui/material'; //

import AccountBillingAddressBook from './AccountBillingAddressBook';
import AccountBillingPaymentMethod from './AccountBillingPaymentMethod';
import AccountBillingInvoiceHistory from './AccountBillingInvoiceHistory'; // ----------------------------------------------------------------------

AccountBilling.propTypes = {
  addressBook: PropTypes.array,
  cards: PropTypes.array,
  invoices: PropTypes.array
};
export default function AccountBilling({
  cards,
  addressBook,
  invoices
}) {
  const [open, setOpen] = useState(false);
  return /*#__PURE__*/React.createElement(Grid, {
    container: true,
    spacing: 5
  }, /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: 12,
    md: 8
  }, /*#__PURE__*/React.createElement(Stack, {
    spacing: 3
  }, /*#__PURE__*/React.createElement(Card, {
    sx: {
      p: 3
    }
  }, /*#__PURE__*/React.createElement(Typography, {
    variant: "overline",
    sx: {
      mb: 3,
      display: 'block',
      color: 'text.secondary'
    }
  }, "Your Plan"), /*#__PURE__*/React.createElement(Typography, {
    variant: "h4"
  }, "Premium"), /*#__PURE__*/React.createElement(Box, {
    sx: {
      mt: {
        xs: 2,
        sm: 0
      },
      position: {
        sm: 'absolute'
      },
      top: {
        sm: 24
      },
      right: {
        sm: 24
      }
    }
  }, /*#__PURE__*/React.createElement(Button, {
    size: "small",
    color: "inherit",
    variant: "outlined",
    sx: {
      mr: 1
    }
  }, "Cancel plan"), /*#__PURE__*/React.createElement(Button, {
    size: "small",
    variant: "outlined"
  }, "Upgrade plan"))), /*#__PURE__*/React.createElement(AccountBillingPaymentMethod, {
    cards: cards,
    isOpen: open,
    onOpen: () => setOpen(!open),
    onCancel: () => setOpen(false)
  }), /*#__PURE__*/React.createElement(AccountBillingAddressBook, {
    addressBook: addressBook
  }))), /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: 12,
    md: 4
  }, /*#__PURE__*/React.createElement(AccountBillingInvoiceHistory, {
    invoices: invoices
  })));
}