import PropTypes from 'prop-types'; // @mui

import { Box, Card, Stack, Paper, Button, Collapse, TextField, Typography, IconButton } from '@mui/material';
import { LoadingButton } from '@mui/lab'; // components

import Image from '../../../../components/Image';
import Iconify from '../../../../components/Iconify'; // ----------------------------------------------------------------------

AccountBillingPaymentMethod.propTypes = {
  cards: PropTypes.array,
  isOpen: PropTypes.bool,
  onOpen: PropTypes.func,
  onCancel: PropTypes.func
};
export default function AccountBillingPaymentMethod({
  cards,
  isOpen,
  onOpen,
  onCancel
}) {
  return /*#__PURE__*/React.createElement(Card, {
    sx: {
      p: 3
    }
  }, /*#__PURE__*/React.createElement(Typography, {
    variant: "overline",
    sx: {
      mb: 3,
      display: 'block',
      color: 'text.secondary'
    }
  }, "Payment Method"), /*#__PURE__*/React.createElement(Stack, {
    spacing: 2,
    direction: {
      xs: 'column',
      md: 'row'
    }
  }, cards.map(card => /*#__PURE__*/React.createElement(Paper, {
    key: card.id,
    sx: {
      p: 3,
      width: 1,
      position: 'relative',
      border: theme => `solid 1px ${theme.palette.grey[500_32]}`
    }
  }, /*#__PURE__*/React.createElement(Image, {
    alt: "icon",
    src: card.cardType === 'master_card' ? 'https://minimal-assets-api.vercel.app/assets/icons/ic_mastercard.svg' : 'https://minimal-assets-api.vercel.app/assets/icons/ic_visa.svg',
    sx: {
      mb: 1,
      maxWidth: 36
    }
  }), /*#__PURE__*/React.createElement(Typography, {
    variant: "subtitle2"
  }, card.cardNumber), /*#__PURE__*/React.createElement(IconButton, {
    sx: {
      top: 8,
      right: 8,
      position: 'absolute'
    }
  }, /*#__PURE__*/React.createElement(Iconify, {
    icon: 'eva:more-vertical-fill',
    width: 20,
    height: 20
  }))))), /*#__PURE__*/React.createElement(Box, {
    sx: {
      mt: 3
    }
  }, /*#__PURE__*/React.createElement(Button, {
    size: "small",
    startIcon: /*#__PURE__*/React.createElement(Iconify, {
      icon: 'eva:plus-fill'
    }),
    onClick: onOpen
  }, "Add new card")), /*#__PURE__*/React.createElement(Collapse, {
    in: isOpen
  }, /*#__PURE__*/React.createElement(Box, {
    sx: {
      padding: 3,
      marginTop: 3,
      borderRadius: 1,
      bgcolor: 'background.neutral'
    }
  }, /*#__PURE__*/React.createElement(Stack, {
    spacing: 3
  }, /*#__PURE__*/React.createElement(Typography, {
    variant: "subtitle1"
  }, "Add new card"), /*#__PURE__*/React.createElement(Stack, {
    direction: {
      xs: 'column',
      sm: 'row'
    },
    spacing: 2
  }, /*#__PURE__*/React.createElement(TextField, {
    fullWidth: true,
    label: "Name on card"
  }), /*#__PURE__*/React.createElement(TextField, {
    fullWidth: true,
    label: "Card number"
  })), /*#__PURE__*/React.createElement(Stack, {
    direction: {
      xs: 'column',
      sm: 'row'
    },
    spacing: 2
  }, /*#__PURE__*/React.createElement(TextField, {
    fullWidth: true,
    label: "Expiration date",
    placeholder: "MM/YY"
  }), /*#__PURE__*/React.createElement(TextField, {
    fullWidth: true,
    label: "Cvv"
  })), /*#__PURE__*/React.createElement(Stack, {
    direction: "row",
    justifyContent: "flex-end",
    spacing: 1.5
  }, /*#__PURE__*/React.createElement(Button, {
    color: "inherit",
    variant: "outlined",
    onClick: onCancel
  }, "Cancel"), /*#__PURE__*/React.createElement(LoadingButton, {
    type: "submit",
    variant: "contained",
    onClick: onCancel
  }, "Save Change"))))));
}