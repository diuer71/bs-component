import { useSnackbar } from 'notistack'; // form

import { useForm } from 'react-hook-form'; // @mui

import { Card, Stack, Typography } from '@mui/material';
import { LoadingButton } from '@mui/lab'; // components

import { FormProvider, RHFSwitch } from '../../../../components/hook-form'; // ----------------------------------------------------------------------

const ACTIVITY_OPTIONS = [{
  value: 'activityComments',
  label: 'Email me when someone comments onmy article'
}, {
  value: 'activityAnswers',
  label: 'Email me when someone answers on my form'
}, {
  value: 'activityFollows',
  label: 'Email me hen someone follows me'
}];
const APPLICATION_OPTIONS = [{
  value: 'applicationNews',
  label: 'News and announcements'
}, {
  value: 'applicationProduct',
  label: 'Weekly product updates'
}, {
  value: 'applicationBlog',
  label: 'Weekly blog digest'
}];
const NOTIFICATION_SETTINGS = {
  activityComments: true,
  activityAnswers: true,
  activityFollows: false,
  applicationNews: true,
  applicationProduct: false,
  applicationBlog: false
}; // ----------------------------------------------------------------------

export default function AccountNotifications() {
  const {
    enqueueSnackbar
  } = useSnackbar();
  const defaultValues = {
    activityComments: NOTIFICATION_SETTINGS.activityComments,
    activityAnswers: NOTIFICATION_SETTINGS.activityAnswers,
    activityFollows: NOTIFICATION_SETTINGS.activityFollows,
    applicationNews: NOTIFICATION_SETTINGS.applicationNews,
    applicationProduct: NOTIFICATION_SETTINGS.applicationProduct,
    applicationBlog: NOTIFICATION_SETTINGS.applicationBlog
  };
  const methods = useForm({
    defaultValues
  });
  const {
    handleSubmit,
    formState: {
      isSubmitting
    }
  } = methods;

  const onSubmit = async () => {
    try {
      await new Promise(resolve => setTimeout(resolve, 500));
      enqueueSnackbar('Update success!');
    } catch (error) {
      console.error(error);
    }
  };

  return /*#__PURE__*/React.createElement(Card, {
    sx: {
      p: 3
    }
  }, /*#__PURE__*/React.createElement(FormProvider, {
    methods: methods,
    onSubmit: handleSubmit(onSubmit)
  }, /*#__PURE__*/React.createElement(Stack, {
    spacing: 3,
    alignItems: "flex-end"
  }, /*#__PURE__*/React.createElement(Stack, {
    spacing: 2,
    sx: {
      width: 1
    }
  }, /*#__PURE__*/React.createElement(Typography, {
    variant: "overline",
    sx: {
      color: 'text.secondary'
    }
  }, "Activity"), /*#__PURE__*/React.createElement(Stack, {
    spacing: 1
  }, ACTIVITY_OPTIONS.map(activity => /*#__PURE__*/React.createElement(RHFSwitch, {
    key: activity.value,
    name: activity.value,
    label: activity.label,
    sx: {
      m: 0
    }
  })))), /*#__PURE__*/React.createElement(Stack, {
    spacing: 2,
    sx: {
      width: 1
    }
  }, /*#__PURE__*/React.createElement(Typography, {
    variant: "overline",
    sx: {
      color: 'text.secondary'
    }
  }, "Application"), /*#__PURE__*/React.createElement(Stack, {
    spacing: 1
  }, APPLICATION_OPTIONS.map(application => /*#__PURE__*/React.createElement(RHFSwitch, {
    key: application.value,
    name: application.value,
    label: application.label,
    sx: {
      m: 0
    }
  })))), /*#__PURE__*/React.createElement(LoadingButton, {
    type: "submit",
    variant: "contained",
    loading: isSubmitting
  }, "Save Changes"))));
}