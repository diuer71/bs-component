import PropTypes from 'prop-types'; // @mui

import { Box, Card, Button, Typography, Stack, Paper } from '@mui/material'; // components

import Iconify from '../../../../components/Iconify'; // ----------------------------------------------------------------------

AccountBillingAddressBook.propTypes = {
  addressBook: PropTypes.array
};
export default function AccountBillingAddressBook({
  addressBook
}) {
  return /*#__PURE__*/React.createElement(Card, {
    sx: {
      p: 3
    }
  }, /*#__PURE__*/React.createElement(Stack, {
    spacing: 3,
    alignItems: "flex-start"
  }, /*#__PURE__*/React.createElement(Typography, {
    variant: "overline",
    sx: {
      color: 'text.secondary'
    }
  }, "Billing Info"), addressBook.map(address => /*#__PURE__*/React.createElement(Paper, {
    key: address.id,
    sx: {
      p: 3,
      width: 1,
      bgcolor: 'background.neutral'
    }
  }, /*#__PURE__*/React.createElement(Typography, {
    variant: "subtitle1",
    gutterBottom: true
  }, address.name), /*#__PURE__*/React.createElement(Typography, {
    variant: "body2",
    gutterBottom: true
  }, /*#__PURE__*/React.createElement(Typography, {
    variant: "body2",
    component: "span",
    sx: {
      color: 'text.secondary'
    }
  }, "Address: \xA0"), `${address.street}, ${address.city}, ${address.state}, ${address.country} ${address.zipCode}`), /*#__PURE__*/React.createElement(Typography, {
    variant: "body2",
    gutterBottom: true
  }, /*#__PURE__*/React.createElement(Typography, {
    variant: "body2",
    component: "span",
    sx: {
      color: 'text.secondary'
    }
  }, "Phone: \xA0"), address.phone), /*#__PURE__*/React.createElement(Box, {
    sx: {
      mt: 1
    }
  }, /*#__PURE__*/React.createElement(Button, {
    color: "error",
    size: "small",
    startIcon: /*#__PURE__*/React.createElement(Iconify, {
      icon: 'eva:trash-2-outline'
    }),
    onClick: () => {},
    sx: {
      mr: 1
    }
  }, "Delete"), /*#__PURE__*/React.createElement(Button, {
    size: "small",
    startIcon: /*#__PURE__*/React.createElement(Iconify, {
      icon: 'eva:edit-fill'
    }),
    onClick: () => {}
  }, "Edit")))), /*#__PURE__*/React.createElement(Button, {
    size: "small",
    startIcon: /*#__PURE__*/React.createElement(Iconify, {
      icon: 'eva:plus-fill'
    })
  }, "Add new address")));
}