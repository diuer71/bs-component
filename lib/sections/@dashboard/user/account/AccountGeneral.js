import * as Yup from 'yup';
import { useSnackbar } from 'notistack';
import { useCallback } from 'react'; // form

import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup'; // @mui

import { Box, Grid, Card, Stack, Typography } from '@mui/material';
import { LoadingButton } from '@mui/lab'; // hooks

import useAuth from '../../../../hooks/useAuth'; // utils

import { fData } from '../../../../utils/formatNumber'; // _mock

import { countries } from '../../../../_mock'; // components

import { FormProvider, RHFSwitch, RHFSelect, RHFTextField, RHFUploadAvatar } from '../../../../components/hook-form'; // ----------------------------------------------------------------------

export default function AccountGeneral() {
  const {
    enqueueSnackbar
  } = useSnackbar();
  const {
    user
  } = useAuth();
  const UpdateUserSchema = Yup.object().shape({
    displayName: Yup.string().required('Name is required')
  });
  const defaultValues = {
    displayName: user?.displayName || '',
    email: user?.email || '',
    photoURL: user?.photoURL || '',
    phoneNumber: user?.phoneNumber || '',
    country: user?.country || '',
    address: user?.address || '',
    state: user?.state || '',
    city: user?.city || '',
    zipCode: user?.zipCode || '',
    about: user?.about || '',
    isPublic: user?.isPublic || ''
  };
  const methods = useForm({
    resolver: yupResolver(UpdateUserSchema),
    defaultValues
  });
  const {
    setValue,
    handleSubmit,
    formState: {
      isSubmitting
    }
  } = methods;

  const onSubmit = async () => {
    try {
      await new Promise(resolve => setTimeout(resolve, 500));
      enqueueSnackbar('Update success!');
    } catch (error) {
      console.error(error);
    }
  };

  const handleDrop = useCallback(acceptedFiles => {
    const file = acceptedFiles[0];

    if (file) {
      setValue('photoURL', Object.assign(file, {
        preview: URL.createObjectURL(file)
      }));
    }
  }, [setValue]);
  return /*#__PURE__*/React.createElement(FormProvider, {
    methods: methods,
    onSubmit: handleSubmit(onSubmit)
  }, /*#__PURE__*/React.createElement(Grid, {
    container: true,
    spacing: 3
  }, /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: 12,
    md: 4
  }, /*#__PURE__*/React.createElement(Card, {
    sx: {
      py: 10,
      px: 3,
      textAlign: 'center'
    }
  }, /*#__PURE__*/React.createElement(RHFUploadAvatar, {
    name: "photoURL",
    accept: "image/*",
    maxSize: 3145728,
    onDrop: handleDrop,
    helperText: /*#__PURE__*/React.createElement(Typography, {
      variant: "caption",
      sx: {
        mt: 2,
        mx: 'auto',
        display: 'block',
        textAlign: 'center',
        color: 'text.secondary'
      }
    }, "Allowed *.jpeg, *.jpg, *.png, *.gif", /*#__PURE__*/React.createElement("br", null), " max size of ", fData(3145728))
  }), /*#__PURE__*/React.createElement(RHFSwitch, {
    name: "isPublic",
    labelPlacement: "start",
    label: "Public Profile",
    sx: {
      mt: 5
    }
  }))), /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: 12,
    md: 8
  }, /*#__PURE__*/React.createElement(Card, {
    sx: {
      p: 3
    }
  }, /*#__PURE__*/React.createElement(Box, {
    sx: {
      display: 'grid',
      rowGap: 3,
      columnGap: 2,
      gridTemplateColumns: {
        xs: 'repeat(1, 1fr)',
        sm: 'repeat(2, 1fr)'
      }
    }
  }, /*#__PURE__*/React.createElement(RHFTextField, {
    name: "displayName",
    label: "Name"
  }), /*#__PURE__*/React.createElement(RHFTextField, {
    name: "email",
    label: "Email Address"
  }), /*#__PURE__*/React.createElement(RHFTextField, {
    name: "phoneNumber",
    label: "Phone Number"
  }), /*#__PURE__*/React.createElement(RHFTextField, {
    name: "address",
    label: "Address"
  }), /*#__PURE__*/React.createElement(RHFSelect, {
    name: "country",
    label: "Country",
    placeholder: "Country"
  }, /*#__PURE__*/React.createElement("option", {
    value: ""
  }), countries.map(option => /*#__PURE__*/React.createElement("option", {
    key: option.code,
    value: option.label
  }, option.label))), /*#__PURE__*/React.createElement(RHFTextField, {
    name: "state",
    label: "State/Region"
  }), /*#__PURE__*/React.createElement(RHFTextField, {
    name: "city",
    label: "City"
  }), /*#__PURE__*/React.createElement(RHFTextField, {
    name: "zipCode",
    label: "Zip/Code"
  })), /*#__PURE__*/React.createElement(Stack, {
    spacing: 3,
    alignItems: "flex-end",
    sx: {
      mt: 3
    }
  }, /*#__PURE__*/React.createElement(RHFTextField, {
    name: "about",
    multiline: true,
    rows: 4,
    label: "About"
  }), /*#__PURE__*/React.createElement(LoadingButton, {
    type: "submit",
    variant: "contained",
    loading: isSubmitting
  }, "Save Changes"))))));
}