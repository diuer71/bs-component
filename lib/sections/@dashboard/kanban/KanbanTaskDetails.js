import PropTypes from 'prop-types';
import { useState, useRef } from 'react'; // @mui

import { MobileDateRangePicker } from '@mui/lab';
import { styled } from '@mui/material/styles';
import { Box, Stack, Drawer, Button, Avatar, Tooltip, Divider, MenuItem, TextField, Typography, OutlinedInput } from '@mui/material'; // hooks

import useResponsive from '../../../hooks/useResponsive'; // components

import Iconify from '../../../components/Iconify';
import Scrollbar from '../../../components/Scrollbar';
import { IconButtonAnimate } from '../../../components/animate'; //

import KanbanTaskCommentList from './KanbanTaskCommentList';
import KanbanTaskAttachments from './KanbanTaskAttachments';
import KanbanTaskCommentInput from './KanbanTaskCommentInput';
import { useDatePicker, DisplayTime } from './KanbanTaskAdd'; // ----------------------------------------------------------------------

const PRIORITIZES = ['low', 'medium', 'hight'];
const LabelStyle = styled(Typography)(({
  theme
}) => ({ ...theme.typography.body2,
  width: 140,
  fontSize: 13,
  flexShrink: 0,
  color: theme.palette.text.secondary
})); // ----------------------------------------------------------------------

KanbanTaskDetails.propTypes = {
  isOpen: PropTypes.bool,
  onClose: PropTypes.func,
  card: PropTypes.object,
  onDeleteTask: PropTypes.func
};
export default function KanbanTaskDetails({
  card,
  isOpen,
  onClose,
  onDeleteTask
}) {
  const isDesktop = useResponsive('up', 'sm');
  const fileInputRef = useRef(null);
  const [taskCompleted, setTaskCompleted] = useState(card.completed);
  const [prioritize, setPrioritize] = useState('low');
  const {
    name,
    description,
    due,
    assignee,
    attachments,
    comments
  } = card;
  const {
    dueDate,
    startTime,
    endTime,
    isSameDays,
    isSameMonths,
    onChangeDueDate,
    openPicker,
    onOpenPicker,
    onClosePicker
  } = useDatePicker({
    date: due
  });

  const handleAttach = () => {
    fileInputRef.current?.click();
  };

  const handleToggleCompleted = () => {
    setTaskCompleted(prev => !prev);
  };

  const handleChangePrioritize = event => {
    setPrioritize(event.target.value);
  };

  return /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement(Drawer, {
    open: isOpen,
    onClose: onClose,
    anchor: "right",
    PaperProps: {
      sx: {
        width: {
          xs: 1,
          sm: 480
        }
      }
    }
  }, /*#__PURE__*/React.createElement(Stack, {
    p: 2.5,
    direction: "row",
    alignItems: "center"
  }, !isDesktop && /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement(Tooltip, {
    title: "Back"
  }, /*#__PURE__*/React.createElement(IconButtonAnimate, {
    onClick: onClose,
    sx: {
      mr: 1
    }
  }, /*#__PURE__*/React.createElement(Iconify, {
    icon: 'eva:arrow-ios-back-fill',
    width: 20,
    height: 20
  })))), /*#__PURE__*/React.createElement(Button, {
    size: "small",
    variant: "outlined",
    color: taskCompleted ? 'primary' : 'inherit',
    startIcon: !taskCompleted && /*#__PURE__*/React.createElement(Iconify, {
      icon: 'eva:checkmark-fill',
      width: 16,
      height: 16
    }),
    onClick: handleToggleCompleted
  }, taskCompleted ? 'Complete' : 'Mark complete'), /*#__PURE__*/React.createElement(Stack, {
    direction: "row",
    spacing: 1,
    justifyContent: "flex-end",
    flexGrow: 1
  }, /*#__PURE__*/React.createElement(Tooltip, {
    title: "Like this"
  }, /*#__PURE__*/React.createElement(IconButtonAnimate, {
    size: "small"
  }, /*#__PURE__*/React.createElement(Iconify, {
    icon: 'ic:round-thumb-up',
    width: 20,
    height: 20
  }))), /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement(Tooltip, {
    title: "Attachment"
  }, /*#__PURE__*/React.createElement(IconButtonAnimate, {
    size: "small",
    onClick: handleAttach
  }, /*#__PURE__*/React.createElement(Iconify, {
    icon: 'eva:attach-2-fill',
    width: 20,
    height: 20
  }))), /*#__PURE__*/React.createElement("input", {
    ref: fileInputRef,
    type: "file",
    style: {
      display: 'none'
    }
  })), /*#__PURE__*/React.createElement(Tooltip, {
    title: "Delete task"
  }, /*#__PURE__*/React.createElement(IconButtonAnimate, {
    onClick: onDeleteTask,
    size: "small"
  }, /*#__PURE__*/React.createElement(Iconify, {
    icon: 'eva:trash-2-outline',
    width: 20,
    height: 20
  }))), /*#__PURE__*/React.createElement(Tooltip, {
    title: "More actions"
  }, /*#__PURE__*/React.createElement(IconButtonAnimate, {
    size: "small"
  }, /*#__PURE__*/React.createElement(Iconify, {
    icon: 'eva:more-horizontal-fill',
    width: 20,
    height: 20
  }))))), /*#__PURE__*/React.createElement(Divider, null), /*#__PURE__*/React.createElement(Scrollbar, null, /*#__PURE__*/React.createElement(Stack, {
    spacing: 3,
    sx: {
      px: 2.5,
      py: 3
    }
  }, /*#__PURE__*/React.createElement(OutlinedInput, {
    fullWidth: true,
    multiline: true,
    size: "small",
    placeholder: "Task name",
    value: name,
    sx: {
      typography: 'h6',
      '& .MuiOutlinedInput-notchedOutline': {
        borderColor: 'transparent'
      }
    }
  }), /*#__PURE__*/React.createElement(Stack, {
    direction: "row"
  }, /*#__PURE__*/React.createElement(LabelStyle, {
    sx: {
      mt: 1.5
    }
  }, "Assignee"), /*#__PURE__*/React.createElement(Stack, {
    direction: "row",
    flexWrap: "wrap",
    alignItems: "center"
  }, assignee.map(user => /*#__PURE__*/React.createElement(Avatar, {
    key: user.id,
    alt: user.name,
    src: user.avatar,
    sx: {
      m: 0.5,
      width: 36,
      height: 36
    }
  })), /*#__PURE__*/React.createElement(Tooltip, {
    title: "Add assignee"
  }, /*#__PURE__*/React.createElement(IconButtonAnimate, {
    sx: {
      p: 1,
      ml: 0.5,
      border: theme => `dashed 1px ${theme.palette.divider}`
    }
  }, /*#__PURE__*/React.createElement(Iconify, {
    icon: 'eva:plus-fill',
    width: 20,
    height: 20
  }))))), /*#__PURE__*/React.createElement(Stack, {
    direction: "row",
    alignItems: "center"
  }, /*#__PURE__*/React.createElement(LabelStyle, null, " Due date"), /*#__PURE__*/React.createElement(React.Fragment, null, startTime && endTime ? /*#__PURE__*/React.createElement(DisplayTime, {
    startTime: startTime,
    endTime: endTime,
    isSameDays: isSameDays,
    isSameMonths: isSameMonths,
    onOpenPicker: onOpenPicker,
    sx: {
      typography: 'body2'
    }
  }) : /*#__PURE__*/React.createElement(Tooltip, {
    title: "Add assignee"
  }, /*#__PURE__*/React.createElement(IconButtonAnimate, {
    onClick: onOpenPicker,
    sx: {
      p: 1,
      ml: 0.5,
      border: theme => `dashed 1px ${theme.palette.divider}`
    }
  }, /*#__PURE__*/React.createElement(Iconify, {
    icon: 'eva:plus-fill',
    width: 20,
    height: 20
  }))), /*#__PURE__*/React.createElement(MobileDateRangePicker, {
    open: openPicker,
    onClose: onClosePicker,
    onOpen: onOpenPicker,
    value: dueDate,
    onChange: onChangeDueDate,
    renderInput: () => {}
  }))), /*#__PURE__*/React.createElement(Stack, {
    direction: "row",
    alignItems: "center"
  }, /*#__PURE__*/React.createElement(LabelStyle, null, "Prioritize"), /*#__PURE__*/React.createElement(TextField, {
    fullWidth: true,
    select: true,
    size: "small",
    value: prioritize,
    onChange: handleChangePrioritize,
    sx: {
      '& svg': {
        display: 'none'
      },
      '& fieldset': {
        display: 'none'
      },
      '& .MuiSelect-select': {
        p: 0,
        display: 'flex',
        alignItems: 'center'
      }
    }
  }, PRIORITIZES.map(option => /*#__PURE__*/React.createElement(MenuItem, {
    key: option,
    value: option,
    sx: {
      mx: 1,
      my: 0.5,
      borderRadius: 1
    }
  }, /*#__PURE__*/React.createElement(Box, {
    sx: {
      mr: 1,
      width: 14,
      height: 14,
      borderRadius: 0.5,
      bgcolor: 'error.main',
      ...(option === 'low' && {
        bgcolor: 'info.main'
      }),
      ...(option === 'medium' && {
        bgcolor: 'warning.main'
      })
    }
  }), /*#__PURE__*/React.createElement(Typography, {
    variant: "body2",
    sx: {
      textTransform: 'capitalize'
    }
  }, option))))), /*#__PURE__*/React.createElement(Stack, {
    direction: "row"
  }, /*#__PURE__*/React.createElement(LabelStyle, {
    sx: {
      mt: 2
    }
  }, "Description"), /*#__PURE__*/React.createElement(OutlinedInput, {
    fullWidth: true,
    multiline: true,
    rows: 3,
    size: "small",
    placeholder: "Task name",
    value: description,
    sx: {
      typography: 'body2'
    }
  })), /*#__PURE__*/React.createElement(Stack, {
    direction: "row"
  }, /*#__PURE__*/React.createElement(LabelStyle, {
    sx: {
      mt: 2
    }
  }, "Attachments"), /*#__PURE__*/React.createElement(Stack, {
    direction: "row",
    flexWrap: "wrap"
  }, /*#__PURE__*/React.createElement(KanbanTaskAttachments, {
    attachments: attachments
  })))), comments.length > 0 && /*#__PURE__*/React.createElement(KanbanTaskCommentList, {
    comments: comments
  })), /*#__PURE__*/React.createElement(Divider, null), /*#__PURE__*/React.createElement(KanbanTaskCommentInput, null)));
}