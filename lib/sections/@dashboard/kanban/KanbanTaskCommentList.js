import PropTypes from 'prop-types';
import { useState } from 'react'; // @mui

import { Stack, Avatar, Typography } from '@mui/material'; // utils

import { fToNow } from '../../../utils/formatTime'; // components

import Image from '../../../components/Image';
import LightboxModal from '../../../components/LightboxModal'; // ----------------------------------------------------------------------

KanbanTaskCommentList.propTypes = {
  comments: PropTypes.array
};
export default function KanbanTaskCommentList({
  comments
}) {
  const [openLightbox, setOpenLightbox] = useState(false);
  const [selectedImage, setSelectedImage] = useState(0);
  const imagesLightbox = comments.filter(comment => comment.messageType === 'image').map(item => item.message);

  const handleOpenLightbox = url => {
    const selectedImage = imagesLightbox.findIndex(index => index === url);
    setOpenLightbox(true);
    setSelectedImage(selectedImage);
  };

  return /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement(Stack, {
    spacing: 3,
    sx: {
      py: 3,
      px: 2.5,
      bgcolor: 'background.neutral'
    }
  }, comments.map(comment => /*#__PURE__*/React.createElement(Stack, {
    key: comment.id,
    direction: "row",
    spacing: 2
  }, /*#__PURE__*/React.createElement(Avatar, {
    src: comment.avatar,
    sx: {
      width: 32,
      height: 32
    }
  }), /*#__PURE__*/React.createElement("div", null, /*#__PURE__*/React.createElement(Stack, {
    direction: "row",
    alignItems: "center",
    spacing: 1
  }, /*#__PURE__*/React.createElement(Typography, {
    variant: "subtitle2"
  }, " ", comment.name), /*#__PURE__*/React.createElement(Typography, {
    variant: "caption",
    sx: {
      color: 'text.secondary'
    }
  }, fToNow(comment.createdAt))), comment.messageType === 'image' ? /*#__PURE__*/React.createElement(Image, {
    src: comment.message,
    onClick: () => handleOpenLightbox(comment.message),
    sx: {
      mt: 2,
      borderRadius: 1
    }
  }) : /*#__PURE__*/React.createElement(Typography, {
    variant: "body2",
    sx: {
      mt: 0.5
    }
  }, comment.message))))), /*#__PURE__*/React.createElement(LightboxModal, {
    images: imagesLightbox,
    mainSrc: imagesLightbox[selectedImage],
    photoIndex: selectedImage,
    setPhotoIndex: setSelectedImage,
    isOpen: openLightbox,
    onCloseRequest: () => setOpenLightbox(false)
  }));
}