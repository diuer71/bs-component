import PropTypes from 'prop-types';
import { format, isSameDay, isSameMonth } from 'date-fns';
import { useState } from 'react'; // @mui

import { Box, Paper, Stack, Tooltip, Checkbox, IconButton, OutlinedInput, ClickAwayListener } from '@mui/material';
import { MobileDateRangePicker } from '@mui/lab'; // utils

import uuidv4 from '../../../utils/uuidv4'; // components

import Iconify from '../../../components/Iconify'; // ----------------------------------------------------------------------

const defaultTask = {
  attachments: [],
  comments: [],
  description: '',
  due: [null, null],
  assignee: []
};
KanbanTaskAdd.propTypes = {
  onAddTask: PropTypes.func,
  onCloseAddTask: PropTypes.func
};
export default function KanbanTaskAdd({
  onAddTask,
  onCloseAddTask
}) {
  const [name, setName] = useState('');
  const [completed, setCompleted] = useState(false);
  const {
    dueDate,
    startTime,
    endTime,
    isSameDays,
    isSameMonths,
    onChangeDueDate,
    openPicker,
    onOpenPicker,
    onClosePicker
  } = useDatePicker({
    date: [null, null]
  });

  const handleKeyUpAddTask = event => {
    if (event.key === 'Enter') {
      if (name.trim() !== '') {
        onAddTask({ ...defaultTask,
          id: uuidv4(),
          name,
          due: dueDate,
          completed
        });
      }
    }
  };

  const handleClickAddTask = () => {
    if (name) {
      onAddTask({ ...defaultTask,
        id: uuidv4(),
        name,
        due: dueDate,
        completed
      });
    }

    onCloseAddTask();
  };

  const handleChangeCompleted = event => {
    setCompleted(event.target.checked);
  };

  return /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement(ClickAwayListener, {
    onClickAway: handleClickAddTask
  }, /*#__PURE__*/React.createElement(Paper, {
    variant: "outlined",
    sx: {
      p: 2
    }
  }, /*#__PURE__*/React.createElement(OutlinedInput, {
    multiline: true,
    size: "small",
    placeholder: "Task name",
    value: name,
    onChange: event => setName(event.target.value),
    onKeyUp: handleKeyUpAddTask,
    sx: {
      '& input': {
        p: 0
      },
      '& fieldset': {
        borderColor: 'transparent !important'
      }
    }
  }), /*#__PURE__*/React.createElement(Stack, {
    direction: "row",
    justifyContent: "space-between"
  }, /*#__PURE__*/React.createElement(Tooltip, {
    title: "Mark task complete"
  }, /*#__PURE__*/React.createElement(Checkbox, {
    disableRipple: true,
    checked: completed,
    onChange: handleChangeCompleted,
    icon: /*#__PURE__*/React.createElement(Iconify, {
      icon: 'eva:radio-button-off-outline'
    }),
    checkedIcon: /*#__PURE__*/React.createElement(Iconify, {
      icon: 'eva:checkmark-circle-2-outline'
    })
  })), /*#__PURE__*/React.createElement(Stack, {
    direction: "row",
    spacing: 1.5,
    alignItems: "center"
  }, /*#__PURE__*/React.createElement(Tooltip, {
    title: "Assign this task"
  }, /*#__PURE__*/React.createElement(IconButton, {
    size: "small"
  }, /*#__PURE__*/React.createElement(Iconify, {
    icon: 'eva:people-fill',
    width: 20,
    height: 20
  }))), startTime && endTime ? /*#__PURE__*/React.createElement(DisplayTime, {
    startTime: startTime,
    endTime: endTime,
    isSameDays: isSameDays,
    isSameMonths: isSameMonths,
    onOpenPicker: onOpenPicker
  }) : /*#__PURE__*/React.createElement(Tooltip, {
    title: "Add due date"
  }, /*#__PURE__*/React.createElement(IconButton, {
    size: "small",
    onClick: onOpenPicker
  }, /*#__PURE__*/React.createElement(Iconify, {
    icon: 'eva:calendar-fill',
    width: 20,
    height: 20
  }))), /*#__PURE__*/React.createElement(MobileDateRangePicker, {
    open: openPicker,
    onClose: onClosePicker,
    onOpen: onOpenPicker,
    value: dueDate,
    onChange: onChangeDueDate,
    renderInput: () => {}
  }))))));
} // ----------------------------------------------------------------------

export function useDatePicker({
  date
}) {
  const [dueDate, setDueDate] = useState([date[0], date[1]]);
  const [openPicker, setOpenPicker] = useState(false);
  const startTime = dueDate[0] || '';
  const endTime = dueDate[1] || '';
  const isSameDays = isSameDay(new Date(startTime), new Date(endTime));
  const isSameMonths = isSameMonth(new Date(startTime), new Date(endTime));

  const handleChangeDueDate = newValue => {
    setDueDate(newValue);
  };

  const handleOpenPicker = () => {
    setOpenPicker(true);
  };

  const handleClosePicker = () => {
    setOpenPicker(false);
  };

  return {
    dueDate,
    startTime,
    endTime,
    isSameDays,
    isSameMonths,
    onChangeDueDate: handleChangeDueDate,
    openPicker,
    onOpenPicker: handleOpenPicker,
    onClosePicker: handleClosePicker
  };
} // ----------------------------------------------------------------------

DisplayTime.propTypes = {
  isSameDays: PropTypes.bool,
  isSameMonths: PropTypes.bool,
  onOpenPicker: PropTypes.func,
  startTime: PropTypes.oneOfType([PropTypes.string, PropTypes.number, PropTypes.instanceOf(Date)]),
  endTime: PropTypes.oneOfType([PropTypes.string, PropTypes.number, PropTypes.instanceOf(Date)]),
  sx: PropTypes.object
};
export function DisplayTime({
  startTime,
  endTime,
  isSameDays,
  isSameMonths,
  onOpenPicker,
  sx
}) {
  const style = {
    typography: 'caption',
    cursor: 'pointer',
    '&:hover': {
      opacity: 0.72
    }
  };

  if (isSameMonths) {
    return /*#__PURE__*/React.createElement(Box, {
      onClick: onOpenPicker,
      sx: { ...style,
        ...sx
      }
    }, isSameDays ? format(new Date(endTime), 'dd MMM') : `${format(new Date(startTime), 'dd')} - ${format(new Date(endTime), 'dd MMM')}`);
  }

  return /*#__PURE__*/React.createElement(Box, {
    onClick: onOpenPicker,
    sx: { ...style,
      ...sx
    }
  }, format(new Date(startTime), 'dd MMM'), " - ", format(new Date(endTime), 'dd MMM'));
}