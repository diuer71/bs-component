function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

import PropTypes from 'prop-types';
import { useState } from 'react';
import { Draggable } from 'react-beautiful-dnd'; // @mui

import { Paper, Typography, Box, Checkbox } from '@mui/material'; // components

import Image from '../../../components/Image';
import Iconify from '../../../components/Iconify'; //

import KanbanTaskDetails from './KanbanTaskDetails'; // ----------------------------------------------------------------------

KanbanTaskCard.propTypes = {
  card: PropTypes.object,
  index: PropTypes.number,
  onDeleteTask: PropTypes.func
};
export default function KanbanTaskCard({
  card,
  onDeleteTask,
  index
}) {
  const {
    name,
    attachments
  } = card;
  const [openDetails, setOpenDetails] = useState(false);
  const [completed, setCompleted] = useState(card.completed);

  const handleOpenDetails = () => {
    setOpenDetails(true);
  };

  const handleCloseDetails = () => {
    setOpenDetails(false);
  };

  const handleChangeComplete = event => {
    setCompleted(event.target.checked);
  };

  return /*#__PURE__*/React.createElement(Draggable, {
    draggableId: card.id,
    index: index
  }, provided => /*#__PURE__*/React.createElement("div", _extends({}, provided.draggableProps, provided.dragHandleProps, {
    ref: provided.innerRef
  }), /*#__PURE__*/React.createElement(Paper, {
    sx: {
      px: 2,
      width: 1,
      position: 'relative',
      boxShadow: theme => theme.customShadows.z1,
      '&:hover': {
        boxShadow: theme => theme.customShadows.z16
      },
      ...(attachments.length > 0 && {
        pt: 2
      })
    }
  }, /*#__PURE__*/React.createElement(Box, {
    onClick: handleOpenDetails,
    sx: {
      cursor: 'pointer'
    }
  }, attachments.length > 0 && /*#__PURE__*/React.createElement(Box, {
    sx: {
      pt: '60%',
      borderRadius: 1,
      overflow: 'hidden',
      position: 'relative',
      transition: theme => theme.transitions.create('opacity', {
        duration: theme.transitions.duration.shortest
      }),
      ...(completed && {
        opacity: 0.48
      })
    }
  }, /*#__PURE__*/React.createElement(Image, {
    src: attachments[0],
    sx: {
      position: 'absolute',
      top: 0,
      width: 1,
      height: 1
    }
  })), /*#__PURE__*/React.createElement(Typography, {
    noWrap: true,
    variant: "subtitle2",
    sx: {
      py: 3,
      pl: 5,
      transition: theme => theme.transitions.create('opacity', {
        duration: theme.transitions.duration.shortest
      }),
      ...(completed && {
        opacity: 0.48
      })
    }
  }, name)), /*#__PURE__*/React.createElement(Checkbox, {
    disableRipple: true,
    checked: completed,
    icon: /*#__PURE__*/React.createElement(Iconify, {
      icon: 'eva:radio-button-off-outline'
    }),
    checkedIcon: /*#__PURE__*/React.createElement(Iconify, {
      icon: 'eva:checkmark-circle-2-outline'
    }),
    onChange: handleChangeComplete,
    sx: {
      position: 'absolute',
      bottom: 15
    }
  })), /*#__PURE__*/React.createElement(KanbanTaskDetails, {
    card: card,
    isOpen: openDetails,
    onClose: handleCloseDetails,
    onDeleteTask: () => onDeleteTask(card.id)
  })));
}