// @mui
import { Stack, Paper, Button, Tooltip, OutlinedInput, IconButton } from '@mui/material'; // components

import Iconify from '../../../components/Iconify';
import MyAvatar from '../../../components/MyAvatar'; // ----------------------------------------------------------------------

export default function KanbanTaskCommentInput() {
  return /*#__PURE__*/React.createElement(Stack, {
    direction: "row",
    spacing: 2,
    sx: {
      py: 3,
      px: 2.5
    }
  }, /*#__PURE__*/React.createElement(MyAvatar, null), /*#__PURE__*/React.createElement(Paper, {
    variant: "outlined",
    sx: {
      p: 1,
      flexGrow: 1
    }
  }, /*#__PURE__*/React.createElement(OutlinedInput, {
    fullWidth: true,
    multiline: true,
    rows: 2,
    placeholder: "Type a message",
    sx: {
      '& fieldset': {
        display: 'none'
      }
    }
  }), /*#__PURE__*/React.createElement(Stack, {
    direction: "row",
    justifyContent: "space-between",
    alignItems: "center"
  }, /*#__PURE__*/React.createElement(Stack, {
    direction: "row",
    spacing: 0.5
  }, /*#__PURE__*/React.createElement(Tooltip, {
    title: "Add photo"
  }, /*#__PURE__*/React.createElement(IconButton, {
    size: "small"
  }, /*#__PURE__*/React.createElement(Iconify, {
    icon: 'ic:round-add-photo-alternate',
    width: 20,
    height: 20
  }))), /*#__PURE__*/React.createElement(IconButton, {
    size: "small"
  }, /*#__PURE__*/React.createElement(Iconify, {
    icon: 'eva:attach-2-fill',
    width: 20,
    height: 20
  }))), /*#__PURE__*/React.createElement(Button, {
    variant: "contained"
  }, "Comment"))));
}