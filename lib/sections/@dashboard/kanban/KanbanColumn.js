function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

import PropTypes from 'prop-types';
import { useState } from 'react';
import { useSnackbar } from 'notistack';
import { useSelector } from 'react-redux';
import { Droppable, Draggable } from 'react-beautiful-dnd'; // @mui

import { Paper, Stack, Button } from '@mui/material'; // redux

import { useDispatch } from '../../../redux/store';
import { deleteColumn, updateColumn, addTask, deleteTask } from '../../../redux/slices/kanban'; // components

import Iconify from '../../../components/Iconify'; //

import KanbanAddTask from './KanbanTaskAdd';
import KanbanTaskCard from './KanbanTaskCard';
import KanbanColumnToolBar from './KanbanColumnToolBar'; // ----------------------------------------------------------------------

KanbanColumn.propTypes = {
  column: PropTypes.object,
  index: PropTypes.number
};
export default function KanbanColumn({
  column,
  index
}) {
  const dispatch = useDispatch();
  const {
    enqueueSnackbar
  } = useSnackbar();
  const {
    board
  } = useSelector(state => state.kanban);
  const [open, setOpen] = useState(false);
  const {
    name,
    cardIds,
    id
  } = column;

  const handleOpenAddTask = () => {
    setOpen(prev => !prev);
  };

  const handleCloseAddTask = () => {
    setOpen(false);
  };

  const handleDeleteTask = cardId => {
    dispatch(deleteTask({
      cardId,
      columnId: id
    }));
  };

  const handleUpdateColumn = async newName => {
    try {
      if (newName !== name) {
        dispatch(updateColumn(id, { ...column,
          name: newName
        }));
        enqueueSnackbar('Update success!');
      }
    } catch (error) {
      console.error(error);
    }
  };

  const handleDeleteColumn = async () => {
    try {
      dispatch(deleteColumn(id));
    } catch (error) {
      console.error(error);
    }
  };

  const handleAddTask = task => {
    dispatch(addTask({
      card: task,
      columnId: id
    }));
    handleCloseAddTask();
  };

  return /*#__PURE__*/React.createElement(Draggable, {
    draggableId: id,
    index: index
  }, provided => /*#__PURE__*/React.createElement(Paper, _extends({}, provided.draggableProps, {
    ref: provided.innerRef,
    variant: "outlined",
    sx: {
      px: 2,
      bgcolor: 'grey.5008'
    }
  }), /*#__PURE__*/React.createElement(Stack, _extends({
    spacing: 3
  }, provided.dragHandleProps), /*#__PURE__*/React.createElement(KanbanColumnToolBar, {
    columnName: name,
    onDelete: handleDeleteColumn,
    onUpdate: handleUpdateColumn
  }), /*#__PURE__*/React.createElement(Droppable, {
    droppableId: id,
    type: "task"
  }, provided => /*#__PURE__*/React.createElement(Stack, _extends({
    ref: provided.innerRef
  }, provided.droppableProps, {
    spacing: 2,
    width: 280
  }), cardIds.map((cardId, index) => /*#__PURE__*/React.createElement(KanbanTaskCard, {
    key: cardId,
    onDeleteTask: handleDeleteTask,
    card: board?.cards[cardId],
    index: index
  })), provided.placeholder)), /*#__PURE__*/React.createElement(Stack, {
    spacing: 2,
    sx: {
      pb: 3
    }
  }, open && /*#__PURE__*/React.createElement(KanbanAddTask, {
    onAddTask: handleAddTask,
    onCloseAddTask: handleCloseAddTask
  }), /*#__PURE__*/React.createElement(Button, {
    fullWidth: true,
    size: "large",
    color: "inherit",
    startIcon: /*#__PURE__*/React.createElement(Iconify, {
      icon: 'eva:plus-fill',
      width: 20,
      height: 20
    }),
    onClick: handleOpenAddTask,
    sx: {
      fontSize: 14
    }
  }, "Add Task")))));
}