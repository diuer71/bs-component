function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

import PropTypes from 'prop-types';
import * as Yup from 'yup';
import merge from 'lodash/merge';
import { isBefore } from 'date-fns';
import { useSnackbar } from 'notistack'; // form

import { useForm, Controller } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup'; // @mui

import { Box, Stack, Button, Tooltip, TextField, IconButton, DialogActions } from '@mui/material';
import { LoadingButton, MobileDateTimePicker } from '@mui/lab'; // redux

import { useDispatch } from '../../../redux/store';
import { createEvent, updateEvent, deleteEvent } from '../../../redux/slices/calendar'; // components

import Iconify from '../../../components/Iconify';
import { ColorSinglePicker } from '../../../components/color-utils';
import { FormProvider, RHFTextField, RHFSwitch } from '../../../components/hook-form'; // ----------------------------------------------------------------------

const COLOR_OPTIONS = ['#00AB55', // theme.palette.primary.main,
'#1890FF', // theme.palette.info.main,
'#54D62C', // theme.palette.success.main,
'#FFC107', // theme.palette.warning.main,
'#FF4842', // theme.palette.error.main
'#04297A', // theme.palette.info.darker
'#7A0C2E' // theme.palette.error.darker
];

const getInitialValues = (event, range) => {
  const _event = {
    title: '',
    description: '',
    textColor: '#1890FF',
    allDay: false,
    start: range ? new Date(range.start) : new Date(),
    end: range ? new Date(range.end) : new Date()
  };

  if (event || range) {
    return merge({}, _event, event);
  }

  return _event;
}; // ----------------------------------------------------------------------


CalendarForm.propTypes = {
  event: PropTypes.object,
  range: PropTypes.object,
  onCancel: PropTypes.func
};
export default function CalendarForm({
  event,
  range,
  onCancel
}) {
  const {
    enqueueSnackbar
  } = useSnackbar();
  const dispatch = useDispatch();
  const isCreating = Object.keys(event).length === 0;
  const EventSchema = Yup.object().shape({
    title: Yup.string().max(255).required('Title is required'),
    description: Yup.string().max(5000)
  });
  const methods = useForm({
    resolver: yupResolver(EventSchema),
    defaultValues: getInitialValues(event, range)
  });
  const {
    reset,
    watch,
    control,
    handleSubmit,
    formState: {
      isSubmitting
    }
  } = methods;

  const onSubmit = async data => {
    try {
      const newEvent = {
        title: data.title,
        description: data.description,
        textColor: data.textColor,
        allDay: data.allDay,
        start: data.start,
        end: data.end
      };

      if (event.id) {
        dispatch(updateEvent(event.id, newEvent));
        enqueueSnackbar('Update success!');
      } else {
        enqueueSnackbar('Create success!');
        dispatch(createEvent(newEvent));
      }

      onCancel();
      reset();
    } catch (error) {
      console.error(error);
    }
  };

  const handleDelete = async () => {
    if (!event.id) return;

    try {
      onCancel();
      dispatch(deleteEvent(event.id));
      enqueueSnackbar('Delete success!');
    } catch (error) {
      console.error(error);
    }
  };

  const values = watch();
  const isDateError = isBefore(new Date(values.end), new Date(values.start));
  return /*#__PURE__*/React.createElement(FormProvider, {
    methods: methods,
    onSubmit: handleSubmit(onSubmit)
  }, /*#__PURE__*/React.createElement(Stack, {
    spacing: 3,
    sx: {
      p: 3
    }
  }, /*#__PURE__*/React.createElement(RHFTextField, {
    name: "title",
    label: "Title"
  }), /*#__PURE__*/React.createElement(RHFTextField, {
    name: "description",
    label: "Description",
    multiline: true,
    rows: 4
  }), /*#__PURE__*/React.createElement(RHFSwitch, {
    name: "allDay",
    label: "All day"
  }), /*#__PURE__*/React.createElement(Controller, {
    name: "start",
    control: control,
    render: ({
      field
    }) => /*#__PURE__*/React.createElement(MobileDateTimePicker, _extends({}, field, {
      label: "Start date",
      inputFormat: "dd/MM/yyyy hh:mm a",
      renderInput: params => /*#__PURE__*/React.createElement(TextField, _extends({}, params, {
        fullWidth: true
      }))
    }))
  }), /*#__PURE__*/React.createElement(Controller, {
    name: "end",
    control: control,
    render: ({
      field
    }) => /*#__PURE__*/React.createElement(MobileDateTimePicker, _extends({}, field, {
      label: "End date",
      inputFormat: "dd/MM/yyyy hh:mm a",
      renderInput: params => /*#__PURE__*/React.createElement(TextField, _extends({}, params, {
        fullWidth: true,
        error: !!isDateError,
        helperText: isDateError && 'End date must be later than start date'
      }))
    }))
  }), /*#__PURE__*/React.createElement(Controller, {
    name: "textColor",
    control: control,
    render: ({
      field
    }) => /*#__PURE__*/React.createElement(ColorSinglePicker, {
      value: field.value,
      onChange: field.onChange,
      colors: COLOR_OPTIONS
    })
  })), /*#__PURE__*/React.createElement(DialogActions, null, !isCreating && /*#__PURE__*/React.createElement(Tooltip, {
    title: "Delete Event"
  }, /*#__PURE__*/React.createElement(IconButton, {
    onClick: handleDelete
  }, /*#__PURE__*/React.createElement(Iconify, {
    icon: "eva:trash-2-outline",
    width: 20,
    height: 20
  }))), /*#__PURE__*/React.createElement(Box, {
    sx: {
      flexGrow: 1
    }
  }), /*#__PURE__*/React.createElement(Button, {
    variant: "outlined",
    color: "inherit",
    onClick: onCancel
  }, "Cancel"), /*#__PURE__*/React.createElement(LoadingButton, {
    type: "submit",
    variant: "contained",
    loading: isSubmitting,
    loadingIndicator: "Loading..."
  }, "Add")));
}