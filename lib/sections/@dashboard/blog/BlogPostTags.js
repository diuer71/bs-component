import PropTypes from 'prop-types'; // @mui

import { Box, Chip, Avatar, Checkbox, AvatarGroup, FormControlLabel } from '@mui/material'; // utils

import { fShortenNumber } from '../../../utils/formatNumber'; // components

import Iconify from '../../../components/Iconify'; // ----------------------------------------------------------------------

BlogPostTags.propTypes = {
  post: PropTypes.object.isRequired
};
export default function BlogPostTags({
  post
}) {
  const {
    favorite,
    tags,
    favoritePerson
  } = post;
  return /*#__PURE__*/React.createElement(Box, {
    sx: {
      py: 3
    }
  }, tags.map(tag => /*#__PURE__*/React.createElement(Chip, {
    key: tag,
    label: tag,
    sx: {
      m: 0.5
    }
  })), /*#__PURE__*/React.createElement(Box, {
    sx: {
      display: 'flex',
      alignItems: 'center',
      mt: 3
    }
  }, /*#__PURE__*/React.createElement(FormControlLabel, {
    control: /*#__PURE__*/React.createElement(Checkbox, {
      defaultChecked: true,
      size: "small",
      color: "error",
      icon: /*#__PURE__*/React.createElement(Iconify, {
        icon: "eva:heart-fill"
      }),
      checkedIcon: /*#__PURE__*/React.createElement(Iconify, {
        icon: "eva:heart-fill"
      })
    }),
    label: fShortenNumber(favorite)
  }), /*#__PURE__*/React.createElement(AvatarGroup, {
    max: 4,
    sx: {
      '& .MuiAvatar-root': {
        width: 32,
        height: 32
      }
    }
  }, favoritePerson.map(person => /*#__PURE__*/React.createElement(Avatar, {
    key: person.name,
    alt: person.name,
    src: person.avatarUrl
  })))));
}