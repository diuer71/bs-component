function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

import { useState } from 'react';
import { paramCase } from 'change-case';
import parse from 'autosuggest-highlight/parse';
import match from 'autosuggest-highlight/match';
import { useNavigate } from 'react-router-dom'; // @mui

import { styled } from '@mui/material/styles';
import { Link, Typography, Autocomplete, InputAdornment, Popper } from '@mui/material'; // hooks

import useIsMountedRef from '../../../hooks/useIsMountedRef'; // utils

import axios from '../../../utils/axios'; // routes

import { PATH_DASHBOARD } from '../../../routes/paths'; // components

import Image from '../../../components/Image';
import Iconify from '../../../components/Iconify';
import InputStyle from '../../../components/InputStyle';
import SearchNotFound from '../../../components/SearchNotFound'; // ----------------------------------------------------------------------

const PopperStyle = styled(props => /*#__PURE__*/React.createElement(Popper, _extends({
  placement: "bottom-start"
}, props)))({
  width: '280px !important'
}); // ----------------------------------------------------------------------

export default function BlogPostsSearch() {
  const navigate = useNavigate();
  const isMountedRef = useIsMountedRef();
  const [searchQuery, setSearchQuery] = useState('');
  const [searchResults, setSearchResults] = useState([]);

  const handleChangeSearch = async value => {
    try {
      setSearchQuery(value);

      if (value) {
        const response = await axios.get('/api/blog/posts/search', {
          params: {
            query: value
          }
        });

        if (isMountedRef.current) {
          setSearchResults(response.data.results);
        }
      }
    } catch (error) {
      console.error(error);
    }
  };

  const handleClick = title => {
    navigate(`${PATH_DASHBOARD.blog.root}/post/${paramCase(title)}`);
  };

  const handleKeyUp = event => {
    if (event.key === 'Enter') {
      handleClick(searchQuery);
    }
  };

  return /*#__PURE__*/React.createElement(Autocomplete, {
    size: "small",
    autoHighlight: true,
    popupIcon: null,
    PopperComponent: PopperStyle,
    options: searchResults,
    onInputChange: (event, value) => handleChangeSearch(value),
    getOptionLabel: post => post.title,
    noOptionsText: /*#__PURE__*/React.createElement(SearchNotFound, {
      searchQuery: searchQuery
    }),
    isOptionEqualToValue: (option, value) => option.id === value.id,
    renderInput: params => /*#__PURE__*/React.createElement(InputStyle, _extends({}, params, {
      stretchStart: 200,
      placeholder: "Search post...",
      onKeyUp: handleKeyUp,
      InputProps: { ...params.InputProps,
        startAdornment: /*#__PURE__*/React.createElement(InputAdornment, {
          position: "start"
        }, /*#__PURE__*/React.createElement(Iconify, {
          icon: 'eva:search-fill',
          sx: {
            ml: 1,
            width: 20,
            height: 20,
            color: 'text.disabled'
          }
        }))
      }
    })),
    renderOption: (props, post, {
      inputValue
    }) => {
      const {
        title,
        cover
      } = post;
      const matches = match(title, inputValue);
      const parts = parse(title, matches);
      return /*#__PURE__*/React.createElement("li", props, /*#__PURE__*/React.createElement(Image, {
        alt: cover,
        src: cover,
        sx: {
          width: 48,
          height: 48,
          borderRadius: 1,
          flexShrink: 0,
          mr: 1.5
        }
      }), /*#__PURE__*/React.createElement(Link, {
        underline: "none",
        onClick: () => handleClick(title)
      }, parts.map((part, index) => /*#__PURE__*/React.createElement(Typography, {
        key: index,
        component: "span",
        variant: "subtitle2",
        color: part.highlight ? 'primary' : 'textPrimary'
      }, part.text))));
    }
  });
}