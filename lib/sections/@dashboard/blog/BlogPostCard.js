import PropTypes from 'prop-types';
import { paramCase } from 'change-case';
import { Link as RouterLink } from 'react-router-dom'; // @mui

import { styled, alpha } from '@mui/material/styles';
import { Box, Link, Card, Avatar, Typography, CardContent, Stack } from '@mui/material'; // routes

import { PATH_DASHBOARD } from '../../../routes/paths'; // hooks

import useResponsive from '../../../hooks/useResponsive'; // utils

import { fDate } from '../../../utils/formatTime';
import { fShortenNumber } from '../../../utils/formatNumber'; // components

import Image from '../../../components/Image';
import Iconify from '../../../components/Iconify';
import TextMaxLine from '../../../components/TextMaxLine';
import TextIconLabel from '../../../components/TextIconLabel';
import SvgIconStyle from '../../../components/SvgIconStyle'; // ----------------------------------------------------------------------

const OverlayStyle = styled('div')(({
  theme
}) => ({
  top: 0,
  zIndex: 1,
  width: '100%',
  height: '100%',
  position: 'absolute',
  backgroundColor: alpha(theme.palette.grey[900], 0.8)
})); // ----------------------------------------------------------------------

BlogPostCard.propTypes = {
  post: PropTypes.object.isRequired,
  index: PropTypes.number
};
export default function BlogPostCard({
  post,
  index
}) {
  const isDesktop = useResponsive('up', 'md');
  const {
    cover,
    title,
    view,
    comment,
    share,
    author,
    createdAt
  } = post;
  const latestPost = index === 0 || index === 1 || index === 2;

  if (isDesktop && latestPost) {
    return /*#__PURE__*/React.createElement(Card, null, /*#__PURE__*/React.createElement(Avatar, {
      alt: author.name,
      src: author.avatarUrl,
      sx: {
        zIndex: 9,
        top: 24,
        left: 24,
        width: 40,
        height: 40,
        position: 'absolute'
      }
    }), /*#__PURE__*/React.createElement(PostContent, {
      title: title,
      view: view,
      comment: comment,
      share: share,
      createdAt: createdAt,
      index: index
    }), /*#__PURE__*/React.createElement(OverlayStyle, null), /*#__PURE__*/React.createElement(Image, {
      alt: "cover",
      src: cover,
      sx: {
        height: 360
      }
    }));
  }

  return /*#__PURE__*/React.createElement(Card, null, /*#__PURE__*/React.createElement(Box, {
    sx: {
      position: 'relative'
    }
  }, /*#__PURE__*/React.createElement(SvgIconStyle, {
    src: "https://minimal-assets-api.vercel.app/assets/icons/shape-avatar.svg",
    sx: {
      width: 80,
      height: 36,
      zIndex: 9,
      bottom: -15,
      position: 'absolute',
      color: 'background.paper'
    }
  }), /*#__PURE__*/React.createElement(Avatar, {
    alt: author.name,
    src: author.avatarUrl,
    sx: {
      left: 24,
      zIndex: 9,
      width: 32,
      height: 32,
      bottom: -16,
      position: 'absolute'
    }
  }), /*#__PURE__*/React.createElement(Image, {
    alt: "cover",
    src: cover,
    ratio: "4/3"
  })), /*#__PURE__*/React.createElement(PostContent, {
    title: title,
    view: view,
    comment: comment,
    share: share,
    createdAt: createdAt
  }));
} // ----------------------------------------------------------------------

PostContent.propTypes = {
  comment: PropTypes.number,
  createdAt: PropTypes.string,
  index: PropTypes.number,
  share: PropTypes.number,
  title: PropTypes.string,
  view: PropTypes.number
};
export function PostContent({
  title,
  view,
  comment,
  share,
  createdAt,
  index
}) {
  const isDesktop = useResponsive('up', 'md');
  const linkTo = `${PATH_DASHBOARD.blog.root}/post/${paramCase(title)}`;
  const latestPostLarge = index === 0;
  const latestPostSmall = index === 1 || index === 2;
  const POST_INFO = [{
    number: comment,
    icon: 'eva:message-circle-fill'
  }, {
    number: view,
    icon: 'eva:eye-fill'
  }, {
    number: share,
    icon: 'eva:share-fill'
  }];
  return /*#__PURE__*/React.createElement(CardContent, {
    sx: {
      pt: 4.5,
      width: 1,
      ...((latestPostLarge || latestPostSmall) && {
        pt: 0,
        zIndex: 9,
        bottom: 0,
        position: 'absolute',
        color: 'common.white'
      })
    }
  }, /*#__PURE__*/React.createElement(Typography, {
    gutterBottom: true,
    variant: "caption",
    component: "div",
    sx: {
      color: 'text.disabled',
      ...((latestPostLarge || latestPostSmall) && {
        opacity: 0.64,
        color: 'common.white'
      })
    }
  }, fDate(createdAt)), /*#__PURE__*/React.createElement(Link, {
    to: linkTo,
    color: "inherit",
    component: RouterLink
  }, /*#__PURE__*/React.createElement(TextMaxLine, {
    variant: isDesktop && latestPostLarge ? 'h5' : 'subtitle2',
    line: 2,
    persistent: true
  }, title)), /*#__PURE__*/React.createElement(Stack, {
    flexWrap: "wrap",
    direction: "row",
    justifyContent: "flex-end",
    sx: {
      mt: 3,
      color: 'text.disabled',
      ...((latestPostLarge || latestPostSmall) && {
        opacity: 0.64,
        color: 'common.white'
      })
    }
  }, POST_INFO.map((info, index) => /*#__PURE__*/React.createElement(TextIconLabel, {
    key: index,
    icon: /*#__PURE__*/React.createElement(Iconify, {
      icon: info.icon,
      sx: {
        width: 16,
        height: 16,
        mr: 0.5
      }
    }),
    value: fShortenNumber(info.number),
    sx: {
      typography: 'caption',
      ml: index === 0 ? 0 : 1.5
    }
  }))));
}