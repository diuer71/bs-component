import PropTypes from 'prop-types'; // @mui

import { Grid, Typography } from '@mui/material'; //

import BlogPostCard from './BlogPostCard'; // ----------------------------------------------------------------------

BlogPostRecent.propTypes = {
  posts: PropTypes.array.isRequired
};
export default function BlogPostRecent({
  posts
}) {
  return /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement(Typography, {
    variant: "h4",
    sx: {
      mt: 10,
      mb: 5
    }
  }, "Recent posts"), /*#__PURE__*/React.createElement(Grid, {
    container: true,
    spacing: 3
  }, posts.map(post => /*#__PURE__*/React.createElement(Grid, {
    key: post.id,
    item: true,
    xs: 12,
    sm: 6,
    md: 3
  }, /*#__PURE__*/React.createElement(BlogPostCard, {
    post: post
  })))));
}