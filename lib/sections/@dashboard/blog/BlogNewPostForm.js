function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

import * as Yup from 'yup';
import { useCallback, useState } from 'react';
import { useSnackbar } from 'notistack';
import { useNavigate } from 'react-router-dom'; // form

import { yupResolver } from '@hookform/resolvers/yup';
import { useForm, Controller } from 'react-hook-form'; // @mui

import { LoadingButton } from '@mui/lab';
import { styled } from '@mui/material/styles';
import { Grid, Card, Chip, Stack, Button, TextField, Typography, Autocomplete } from '@mui/material'; // routes

import { PATH_DASHBOARD } from '../../../routes/paths'; // components

import { RHFSwitch, RHFEditor, FormProvider, RHFTextField, RHFUploadSingleFile } from '../../../components/hook-form'; //

import BlogNewPostPreview from './BlogNewPostPreview'; // ----------------------------------------------------------------------

const TAGS_OPTION = ['Toy Story 3', 'Logan', 'Full Metal Jacket', 'Dangal', 'The Sting', '2001: A Space Odyssey', "Singin' in the Rain", 'Toy Story', 'Bicycle Thieves', 'The Kid', 'Inglourious Basterds', 'Snatch', '3 Idiots'];
const LabelStyle = styled(Typography)(({
  theme
}) => ({ ...theme.typography.subtitle2,
  color: theme.palette.text.secondary,
  marginBottom: theme.spacing(1)
})); // ----------------------------------------------------------------------

export default function BlogNewPostForm() {
  const navigate = useNavigate();
  const [open, setOpen] = useState(false);
  const {
    enqueueSnackbar
  } = useSnackbar();

  const handleOpenPreview = () => {
    setOpen(true);
  };

  const handleClosePreview = () => {
    setOpen(false);
  };

  const NewBlogSchema = Yup.object().shape({
    title: Yup.string().required('Title is required'),
    description: Yup.string().required('Description is required'),
    content: Yup.string().min(1000).required('Content is required'),
    cover: Yup.mixed().required('Cover is required')
  });
  const defaultValues = {
    title: '',
    description: '',
    content: '',
    cover: null,
    tags: ['Logan'],
    publish: true,
    comments: true,
    metaTitle: '',
    metaDescription: '',
    metaKeywords: ['Logan']
  };
  const methods = useForm({
    resolver: yupResolver(NewBlogSchema),
    defaultValues
  });
  const {
    reset,
    watch,
    control,
    setValue,
    handleSubmit,
    formState: {
      isSubmitting,
      isValid
    }
  } = methods;
  const values = watch();

  const onSubmit = async () => {
    try {
      await new Promise(resolve => setTimeout(resolve, 500));
      reset();
      handleClosePreview();
      enqueueSnackbar('Post success!');
      navigate(PATH_DASHBOARD.blog.posts);
    } catch (error) {
      console.error(error);
    }
  };

  const handleDrop = useCallback(acceptedFiles => {
    const file = acceptedFiles[0];

    if (file) {
      setValue('cover', Object.assign(file, {
        preview: URL.createObjectURL(file)
      }));
    }
  }, [setValue]);
  return /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement(FormProvider, {
    methods: methods,
    onSubmit: handleSubmit(onSubmit)
  }, /*#__PURE__*/React.createElement(Grid, {
    container: true,
    spacing: 3
  }, /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: 12,
    md: 8
  }, /*#__PURE__*/React.createElement(Card, {
    sx: {
      p: 3
    }
  }, /*#__PURE__*/React.createElement(Stack, {
    spacing: 3
  }, /*#__PURE__*/React.createElement(RHFTextField, {
    name: "title",
    label: "Post Title"
  }), /*#__PURE__*/React.createElement(RHFTextField, {
    name: "description",
    label: "Description",
    multiline: true,
    rows: 3
  }), /*#__PURE__*/React.createElement("div", null, /*#__PURE__*/React.createElement(LabelStyle, null, "Content"), /*#__PURE__*/React.createElement(RHFEditor, {
    name: "content"
  })), /*#__PURE__*/React.createElement("div", null, /*#__PURE__*/React.createElement(LabelStyle, null, "Cover"), /*#__PURE__*/React.createElement(RHFUploadSingleFile, {
    name: "cover",
    accept: "image/*",
    maxSize: 3145728,
    onDrop: handleDrop
  }))))), /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: 12,
    md: 4
  }, /*#__PURE__*/React.createElement(Card, {
    sx: {
      p: 3
    }
  }, /*#__PURE__*/React.createElement(Stack, {
    spacing: 3
  }, /*#__PURE__*/React.createElement("div", null, /*#__PURE__*/React.createElement(RHFSwitch, {
    name: "publish",
    label: "Publish",
    labelPlacement: "start",
    sx: {
      mb: 1,
      mx: 0,
      width: 1,
      justifyContent: 'space-between'
    }
  }), /*#__PURE__*/React.createElement(RHFSwitch, {
    name: "comments",
    label: "Enable comments",
    labelPlacement: "start",
    sx: {
      mx: 0,
      width: 1,
      justifyContent: 'space-between'
    }
  })), /*#__PURE__*/React.createElement(Controller, {
    name: "tags",
    control: control,
    render: ({
      field
    }) => /*#__PURE__*/React.createElement(Autocomplete, {
      multiple: true,
      freeSolo: true,
      onChange: (event, newValue) => field.onChange(newValue),
      options: TAGS_OPTION.map(option => option),
      renderTags: (value, getTagProps) => value.map((option, index) => /*#__PURE__*/React.createElement(Chip, _extends({}, getTagProps({
        index
      }), {
        key: option,
        size: "small",
        label: option
      }))),
      renderInput: params => /*#__PURE__*/React.createElement(TextField, _extends({
        label: "Tags"
      }, params))
    })
  }), /*#__PURE__*/React.createElement(RHFTextField, {
    name: "metaTitle",
    label: "Meta title"
  }), /*#__PURE__*/React.createElement(RHFTextField, {
    name: "metaDescription",
    label: "Meta description",
    fullWidth: true,
    multiline: true,
    rows: 3
  }), /*#__PURE__*/React.createElement(Controller, {
    name: "metaKeywords",
    control: control,
    render: ({
      field
    }) => /*#__PURE__*/React.createElement(Autocomplete, {
      multiple: true,
      freeSolo: true,
      onChange: (event, newValue) => field.onChange(newValue),
      options: TAGS_OPTION.map(option => option),
      renderTags: (value, getTagProps) => value.map((option, index) => /*#__PURE__*/React.createElement(Chip, _extends({}, getTagProps({
        index
      }), {
        key: option,
        size: "small",
        label: option
      }))),
      renderInput: params => /*#__PURE__*/React.createElement(TextField, _extends({
        label: "Meta keywords"
      }, params))
    })
  }))), /*#__PURE__*/React.createElement(Stack, {
    direction: "row",
    spacing: 1.5,
    sx: {
      mt: 3
    }
  }, /*#__PURE__*/React.createElement(Button, {
    fullWidth: true,
    color: "inherit",
    variant: "outlined",
    size: "large",
    onClick: handleOpenPreview
  }, "Preview"), /*#__PURE__*/React.createElement(LoadingButton, {
    fullWidth: true,
    type: "submit",
    variant: "contained",
    size: "large",
    loading: isSubmitting
  }, "Post"))))), /*#__PURE__*/React.createElement(BlogNewPostPreview, {
    values: values,
    isOpen: open,
    isValid: isValid,
    isSubmitting: isSubmitting,
    onClose: handleClosePreview,
    onSubmit: handleSubmit(onSubmit)
  }));
}