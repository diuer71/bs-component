import PropTypes from 'prop-types'; // @mui

import { Box, List } from '@mui/material'; //

import BlogPostCommentItem from './BlogPostCommentItem'; // ----------------------------------------------------------------------

BlogPostCommentList.propTypes = {
  post: PropTypes.object.isRequired
};
export default function BlogPostCommentList({
  post
}) {
  const {
    comments
  } = post;
  return /*#__PURE__*/React.createElement(List, {
    disablePadding: true
  }, comments.map(comment => {
    const {
      id,
      replyComment,
      users
    } = comment;
    const hasReply = replyComment.length > 0;
    return /*#__PURE__*/React.createElement(Box, {
      key: id,
      sx: {}
    }, /*#__PURE__*/React.createElement(BlogPostCommentItem, {
      name: comment.name,
      avatarUrl: comment.avatarUrl,
      postedAt: comment.postedAt,
      message: comment.message
    }), hasReply && replyComment.map(reply => {
      const user = users.find(user => user.id === reply.userId);
      return /*#__PURE__*/React.createElement(BlogPostCommentItem, {
        key: reply.id,
        message: reply.message,
        tagUser: reply.tagUser,
        postedAt: reply.postedAt,
        name: user.name,
        avatarUrl: user.avatarUrl,
        hasReply: true
      });
    }));
  }));
}