import PropTypes from 'prop-types';
import isString from 'lodash/isString'; // @mui

import { LoadingButton } from '@mui/lab';
import { alpha } from '@mui/material/styles';
import { Box, Button, Container, Typography, DialogActions } from '@mui/material'; // components

import Image from '../../../components/Image';
import Markdown from '../../../components/Markdown';
import Scrollbar from '../../../components/Scrollbar';
import EmptyContent from '../../../components/EmptyContent';
import { DialogAnimate } from '../../../components/animate'; // ----------------------------------------------------------------------

BlogNewPostPreview.propTypes = {
  values: PropTypes.object,
  isValid: PropTypes.bool,
  isSubmitting: PropTypes.bool,
  isOpen: PropTypes.bool,
  onClose: PropTypes.func,
  onSubmit: PropTypes.func
};
export default function BlogNewPostPreview({
  values,
  isValid,
  isSubmitting,
  isOpen,
  onClose,
  onSubmit
}) {
  const {
    title,
    content,
    description
  } = values;
  const cover = isString(values.cover) ? values.cover : values.cover?.preview;
  const hasContent = title || description || content || cover;
  const hasHero = title || cover;
  return /*#__PURE__*/React.createElement(DialogAnimate, {
    fullScreen: true,
    open: isOpen,
    onClose: onClose
  }, /*#__PURE__*/React.createElement(DialogActions, {
    sx: {
      py: 2,
      px: 3
    }
  }, /*#__PURE__*/React.createElement(Typography, {
    variant: "subtitle1",
    sx: {
      flexGrow: 1
    }
  }, "Preview Post"), /*#__PURE__*/React.createElement(Button, {
    onClick: onClose
  }, "Cancel"), /*#__PURE__*/React.createElement(LoadingButton, {
    type: "submit",
    variant: "contained",
    disabled: !isValid,
    loading: isSubmitting,
    onClick: onSubmit
  }, "Post")), hasContent ? /*#__PURE__*/React.createElement(Scrollbar, null, hasHero && /*#__PURE__*/React.createElement(PreviewHero, {
    title: title || '',
    cover: cover
  }), /*#__PURE__*/React.createElement(Container, null, /*#__PURE__*/React.createElement(Box, {
    sx: {
      mt: 5,
      mb: 10
    }
  }, /*#__PURE__*/React.createElement(Typography, {
    variant: "h6",
    sx: {
      mb: 5
    }
  }, description), /*#__PURE__*/React.createElement(Markdown, {
    children: content || ''
  })))) : /*#__PURE__*/React.createElement(EmptyContent, {
    title: "Empty content"
  }));
} // ----------------------------------------------------------------------

PreviewHero.propTypes = {
  cover: PropTypes.string,
  title: PropTypes.string
};

function PreviewHero({
  title,
  cover
}) {
  return /*#__PURE__*/React.createElement(Box, {
    sx: {
      position: 'relative'
    }
  }, /*#__PURE__*/React.createElement(Container, {
    sx: {
      top: 0,
      left: 0,
      right: 0,
      zIndex: 9,
      position: 'absolute',
      color: 'common.white',
      pt: {
        xs: 3,
        lg: 10
      }
    }
  }, /*#__PURE__*/React.createElement(Typography, {
    variant: "h2",
    component: "h1"
  }, title)), /*#__PURE__*/React.createElement(Box, {
    sx: {
      top: 0,
      left: 0,
      right: 0,
      bottom: 0,
      zIndex: 8,
      position: 'absolute',
      bgcolor: theme => alpha(theme.palette.grey[900], 0.8)
    }
  }), /*#__PURE__*/React.createElement(Image, {
    alt: "cover",
    src: cover,
    ratio: "16/9"
  }));
}