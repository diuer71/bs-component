import { useState } from 'react';
import PropTypes from 'prop-types'; // @mui

import { Box, Button, Avatar, Divider, ListItem, TextField, Typography, ListItemText, ListItemAvatar } from '@mui/material'; // utils

import { fDate } from '../../../utils/formatTime'; // ----------------------------------------------------------------------

BlogPostCommentItem.propTypes = {
  name: PropTypes.string,
  avatarUrl: PropTypes.string,
  message: PropTypes.string,
  tagUser: PropTypes.string,
  postedAt: PropTypes.string,
  hasReply: PropTypes.bool
};
export default function BlogPostCommentItem({
  name,
  avatarUrl,
  message,
  tagUser,
  postedAt,
  hasReply
}) {
  const [openReply, setOpenReply] = useState(false);

  const handleOpenReply = () => {
    setOpenReply(true);
  };

  return /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement(ListItem, {
    disableGutters: true,
    sx: {
      alignItems: 'flex-start',
      py: 3,
      ...(hasReply && {
        ml: 'auto',
        width: theme => `calc(100% - ${theme.spacing(7)})`
      })
    }
  }, /*#__PURE__*/React.createElement(ListItemAvatar, null, /*#__PURE__*/React.createElement(Avatar, {
    alt: name,
    src: avatarUrl,
    sx: {
      width: 48,
      height: 48
    }
  })), /*#__PURE__*/React.createElement(ListItemText, {
    primary: name,
    primaryTypographyProps: {
      variant: 'subtitle1'
    },
    secondary: /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement(Typography, {
      gutterBottom: true,
      variant: "caption",
      sx: {
        display: 'block',
        color: 'text.disabled'
      }
    }, fDate(postedAt)), /*#__PURE__*/React.createElement(Typography, {
      component: "span",
      variant: "body2"
    }, /*#__PURE__*/React.createElement("strong", null, tagUser), " ", message))
  }), !hasReply && /*#__PURE__*/React.createElement(Button, {
    size: "small",
    onClick: handleOpenReply,
    sx: {
      position: 'absolute',
      right: 0
    }
  }, "Reply")), !hasReply && openReply && /*#__PURE__*/React.createElement(Box, {
    sx: {
      mb: 3,
      ml: 'auto',
      width: theme => `calc(100% - ${theme.spacing(7)})`
    }
  }, /*#__PURE__*/React.createElement(TextField, {
    fullWidth: true,
    size: "small",
    placeholder: "Write comment",
    sx: {
      '& fieldset': {
        borderWidth: `1px !important`,
        borderColor: theme => `${theme.palette.grey[500_32]} !important`
      }
    }
  })), /*#__PURE__*/React.createElement(Divider, {
    sx: {
      ml: 'auto',
      width: theme => `calc(100% - ${theme.spacing(7)})`
    }
  }));
}