import * as Yup from 'yup'; // form

import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup'; // @mui

import { styled } from '@mui/material/styles';
import { Typography, Stack } from '@mui/material';
import { LoadingButton } from '@mui/lab'; // components

import { FormProvider, RHFTextField } from '../../../components/hook-form'; // ----------------------------------------------------------------------

const RootStyles = styled('div')(({
  theme
}) => ({
  padding: theme.spacing(3),
  borderRadius: Number(theme.shape.borderRadius) * 2,
  backgroundColor: theme.palette.background.neutral
})); // ----------------------------------------------------------------------

export default function BlogPostCommentForm() {
  const CommentSchema = Yup.object().shape({
    comment: Yup.string().required('Comment is required'),
    name: Yup.string().required('Name is required'),
    email: Yup.string().email('Email must be a valid email address').required('Email is required')
  });
  const defaultValues = {
    comment: '',
    name: '',
    email: ''
  };
  const methods = useForm({
    resolver: yupResolver(CommentSchema),
    defaultValues
  });
  const {
    reset,
    handleSubmit,
    formState: {
      isSubmitting
    }
  } = methods;

  const onSubmit = async () => {
    try {
      await new Promise(resolve => setTimeout(resolve, 500));
      reset();
    } catch (error) {
      console.error(error);
    }
  };

  return /*#__PURE__*/React.createElement(RootStyles, null, /*#__PURE__*/React.createElement(Typography, {
    variant: "subtitle1",
    sx: {
      mb: 3
    }
  }, "Add Comment"), /*#__PURE__*/React.createElement(FormProvider, {
    methods: methods,
    onSubmit: handleSubmit(onSubmit)
  }, /*#__PURE__*/React.createElement(Stack, {
    spacing: 3,
    alignItems: "flex-end"
  }, /*#__PURE__*/React.createElement(RHFTextField, {
    name: "comment",
    label: "Comment *",
    multiline: true,
    rows: 3
  }), /*#__PURE__*/React.createElement(RHFTextField, {
    name: "name",
    label: "Name *"
  }), /*#__PURE__*/React.createElement(RHFTextField, {
    name: "email",
    label: "Email *"
  }), /*#__PURE__*/React.createElement(LoadingButton, {
    type: "submit",
    variant: "contained",
    loading: isSubmitting
  }, "Post comment"))));
}