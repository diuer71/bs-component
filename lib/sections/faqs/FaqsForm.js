// @mui
import { Button, Typography, TextField, Stack } from '@mui/material'; //

import { varFade, MotionInView } from '../../components/animate'; // ----------------------------------------------------------------------

export default function FaqsForm() {
  return /*#__PURE__*/React.createElement(Stack, {
    spacing: 3
  }, /*#__PURE__*/React.createElement(MotionInView, {
    variants: varFade().inUp
  }, /*#__PURE__*/React.createElement(Typography, {
    variant: "h4"
  }, "Haven't found the right help?")), /*#__PURE__*/React.createElement(MotionInView, {
    variants: varFade().inUp
  }, /*#__PURE__*/React.createElement(TextField, {
    fullWidth: true,
    label: "Name"
  })), /*#__PURE__*/React.createElement(MotionInView, {
    variants: varFade().inUp
  }, /*#__PURE__*/React.createElement(TextField, {
    fullWidth: true,
    label: "Email"
  })), /*#__PURE__*/React.createElement(MotionInView, {
    variants: varFade().inUp
  }, /*#__PURE__*/React.createElement(TextField, {
    fullWidth: true,
    label: "Subject"
  })), /*#__PURE__*/React.createElement(MotionInView, {
    variants: varFade().inUp
  }, /*#__PURE__*/React.createElement(TextField, {
    fullWidth: true,
    label: "Enter your message here.",
    multiline: true,
    rows: 4
  })), /*#__PURE__*/React.createElement(MotionInView, {
    variants: varFade().inUp
  }, /*#__PURE__*/React.createElement(Button, {
    size: "large",
    variant: "contained"
  }, "Submit Now")));
}