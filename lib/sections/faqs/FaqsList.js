// @mui
import { Accordion, Typography, AccordionSummary, AccordionDetails } from '@mui/material'; // _mock_

import { _faqs } from '../../_mock'; // components

import Iconify from '../../components/Iconify'; // ----------------------------------------------------------------------

export default function FaqsList() {
  return /*#__PURE__*/React.createElement(React.Fragment, null, _faqs.map(accordion => /*#__PURE__*/React.createElement(Accordion, {
    key: accordion.id
  }, /*#__PURE__*/React.createElement(AccordionSummary, {
    expandIcon: /*#__PURE__*/React.createElement(Iconify, {
      icon: 'eva:arrow-ios-downward-fill',
      width: 20,
      height: 20
    })
  }, /*#__PURE__*/React.createElement(Typography, {
    variant: "subtitle1"
  }, accordion.heading)), /*#__PURE__*/React.createElement(AccordionDetails, null, /*#__PURE__*/React.createElement(Typography, null, accordion.detail)))));
}