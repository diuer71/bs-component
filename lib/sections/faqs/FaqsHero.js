import { m } from 'framer-motion'; // @mui

import { styled } from '@mui/material/styles';
import { Box, Container, Stack, InputAdornment } from '@mui/material'; // components

import Iconify from '../../components/Iconify';
import InputStyle from '../../components/InputStyle';
import { MotionContainer, TextAnimate, varFade } from '../../components/animate'; // ----------------------------------------------------------------------

const RootStyle = styled('div')(({
  theme
}) => ({
  backgroundSize: 'cover',
  backgroundImage: 'url(https://minimal-assets-api.vercel.app/assets/overlay.svg), url(https://minimal-assets-api.vercel.app/assets/images/faqs/hero.jpg)',
  padding: theme.spacing(10, 0),
  [theme.breakpoints.up('md')]: {
    height: 560,
    padding: 0
  }
}));
const ContentStyle = styled(Stack)(({
  theme
}) => ({
  textAlign: 'center',
  [theme.breakpoints.up('md')]: {
    textAlign: 'left',
    position: 'absolute',
    bottom: theme.spacing(10)
  }
})); // ----------------------------------------------------------------------

export default function FaqsHero() {
  return /*#__PURE__*/React.createElement(RootStyle, null, /*#__PURE__*/React.createElement(Container, {
    component: MotionContainer,
    sx: {
      position: 'relative',
      height: '100%'
    }
  }, /*#__PURE__*/React.createElement(ContentStyle, {
    spacing: 5
  }, /*#__PURE__*/React.createElement("div", null, /*#__PURE__*/React.createElement(TextAnimate, {
    text: "How",
    sx: {
      color: 'primary.main'
    },
    variants: varFade().inRight
  }), /*#__PURE__*/React.createElement("br", null), /*#__PURE__*/React.createElement(Box, {
    sx: {
      display: 'inline-flex',
      color: 'common.white'
    }
  }, /*#__PURE__*/React.createElement(TextAnimate, {
    text: "can",
    sx: {
      mr: 2
    }
  }), /*#__PURE__*/React.createElement(TextAnimate, {
    text: "we",
    sx: {
      mr: 2
    }
  }), /*#__PURE__*/React.createElement(TextAnimate, {
    text: "help",
    sx: {
      mr: 2
    }
  }), /*#__PURE__*/React.createElement(TextAnimate, {
    text: "you?"
  }))), /*#__PURE__*/React.createElement(m.div, {
    variants: varFade().inUp
  }, /*#__PURE__*/React.createElement(InputStyle, {
    stretchStart: 280,
    placeholder: "Search support",
    InputProps: {
      startAdornment: /*#__PURE__*/React.createElement(InputAdornment, {
        position: "start"
      }, /*#__PURE__*/React.createElement(Iconify, {
        icon: 'eva:search-fill',
        sx: {
          color: 'text.disabled',
          width: 20,
          height: 20
        }
      }))
    },
    sx: {
      '& .MuiOutlinedInput-root': {
        color: 'common.white'
      }
    }
  })))));
}