// @mui
import { Stack, Button, Divider, Typography } from '@mui/material'; // components

import Iconify from '../../components/Iconify'; // ----------------------------------------------------------------------

export default function AuthFirebaseSocial() {
  return /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement(Stack, {
    direction: "row",
    spacing: 2
  }, /*#__PURE__*/React.createElement(Button, {
    fullWidth: true,
    size: "large",
    color: "inherit",
    variant: "outlined"
  }, /*#__PURE__*/React.createElement(Iconify, {
    icon: 'eva:google-fill',
    color: "#DF3E30",
    width: 24,
    height: 24
  })), /*#__PURE__*/React.createElement(Button, {
    fullWidth: true,
    size: "large",
    color: "inherit",
    variant: "outlined"
  }, /*#__PURE__*/React.createElement(Iconify, {
    icon: 'eva:facebook-fill',
    color: "#1877F2",
    width: 24,
    height: 24
  })), /*#__PURE__*/React.createElement(Button, {
    fullWidth: true,
    size: "large",
    color: "inherit",
    variant: "outlined"
  }, /*#__PURE__*/React.createElement(Iconify, {
    icon: 'eva:twitter-fill',
    color: "#1C9CEA",
    width: 24,
    height: 24
  }))), /*#__PURE__*/React.createElement(Divider, {
    sx: {
      my: 3
    }
  }, /*#__PURE__*/React.createElement(Typography, {
    variant: "body2",
    sx: {
      color: 'text.secondary'
    }
  }, "OR")));
}