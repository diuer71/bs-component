import PropTypes from 'prop-types';
import * as Yup from 'yup'; // form

import { yupResolver } from '@hookform/resolvers/yup';
import { useForm } from 'react-hook-form'; // @mui

import { Stack } from '@mui/material';
import { LoadingButton } from '@mui/lab'; // hooks

import useIsMountedRef from '../../../hooks/useIsMountedRef'; // components

import { FormProvider, RHFTextField } from '../../../components/hook-form'; // ----------------------------------------------------------------------

ResetPasswordForm.propTypes = {
  onSent: PropTypes.func,
  onGetEmail: PropTypes.func
};
export default function ResetPasswordForm({
  onSent,
  onGetEmail
}) {
  const isMountedRef = useIsMountedRef();
  const ResetPasswordSchema = Yup.object().shape({
    email: Yup.string().email('Email must be a valid email address').required('Email is required')
  });
  const methods = useForm({
    resolver: yupResolver(ResetPasswordSchema),
    defaultValues: {
      email: 'demo@minimals.cc'
    }
  });
  const {
    handleSubmit,
    formState: {
      isSubmitting
    }
  } = methods;

  const onSubmit = async data => {
    try {
      await new Promise(resolve => setTimeout(resolve, 500));

      if (isMountedRef.current) {
        onSent();
        onGetEmail(data.email);
      }
    } catch (error) {
      console.error(error);
    }
  };

  return /*#__PURE__*/React.createElement(FormProvider, {
    methods: methods,
    onSubmit: handleSubmit(onSubmit)
  }, /*#__PURE__*/React.createElement(Stack, {
    spacing: 3
  }, /*#__PURE__*/React.createElement(RHFTextField, {
    name: "email",
    label: "Email address"
  }), /*#__PURE__*/React.createElement(LoadingButton, {
    fullWidth: true,
    size: "large",
    type: "submit",
    variant: "contained",
    loading: isSubmitting
  }, "Reset Password")));
}