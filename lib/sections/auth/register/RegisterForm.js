import * as Yup from 'yup';
import { useState } from 'react'; // form

import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup'; // @mui

import { Stack, IconButton, InputAdornment, Alert } from '@mui/material';
import { LoadingButton } from '@mui/lab'; // hooks

import useAuth from '../../../hooks/useAuth';
import useIsMountedRef from '../../../hooks/useIsMountedRef'; // components

import Iconify from '../../../components/Iconify';
import { FormProvider, RHFTextField } from '../../../components/hook-form'; // ----------------------------------------------------------------------

export default function RegisterForm() {
  const {
    register
  } = useAuth();
  const isMountedRef = useIsMountedRef();
  const [showPassword, setShowPassword] = useState(false);
  const RegisterSchema = Yup.object().shape({
    firstName: Yup.string().required('First name required'),
    lastName: Yup.string().required('Last name required'),
    email: Yup.string().email('Email must be a valid email address').required('Email is required'),
    password: Yup.string().required('Password is required')
  });
  const defaultValues = {
    firstName: '',
    lastName: '',
    email: '',
    password: ''
  };
  const methods = useForm({
    resolver: yupResolver(RegisterSchema),
    defaultValues
  });
  const {
    reset,
    setError,
    handleSubmit,
    formState: {
      errors,
      isSubmitting
    }
  } = methods;

  const onSubmit = async data => {
    try {
      await register(data.email, data.password, data.firstName, data.lastName);
    } catch (error) {
      console.error(error);
      reset();

      if (isMountedRef.current) {
        setError('afterSubmit', error);
      }
    }
  };

  return /*#__PURE__*/React.createElement(FormProvider, {
    methods: methods,
    onSubmit: handleSubmit(onSubmit)
  }, /*#__PURE__*/React.createElement(Stack, {
    spacing: 3
  }, !!errors.afterSubmit && /*#__PURE__*/React.createElement(Alert, {
    severity: "error"
  }, errors.afterSubmit.message), /*#__PURE__*/React.createElement(Stack, {
    direction: {
      xs: 'column',
      sm: 'row'
    },
    spacing: 2
  }, /*#__PURE__*/React.createElement(RHFTextField, {
    name: "firstName",
    label: "First name"
  }), /*#__PURE__*/React.createElement(RHFTextField, {
    name: "lastName",
    label: "Last name"
  })), /*#__PURE__*/React.createElement(RHFTextField, {
    name: "email",
    label: "Email address"
  }), /*#__PURE__*/React.createElement(RHFTextField, {
    name: "password",
    label: "Password",
    type: showPassword ? 'text' : 'password',
    InputProps: {
      endAdornment: /*#__PURE__*/React.createElement(InputAdornment, {
        position: "end"
      }, /*#__PURE__*/React.createElement(IconButton, {
        edge: "end",
        onClick: () => setShowPassword(!showPassword)
      }, /*#__PURE__*/React.createElement(Iconify, {
        icon: showPassword ? 'eva:eye-fill' : 'eva:eye-off-fill'
      })))
    }
  }), /*#__PURE__*/React.createElement(LoadingButton, {
    fullWidth: true,
    size: "large",
    type: "submit",
    variant: "contained",
    loading: isSubmitting
  }, "Register")));
}