import * as Yup from 'yup';
import { useState } from 'react';
import { Link as RouterLink } from 'react-router-dom'; // form

import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup'; // @mui

import { Link, Stack, Alert, IconButton, InputAdornment } from '@mui/material';
import { LoadingButton } from '@mui/lab'; // routes

import { PATH_AUTH } from '../../../routes/paths'; // hooks

import useAuth from '../../../hooks/useAuth';
import useIsMountedRef from '../../../hooks/useIsMountedRef'; // components

import Iconify from '../../../components/Iconify';
import { FormProvider, RHFTextField, RHFCheckbox } from '../../../components/hook-form'; // ----------------------------------------------------------------------

export default function LoginForm() {
  const {
    login
  } = useAuth();
  const isMountedRef = useIsMountedRef();
  const [showPassword, setShowPassword] = useState(false);
  const LoginSchema = Yup.object().shape({
    email: Yup.string().email('Email must be a valid email address').required('Email is required'),
    password: Yup.string().required('Password is required')
  });
  const defaultValues = {
    email: 'demo@minimals.cc',
    password: 'demo1234',
    remember: true
  };
  const methods = useForm({
    resolver: yupResolver(LoginSchema),
    defaultValues
  });
  const {
    reset,
    setError,
    handleSubmit,
    formState: {
      errors,
      isSubmitting
    }
  } = methods;

  const onSubmit = async data => {
    try {
      await login(data.email, data.password);
    } catch (error) {
      console.error(error);
      reset();

      if (isMountedRef.current) {
        setError('afterSubmit', error);
      }
    }
  };

  return /*#__PURE__*/React.createElement(FormProvider, {
    methods: methods,
    onSubmit: handleSubmit(onSubmit)
  }, /*#__PURE__*/React.createElement(Stack, {
    spacing: 3
  }, !!errors.afterSubmit && /*#__PURE__*/React.createElement(Alert, {
    severity: "error"
  }, errors.afterSubmit.message), /*#__PURE__*/React.createElement(RHFTextField, {
    name: "email",
    label: "Email address"
  }), /*#__PURE__*/React.createElement(RHFTextField, {
    name: "password",
    label: "Password",
    type: showPassword ? 'text' : 'password',
    InputProps: {
      endAdornment: /*#__PURE__*/React.createElement(InputAdornment, {
        position: "end"
      }, /*#__PURE__*/React.createElement(IconButton, {
        onClick: () => setShowPassword(!showPassword),
        edge: "end"
      }, /*#__PURE__*/React.createElement(Iconify, {
        icon: showPassword ? 'eva:eye-fill' : 'eva:eye-off-fill'
      })))
    }
  })), /*#__PURE__*/React.createElement(Stack, {
    direction: "row",
    alignItems: "center",
    justifyContent: "space-between",
    sx: {
      my: 2
    }
  }, /*#__PURE__*/React.createElement(RHFCheckbox, {
    name: "remember",
    label: "Remember me"
  }), /*#__PURE__*/React.createElement(Link, {
    component: RouterLink,
    variant: "subtitle2",
    to: PATH_AUTH.resetPassword
  }, "Forgot password?")), /*#__PURE__*/React.createElement(LoadingButton, {
    fullWidth: true,
    size: "large",
    type: "submit",
    variant: "contained",
    loading: isSubmitting
  }, "Login"));
}