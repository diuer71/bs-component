function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

import MapGL from 'react-map-gl';
import { useState } from 'react';
import DeckGL, { ArcLayer } from 'deck.gl'; // components

import { MapControlScale, MapControlGeolocate, MapControlNavigation, MapControlFullscreen } from '../../components/map'; // ----------------------------------------------------------------------

export default function MapDeckglOverlay({ ...other
}) {
  const [viewport, setViewport] = useState({
    longitude: -122.45,
    latitude: 37.78,
    zoom: 11,
    bearing: 0,
    pitch: 30
  });
  return /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement(MapGL, _extends({}, viewport, {
    onViewportChange: setViewport,
    maxPitch: 85
  }, other), /*#__PURE__*/React.createElement(MapControlScale, null), /*#__PURE__*/React.createElement(MapControlNavigation, null), /*#__PURE__*/React.createElement(MapControlFullscreen, null), /*#__PURE__*/React.createElement(MapControlGeolocate, null), /*#__PURE__*/React.createElement(DeckGL, {
    viewState: viewport,
    layers: [new ArcLayer({
      data: [{
        sourcePosition: [-122.41669, 37.7853],
        targetPosition: [-122.45669, 37.781]
      }],
      strokeWidth: 4,
      getSourceColor: () => [0, 0, 255],
      getTargetColor: () => [0, 255, 0]
    })]
  })));
}