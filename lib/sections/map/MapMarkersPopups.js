function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

import MapGL from 'react-map-gl';
import PropTypes from 'prop-types';
import { useState } from 'react'; // @mui

import { Box, Typography } from '@mui/material'; // components

import Image from '../../components/Image';
import { MapControlPopup, MapControlMarker, MapControlScale, MapControlGeolocate, MapControlNavigation, MapControlFullscreen } from '../../components/map'; // ----------------------------------------------------------------------

MapMarkersPopups.propTypes = {
  data: PropTypes.array
};
export default function MapMarkersPopups({
  data,
  ...other
}) {
  const [tooltip, setTooltip] = useState(null);
  const [viewport, setViewport] = useState({
    zoom: 2
  });
  return /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement(MapGL, _extends({}, viewport, {
    onViewportChange: setViewport
  }, other), /*#__PURE__*/React.createElement(MapControlScale, null), /*#__PURE__*/React.createElement(MapControlNavigation, null), /*#__PURE__*/React.createElement(MapControlFullscreen, null), /*#__PURE__*/React.createElement(MapControlGeolocate, null), data.map(country => /*#__PURE__*/React.createElement(MapControlMarker, {
    key: country.name,
    latitude: country.latlng[0],
    longitude: country.latlng[1],
    onClick: () => setTooltip(country)
  })), tooltip && /*#__PURE__*/React.createElement(MapControlPopup, {
    longitude: tooltip.latlng[1],
    latitude: tooltip.latlng[0],
    onClose: () => setTooltip(null)
  }, /*#__PURE__*/React.createElement(Box, {
    sx: {
      color: 'common.white'
    }
  }, /*#__PURE__*/React.createElement(Box, {
    sx: {
      mb: 1,
      display: 'flex',
      alignItems: 'center'
    }
  }, /*#__PURE__*/React.createElement(Box, {
    sx: {
      height: '18px',
      minWidth: '28px',
      marginRight: '8px',
      borderRadius: '4px',
      backgroundSize: 'cover',
      backgroundPosition: 'center',
      backgroundRepeat: 'no-repeat',
      backgroundImage: `url(https://cdn.staticaly.com/gh/hjnilsson/country-flags/master/svg/${tooltip.country_code.toLowerCase()}.svg)`
    }
  }), /*#__PURE__*/React.createElement(Typography, {
    variant: "subtitle2"
  }, tooltip.name)), /*#__PURE__*/React.createElement(Typography, {
    component: "div",
    variant: "caption"
  }, "Timezones: ", tooltip.timezones), /*#__PURE__*/React.createElement(Typography, {
    component: "div",
    variant: "caption"
  }, "Lat: ", tooltip.latlng[0]), /*#__PURE__*/React.createElement(Typography, {
    component: "div",
    variant: "caption"
  }, "Long: ", tooltip.latlng[1]), /*#__PURE__*/React.createElement(Image, {
    alt: tooltip.name,
    src: tooltip.photo,
    ratio: "4/3",
    sx: {
      mt: 1,
      borderRadius: 1
    }
  })))));
}