import PropTypes from 'prop-types';
import { memo } from 'react'; // @mui

import { styled } from '@mui/material/styles';
import { Radio, Typography, RadioGroup, FormControlLabel } from '@mui/material'; // utils

import cssStyles from '../../../utils/cssStyles'; // ----------------------------------------------------------------------

const RootStyle = styled('div')(({
  theme
}) => ({ ...cssStyles().bgBlur({
    color: theme.palette.grey[900]
  }),
  zIndex: 9,
  minWidth: 200,
  position: 'absolute',
  top: theme.spacing(1),
  right: theme.spacing(1),
  padding: theme.spacing(2),
  borderRadius: theme.shape.borderRadius
})); // ----------------------------------------------------------------------

ControlPanel.propTypes = {
  themes: PropTypes.object,
  selectTheme: PropTypes.string,
  onChangeTheme: PropTypes.func
};

function ControlPanel({
  themes,
  selectTheme,
  onChangeTheme
}) {
  return /*#__PURE__*/React.createElement(RootStyle, null, /*#__PURE__*/React.createElement(Typography, {
    gutterBottom: true,
    variant: "subtitle2",
    sx: {
      color: 'common.white'
    }
  }, "Select Theme:"), /*#__PURE__*/React.createElement(RadioGroup, {
    value: selectTheme,
    onChange: onChangeTheme
  }, Object.keys(themes).map(item => /*#__PURE__*/React.createElement(FormControlLabel, {
    key: item,
    value: item,
    control: /*#__PURE__*/React.createElement(Radio, {
      size: "small"
    }),
    label: item,
    sx: {
      color: 'common.white',
      textTransform: 'capitalize'
    }
  }))));
}

export default /*#__PURE__*/memo(ControlPanel);