function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

import PropTypes from 'prop-types';
import ReactMapGL from 'react-map-gl';
import { useState, useCallback } from 'react'; // components

import { MapControlScale, MapControlGeolocate, MapControlNavigation, MapControlFullscreen } from '../../../components/map'; //

import ControlPanel from './ControlPanel'; // ----------------------------------------------------------------------

MapChangeTheme.propTypes = {
  themes: PropTypes.object
};
export default function MapChangeTheme({
  themes,
  ...other
}) {
  const [selectTheme, setSelectTheme] = useState('outdoors');
  const [viewport, setViewport] = useState({
    latitude: 37.785164,
    longitude: -100,
    zoom: 3.5,
    bearing: 0,
    pitch: 0
  });
  const handleChangeTheme = useCallback(event => setSelectTheme(event.target.value), []);
  return /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement(ReactMapGL, _extends({}, viewport, {
    onViewportChange: setViewport,
    mapStyle: themes[selectTheme]
  }, other), /*#__PURE__*/React.createElement(MapControlScale, null), /*#__PURE__*/React.createElement(MapControlNavigation, null), /*#__PURE__*/React.createElement(MapControlFullscreen, null), /*#__PURE__*/React.createElement(MapControlGeolocate, null)), /*#__PURE__*/React.createElement(ControlPanel, {
    themes: themes,
    selectTheme: selectTheme,
    onChangeTheme: handleChangeTheme
  }));
}