function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

import MapGL from 'react-map-gl';
import { useState, useCallback } from 'react'; // components

import { MapControlScale, MapControlMarker, MapControlGeolocate, MapControlNavigation, MapControlFullscreen } from '../../../components/map'; //

import ControlPanel from './ControlPanel'; // ----------------------------------------------------------------------

export default function MapDraggableMarkers({ ...other
}) {
  const [events, logEvents] = useState({});
  const [marker, setMarker] = useState({
    latitude: 40,
    longitude: -100
  });
  const [viewport, setViewport] = useState({
    latitude: 40,
    longitude: -100,
    zoom: 3.5,
    bearing: 0,
    pitch: 0
  });
  const onMarkerDragStart = useCallback(event => {
    logEvents(_events => ({ ..._events,
      onDragStart: event.lngLat
    }));
  }, []);
  const onMarkerDrag = useCallback(event => {
    logEvents(_events => ({ ..._events,
      onDrag: event.lngLat
    }));
  }, []);
  const onMarkerDragEnd = useCallback(event => {
    logEvents(_events => ({ ..._events,
      onDragEnd: event.lngLat
    }));
    setMarker({
      longitude: event.lngLat[0],
      latitude: event.lngLat[1]
    });
  }, []);
  return /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement(MapGL, _extends({}, viewport, {
    onViewportChange: setViewport
  }, other), /*#__PURE__*/React.createElement(MapControlScale, null), /*#__PURE__*/React.createElement(MapControlNavigation, null), /*#__PURE__*/React.createElement(MapControlFullscreen, null), /*#__PURE__*/React.createElement(MapControlGeolocate, null), /*#__PURE__*/React.createElement(MapControlMarker, {
    draggable: true,
    longitude: marker.longitude,
    latitude: marker.latitude,
    offsetTop: -20,
    offsetLeft: -10,
    onDragStart: onMarkerDragStart,
    onDrag: onMarkerDrag,
    onDragEnd: onMarkerDragEnd
  })), /*#__PURE__*/React.createElement(ControlPanel, {
    events: events
  }));
}