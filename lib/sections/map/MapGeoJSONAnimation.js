function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

import { useState, useEffect } from 'react';
import MapGL, { Layer, Source } from 'react-map-gl'; // @mui

import { useTheme } from '@mui/material/styles'; // components

import { MapControlScale, MapControlGeolocate, MapControlNavigation, MapControlFullscreen } from '../../components/map'; // ----------------------------------------------------------------------

function pointOnCircle({
  center,
  angle,
  radius
}) {
  return {
    type: 'Point',
    coordinates: [center[0] + Math.cos(angle) * radius, center[1] + Math.sin(angle) * radius]
  };
}

export default function MapGeoJSONAnimation({ ...other
}) {
  const theme = useTheme();
  const [pointData, setPointData] = useState(null);
  const [viewport, setViewport] = useState({
    latitude: 0,
    longitude: -100,
    zoom: 3,
    bearing: 0,
    pitch: 0
  });
  const pointLayer = {
    type: 'circle',
    paint: {
      'circle-radius': 10,
      'circle-color': theme.palette.error.main
    }
  };
  useEffect(() => {
    const animation = window.requestAnimationFrame(() => setPointData(pointOnCircle({
      center: [-100, 0],
      angle: Date.now() / 1000,
      radius: 20
    })));
    return () => window.cancelAnimationFrame(animation);
  });
  return /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement(MapGL, _extends({}, viewport, {
    onViewportChange: setViewport
  }, other), /*#__PURE__*/React.createElement(MapControlScale, null), /*#__PURE__*/React.createElement(MapControlNavigation, null), /*#__PURE__*/React.createElement(MapControlFullscreen, null), /*#__PURE__*/React.createElement(MapControlGeolocate, null), pointData && /*#__PURE__*/React.createElement(Source, {
    type: "geojson",
    data: pointData
  }, /*#__PURE__*/React.createElement(Layer, pointLayer))));
}