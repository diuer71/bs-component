function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

import MapGL, { Layer, Source } from 'react-map-gl';
import { useState, useCallback, useMemo } from 'react'; // @mui

import { useTheme } from '@mui/material/styles';
import { Typography } from '@mui/material'; // components

import { MapControlPopup, MapControlScale, MapControlGeolocate, MapControlNavigation, MapControlFullscreen } from '../../components/map'; // ----------------------------------------------------------------------

export default function MapHighlightByFilter({ ...other
}) {
  const theme = useTheme();
  const [hoverInfo, setHoverInfo] = useState(null);
  const [viewport, setViewport] = useState({
    latitude: 38.88,
    longitude: -98,
    zoom: 3,
    minZoom: 2,
    bearing: 0,
    pitch: 0
  });
  const selectedCounty = hoverInfo && hoverInfo.countyName || '';
  const filter = useMemo(() => ['in', 'COUNTY', selectedCounty], [selectedCounty]);
  const countiesLayer = {
    id: 'counties',
    type: 'fill',
    source: 'counties',
    'source-layer': 'original',
    paint: {
      'fill-outline-color': theme.palette.grey[900],
      'fill-color': theme.palette.grey[900],
      'fill-opacity': 0.12
    }
  };
  const highlightLayer = {
    id: 'counties-highlighted',
    type: 'fill',
    source: 'counties',
    'source-layer': 'original',
    paint: {
      'fill-outline-color': theme.palette.error.main,
      'fill-color': theme.palette.error.main,
      'fill-opacity': 0.48
    }
  };
  const onHover = useCallback(event => {
    const county = event.features && event.features[0];
    setHoverInfo({
      longitude: event.lngLat[0],
      latitude: event.lngLat[1],
      countyName: county && county.properties.COUNTY
    });
  }, []);
  return /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement(MapGL, _extends({}, viewport, {
    onViewportChange: setViewport,
    onHover: onHover,
    interactiveLayerIds: ['counties']
  }, other), /*#__PURE__*/React.createElement(MapControlScale, null), /*#__PURE__*/React.createElement(MapControlNavigation, null), /*#__PURE__*/React.createElement(MapControlFullscreen, null), /*#__PURE__*/React.createElement(MapControlGeolocate, null), /*#__PURE__*/React.createElement(Source, {
    type: "vector",
    url: "mapbox://mapbox.82pkq93d"
  }, /*#__PURE__*/React.createElement(Layer, _extends({
    beforeId: "waterway-label"
  }, countiesLayer)), /*#__PURE__*/React.createElement(Layer, _extends({
    beforeId: "waterway-label"
  }, highlightLayer, {
    filter: filter
  }))), selectedCounty && /*#__PURE__*/React.createElement(MapControlPopup, {
    longitude: hoverInfo.longitude,
    latitude: hoverInfo.latitude,
    closeButton: false
  }, /*#__PURE__*/React.createElement(Typography, {
    variant: "body2",
    sx: {
      color: 'common.white'
    }
  }, selectedCounty))));
}