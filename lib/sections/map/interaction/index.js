function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

import MapGL from 'react-map-gl';
import { useState, useCallback } from 'react'; // components

import { MapControlScale, MapControlGeolocate, MapControlNavigation, MapControlFullscreen } from '../../../components/map'; //

import ControlPanel from './ControlPanel'; // ----------------------------------------------------------------------

export default function MapInteraction({ ...other
}) {
  const [interactionState, setInteractionState] = useState({});
  const [viewport, setViewport] = useState({
    latitude: 37.729,
    longitude: -122.36,
    zoom: 11,
    bearing: 0,
    pitch: 50
  });
  const [settings, setSettings] = useState({
    dragPan: true,
    dragRotate: true,
    scrollZoom: true,
    touchZoom: true,
    touchRotate: true,
    keyboard: true,
    doubleClickZoom: true,
    minZoom: 0,
    maxZoom: 20,
    minPitch: 0,
    maxPitch: 85
  });
  const handleChangeSetting = useCallback((name, value) => setSettings(settings => ({ ...settings,
    [name]: value
  })), []);
  return /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement(MapGL, _extends({}, viewport, settings, {
    onViewportChange: setViewport,
    onInteractionStateChange: interactionState => setInteractionState(interactionState)
  }, other), /*#__PURE__*/React.createElement(MapControlScale, null), /*#__PURE__*/React.createElement(MapControlNavigation, null), /*#__PURE__*/React.createElement(MapControlFullscreen, null), /*#__PURE__*/React.createElement(MapControlGeolocate, null), /*#__PURE__*/React.createElement(ControlPanel, {
    settings: settings,
    interactionState: interactionState,
    onChange: handleChangeSetting
  })));
}