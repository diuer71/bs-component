// routes
import Router from './routes'; // theme

import ThemeProvider from './theme'; // components

import Settings from './components/settings';
import RtlLayout from './components/RtlLayout';
import { ChartStyle } from './components/chart';
import ScrollToTop from './components/ScrollToTop';
import { ProgressBarStyle } from './components/ProgressBar';
import NotistackProvider from './components/NotistackProvider';
import ThemeColorPresets from './components/ThemeColorPresets';
import ThemeLocalization from './components/ThemeLocalization';
import MotionLazyContainer from './components/animate/MotionLazyContainer'; // ----------------------------------------------------------------------

export default function App() {
  return /*#__PURE__*/React.createElement(ThemeProvider, null, /*#__PURE__*/React.createElement(ThemeColorPresets, null, /*#__PURE__*/React.createElement(ThemeLocalization, null, /*#__PURE__*/React.createElement(RtlLayout, null, /*#__PURE__*/React.createElement(NotistackProvider, null, /*#__PURE__*/React.createElement(MotionLazyContainer, null, /*#__PURE__*/React.createElement(ProgressBarStyle, null), /*#__PURE__*/React.createElement(ChartStyle, null), /*#__PURE__*/React.createElement(Settings, null), /*#__PURE__*/React.createElement(ScrollToTop, null), /*#__PURE__*/React.createElement(Router, null)))))));
}