import React from 'react'; // @mui

import { useTheme } from '@mui/material/styles';
import { Container, Grid, Stack } from '@mui/material'; // hooks

import useAuth from '../../hooks/useAuth';
import useSettings from '../../hooks/useSettings'; // components

import Page from '../../components/Page'; // sections

import { AppWidget, AppWelcome, AppFeatured, AppNewInvoice, AppTopAuthors, AppTopRelated, AppAreaInstalled, AppWidgetSummary, AppCurrentDownload, AppTopInstalledCountries } from '../../sections/@dashboard/general/app'; // ----------------------------------------------------------------------

export default function GeneralApp() {
  const {
    user
  } = useAuth();
  const theme = useTheme();
  const {
    themeStretch
  } = useSettings();
  return /*#__PURE__*/React.createElement(Page, {
    title: "General: App"
  }, /*#__PURE__*/React.createElement(Container, {
    maxWidth: themeStretch ? false : 'xl'
  }, /*#__PURE__*/React.createElement(Grid, {
    container: true,
    spacing: 3
  }, /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: 12,
    md: 8
  }, /*#__PURE__*/React.createElement(AppWelcome, {
    displayName: user?.displayName
  })), /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: 12,
    md: 4
  }, /*#__PURE__*/React.createElement(AppFeatured, null)), /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: 12,
    md: 4
  }, /*#__PURE__*/React.createElement(AppWidgetSummary, {
    title: "Total Active Users",
    percent: 2.6,
    total: 18765,
    chartColor: theme.palette.primary.main,
    chartData: [5, 18, 12, 51, 68, 11, 39, 37, 27, 20]
  })), /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: 12,
    md: 4
  }, /*#__PURE__*/React.createElement(AppWidgetSummary, {
    title: "Total Installed",
    percent: 0.2,
    total: 4876,
    chartColor: theme.palette.chart.blue[0],
    chartData: [20, 41, 63, 33, 28, 35, 50, 46, 11, 26]
  })), /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: 12,
    md: 4
  }, /*#__PURE__*/React.createElement(AppWidgetSummary, {
    title: "Total Downloads",
    percent: -0.1,
    total: 678,
    chartColor: theme.palette.chart.red[0],
    chartData: [8, 9, 31, 8, 16, 37, 8, 33, 46, 31]
  })), /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: 12,
    md: 6,
    lg: 4
  }, /*#__PURE__*/React.createElement(AppCurrentDownload, null)), /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: 12,
    md: 6,
    lg: 8
  }, /*#__PURE__*/React.createElement(AppAreaInstalled, null)), /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: 12,
    lg: 8
  }, /*#__PURE__*/React.createElement(AppNewInvoice, null)), /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: 12,
    md: 6,
    lg: 4
  }, /*#__PURE__*/React.createElement(AppTopRelated, null)), /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: 12,
    md: 6,
    lg: 4
  }, /*#__PURE__*/React.createElement(AppTopInstalledCountries, null)), /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: 12,
    md: 6,
    lg: 4
  }, /*#__PURE__*/React.createElement(AppTopAuthors, null)), /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: 12,
    md: 6,
    lg: 4
  }, /*#__PURE__*/React.createElement(Stack, {
    spacing: 3
  }, /*#__PURE__*/React.createElement(AppWidget, {
    title: "Conversion",
    total: 38566,
    icon: 'eva:person-fill',
    chartData: 48
  }), /*#__PURE__*/React.createElement(AppWidget, {
    title: "Applications",
    total: 55566,
    icon: 'eva:email-fill',
    color: "warning",
    chartData: 75
  }))))));
}