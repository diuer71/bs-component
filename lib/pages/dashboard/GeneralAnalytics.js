import React from 'react'; // @mui

import { Grid, Container, Typography } from '@mui/material'; // hooks

import useSettings from '../../hooks/useSettings'; // components

import Page from '../../components/Page'; // sections

import { AnalyticsTasks, AnalyticsNewsUpdate, AnalyticsOrderTimeline, AnalyticsCurrentVisits, AnalyticsWebsiteVisits, AnalyticsTrafficBySite, AnalyticsWidgetSummary, AnalyticsCurrentSubject, AnalyticsConversionRates } from '../../sections/@dashboard/general/analytics'; // ----------------------------------------------------------------------

export default function GeneralAnalytics() {
  const {
    themeStretch
  } = useSettings();
  return /*#__PURE__*/React.createElement(Page, {
    title: "General: Analytics"
  }, /*#__PURE__*/React.createElement(Container, {
    maxWidth: themeStretch ? false : 'xl'
  }, /*#__PURE__*/React.createElement(Typography, {
    variant: "h4",
    sx: {
      mb: 5
    }
  }, "Hi, Welcome back"), /*#__PURE__*/React.createElement(Grid, {
    container: true,
    spacing: 3
  }, /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: 12,
    sm: 6,
    md: 3
  }, /*#__PURE__*/React.createElement(AnalyticsWidgetSummary, {
    title: "Weekly Sales",
    total: 714000,
    icon: 'ant-design:android-filled'
  })), /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: 12,
    sm: 6,
    md: 3
  }, /*#__PURE__*/React.createElement(AnalyticsWidgetSummary, {
    title: "New Users",
    total: 1352831,
    color: "info",
    icon: 'ant-design:apple-filled'
  })), /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: 12,
    sm: 6,
    md: 3
  }, /*#__PURE__*/React.createElement(AnalyticsWidgetSummary, {
    title: "Item Orders",
    total: 1723315,
    color: "warning",
    icon: 'ant-design:windows-filled'
  })), /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: 12,
    sm: 6,
    md: 3
  }, /*#__PURE__*/React.createElement(AnalyticsWidgetSummary, {
    title: "Bug Reports",
    total: 234,
    color: "error",
    icon: 'ant-design:bug-filled'
  })), /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: 12,
    md: 6,
    lg: 8
  }, /*#__PURE__*/React.createElement(AnalyticsWebsiteVisits, null)), /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: 12,
    md: 6,
    lg: 4
  }, /*#__PURE__*/React.createElement(AnalyticsCurrentVisits, null)), /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: 12,
    md: 6,
    lg: 8
  }, /*#__PURE__*/React.createElement(AnalyticsConversionRates, null)), /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: 12,
    md: 6,
    lg: 4
  }, /*#__PURE__*/React.createElement(AnalyticsCurrentSubject, null)), /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: 12,
    md: 6,
    lg: 8
  }, /*#__PURE__*/React.createElement(AnalyticsNewsUpdate, null)), /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: 12,
    md: 6,
    lg: 4
  }, /*#__PURE__*/React.createElement(AnalyticsOrderTimeline, null)), /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: 12,
    md: 6,
    lg: 4
  }, /*#__PURE__*/React.createElement(AnalyticsTrafficBySite, null)), /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: 12,
    md: 6,
    lg: 8
  }, /*#__PURE__*/React.createElement(AnalyticsTasks, null)))));
}