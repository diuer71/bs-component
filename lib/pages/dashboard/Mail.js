import React from 'react';
import { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom'; // @mui

import { Container, Card } from '@mui/material'; // redux

import { useDispatch } from '../../redux/store';
import { getLabels } from '../../redux/slices/mail'; // routes

import { PATH_DASHBOARD } from '../../routes/paths'; // hooks

import useSettings from '../../hooks/useSettings'; // components

import Page from '../../components/Page';
import HeaderBreadcrumbs from '../../components/HeaderBreadcrumbs';
import { MailList, MailDetails, MailSidebar, MailCompose } from '../../sections/@dashboard/mail'; // ----------------------------------------------------------------------

export default function Mail() {
  const {
    themeStretch
  } = useSettings();
  const dispatch = useDispatch();
  const {
    mailId
  } = useParams();
  const [openSidebar, setOpenSidebar] = useState(false);
  const [openCompose, setOpenCompose] = useState(false);
  useEffect(() => {
    dispatch(getLabels());
  }, [dispatch]);
  return /*#__PURE__*/React.createElement(Page, {
    title: "Mail"
  }, /*#__PURE__*/React.createElement(Container, {
    maxWidth: themeStretch ? false : 'xl'
  }, /*#__PURE__*/React.createElement(HeaderBreadcrumbs, {
    heading: "Mail",
    links: [{
      name: 'Dashboard',
      href: PATH_DASHBOARD.root
    }, {
      name: 'Mail'
    }]
  }), /*#__PURE__*/React.createElement(Card, {
    sx: {
      height: {
        md: '72vh'
      },
      display: {
        md: 'flex'
      }
    }
  }, /*#__PURE__*/React.createElement(MailSidebar, {
    isOpenSidebar: openSidebar,
    onCloseSidebar: () => setOpenSidebar(false),
    onOpenCompose: () => setOpenCompose(true)
  }), mailId ? /*#__PURE__*/React.createElement(MailDetails, null) : /*#__PURE__*/React.createElement(MailList, {
    onOpenSidebar: () => setOpenSidebar(true)
  }), /*#__PURE__*/React.createElement(MailCompose, {
    isOpenCompose: openCompose,
    onCloseCompose: () => setOpenCompose(false)
  }))));
}