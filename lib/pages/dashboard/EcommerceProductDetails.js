import React from 'react';
import { sentenceCase } from 'change-case';
import { useParams } from 'react-router-dom';
import { useEffect, useState } from 'react'; // @mui

import { alpha, styled } from '@mui/material/styles';
import { Box, Tab, Card, Grid, Divider, Container, Typography } from '@mui/material';
import { TabContext, TabList, TabPanel } from '@mui/lab'; // redux

import { useDispatch, useSelector } from '../../redux/store';
import { getProduct, addCart, onGotoStep } from '../../redux/slices/product'; // routes

import { PATH_DASHBOARD } from '../../routes/paths'; // hooks

import useSettings from '../../hooks/useSettings'; // components

import Page from '../../components/Page';
import Iconify from '../../components/Iconify';
import Markdown from '../../components/Markdown';
import { SkeletonProduct } from '../../components/skeleton';
import HeaderBreadcrumbs from '../../components/HeaderBreadcrumbs'; // sections

import { ProductDetailsSummary, ProductDetailsReview, ProductDetailsCarousel } from '../../sections/@dashboard/e-commerce/product-details';
import CartWidget from '../../sections/@dashboard/e-commerce/CartWidget'; // ----------------------------------------------------------------------

const PRODUCT_DESCRIPTION = [{
  title: '100% Original',
  description: 'Chocolate bar candy canes ice cream toffee cookie halvah.',
  icon: 'ic:round-verified'
}, {
  title: '10 Day Replacement',
  description: 'Marshmallow biscuit donut dragée fruitcake wafer.',
  icon: 'eva:clock-fill'
}, {
  title: 'Year Warranty',
  description: 'Cotton candy gingerbread cake I love sugar sweet.',
  icon: 'ic:round-verified-user'
}];
const IconWrapperStyle = styled('div')(({
  theme
}) => ({
  margin: 'auto',
  display: 'flex',
  borderRadius: '50%',
  alignItems: 'center',
  width: theme.spacing(8),
  justifyContent: 'center',
  height: theme.spacing(8),
  marginBottom: theme.spacing(3),
  color: theme.palette.primary.main,
  backgroundColor: `${alpha(theme.palette.primary.main, 0.08)}`
})); // ----------------------------------------------------------------------

export default function EcommerceProductDetails() {
  const {
    themeStretch
  } = useSettings();
  const dispatch = useDispatch();
  const [value, setValue] = useState('1');
  const {
    name = ''
  } = useParams();
  const {
    product,
    error,
    checkout
  } = useSelector(state => state.product);
  useEffect(() => {
    dispatch(getProduct(name));
  }, [dispatch, name]);

  const handleAddCart = product => {
    dispatch(addCart(product));
  };

  const handleGotoStep = step => {
    dispatch(onGotoStep(step));
  };

  return /*#__PURE__*/React.createElement(Page, {
    title: "Ecommerce: Product Details"
  }, /*#__PURE__*/React.createElement(Container, {
    maxWidth: themeStretch ? false : 'lg'
  }, /*#__PURE__*/React.createElement(HeaderBreadcrumbs, {
    heading: "Product Details",
    links: [{
      name: 'Dashboard',
      href: PATH_DASHBOARD.root
    }, {
      name: 'E-Commerce',
      href: PATH_DASHBOARD.eCommerce.root
    }, {
      name: 'Shop',
      href: PATH_DASHBOARD.eCommerce.shop
    }, {
      name: sentenceCase(name)
    }]
  }), /*#__PURE__*/React.createElement(CartWidget, null), product && /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement(Card, null, /*#__PURE__*/React.createElement(Grid, {
    container: true
  }, /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: 12,
    md: 6,
    lg: 7
  }, /*#__PURE__*/React.createElement(ProductDetailsCarousel, {
    product: product
  })), /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: 12,
    md: 6,
    lg: 5
  }, /*#__PURE__*/React.createElement(ProductDetailsSummary, {
    product: product,
    cart: checkout.cart,
    onAddCart: handleAddCart,
    onGotoStep: handleGotoStep
  })))), /*#__PURE__*/React.createElement(Grid, {
    container: true,
    sx: {
      my: 8
    }
  }, PRODUCT_DESCRIPTION.map(item => /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: 12,
    md: 4,
    key: item.title
  }, /*#__PURE__*/React.createElement(Box, {
    sx: {
      my: 2,
      mx: 'auto',
      maxWidth: 280,
      textAlign: 'center'
    }
  }, /*#__PURE__*/React.createElement(IconWrapperStyle, null, /*#__PURE__*/React.createElement(Iconify, {
    icon: item.icon,
    width: 36,
    height: 36
  })), /*#__PURE__*/React.createElement(Typography, {
    variant: "subtitle1",
    gutterBottom: true
  }, item.title), /*#__PURE__*/React.createElement(Typography, {
    sx: {
      color: 'text.secondary'
    }
  }, item.description))))), /*#__PURE__*/React.createElement(Card, null, /*#__PURE__*/React.createElement(TabContext, {
    value: value
  }, /*#__PURE__*/React.createElement(Box, {
    sx: {
      px: 3,
      bgcolor: 'background.neutral'
    }
  }, /*#__PURE__*/React.createElement(TabList, {
    onChange: (e, value) => setValue(value)
  }, /*#__PURE__*/React.createElement(Tab, {
    disableRipple: true,
    value: "1",
    label: "Description"
  }), /*#__PURE__*/React.createElement(Tab, {
    disableRipple: true,
    value: "2",
    label: `Review (${product.reviews.length})`,
    sx: {
      '& .MuiTab-wrapper': {
        whiteSpace: 'nowrap'
      }
    }
  }))), /*#__PURE__*/React.createElement(Divider, null), /*#__PURE__*/React.createElement(TabPanel, {
    value: "1"
  }, /*#__PURE__*/React.createElement(Box, {
    sx: {
      p: 3
    }
  }, /*#__PURE__*/React.createElement(Markdown, {
    children: product.description
  }))), /*#__PURE__*/React.createElement(TabPanel, {
    value: "2"
  }, /*#__PURE__*/React.createElement(ProductDetailsReview, {
    product: product
  }))))), !product && /*#__PURE__*/React.createElement(SkeletonProduct, null), error && /*#__PURE__*/React.createElement(Typography, {
    variant: "h6"
  }, "404 Product not found")));
}