import React from 'react';
import { useEffect } from 'react';
import { paramCase } from 'change-case';
import { useParams, useLocation } from 'react-router-dom'; // @mui

import { Container } from '@mui/material'; // redux

import { useDispatch, useSelector } from '../../redux/store';
import { getProducts } from '../../redux/slices/product'; // routes

import { PATH_DASHBOARD } from '../../routes/paths'; // hooks

import useSettings from '../../hooks/useSettings'; // components

import Page from '../../components/Page';
import HeaderBreadcrumbs from '../../components/HeaderBreadcrumbs';
import ProductNewForm from '../../sections/@dashboard/e-commerce/ProductNewForm'; // ----------------------------------------------------------------------

export default function EcommerceProductCreate() {
  const {
    themeStretch
  } = useSettings();
  const dispatch = useDispatch();
  const {
    pathname
  } = useLocation();
  const {
    name
  } = useParams();
  const {
    products
  } = useSelector(state => state.product);
  const isEdit = pathname.includes('edit');
  const currentProduct = products.find(product => paramCase(product.name) === name);
  useEffect(() => {
    dispatch(getProducts());
  }, [dispatch]);
  return /*#__PURE__*/React.createElement(Page, {
    title: "Ecommerce: Create a new product"
  }, /*#__PURE__*/React.createElement(Container, {
    maxWidth: themeStretch ? false : 'lg'
  }, /*#__PURE__*/React.createElement(HeaderBreadcrumbs, {
    heading: !isEdit ? 'Create a new product' : 'Edit product',
    links: [{
      name: 'Dashboard',
      href: PATH_DASHBOARD.root
    }, {
      name: 'E-Commerce',
      href: PATH_DASHBOARD.eCommerce.root
    }, {
      name: !isEdit ? 'New product' : name
    }]
  }), /*#__PURE__*/React.createElement(ProductNewForm, {
    isEdit: isEdit,
    currentProduct: currentProduct
  })));
}