import React from 'react';
import sum from 'lodash/sum'; // @mui

import { styled } from '@mui/material/styles';
import { Box, Grid, Card, Table, Divider, TableRow, Container, TableBody, TableHead, TableCell, Typography, TableContainer } from '@mui/material'; // routes

import { PATH_DASHBOARD } from '../../routes/paths'; // utils

import { fCurrency } from '../../utils/formatNumber'; // _mock_

import { _invoice } from '../../_mock'; // hooks

import useSettings from '../../hooks/useSettings'; // components

import Page from '../../components/Page';
import Label from '../../components/Label';
import Image from '../../components/Image';
import Scrollbar from '../../components/Scrollbar';
import HeaderBreadcrumbs from '../../components/HeaderBreadcrumbs'; // sections

import { InvoiceToolbar } from '../../sections/@dashboard/e-commerce/invoice'; // ----------------------------------------------------------------------

const RowResultStyle = styled(TableRow)(({
  theme
}) => ({
  '& td': {
    paddingTop: theme.spacing(1),
    paddingBottom: theme.spacing(1)
  }
})); // ----------------------------------------------------------------------

export default function EcommerceInvoice() {
  const {
    themeStretch
  } = useSettings();
  const subTotal = sum(_invoice.items.map(item => item.price * item.qty));
  const total = subTotal - _invoice.discount + _invoice.taxes;
  return /*#__PURE__*/React.createElement(Page, {
    title: "Ecommerce: Invoice"
  }, /*#__PURE__*/React.createElement(Container, {
    maxWidth: themeStretch ? false : 'lg'
  }, /*#__PURE__*/React.createElement(HeaderBreadcrumbs, {
    heading: "Invoice Details",
    links: [{
      name: 'Dashboard',
      href: PATH_DASHBOARD.root
    }, {
      name: 'E-Commerce',
      href: PATH_DASHBOARD.eCommerce.root
    }, {
      name: 'Invoice'
    }]
  }), /*#__PURE__*/React.createElement(InvoiceToolbar, {
    invoice: _invoice
  }), /*#__PURE__*/React.createElement(Card, {
    sx: {
      pt: 5,
      px: 5
    }
  }, /*#__PURE__*/React.createElement(Grid, {
    container: true
  }, /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: 12,
    sm: 6,
    sx: {
      mb: 5
    }
  }, /*#__PURE__*/React.createElement(Image, {
    disabledEffect: true,
    visibleByDefault: true,
    alt: "logo",
    src: "/logo/logo_full.svg",
    sx: {
      maxWidth: 120
    }
  })), /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: 12,
    sm: 6,
    sx: {
      mb: 5
    }
  }, /*#__PURE__*/React.createElement(Box, {
    sx: {
      textAlign: {
        sm: 'right'
      }
    }
  }, /*#__PURE__*/React.createElement(Label, {
    color: "success",
    sx: {
      textTransform: 'uppercase',
      mb: 1
    }
  }, _invoice.status), /*#__PURE__*/React.createElement(Typography, {
    variant: "h6"
  }, "INV-", _invoice.id))), /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: 12,
    sm: 6,
    sx: {
      mb: 5
    }
  }, /*#__PURE__*/React.createElement(Typography, {
    paragraph: true,
    variant: "overline",
    sx: {
      color: 'text.disabled'
    }
  }, "Invoice from"), /*#__PURE__*/React.createElement(Typography, {
    variant: "body2"
  }, _invoice.invoiceFrom.name), /*#__PURE__*/React.createElement(Typography, {
    variant: "body2"
  }, _invoice.invoiceFrom.address), /*#__PURE__*/React.createElement(Typography, {
    variant: "body2"
  }, "Phone: ", _invoice.invoiceFrom.phone)), /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: 12,
    sm: 6,
    sx: {
      mb: 5
    }
  }, /*#__PURE__*/React.createElement(Typography, {
    paragraph: true,
    variant: "overline",
    sx: {
      color: 'text.disabled'
    }
  }, "Invoice to"), /*#__PURE__*/React.createElement(Typography, {
    variant: "body2"
  }, _invoice.invoiceTo.name), /*#__PURE__*/React.createElement(Typography, {
    variant: "body2"
  }, _invoice.invoiceTo.address), /*#__PURE__*/React.createElement(Typography, {
    variant: "body2"
  }, "Phone: ", _invoice.invoiceTo.phone))), /*#__PURE__*/React.createElement(Scrollbar, null, /*#__PURE__*/React.createElement(TableContainer, {
    sx: {
      minWidth: 960
    }
  }, /*#__PURE__*/React.createElement(Table, null, /*#__PURE__*/React.createElement(TableHead, {
    sx: {
      borderBottom: theme => `solid 1px ${theme.palette.divider}`,
      '& th': {
        backgroundColor: 'transparent'
      }
    }
  }, /*#__PURE__*/React.createElement(TableRow, null, /*#__PURE__*/React.createElement(TableCell, {
    width: 40
  }, "#"), /*#__PURE__*/React.createElement(TableCell, {
    align: "left"
  }, "Description"), /*#__PURE__*/React.createElement(TableCell, {
    align: "left"
  }, "Qty"), /*#__PURE__*/React.createElement(TableCell, {
    align: "right"
  }, "Unit price"), /*#__PURE__*/React.createElement(TableCell, {
    align: "right"
  }, "Total"))), /*#__PURE__*/React.createElement(TableBody, null, _invoice.items.map((row, index) => /*#__PURE__*/React.createElement(TableRow, {
    key: index,
    sx: {
      borderBottom: theme => `solid 1px ${theme.palette.divider}`
    }
  }, /*#__PURE__*/React.createElement(TableCell, null, index + 1), /*#__PURE__*/React.createElement(TableCell, {
    align: "left"
  }, /*#__PURE__*/React.createElement(Box, {
    sx: {
      maxWidth: 560
    }
  }, /*#__PURE__*/React.createElement(Typography, {
    variant: "subtitle2"
  }, row.title), /*#__PURE__*/React.createElement(Typography, {
    variant: "body2",
    sx: {
      color: 'text.secondary'
    },
    noWrap: true
  }, row.description))), /*#__PURE__*/React.createElement(TableCell, {
    align: "left"
  }, row.qty), /*#__PURE__*/React.createElement(TableCell, {
    align: "right"
  }, fCurrency(row.price)), /*#__PURE__*/React.createElement(TableCell, {
    align: "right"
  }, fCurrency(row.price * row.qty)))), /*#__PURE__*/React.createElement(RowResultStyle, null, /*#__PURE__*/React.createElement(TableCell, {
    colSpan: 3
  }), /*#__PURE__*/React.createElement(TableCell, {
    align: "right"
  }, /*#__PURE__*/React.createElement(Box, {
    sx: {
      mt: 2
    }
  }), /*#__PURE__*/React.createElement(Typography, null, "Subtotal")), /*#__PURE__*/React.createElement(TableCell, {
    align: "right",
    width: 120
  }, /*#__PURE__*/React.createElement(Box, {
    sx: {
      mt: 2
    }
  }), /*#__PURE__*/React.createElement(Typography, null, fCurrency(subTotal)))), /*#__PURE__*/React.createElement(RowResultStyle, null, /*#__PURE__*/React.createElement(TableCell, {
    colSpan: 3
  }), /*#__PURE__*/React.createElement(TableCell, {
    align: "right"
  }, /*#__PURE__*/React.createElement(Typography, null, "Discount")), /*#__PURE__*/React.createElement(TableCell, {
    align: "right",
    width: 120
  }, /*#__PURE__*/React.createElement(Typography, {
    sx: {
      color: 'error.main'
    }
  }, fCurrency(-_invoice.discount)))), /*#__PURE__*/React.createElement(RowResultStyle, null, /*#__PURE__*/React.createElement(TableCell, {
    colSpan: 3
  }), /*#__PURE__*/React.createElement(TableCell, {
    align: "right"
  }, /*#__PURE__*/React.createElement(Typography, null, "Taxes")), /*#__PURE__*/React.createElement(TableCell, {
    align: "right",
    width: 120
  }, /*#__PURE__*/React.createElement(Typography, null, fCurrency(_invoice.taxes)))), /*#__PURE__*/React.createElement(RowResultStyle, null, /*#__PURE__*/React.createElement(TableCell, {
    colSpan: 3
  }), /*#__PURE__*/React.createElement(TableCell, {
    align: "right"
  }, /*#__PURE__*/React.createElement(Typography, {
    variant: "h6"
  }, "Total")), /*#__PURE__*/React.createElement(TableCell, {
    align: "right",
    width: 140
  }, /*#__PURE__*/React.createElement(Typography, {
    variant: "h6"
  }, fCurrency(total)))))))), /*#__PURE__*/React.createElement(Divider, {
    sx: {
      mt: 5
    }
  }), /*#__PURE__*/React.createElement(Grid, {
    container: true
  }, /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: 12,
    md: 9,
    sx: {
      py: 3
    }
  }, /*#__PURE__*/React.createElement(Typography, {
    variant: "subtitle2"
  }, "NOTES"), /*#__PURE__*/React.createElement(Typography, {
    variant: "body2"
  }, "We appreciate your business. Should you need us to add VAT or extra notes let us know!")), /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: 12,
    md: 3,
    sx: {
      py: 3,
      textAlign: 'right'
    }
  }, /*#__PURE__*/React.createElement(Typography, {
    variant: "subtitle2"
  }, "Have a Question?"), /*#__PURE__*/React.createElement(Typography, {
    variant: "body2"
  }, "support@minimals.cc"))))));
}