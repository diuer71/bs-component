import React from 'react';
import { capitalCase } from 'change-case';
import { useState } from 'react'; // @mui

import { Container, Tab, Box, Tabs } from '@mui/material'; // routes

import { PATH_DASHBOARD } from '../../routes/paths'; // hooks

import useSettings from '../../hooks/useSettings'; // _mock_

import { _userPayment, _userAddressBook, _userInvoices, _userAbout } from '../../_mock'; // components

import Page from '../../components/Page';
import Iconify from '../../components/Iconify';
import HeaderBreadcrumbs from '../../components/HeaderBreadcrumbs'; // sections

import { AccountGeneral, AccountBilling, AccountSocialLinks, AccountNotifications, AccountChangePassword } from '../../sections/@dashboard/user/account'; // ----------------------------------------------------------------------

export default function UserAccount() {
  const {
    themeStretch
  } = useSettings();
  const [currentTab, setCurrentTab] = useState('general');
  const ACCOUNT_TABS = [{
    value: 'general',
    icon: /*#__PURE__*/React.createElement(Iconify, {
      icon: 'ic:round-account-box',
      width: 20,
      height: 20
    }),
    component: /*#__PURE__*/React.createElement(AccountGeneral, null)
  }, {
    value: 'billing',
    icon: /*#__PURE__*/React.createElement(Iconify, {
      icon: 'ic:round-receipt',
      width: 20,
      height: 20
    }),
    component: /*#__PURE__*/React.createElement(AccountBilling, {
      cards: _userPayment,
      addressBook: _userAddressBook,
      invoices: _userInvoices
    })
  }, {
    value: 'notifications',
    icon: /*#__PURE__*/React.createElement(Iconify, {
      icon: 'eva:bell-fill',
      width: 20,
      height: 20
    }),
    component: /*#__PURE__*/React.createElement(AccountNotifications, null)
  }, {
    value: 'social_links',
    icon: /*#__PURE__*/React.createElement(Iconify, {
      icon: 'eva:share-fill',
      width: 20,
      height: 20
    }),
    component: /*#__PURE__*/React.createElement(AccountSocialLinks, {
      myProfile: _userAbout
    })
  }, {
    value: 'change_password',
    icon: /*#__PURE__*/React.createElement(Iconify, {
      icon: 'ic:round-vpn-key',
      width: 20,
      height: 20
    }),
    component: /*#__PURE__*/React.createElement(AccountChangePassword, null)
  }];
  return /*#__PURE__*/React.createElement(Page, {
    title: "User: Account Settings"
  }, /*#__PURE__*/React.createElement(Container, {
    maxWidth: themeStretch ? false : 'lg'
  }, /*#__PURE__*/React.createElement(HeaderBreadcrumbs, {
    heading: "Account",
    links: [{
      name: 'Dashboard',
      href: PATH_DASHBOARD.root
    }, {
      name: 'User',
      href: PATH_DASHBOARD.user.root
    }, {
      name: 'Account Settings'
    }]
  }), /*#__PURE__*/React.createElement(Tabs, {
    value: currentTab,
    scrollButtons: "auto",
    variant: "scrollable",
    allowScrollButtonsMobile: true,
    onChange: (e, value) => setCurrentTab(value)
  }, ACCOUNT_TABS.map(tab => /*#__PURE__*/React.createElement(Tab, {
    disableRipple: true,
    key: tab.value,
    label: capitalCase(tab.value),
    icon: tab.icon,
    value: tab.value
  }))), /*#__PURE__*/React.createElement(Box, {
    sx: {
      mb: 5
    }
  }), ACCOUNT_TABS.map(tab => {
    const isMatched = tab.value === currentTab;
    return isMatched && /*#__PURE__*/React.createElement(Box, {
      key: tab.value
    }, tab.component);
  })));
}