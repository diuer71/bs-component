import React from 'react'; // @mui

import { useTheme } from '@mui/material/styles';
import { Container, Grid } from '@mui/material'; // hooks

import useSettings from '../../hooks/useSettings'; // components

import Page from '../../components/Page'; // sections

import { EcommerceWelcome, EcommerceNewProducts, EcommerceYearlySales, EcommerceBestSalesman, EcommerceSaleByGender, EcommerceWidgetSummary, EcommerceSalesOverview, EcommerceLatestProducts, EcommerceCurrentBalance } from '../../sections/@dashboard/general/e-commerce'; // ----------------------------------------------------------------------

export default function GeneralEcommerce() {
  const theme = useTheme();
  const {
    themeStretch
  } = useSettings();
  return /*#__PURE__*/React.createElement(Page, {
    title: "General: E-commerce"
  }, /*#__PURE__*/React.createElement(Container, {
    maxWidth: themeStretch ? false : 'xl'
  }, /*#__PURE__*/React.createElement(Grid, {
    container: true,
    spacing: 3
  }, /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: 12,
    md: 8
  }, /*#__PURE__*/React.createElement(EcommerceWelcome, null)), /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: 12,
    md: 4
  }, /*#__PURE__*/React.createElement(EcommerceNewProducts, null)), /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: 12,
    md: 4
  }, /*#__PURE__*/React.createElement(EcommerceWidgetSummary, {
    title: "Product Sold",
    percent: 2.6,
    total: 765,
    chartColor: theme.palette.primary.main,
    chartData: [22, 8, 35, 50, 82, 84, 77, 12, 87, 43]
  })), /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: 12,
    md: 4
  }, /*#__PURE__*/React.createElement(EcommerceWidgetSummary, {
    title: "Total Balance",
    percent: -0.1,
    total: 18765,
    chartColor: theme.palette.chart.green[0],
    chartData: [56, 47, 40, 62, 73, 30, 23, 54, 67, 68]
  })), /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: 12,
    md: 4
  }, /*#__PURE__*/React.createElement(EcommerceWidgetSummary, {
    title: "Sales Profit",
    percent: 0.6,
    total: 4876,
    chartColor: theme.palette.chart.red[0],
    chartData: [40, 70, 75, 70, 50, 28, 7, 64, 38, 27]
  })), /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: 12,
    md: 6,
    lg: 4
  }, /*#__PURE__*/React.createElement(EcommerceSaleByGender, null)), /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: 12,
    md: 6,
    lg: 8
  }, /*#__PURE__*/React.createElement(EcommerceYearlySales, null)), /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: 12,
    md: 6,
    lg: 8
  }, /*#__PURE__*/React.createElement(EcommerceSalesOverview, null)), /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: 12,
    md: 6,
    lg: 4
  }, /*#__PURE__*/React.createElement(EcommerceCurrentBalance, null)), /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: 12,
    md: 6,
    lg: 8
  }, /*#__PURE__*/React.createElement(EcommerceBestSalesman, null)), /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: 12,
    md: 6,
    lg: 4
  }, /*#__PURE__*/React.createElement(EcommerceLatestProducts, null)))));
}