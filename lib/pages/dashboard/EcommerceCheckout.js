import React from 'react';
import PropTypes from 'prop-types';
import { useEffect } from 'react'; // @mui

import { styled } from '@mui/material/styles';
import { Box, Grid, Step, Stepper, Container, StepLabel, StepConnector } from '@mui/material'; // redux

import { useDispatch, useSelector } from '../../redux/store';
import { getCart, createBilling } from '../../redux/slices/product'; // routes

import { PATH_DASHBOARD } from '../../routes/paths'; // hooks

import useIsMountedRef from '../../hooks/useIsMountedRef';
import useSettings from '../../hooks/useSettings'; // components

import Page from '../../components/Page';
import Iconify from '../../components/Iconify';
import HeaderBreadcrumbs from '../../components/HeaderBreadcrumbs'; // sections

import { CheckoutCart, CheckoutPayment, CheckoutOrderComplete, CheckoutBillingAddress } from '../../sections/@dashboard/e-commerce/checkout'; // ----------------------------------------------------------------------

const STEPS = ['Cart', 'Billing & address', 'Payment'];
const QontoConnector = styled(StepConnector)(({
  theme
}) => ({
  top: 10,
  left: 'calc(-50% + 20px)',
  right: 'calc(50% + 20px)',
  '& .MuiStepConnector-line': {
    borderTopWidth: 2,
    borderColor: theme.palette.divider
  },
  '&.Mui-active, &.Mui-completed': {
    '& .MuiStepConnector-line': {
      borderColor: theme.palette.primary.main
    }
  }
}));
QontoStepIcon.propTypes = {
  active: PropTypes.bool,
  completed: PropTypes.bool
};

function QontoStepIcon({
  active,
  completed
}) {
  return /*#__PURE__*/React.createElement(Box, {
    sx: {
      zIndex: 9,
      width: 24,
      height: 24,
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      color: active ? 'primary.main' : 'text.disabled'
    }
  }, completed ? /*#__PURE__*/React.createElement(Iconify, {
    icon: 'eva:checkmark-fill',
    sx: {
      zIndex: 1,
      width: 20,
      height: 20,
      color: 'primary.main'
    }
  }) : /*#__PURE__*/React.createElement(Box, {
    sx: {
      width: 8,
      height: 8,
      borderRadius: '50%',
      backgroundColor: 'currentColor'
    }
  }));
}

export default function EcommerceCheckout() {
  const {
    themeStretch
  } = useSettings();
  const dispatch = useDispatch();
  const isMountedRef = useIsMountedRef();
  const {
    checkout
  } = useSelector(state => state.product);
  const {
    cart,
    billing,
    activeStep
  } = checkout;
  const isComplete = activeStep === STEPS.length;
  useEffect(() => {
    if (isMountedRef.current) {
      dispatch(getCart(cart));
    }
  }, [dispatch, isMountedRef, cart]);
  useEffect(() => {
    if (activeStep === 1) {
      dispatch(createBilling(null));
    }
  }, [dispatch, activeStep]);
  return /*#__PURE__*/React.createElement(Page, {
    title: "Ecommerce: Checkout"
  }, /*#__PURE__*/React.createElement(Container, {
    maxWidth: themeStretch ? false : 'lg'
  }, /*#__PURE__*/React.createElement(HeaderBreadcrumbs, {
    heading: "Checkout",
    links: [{
      name: 'Dashboard',
      href: PATH_DASHBOARD.root
    }, {
      name: 'E-Commerce',
      href: PATH_DASHBOARD.eCommerce.root
    }, {
      name: 'Checkout'
    }]
  }), /*#__PURE__*/React.createElement(Grid, {
    container: true,
    justifyContent: isComplete ? 'center' : 'flex-start'
  }, /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: 12,
    md: 8,
    sx: {
      mb: 5
    }
  }, /*#__PURE__*/React.createElement(Stepper, {
    alternativeLabel: true,
    activeStep: activeStep,
    connector: /*#__PURE__*/React.createElement(QontoConnector, null)
  }, STEPS.map(label => /*#__PURE__*/React.createElement(Step, {
    key: label
  }, /*#__PURE__*/React.createElement(StepLabel, {
    StepIconComponent: QontoStepIcon,
    sx: {
      '& .MuiStepLabel-label': {
        typography: 'subtitle2',
        color: 'text.disabled'
      }
    }
  }, label)))))), !isComplete ? /*#__PURE__*/React.createElement(React.Fragment, null, activeStep === 0 && /*#__PURE__*/React.createElement(CheckoutCart, null), activeStep === 1 && /*#__PURE__*/React.createElement(CheckoutBillingAddress, null), activeStep === 2 && billing && /*#__PURE__*/React.createElement(CheckoutPayment, null)) : /*#__PURE__*/React.createElement(CheckoutOrderComplete, {
    open: isComplete
  })));
}