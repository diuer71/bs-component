import React from 'react'; // @mui

import { Container } from '@mui/material'; // routes

import { PATH_DASHBOARD } from '../../routes/paths'; // hooks

import useSettings from '../../hooks/useSettings'; // components

import Page from '../../components/Page';
import HeaderBreadcrumbs from '../../components/HeaderBreadcrumbs'; // sections

import { BlogNewPostForm } from '../../sections/@dashboard/blog'; // ----------------------------------------------------------------------

export default function BlogNewPost() {
  const {
    themeStretch
  } = useSettings();
  return /*#__PURE__*/React.createElement(Page, {
    title: "Blog: New Post"
  }, /*#__PURE__*/React.createElement(Container, {
    maxWidth: themeStretch ? false : 'lg'
  }, /*#__PURE__*/React.createElement(HeaderBreadcrumbs, {
    heading: "Create a new post",
    links: [{
      name: 'Dashboard',
      href: PATH_DASHBOARD.root
    }, {
      name: 'Blog',
      href: PATH_DASHBOARD.blog.root
    }, {
      name: 'New Post'
    }]
  }), /*#__PURE__*/React.createElement(BlogNewPostForm, null)));
}