import React from 'react';
import { useEffect } from 'react'; // @mui

import { Card, Container } from '@mui/material'; // redux

import { useDispatch } from '../../redux/store';
import { getConversations, getContacts } from '../../redux/slices/chat'; // routes

import { PATH_DASHBOARD } from '../../routes/paths'; // hooks

import useSettings from '../../hooks/useSettings'; // components

import Page from '../../components/Page';
import HeaderBreadcrumbs from '../../components/HeaderBreadcrumbs';
import { ChatSidebar, ChatWindow } from '../../sections/@dashboard/chat'; // ----------------------------------------------------------------------

export default function Chat() {
  const {
    themeStretch
  } = useSettings();
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(getConversations());
    dispatch(getContacts());
  }, [dispatch]);
  return /*#__PURE__*/React.createElement(Page, {
    title: "Chat"
  }, /*#__PURE__*/React.createElement(Container, {
    maxWidth: themeStretch ? false : 'xl'
  }, /*#__PURE__*/React.createElement(HeaderBreadcrumbs, {
    heading: "Chat",
    links: [{
      name: 'Dashboard',
      href: PATH_DASHBOARD.root
    }, {
      name: 'Chat'
    }]
  }), /*#__PURE__*/React.createElement(Card, {
    sx: {
      height: '72vh',
      display: 'flex'
    }
  }, /*#__PURE__*/React.createElement(ChatSidebar, null), /*#__PURE__*/React.createElement(ChatWindow, null))));
}