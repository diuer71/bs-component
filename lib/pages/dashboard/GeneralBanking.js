import React from 'react'; // @mui

import { Grid, Container, Stack } from '@mui/material'; // hooks

import useSettings from '../../hooks/useSettings'; // components

import Page from '../../components/Page'; // sections

import { BankingContacts, BankingWidgetSummary, BankingInviteFriends, BankingQuickTransfer, BankingCurrentBalance, BankingBalanceStatistics, BankingRecentTransitions, BankingExpensesCategories } from '../../sections/@dashboard/general/banking'; // ----------------------------------------------------------------------

export default function GeneralBanking() {
  const {
    themeStretch
  } = useSettings();
  return /*#__PURE__*/React.createElement(Page, {
    title: "General: Banking"
  }, /*#__PURE__*/React.createElement(Container, {
    maxWidth: themeStretch ? false : 'xl'
  }, /*#__PURE__*/React.createElement(Grid, {
    container: true,
    spacing: 3
  }, /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: 12,
    md: 7
  }, /*#__PURE__*/React.createElement(Stack, {
    direction: {
      xs: 'column',
      sm: 'row'
    },
    spacing: 3
  }, /*#__PURE__*/React.createElement(BankingWidgetSummary, {
    title: "Income",
    icon: 'eva:diagonal-arrow-left-down-fill',
    percent: 2.6,
    total: 18765,
    chartData: [111, 136, 76, 108, 74, 54, 57, 84]
  }), /*#__PURE__*/React.createElement(BankingWidgetSummary, {
    title: "Expenses",
    color: "warning",
    icon: 'eva:diagonal-arrow-right-up-fill',
    percent: -0.5,
    total: 8938,
    chartData: [111, 136, 76, 108, 74, 54, 57, 84]
  }))), /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: 12,
    md: 5
  }, /*#__PURE__*/React.createElement(BankingCurrentBalance, null)), /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: 12,
    md: 8
  }, /*#__PURE__*/React.createElement(Stack, {
    spacing: 3
  }, /*#__PURE__*/React.createElement(BankingBalanceStatistics, null), /*#__PURE__*/React.createElement(BankingExpensesCategories, null), /*#__PURE__*/React.createElement(BankingRecentTransitions, null))), /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: 12,
    md: 4
  }, /*#__PURE__*/React.createElement(Stack, {
    spacing: 3
  }, /*#__PURE__*/React.createElement(BankingQuickTransfer, null), /*#__PURE__*/React.createElement(BankingContacts, null), /*#__PURE__*/React.createElement(BankingInviteFriends, null))))));
}