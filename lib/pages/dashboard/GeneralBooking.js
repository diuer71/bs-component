import React from 'react'; // @mui

import { Grid, Container } from '@mui/material'; // hooks

import useSettings from '../../hooks/useSettings'; // components

import Page from '../../components/Page'; // sections

import { BookingDetails, BookingBookedRoom, BookingTotalIncomes, BookingRoomAvailable, BookingNewestBooking, BookingWidgetSummary, BookingCheckInWidgets, BookingCustomerReviews, BookingReservationStats } from '../../sections/@dashboard/general/booking'; // assets

import { BookingIllustration, CheckInIllustration, CheckOutIllustration } from '../../assets'; // ----------------------------------------------------------------------

export default function GeneralBooking() {
  const {
    themeStretch
  } = useSettings();
  return /*#__PURE__*/React.createElement(Page, {
    title: "General: Banking"
  }, /*#__PURE__*/React.createElement(Container, {
    maxWidth: themeStretch ? false : 'xl'
  }, /*#__PURE__*/React.createElement(Grid, {
    container: true,
    spacing: 3
  }, /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: 12,
    md: 4
  }, /*#__PURE__*/React.createElement(BookingWidgetSummary, {
    title: "Total Booking",
    total: 714000,
    icon: /*#__PURE__*/React.createElement(BookingIllustration, null)
  })), /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: 12,
    md: 4
  }, /*#__PURE__*/React.createElement(BookingWidgetSummary, {
    title: "Check In",
    total: 311000,
    icon: /*#__PURE__*/React.createElement(CheckInIllustration, null)
  })), /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: 12,
    md: 4
  }, /*#__PURE__*/React.createElement(BookingWidgetSummary, {
    title: "Check Out",
    total: 124000,
    icon: /*#__PURE__*/React.createElement(CheckOutIllustration, null)
  })), /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: 12,
    md: 8
  }, /*#__PURE__*/React.createElement(Grid, {
    container: true,
    spacing: 3
  }, /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: 12,
    md: 6
  }, /*#__PURE__*/React.createElement(BookingTotalIncomes, null)), /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: 12,
    md: 6
  }, /*#__PURE__*/React.createElement(BookingBookedRoom, null)), /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: 12,
    md: 12
  }, /*#__PURE__*/React.createElement(BookingCheckInWidgets, null)))), /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: 12,
    md: 4
  }, /*#__PURE__*/React.createElement(BookingRoomAvailable, null)), /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: 12,
    md: 8
  }, /*#__PURE__*/React.createElement(BookingReservationStats, null)), /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: 12,
    md: 4
  }, /*#__PURE__*/React.createElement(BookingCustomerReviews, null)), /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: 12
  }, /*#__PURE__*/React.createElement(BookingNewestBooking, null)), /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: 12
  }, /*#__PURE__*/React.createElement(BookingDetails, null)))));
}