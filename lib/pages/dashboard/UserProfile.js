import React from 'react';
import { capitalCase } from 'change-case';
import { useState } from 'react'; // @mui

import { styled } from '@mui/material/styles';
import { Tab, Box, Card, Tabs, Container } from '@mui/material'; // routes

import { PATH_DASHBOARD } from '../../routes/paths'; // hooks

import useAuth from '../../hooks/useAuth';
import useSettings from '../../hooks/useSettings'; // _mock_

import { _userAbout, _userFeeds, _userFriends, _userGallery, _userFollowers } from '../../_mock'; // components

import Page from '../../components/Page';
import Iconify from '../../components/Iconify';
import HeaderBreadcrumbs from '../../components/HeaderBreadcrumbs'; // sections

import { Profile, ProfileCover, ProfileFriends, ProfileGallery, ProfileFollowers } from '../../sections/@dashboard/user/profile'; // ----------------------------------------------------------------------

const TabsWrapperStyle = styled('div')(({
  theme
}) => ({
  zIndex: 9,
  bottom: 0,
  width: '100%',
  display: 'flex',
  position: 'absolute',
  backgroundColor: theme.palette.background.paper,
  [theme.breakpoints.up('sm')]: {
    justifyContent: 'center'
  },
  [theme.breakpoints.up('md')]: {
    justifyContent: 'flex-end',
    paddingRight: theme.spacing(3)
  }
})); // ----------------------------------------------------------------------

export default function UserProfile() {
  const {
    themeStretch
  } = useSettings();
  const {
    user
  } = useAuth();
  const [currentTab, setCurrentTab] = useState('profile');
  const [findFriends, setFindFriends] = useState('');

  const handleChangeTab = newValue => {
    setCurrentTab(newValue);
  };

  const handleFindFriends = value => {
    setFindFriends(value);
  };

  const PROFILE_TABS = [{
    value: 'profile',
    icon: /*#__PURE__*/React.createElement(Iconify, {
      icon: 'ic:round-account-box',
      width: 20,
      height: 20
    }),
    component: /*#__PURE__*/React.createElement(Profile, {
      myProfile: _userAbout,
      posts: _userFeeds
    })
  }, {
    value: 'followers',
    icon: /*#__PURE__*/React.createElement(Iconify, {
      icon: 'eva:heart-fill',
      width: 20,
      height: 20
    }),
    component: /*#__PURE__*/React.createElement(ProfileFollowers, {
      followers: _userFollowers
    })
  }, {
    value: 'friends',
    icon: /*#__PURE__*/React.createElement(Iconify, {
      icon: 'eva:people-fill',
      width: 20,
      height: 20
    }),
    component: /*#__PURE__*/React.createElement(ProfileFriends, {
      friends: _userFriends,
      findFriends: findFriends,
      onFindFriends: handleFindFriends
    })
  }, {
    value: 'gallery',
    icon: /*#__PURE__*/React.createElement(Iconify, {
      icon: 'ic:round-perm-media',
      width: 20,
      height: 20
    }),
    component: /*#__PURE__*/React.createElement(ProfileGallery, {
      gallery: _userGallery
    })
  }];
  return /*#__PURE__*/React.createElement(Page, {
    title: "User: Profile"
  }, /*#__PURE__*/React.createElement(Container, {
    maxWidth: themeStretch ? false : 'lg'
  }, /*#__PURE__*/React.createElement(HeaderBreadcrumbs, {
    heading: "Profile",
    links: [{
      name: 'Dashboard',
      href: PATH_DASHBOARD.root
    }, {
      name: 'User',
      href: PATH_DASHBOARD.user.root
    }, {
      name: user?.displayName || ''
    }]
  }), /*#__PURE__*/React.createElement(Card, {
    sx: {
      mb: 3,
      height: 280,
      position: 'relative'
    }
  }, /*#__PURE__*/React.createElement(ProfileCover, {
    myProfile: _userAbout
  }), /*#__PURE__*/React.createElement(TabsWrapperStyle, null, /*#__PURE__*/React.createElement(Tabs, {
    value: currentTab,
    scrollButtons: "auto",
    variant: "scrollable",
    allowScrollButtonsMobile: true,
    onChange: (e, value) => handleChangeTab(value)
  }, PROFILE_TABS.map(tab => /*#__PURE__*/React.createElement(Tab, {
    disableRipple: true,
    key: tab.value,
    value: tab.value,
    icon: tab.icon,
    label: capitalCase(tab.value)
  }))))), PROFILE_TABS.map(tab => {
    const isMatched = tab.value === currentTab;
    return isMatched && /*#__PURE__*/React.createElement(Box, {
      key: tab.value
    }, tab.component);
  })));
}