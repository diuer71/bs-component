import React from 'react';
import { useEffect, useState, useCallback } from 'react';
import { sentenceCase } from 'change-case';
import { useParams } from 'react-router-dom'; // @mui

import { Box, Card, Divider, Container, Typography, Pagination } from '@mui/material'; // routes

import { PATH_DASHBOARD } from '../../routes/paths'; // hooks

import useSettings from '../../hooks/useSettings';
import useIsMountedRef from '../../hooks/useIsMountedRef'; // utils

import axios from '../../utils/axios'; // components

import Page from '../../components/Page';
import Markdown from '../../components/Markdown';
import HeaderBreadcrumbs from '../../components/HeaderBreadcrumbs';
import { SkeletonPost } from '../../components/skeleton'; // sections

import { BlogPostHero, BlogPostTags, BlogPostRecent, BlogPostCommentList, BlogPostCommentForm } from '../../sections/@dashboard/blog'; // ----------------------------------------------------------------------

export default function BlogPost() {
  const {
    themeStretch
  } = useSettings();
  const isMountedRef = useIsMountedRef();
  const {
    title
  } = useParams();
  const [recentPosts, setRecentPosts] = useState([]);
  const [post, setPost] = useState(null);
  const [error, setError] = useState(null);
  const getPost = useCallback(async () => {
    try {
      const response = await axios.get('/api/blog/post', {
        params: {
          title
        }
      });

      if (isMountedRef.current) {
        setPost(response.data.post);
      }
    } catch (error) {
      console.error(error);
      setError(error.message);
    }
  }, [isMountedRef, title]);
  const getRecentPosts = useCallback(async () => {
    try {
      const response = await axios.get('/api/blog/posts/recent', {
        params: {
          title
        }
      });

      if (isMountedRef.current) {
        setRecentPosts(response.data.recentPosts);
      }
    } catch (error) {
      console.error(error);
    }
  }, [isMountedRef, title]);
  useEffect(() => {
    getPost();
    getRecentPosts();
  }, [getRecentPosts, getPost]);
  return /*#__PURE__*/React.createElement(Page, {
    title: "Blog: Post Details"
  }, /*#__PURE__*/React.createElement(Container, {
    maxWidth: themeStretch ? false : 'lg'
  }, /*#__PURE__*/React.createElement(HeaderBreadcrumbs, {
    heading: "Post Details",
    links: [{
      name: 'Dashboard',
      href: PATH_DASHBOARD.root
    }, {
      name: 'Blog',
      href: PATH_DASHBOARD.blog.root
    }, {
      name: sentenceCase(title)
    }]
  }), post && /*#__PURE__*/React.createElement(Card, null, /*#__PURE__*/React.createElement(BlogPostHero, {
    post: post
  }), /*#__PURE__*/React.createElement(Box, {
    sx: {
      p: {
        xs: 3,
        md: 5
      }
    }
  }, /*#__PURE__*/React.createElement(Typography, {
    variant: "h6",
    sx: {
      mb: 5
    }
  }, post.description), /*#__PURE__*/React.createElement(Markdown, {
    children: post.body
  }), /*#__PURE__*/React.createElement(Box, {
    sx: {
      my: 5
    }
  }, /*#__PURE__*/React.createElement(Divider, null), /*#__PURE__*/React.createElement(BlogPostTags, {
    post: post
  }), /*#__PURE__*/React.createElement(Divider, null)), /*#__PURE__*/React.createElement(Box, {
    sx: {
      display: 'flex',
      mb: 2
    }
  }, /*#__PURE__*/React.createElement(Typography, {
    variant: "h4"
  }, "Comments"), /*#__PURE__*/React.createElement(Typography, {
    variant: "subtitle2",
    sx: {
      color: 'text.disabled'
    }
  }, "(", post.comments.length, ")")), /*#__PURE__*/React.createElement(BlogPostCommentList, {
    post: post
  }), /*#__PURE__*/React.createElement(Box, {
    sx: {
      mb: 5,
      mt: 3,
      display: 'flex',
      justifyContent: 'flex-end'
    }
  }, /*#__PURE__*/React.createElement(Pagination, {
    count: 8,
    color: "primary"
  })), /*#__PURE__*/React.createElement(BlogPostCommentForm, null))), !post && !error && /*#__PURE__*/React.createElement(SkeletonPost, null), error && /*#__PURE__*/React.createElement(Typography, {
    variant: "h6"
  }, "404 ", error, "!"), /*#__PURE__*/React.createElement(BlogPostRecent, {
    posts: recentPosts
  })));
}