function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

import React from 'react';
import { useEffect } from 'react'; // @mui

import { Container, Stack } from '@mui/material';
import { DragDropContext, Droppable } from 'react-beautiful-dnd'; // redux

import { useDispatch, useSelector } from '../../redux/store';
import { getBoard, persistColumn, persistCard } from '../../redux/slices/kanban'; // routes

import { PATH_DASHBOARD } from '../../routes/paths'; // components

import Page from '../../components/Page';
import HeaderBreadcrumbs from '../../components/HeaderBreadcrumbs';
import { SkeletonKanbanColumn } from '../../components/skeleton'; // sections

import { KanbanColumn, KanbanColumnAdd } from '../../sections/@dashboard/kanban'; // ----------------------------------------------------------------------

export default function Kanban() {
  const dispatch = useDispatch();
  const {
    board
  } = useSelector(state => state.kanban);
  useEffect(() => {
    dispatch(getBoard());
  }, [dispatch]);

  const onDragEnd = result => {
    // Reorder card
    const {
      destination,
      source,
      draggableId,
      type
    } = result;
    if (!destination) return;
    if (destination.droppableId === source.droppableId && destination.index === source.index) return;

    if (type === 'column') {
      const newColumnOrder = Array.from(board.columnOrder);
      newColumnOrder.splice(source.index, 1);
      newColumnOrder.splice(destination.index, 0, draggableId);
      dispatch(persistColumn(newColumnOrder));
      return;
    }

    const start = board.columns[source.droppableId];
    const finish = board.columns[destination.droppableId];

    if (start.id === finish.id) {
      const updatedCardIds = [...start.cardIds];
      updatedCardIds.splice(source.index, 1);
      updatedCardIds.splice(destination.index, 0, draggableId);
      const updatedColumn = { ...start,
        cardIds: updatedCardIds
      };
      dispatch(persistCard({ ...board.columns,
        [updatedColumn.id]: updatedColumn
      }));
      return;
    }

    const startCardIds = [...start.cardIds];
    startCardIds.splice(source.index, 1);
    const updatedStart = { ...start,
      cardIds: startCardIds
    };
    const finishCardIds = [...finish.cardIds];
    finishCardIds.splice(destination.index, 0, draggableId);
    const updatedFinish = { ...finish,
      cardIds: finishCardIds
    };
    dispatch(persistCard({ ...board.columns,
      [updatedStart.id]: updatedStart,
      [updatedFinish.id]: updatedFinish
    }));
  };

  return /*#__PURE__*/React.createElement(Page, {
    title: "Kanban",
    sx: {
      height: 1
    }
  }, /*#__PURE__*/React.createElement(Container, {
    maxWidth: false,
    sx: {
      height: 1
    }
  }, /*#__PURE__*/React.createElement(HeaderBreadcrumbs, {
    heading: "Kanban",
    links: [{
      name: 'Dashboard',
      href: PATH_DASHBOARD.root
    }, {
      name: 'Kanban'
    }]
  }), /*#__PURE__*/React.createElement(DragDropContext, {
    onDragEnd: onDragEnd
  }, /*#__PURE__*/React.createElement(Droppable, {
    droppableId: "all-columns",
    direction: "horizontal",
    type: "column"
  }, provided => /*#__PURE__*/React.createElement(Stack, _extends({}, provided.droppableProps, {
    ref: provided.innerRef,
    direction: "row",
    alignItems: "flex-start",
    spacing: 3,
    sx: {
      height: 'calc(100% - 32px)',
      overflowY: 'hidden'
    }
  }), !board.columnOrder.length ? /*#__PURE__*/React.createElement(SkeletonKanbanColumn, null) : board.columnOrder.map((columnId, index) => /*#__PURE__*/React.createElement(KanbanColumn, {
    index: index,
    key: columnId,
    column: board.columns[columnId]
  })), provided.placeholder, /*#__PURE__*/React.createElement(KanbanColumnAdd, null))))));
}