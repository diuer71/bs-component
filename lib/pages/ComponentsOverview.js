import React from 'react'; // @mui

import { styled } from '@mui/material/styles';
import { Container } from '@mui/material'; // components

import Page from '../components/Page'; // sections

import { ComponentHero, ComponentOther, ComponentFoundation, ComponentMUI } from '../sections/overview'; // ----------------------------------------------------------------------

const RootStyle = styled('div')(({
  theme
}) => ({
  paddingTop: theme.spacing(8),
  paddingBottom: theme.spacing(15),
  [theme.breakpoints.up('md')]: {
    paddingTop: theme.spacing(11)
  }
})); // ----------------------------------------------------------------------

export default function ComponentsOverview() {
  return /*#__PURE__*/React.createElement(Page, {
    title: "Components Overview"
  }, /*#__PURE__*/React.createElement(RootStyle, null, /*#__PURE__*/React.createElement(ComponentHero, null), /*#__PURE__*/React.createElement(Container, null, /*#__PURE__*/React.createElement(ComponentFoundation, null), /*#__PURE__*/React.createElement(ComponentMUI, null), /*#__PURE__*/React.createElement(ComponentOther, null))));
}