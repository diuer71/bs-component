import React from 'react'; // @mui

import { styled } from '@mui/material/styles';
import { Divider } from '@mui/material'; // components

import Page from '../components/Page';
import { AboutHero, AboutWhat, AboutTeam, AboutVision, AboutTestimonials } from '../sections/about'; // ----------------------------------------------------------------------

const RootStyle = styled('div')(({
  theme
}) => ({
  paddingTop: theme.spacing(8),
  [theme.breakpoints.up('md')]: {
    paddingTop: theme.spacing(11)
  }
})); // ----------------------------------------------------------------------

export default function About() {
  return /*#__PURE__*/React.createElement(Page, {
    title: "About us"
  }, /*#__PURE__*/React.createElement(RootStyle, null, /*#__PURE__*/React.createElement(AboutHero, null), /*#__PURE__*/React.createElement(AboutWhat, null), /*#__PURE__*/React.createElement(AboutVision, null), /*#__PURE__*/React.createElement(Divider, {
    orientation: "vertical",
    sx: {
      my: 10,
      mx: 'auto',
      width: 2,
      height: 40
    }
  }), /*#__PURE__*/React.createElement(AboutTeam, null), /*#__PURE__*/React.createElement(AboutTestimonials, null)));
}