import React from 'react'; // @mui

import { styled } from '@mui/material/styles';
import { Box, Button, Stack, Container, Typography, InputAdornment } from '@mui/material'; // hooks

import useCountdown from '../hooks/useCountdown'; // components

import Page from '../components/Page';
import InputStyle from '../components/InputStyle';
import SocialsButton from '../components/SocialsButton'; // assets

import { ComingSoonIllustration } from '../assets'; // ----------------------------------------------------------------------

const RootStyle = styled('div')(({
  theme
}) => ({
  height: '100%',
  display: 'flex',
  alignItems: 'center',
  paddingTop: theme.spacing(15),
  paddingBottom: theme.spacing(10)
}));
const CountdownStyle = styled('div')({
  display: 'flex',
  justifyContent: 'center'
});
const SeparatorStyle = styled(Typography)(({
  theme
}) => ({
  margin: theme.spacing(0, 1),
  [theme.breakpoints.up('sm')]: {
    margin: theme.spacing(0, 2.5)
  }
})); // ----------------------------------------------------------------------

export default function ComingSoon() {
  const countdown = useCountdown(new Date('07/07/2022 21:30'));
  return /*#__PURE__*/React.createElement(Page, {
    title: "Coming Soon",
    sx: {
      height: 1
    }
  }, /*#__PURE__*/React.createElement(RootStyle, null, /*#__PURE__*/React.createElement(Container, null, /*#__PURE__*/React.createElement(Box, {
    sx: {
      maxWidth: 480,
      margin: 'auto',
      textAlign: 'center'
    }
  }, /*#__PURE__*/React.createElement(Typography, {
    variant: "h3",
    paragraph: true
  }, "Coming Soon!"), /*#__PURE__*/React.createElement(Typography, {
    sx: {
      color: 'text.secondary'
    }
  }, "We are currently working hard on this page!"), /*#__PURE__*/React.createElement(ComingSoonIllustration, {
    sx: {
      my: 10,
      height: 240
    }
  }), /*#__PURE__*/React.createElement(CountdownStyle, null, /*#__PURE__*/React.createElement("div", null, /*#__PURE__*/React.createElement(Typography, {
    variant: "h2"
  }, countdown.days), /*#__PURE__*/React.createElement(Typography, {
    sx: {
      color: 'text.secondary'
    }
  }, "Days")), /*#__PURE__*/React.createElement(SeparatorStyle, {
    variant: "h2"
  }, ":"), /*#__PURE__*/React.createElement("div", null, /*#__PURE__*/React.createElement(Typography, {
    variant: "h2"
  }, countdown.hours), /*#__PURE__*/React.createElement(Typography, {
    sx: {
      color: 'text.secondary'
    }
  }, "Hours")), /*#__PURE__*/React.createElement(SeparatorStyle, {
    variant: "h2"
  }, ":"), /*#__PURE__*/React.createElement("div", null, /*#__PURE__*/React.createElement(Typography, {
    variant: "h2"
  }, countdown.minutes), /*#__PURE__*/React.createElement(Typography, {
    sx: {
      color: 'text.secondary'
    }
  }, "Minutes")), /*#__PURE__*/React.createElement(SeparatorStyle, {
    variant: "h2"
  }, ":"), /*#__PURE__*/React.createElement("div", null, /*#__PURE__*/React.createElement(Typography, {
    variant: "h2"
  }, countdown.seconds), /*#__PURE__*/React.createElement(Typography, {
    sx: {
      color: 'text.secondary'
    }
  }, "Seconds"))), /*#__PURE__*/React.createElement(InputStyle, {
    fullWidth: true,
    placeholder: "Enter your email",
    InputProps: {
      endAdornment: /*#__PURE__*/React.createElement(InputAdornment, {
        position: "end"
      }, /*#__PURE__*/React.createElement(Button, {
        variant: "contained",
        size: "large"
      }, "Notify Me"))
    },
    sx: {
      my: 5,
      '& .MuiOutlinedInput-root': {
        pr: 0.5
      }
    }
  }), /*#__PURE__*/React.createElement(Stack, {
    alignItems: "center"
  }, /*#__PURE__*/React.createElement(SocialsButton, {
    size: "large",
    initialColor: true
  }))))));
}