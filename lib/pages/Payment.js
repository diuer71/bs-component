import React from 'react'; // @mui

import { styled } from '@mui/material/styles';
import { Box, Grid, Container, Typography } from '@mui/material'; // hooks

import useResponsive from '../hooks/useResponsive'; // components

import Page from '../components/Page'; // sections

import { PaymentSummary, PaymentMethods, PaymentBillingAddress } from '../sections/payment'; // ----------------------------------------------------------------------

const RootStyle = styled('div')(({
  theme
}) => ({
  minHeight: '100%',
  paddingTop: theme.spacing(15),
  paddingBottom: theme.spacing(10)
})); // ----------------------------------------------------------------------

export default function Payment() {
  const isDesktop = useResponsive('up', 'md');
  return /*#__PURE__*/React.createElement(Page, {
    title: "Payment"
  }, /*#__PURE__*/React.createElement(RootStyle, null, /*#__PURE__*/React.createElement(Container, null, /*#__PURE__*/React.createElement(Box, {
    sx: {
      mb: 5
    }
  }, /*#__PURE__*/React.createElement(Typography, {
    variant: "h3",
    align: "center",
    paragraph: true
  }, "Let's finish powering you up!"), /*#__PURE__*/React.createElement(Typography, {
    align: "center",
    sx: {
      color: 'text.secondary'
    }
  }, "Professional plan is right for you.")), /*#__PURE__*/React.createElement(Grid, {
    container: true,
    spacing: isDesktop ? 3 : 5
  }, /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: 12,
    md: 8
  }, /*#__PURE__*/React.createElement(Box, {
    sx: {
      display: 'grid',
      gap: 5,
      p: {
        md: 5
      },
      borderRadius: 2,
      border: theme => ({
        md: `dashed 1px ${theme.palette.divider}`
      }),
      gridTemplateColumns: {
        xs: 'repeat(1, 1fr)',
        md: 'repeat(2, 1fr)'
      }
    }
  }, /*#__PURE__*/React.createElement(PaymentBillingAddress, null), /*#__PURE__*/React.createElement(PaymentMethods, null))), /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: 12,
    md: 4
  }, /*#__PURE__*/React.createElement(PaymentSummary, null))))));
}