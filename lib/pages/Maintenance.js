import React from 'react';
import { Link as RouterLink } from 'react-router-dom'; // @mui

import { styled } from '@mui/material/styles';
import { Button, Typography, Container } from '@mui/material'; // components

import Page from '../components/Page'; //

import { MaintenanceIllustration } from '../assets'; // ----------------------------------------------------------------------

const RootStyle = styled('div')(({
  theme
}) => ({
  height: '100%',
  display: 'flex',
  alignItems: 'center',
  paddingTop: theme.spacing(15),
  paddingBottom: theme.spacing(10)
})); // ----------------------------------------------------------------------

export default function Maintenance() {
  return /*#__PURE__*/React.createElement(Page, {
    title: "Maintenance",
    sx: {
      height: 1
    }
  }, /*#__PURE__*/React.createElement(RootStyle, null, /*#__PURE__*/React.createElement(Container, {
    sx: {
      textAlign: 'center'
    }
  }, /*#__PURE__*/React.createElement(Typography, {
    variant: "h3",
    paragraph: true
  }, "Website currently under maintenance"), /*#__PURE__*/React.createElement(Typography, {
    sx: {
      color: 'text.secondary'
    }
  }, "We are currently working hard on this page!"), /*#__PURE__*/React.createElement(MaintenanceIllustration, {
    sx: {
      my: 10,
      height: 240
    }
  }), /*#__PURE__*/React.createElement(Button, {
    variant: "contained",
    size: "large",
    component: RouterLink,
    to: "/"
  }, "Go to Home"))));
}