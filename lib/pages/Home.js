import React from 'react'; // @mui

import { styled } from '@mui/material/styles'; // components

import Page from '../components/Page'; // sections

import { HomeHero, HomeMinimal, HomeDarkMode, HomeLookingFor, HomeColorPresets, HomePricingPlans, HomeAdvertisement, HomeCleanInterfaces, HomeHugePackElements } from '../sections/home'; // ----------------------------------------------------------------------

const RootStyle = styled('div')(() => ({
  height: '100%'
}));
const ContentStyle = styled('div')(({
  theme
}) => ({
  overflow: 'hidden',
  position: 'relative',
  backgroundColor: theme.palette.background.default
})); // ----------------------------------------------------------------------

export default function HomePage() {
  return /*#__PURE__*/React.createElement(Page, {
    title: "The starting point for your next project"
  }, /*#__PURE__*/React.createElement(RootStyle, null, /*#__PURE__*/React.createElement(HomeHero, null), /*#__PURE__*/React.createElement(ContentStyle, null, /*#__PURE__*/React.createElement(HomeMinimal, null), /*#__PURE__*/React.createElement(HomeHugePackElements, null), /*#__PURE__*/React.createElement(HomeDarkMode, null), /*#__PURE__*/React.createElement(HomeColorPresets, null), /*#__PURE__*/React.createElement(HomeCleanInterfaces, null), /*#__PURE__*/React.createElement(HomePricingPlans, null), /*#__PURE__*/React.createElement(HomeLookingFor, null), /*#__PURE__*/React.createElement(HomeAdvertisement, null))));
}