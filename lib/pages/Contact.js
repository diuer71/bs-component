import React from 'react'; // @mui

import { styled } from '@mui/material/styles';
import { Grid, Container } from '@mui/material'; // components

import Page from '../components/Page';
import { ContactHero, ContactForm, ContactMap } from '../sections/contact'; // ----------------------------------------------------------------------

const RootStyle = styled('div')(({
  theme
}) => ({
  paddingTop: theme.spacing(8),
  [theme.breakpoints.up('md')]: {
    paddingTop: theme.spacing(11)
  }
})); // ----------------------------------------------------------------------

export default function Contact() {
  return /*#__PURE__*/React.createElement(Page, {
    title: "Contact us"
  }, /*#__PURE__*/React.createElement(RootStyle, null, /*#__PURE__*/React.createElement(ContactHero, null), /*#__PURE__*/React.createElement(Container, {
    sx: {
      my: 10
    }
  }, /*#__PURE__*/React.createElement(Grid, {
    container: true,
    spacing: 10
  }, /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: 12,
    md: 6
  }, /*#__PURE__*/React.createElement(ContactForm, null)), /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: 12,
    md: 6
  }, /*#__PURE__*/React.createElement(ContactMap, null))))));
}