import React from 'react'; // @mui

import { styled } from '@mui/material/styles';
import { Box, Grid, Switch, Container, Typography, Stack } from '@mui/material'; // _mock_

import { _pricingPlans } from '../_mock'; // components

import Page from '../components/Page'; // sections

import { PricingPlanCard } from '../sections/pricing'; // ----------------------------------------------------------------------

const RootStyle = styled('div')(({
  theme
}) => ({
  minHeight: '100%',
  paddingTop: theme.spacing(15),
  paddingBottom: theme.spacing(10)
})); // ----------------------------------------------------------------------

export default function Pricing() {
  return /*#__PURE__*/React.createElement(Page, {
    title: "Pricing"
  }, /*#__PURE__*/React.createElement(RootStyle, null, /*#__PURE__*/React.createElement(Container, null, /*#__PURE__*/React.createElement(Typography, {
    variant: "h3",
    align: "center",
    paragraph: true
  }, "Flexible plans for your", /*#__PURE__*/React.createElement("br", null), " community's size and needs"), /*#__PURE__*/React.createElement(Typography, {
    align: "center",
    sx: {
      color: 'text.secondary'
    }
  }, "Choose your plan and make modern online conversation magic"), /*#__PURE__*/React.createElement(Box, {
    sx: {
      my: 5
    }
  }, /*#__PURE__*/React.createElement(Stack, {
    direction: "row",
    alignItems: "center",
    justifyContent: "flex-end"
  }, /*#__PURE__*/React.createElement(Typography, {
    variant: "overline",
    sx: {
      mr: 1.5
    }
  }, "MONTHLY"), /*#__PURE__*/React.createElement(Switch, null), /*#__PURE__*/React.createElement(Typography, {
    variant: "overline",
    sx: {
      ml: 1.5
    }
  }, "YEARLY (save 10%)")), /*#__PURE__*/React.createElement(Typography, {
    variant: "caption",
    align: "right",
    sx: {
      color: 'text.secondary',
      display: 'block'
    }
  }, "* Plus applicable taxes")), /*#__PURE__*/React.createElement(Grid, {
    container: true,
    spacing: 3
  }, _pricingPlans.map((card, index) => /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: 12,
    md: 4,
    key: card.subscription
  }, /*#__PURE__*/React.createElement(PricingPlanCard, {
    card: card,
    index: index
  })))))));
}