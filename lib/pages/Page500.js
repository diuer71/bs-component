import React from 'react';
import { Link as RouterLink } from 'react-router-dom';
import { styled } from '@mui/material/styles'; // @mui

import { Box, Button, Typography, Container } from '@mui/material'; // components

import Page from '../components/Page';
import { SeverErrorIllustration } from '../assets'; // ----------------------------------------------------------------------

const RootStyle = styled('div')(({
  theme
}) => ({
  display: 'flex',
  height: '100%',
  alignItems: 'center',
  paddingTop: theme.spacing(15),
  paddingBottom: theme.spacing(10)
})); // ----------------------------------------------------------------------

export default function Page500() {
  return /*#__PURE__*/React.createElement(Page, {
    title: "500 Internal Server Error",
    sx: {
      height: 1
    }
  }, /*#__PURE__*/React.createElement(RootStyle, null, /*#__PURE__*/React.createElement(Container, null, /*#__PURE__*/React.createElement(Box, {
    sx: {
      maxWidth: 480,
      margin: 'auto',
      textAlign: 'center'
    }
  }, /*#__PURE__*/React.createElement(Typography, {
    variant: "h3",
    paragraph: true
  }, "500 Internal Server Error"), /*#__PURE__*/React.createElement(Typography, {
    sx: {
      color: 'text.secondary'
    }
  }, "There was an error, please try again later."), /*#__PURE__*/React.createElement(SeverErrorIllustration, {
    sx: {
      height: 260,
      my: {
        xs: 5,
        sm: 10
      }
    }
  }), /*#__PURE__*/React.createElement(Button, {
    to: "/",
    size: "large",
    variant: "contained",
    component: RouterLink
  }, "Go to Home")))));
}