import React from 'react';
import { m } from 'framer-motion';
import { Link as RouterLink } from 'react-router-dom'; // @mui

import { styled } from '@mui/material/styles';
import { Box, Button, Typography, Container } from '@mui/material'; // components

import Page from '../components/Page';
import { MotionContainer, varBounce } from '../components/animate'; // assets

import { PageNotFoundIllustration } from '../assets'; // ----------------------------------------------------------------------

const RootStyle = styled('div')(({
  theme
}) => ({
  display: 'flex',
  height: '100%',
  alignItems: 'center',
  paddingTop: theme.spacing(15),
  paddingBottom: theme.spacing(10)
})); // ----------------------------------------------------------------------

export default function Page404() {
  return /*#__PURE__*/React.createElement(Page, {
    title: "404 Page Not Found",
    sx: {
      height: 1
    }
  }, /*#__PURE__*/React.createElement(RootStyle, null, /*#__PURE__*/React.createElement(Container, {
    component: MotionContainer
  }, /*#__PURE__*/React.createElement(Box, {
    sx: {
      maxWidth: 480,
      margin: 'auto',
      textAlign: 'center'
    }
  }, /*#__PURE__*/React.createElement(m.div, {
    variants: varBounce().in
  }, /*#__PURE__*/React.createElement(Typography, {
    variant: "h3",
    paragraph: true
  }, "Sorry, page not found!")), /*#__PURE__*/React.createElement(Typography, {
    sx: {
      color: 'text.secondary'
    }
  }, "Sorry, we couldn\u2019t find the page you\u2019re looking for. Perhaps you\u2019ve mistyped the URL? Be sure to check your spelling."), /*#__PURE__*/React.createElement(m.div, {
    variants: varBounce().in
  }, /*#__PURE__*/React.createElement(PageNotFoundIllustration, {
    sx: {
      height: 260,
      my: {
        xs: 5,
        sm: 10
      }
    }
  })), /*#__PURE__*/React.createElement(Button, {
    to: "/",
    size: "large",
    variant: "contained",
    component: RouterLink
  }, "Go to Home")))));
}