import React from 'react'; // @mui

import { styled } from '@mui/material/styles';
import { Grid, Container, Typography } from '@mui/material'; // components

import Page from '../components/Page';
import { FaqsHero, FaqsCategory, FaqsList, FaqsForm } from '../sections/faqs'; // ----------------------------------------------------------------------

const RootStyle = styled('div')(({
  theme
}) => ({
  paddingTop: theme.spacing(8),
  [theme.breakpoints.up('md')]: {
    paddingTop: theme.spacing(11)
  }
})); // ----------------------------------------------------------------------

export default function Faqs() {
  return /*#__PURE__*/React.createElement(Page, {
    title: "Faqs"
  }, /*#__PURE__*/React.createElement(RootStyle, null, /*#__PURE__*/React.createElement(FaqsHero, null), /*#__PURE__*/React.createElement(Container, {
    sx: {
      mt: 15,
      mb: 10
    }
  }, /*#__PURE__*/React.createElement(FaqsCategory, null), /*#__PURE__*/React.createElement(Typography, {
    variant: "h3",
    sx: {
      mb: 5
    }
  }, "Frequently asked questions"), /*#__PURE__*/React.createElement(Grid, {
    container: true,
    spacing: 10
  }, /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: 12,
    md: 6
  }, /*#__PURE__*/React.createElement(FaqsList, null)), /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: 12,
    md: 6
  }, /*#__PURE__*/React.createElement(FaqsForm, null))))));
}