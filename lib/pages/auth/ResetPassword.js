import React from 'react';
import { useState } from 'react';
import { Link as RouterLink } from 'react-router-dom'; // @mui

import { styled } from '@mui/material/styles';
import { Box, Button, Container, Typography } from '@mui/material'; // layouts

import LogoOnlyLayout from '../../layouts/LogoOnlyLayout'; // routes

import { PATH_AUTH } from '../../routes/paths'; // components

import Page from '../../components/Page'; // sections

import { ResetPasswordForm } from '../../sections/auth/reset-password'; // assets

import { SentIcon } from '../../assets'; // ----------------------------------------------------------------------

const RootStyle = styled('div')(({
  theme
}) => ({
  display: 'flex',
  minHeight: '100%',
  alignItems: 'center',
  justifyContent: 'center',
  padding: theme.spacing(12, 0)
})); // ----------------------------------------------------------------------

export default function ResetPassword() {
  const [email, setEmail] = useState('');
  const [sent, setSent] = useState(false);
  return /*#__PURE__*/React.createElement(Page, {
    title: "Reset Password",
    sx: {
      height: 1
    }
  }, /*#__PURE__*/React.createElement(RootStyle, null, /*#__PURE__*/React.createElement(LogoOnlyLayout, null), /*#__PURE__*/React.createElement(Container, null, /*#__PURE__*/React.createElement(Box, {
    sx: {
      maxWidth: 480,
      mx: 'auto'
    }
  }, !sent ? /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement(Typography, {
    variant: "h3",
    paragraph: true
  }, "Forgot your password?"), /*#__PURE__*/React.createElement(Typography, {
    sx: {
      color: 'text.secondary',
      mb: 5
    }
  }, "Please enter the email address associated with your account and We will email you a link to reset your password."), /*#__PURE__*/React.createElement(ResetPasswordForm, {
    onSent: () => setSent(true),
    onGetEmail: value => setEmail(value)
  }), /*#__PURE__*/React.createElement(Button, {
    fullWidth: true,
    size: "large",
    component: RouterLink,
    to: PATH_AUTH.login,
    sx: {
      mt: 1
    }
  }, "Back")) : /*#__PURE__*/React.createElement(Box, {
    sx: {
      textAlign: 'center'
    }
  }, /*#__PURE__*/React.createElement(SentIcon, {
    sx: {
      mb: 5,
      mx: 'auto',
      height: 160
    }
  }), /*#__PURE__*/React.createElement(Typography, {
    variant: "h3",
    gutterBottom: true
  }, "Request sent successfully"), /*#__PURE__*/React.createElement(Typography, null, "We have sent a confirmation email to \xA0", /*#__PURE__*/React.createElement("strong", null, email), /*#__PURE__*/React.createElement("br", null), "Please check your email."), /*#__PURE__*/React.createElement(Button, {
    size: "large",
    variant: "contained",
    component: RouterLink,
    to: PATH_AUTH.login,
    sx: {
      mt: 5
    }
  }, "Back"))))));
}