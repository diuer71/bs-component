import React from 'react';
import { Link as RouterLink } from 'react-router-dom'; // @mui

import { styled } from '@mui/material/styles';
import { Box, Button, Link, Container, Typography } from '@mui/material'; // layouts

import LogoOnlyLayout from '../../layouts/LogoOnlyLayout'; // routes

import { PATH_AUTH } from '../../routes/paths'; // components

import Page from '../../components/Page';
import Iconify from '../../components/Iconify'; // sections

import { VerifyCodeForm } from '../../sections/auth/verify-code'; // ----------------------------------------------------------------------

const RootStyle = styled('div')(({
  theme
}) => ({
  display: 'flex',
  height: '100%',
  alignItems: 'center',
  padding: theme.spacing(12, 0)
})); // ----------------------------------------------------------------------

export default function VerifyCode() {
  return /*#__PURE__*/React.createElement(Page, {
    title: "Verify",
    sx: {
      height: 1
    }
  }, /*#__PURE__*/React.createElement(RootStyle, null, /*#__PURE__*/React.createElement(LogoOnlyLayout, null), /*#__PURE__*/React.createElement(Container, null, /*#__PURE__*/React.createElement(Box, {
    sx: {
      maxWidth: 480,
      mx: 'auto'
    }
  }, /*#__PURE__*/React.createElement(Button, {
    size: "small",
    component: RouterLink,
    to: PATH_AUTH.login,
    startIcon: /*#__PURE__*/React.createElement(Iconify, {
      icon: 'eva:arrow-ios-back-fill',
      width: 20,
      height: 20
    }),
    sx: {
      mb: 3
    }
  }, "Back"), /*#__PURE__*/React.createElement(Typography, {
    variant: "h3",
    paragraph: true
  }, "Please check your email!"), /*#__PURE__*/React.createElement(Typography, {
    sx: {
      color: 'text.secondary'
    }
  }, "We have emailed a 6-digit confirmation code to acb@domain, please enter the code in below box to verify your email."), /*#__PURE__*/React.createElement(Box, {
    sx: {
      mt: 5,
      mb: 3
    }
  }, /*#__PURE__*/React.createElement(VerifyCodeForm, null)), /*#__PURE__*/React.createElement(Typography, {
    variant: "body2",
    align: "center"
  }, "Don\u2019t have a code? \xA0", /*#__PURE__*/React.createElement(Link, {
    variant: "subtitle2",
    underline: "none",
    onClick: () => {}
  }, "Resend code"))))));
}