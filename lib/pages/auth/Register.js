import React from 'react';
import { capitalCase } from 'change-case';
import { Link as RouterLink } from 'react-router-dom'; // @mui

import { styled } from '@mui/material/styles';
import { Box, Card, Link, Container, Typography, Tooltip } from '@mui/material'; // hooks

import useAuth from '../../hooks/useAuth';
import useResponsive from '../../hooks/useResponsive'; // routes

import { PATH_AUTH } from '../../routes/paths'; // components

import Page from '../../components/Page';
import Logo from '../../components/Logo';
import Image from '../../components/Image'; // sections

import { RegisterForm } from '../../sections/auth/register'; // ----------------------------------------------------------------------

const RootStyle = styled('div')(({
  theme
}) => ({
  [theme.breakpoints.up('md')]: {
    display: 'flex'
  }
}));
const HeaderStyle = styled('header')(({
  theme
}) => ({
  top: 0,
  zIndex: 9,
  lineHeight: 0,
  width: '100%',
  display: 'flex',
  alignItems: 'center',
  position: 'absolute',
  padding: theme.spacing(3),
  justifyContent: 'space-between',
  [theme.breakpoints.up('md')]: {
    alignItems: 'flex-start',
    padding: theme.spacing(7, 5, 0, 7)
  }
}));
const SectionStyle = styled(Card)(({
  theme
}) => ({
  width: '100%',
  maxWidth: 464,
  display: 'flex',
  flexDirection: 'column',
  justifyContent: 'center',
  margin: theme.spacing(2, 0, 2, 2)
}));
const ContentStyle = styled('div')(({
  theme
}) => ({
  maxWidth: 480,
  margin: 'auto',
  display: 'flex',
  minHeight: '100vh',
  flexDirection: 'column',
  justifyContent: 'center',
  padding: theme.spacing(12, 0)
})); // ----------------------------------------------------------------------

export default function Register() {
  const {
    method
  } = useAuth();
  const smUp = useResponsive('up', 'sm');
  const mdUp = useResponsive('up', 'md');
  return /*#__PURE__*/React.createElement(Page, {
    title: "Register"
  }, /*#__PURE__*/React.createElement(RootStyle, null, /*#__PURE__*/React.createElement(HeaderStyle, null, /*#__PURE__*/React.createElement(Logo, null), smUp && /*#__PURE__*/React.createElement(Typography, {
    variant: "body2",
    sx: {
      mt: {
        md: -2
      }
    }
  }, "Already have an account?", ' ', /*#__PURE__*/React.createElement(Link, {
    variant: "subtitle2",
    component: RouterLink,
    to: PATH_AUTH.login
  }, "Login"))), mdUp && /*#__PURE__*/React.createElement(SectionStyle, null, /*#__PURE__*/React.createElement(Typography, {
    variant: "h3",
    sx: {
      px: 5,
      mt: 10,
      mb: 5
    }
  }, "Manage the job more effectively with Minimal"), /*#__PURE__*/React.createElement(Image, {
    alt: "register",
    src: "https://minimal-assets-api.vercel.app/assets/illustrations/illustration_register.png"
  })), /*#__PURE__*/React.createElement(Container, null, /*#__PURE__*/React.createElement(ContentStyle, null, /*#__PURE__*/React.createElement(Box, {
    sx: {
      mb: 5,
      display: 'flex',
      alignItems: 'center'
    }
  }, /*#__PURE__*/React.createElement(Box, {
    sx: {
      flexGrow: 1
    }
  }, /*#__PURE__*/React.createElement(Typography, {
    variant: "h4",
    gutterBottom: true
  }, "Get started absolutely free."), /*#__PURE__*/React.createElement(Typography, {
    sx: {
      color: 'text.secondary'
    }
  }, "Free forever. No credit card needed.")), /*#__PURE__*/React.createElement(Tooltip, {
    title: capitalCase(method)
  }, /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement(Image, {
    disabledEffect: true,
    src: `https://minimal-assets-api.vercel.app/assets/icons/auth/ic_${method}.png`,
    sx: {
      width: 32,
      height: 32
    }
  })))), /*#__PURE__*/React.createElement(RegisterForm, null), /*#__PURE__*/React.createElement(Typography, {
    variant: "body2",
    align: "center",
    sx: {
      color: 'text.secondary',
      mt: 3
    }
  }, "By registering, I agree to Minimal\xA0", /*#__PURE__*/React.createElement(Link, {
    underline: "always",
    color: "text.primary",
    href: "#"
  }, "Terms of Service"), "and", /*#__PURE__*/React.createElement(Link, {
    underline: "always",
    color: "text.primary",
    href: "#"
  }, "Privacy Policy"), "."), !smUp && /*#__PURE__*/React.createElement(Typography, {
    variant: "body2",
    sx: {
      mt: 3,
      textAlign: 'center'
    }
  }, "Already have an account?", ' ', /*#__PURE__*/React.createElement(Link, {
    variant: "subtitle2",
    to: PATH_AUTH.login,
    component: RouterLink
  }, "Login"))))));
}