import React from 'react';
import { capitalCase } from 'change-case';
import { Link as RouterLink } from 'react-router-dom'; // @mui

import { styled } from '@mui/material/styles';
import { Box, Card, Stack, Link, Alert, Tooltip, Container, Typography } from '@mui/material'; // routes

import { PATH_AUTH } from '../../routes/paths'; // hooks

import useAuth from '../../hooks/useAuth';
import useResponsive from '../../hooks/useResponsive'; // components

import Page from '../../components/Page';
import Logo from '../../components/Logo';
import Image from '../../components/Image'; // sections

import { LoginForm } from '../../sections/auth/login'; // ----------------------------------------------------------------------

const RootStyle = styled('div')(({
  theme
}) => ({
  [theme.breakpoints.up('md')]: {
    display: 'flex'
  }
}));
const HeaderStyle = styled('header')(({
  theme
}) => ({
  top: 0,
  zIndex: 9,
  lineHeight: 0,
  width: '100%',
  display: 'flex',
  alignItems: 'center',
  position: 'absolute',
  padding: theme.spacing(3),
  justifyContent: 'space-between',
  [theme.breakpoints.up('md')]: {
    alignItems: 'flex-start',
    padding: theme.spacing(7, 5, 0, 7)
  }
}));
const SectionStyle = styled(Card)(({
  theme
}) => ({
  width: '100%',
  maxWidth: 464,
  display: 'flex',
  flexDirection: 'column',
  justifyContent: 'center',
  margin: theme.spacing(2, 0, 2, 2)
}));
const ContentStyle = styled('div')(({
  theme
}) => ({
  maxWidth: 480,
  margin: 'auto',
  display: 'flex',
  minHeight: '100vh',
  flexDirection: 'column',
  justifyContent: 'center',
  padding: theme.spacing(12, 0)
})); // ----------------------------------------------------------------------

export default function Login() {
  const {
    method
  } = useAuth();
  const smUp = useResponsive('up', 'sm');
  const mdUp = useResponsive('up', 'md');
  return /*#__PURE__*/React.createElement(Page, {
    title: "Login"
  }, /*#__PURE__*/React.createElement(RootStyle, null, /*#__PURE__*/React.createElement(HeaderStyle, null, /*#__PURE__*/React.createElement(Logo, null), smUp && /*#__PURE__*/React.createElement(Typography, {
    variant: "body2",
    sx: {
      mt: {
        md: -2
      }
    }
  }, "Don\u2019t have an account? ", '', /*#__PURE__*/React.createElement(Link, {
    variant: "subtitle2",
    component: RouterLink,
    to: PATH_AUTH.register
  }, "Get started"))), mdUp && /*#__PURE__*/React.createElement(SectionStyle, null, /*#__PURE__*/React.createElement(Typography, {
    variant: "h3",
    sx: {
      px: 5,
      mt: 10,
      mb: 5
    }
  }, "Hi, Welcome Back"), /*#__PURE__*/React.createElement(Image, {
    alt: "login",
    src: "https://minimal-assets-api.vercel.app/assets/illustrations/illustration_login.png"
  })), /*#__PURE__*/React.createElement(Container, {
    maxWidth: "sm"
  }, /*#__PURE__*/React.createElement(ContentStyle, null, /*#__PURE__*/React.createElement(Stack, {
    direction: "row",
    alignItems: "center",
    sx: {
      mb: 5
    }
  }, /*#__PURE__*/React.createElement(Box, {
    sx: {
      flexGrow: 1
    }
  }, /*#__PURE__*/React.createElement(Typography, {
    variant: "h4",
    gutterBottom: true
  }, "Sign in to Minimal"), /*#__PURE__*/React.createElement(Typography, {
    sx: {
      color: 'text.secondary'
    }
  }, "Enter your details below.")), /*#__PURE__*/React.createElement(Tooltip, {
    title: capitalCase(method),
    placement: "right"
  }, /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement(Image, {
    disabledEffect: true,
    src: `https://minimal-assets-api.vercel.app/assets/icons/auth/ic_${method}.png`,
    sx: {
      width: 32,
      height: 32
    }
  })))), /*#__PURE__*/React.createElement(Alert, {
    severity: "info",
    sx: {
      mb: 3
    }
  }, "Use email : ", /*#__PURE__*/React.createElement("strong", null, "demo@minimals.cc"), " / password :", /*#__PURE__*/React.createElement("strong", null, " demo1234")), /*#__PURE__*/React.createElement(LoginForm, null), !smUp && /*#__PURE__*/React.createElement(Typography, {
    variant: "body2",
    align: "center",
    sx: {
      mt: 3
    }
  }, "Don\u2019t have an account?", ' ', /*#__PURE__*/React.createElement(Link, {
    variant: "subtitle2",
    component: RouterLink,
    to: PATH_AUTH.register
  }, "Get started"))))));
}