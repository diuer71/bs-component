function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

import React from 'react';
import PropTypes from 'prop-types';
import SimpleBarReact from 'simplebar-react'; // @mui

import { alpha, styled } from '@mui/material/styles';
import { Box } from '@mui/material'; // ----------------------------------------------------------------------

const RootStyle = styled('div')(() => ({
  flexGrow: 1,
  height: '100%',
  overflow: 'hidden'
}));
const SimpleBarStyle = styled(SimpleBarReact)(({
  theme
}) => ({
  maxHeight: '100%',
  '& .simplebar-scrollbar': {
    '&:before': {
      backgroundColor: alpha(theme.palette.grey[600], 0.48)
    },
    '&.simplebar-visible:before': {
      opacity: 1
    }
  },
  '& .simplebar-track.simplebar-vertical': {
    width: 10
  },
  '& .simplebar-track.simplebar-horizontal .simplebar-scrollbar': {
    height: 6
  },
  '& .simplebar-mask': {
    zIndex: 'inherit'
  }
})); // ----------------------------------------------------------------------

Scrollbar.propTypes = {
  children: PropTypes.node.isRequired,
  sx: PropTypes.object
};
export default function Scrollbar({
  children,
  sx,
  ...other
}) {
  const userAgent = typeof navigator === 'undefined' ? 'SSR' : navigator.userAgent;
  const isMobile = /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(userAgent);

  if (isMobile) {
    return /*#__PURE__*/React.createElement(Box, _extends({
      sx: {
        overflowX: 'auto',
        ...sx
      }
    }, other), children);
  }

  return /*#__PURE__*/React.createElement(RootStyle, null, /*#__PURE__*/React.createElement(SimpleBarStyle, _extends({
    timeout: 500,
    clickOnTrack: false,
    sx: sx
  }, other), children));
}