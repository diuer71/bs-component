import React from 'react';
import PropTypes from 'prop-types';
import { forwardRef } from 'react';
import { NavLink as RouterLink } from 'react-router-dom'; // @mui

import { Box, Link } from '@mui/material'; // config

import { ICON } from '../../../config'; //

import Iconify from '../../Iconify';
import { ListItemStyle } from './style';
import { isExternalLink } from '..'; // ----------------------------------------------------------------------

export const NavItemRoot = /*#__PURE__*/forwardRef(({
  item,
  active,
  open,
  onMouseEnter,
  onMouseLeave
}, ref) => {
  const {
    title,
    path,
    icon,
    children
  } = item;

  if (children) {
    return /*#__PURE__*/React.createElement(ListItemStyle, {
      ref: ref,
      open: open,
      activeRoot: active,
      onMouseEnter: onMouseEnter,
      onMouseLeave: onMouseLeave
    }, /*#__PURE__*/React.createElement(NavItemContent, {
      icon: icon,
      title: title,
      children: children
    }));
  }

  return isExternalLink(path) ? /*#__PURE__*/React.createElement(ListItemStyle, {
    component: Link,
    href: path,
    target: "_blank",
    rel: "noopener"
  }, /*#__PURE__*/React.createElement(NavItemContent, {
    icon: icon,
    title: title,
    children: children
  })) : /*#__PURE__*/React.createElement(ListItemStyle, {
    component: RouterLink,
    to: path,
    activeRoot: active
  }, /*#__PURE__*/React.createElement(NavItemContent, {
    icon: icon,
    title: title,
    children: children
  }));
});
NavItemRoot.propTypes = {
  active: PropTypes.bool,
  open: PropTypes.bool,
  onMouseEnter: PropTypes.func,
  onMouseLeave: PropTypes.func,
  item: PropTypes.shape({
    children: PropTypes.array,
    icon: PropTypes.any,
    path: PropTypes.string,
    title: PropTypes.string
  })
}; // ----------------------------------------------------------------------

export const NavItemSub = /*#__PURE__*/forwardRef(({
  item,
  active,
  open,
  onMouseEnter,
  onMouseLeave
}, ref) => {
  const {
    title,
    path,
    icon,
    children
  } = item;

  if (children) {
    return /*#__PURE__*/React.createElement(ListItemStyle, {
      ref: ref,
      subItem: true,
      disableRipple: true,
      open: open,
      activeSub: active,
      onMouseEnter: onMouseEnter,
      onMouseLeave: onMouseLeave
    }, /*#__PURE__*/React.createElement(NavItemContent, {
      icon: icon,
      title: title,
      children: children,
      subItem: true
    }));
  }

  return isExternalLink(path) ? /*#__PURE__*/React.createElement(ListItemStyle, {
    subItem: true,
    href: path,
    disableRipple: true,
    rel: "noopener",
    target: "_blank",
    component: Link
  }, /*#__PURE__*/React.createElement(NavItemContent, {
    icon: icon,
    title: title,
    children: children,
    subItem: true
  })) : /*#__PURE__*/React.createElement(ListItemStyle, {
    disableRipple: true,
    component: RouterLink,
    to: path,
    activeSub: active,
    subItem: true
  }, /*#__PURE__*/React.createElement(NavItemContent, {
    icon: icon,
    title: title,
    children: children,
    subItem: true
  }));
});
NavItemSub.propTypes = {
  active: PropTypes.bool,
  open: PropTypes.bool,
  onMouseEnter: PropTypes.func,
  onMouseLeave: PropTypes.func,
  item: PropTypes.shape({
    children: PropTypes.array,
    icon: PropTypes.any,
    path: PropTypes.string,
    title: PropTypes.string
  })
}; // ----------------------------------------------------------------------

NavItemContent.propTypes = {
  children: PropTypes.array,
  icon: PropTypes.any,
  subItem: PropTypes.bool,
  title: PropTypes.string
};

function NavItemContent({
  icon,
  title,
  children,
  subItem
}) {
  return /*#__PURE__*/React.createElement(React.Fragment, null, icon && /*#__PURE__*/React.createElement(Box, {
    component: "span",
    sx: {
      mr: 1,
      width: ICON.NAVBAR_ITEM_HORIZONTAL,
      height: ICON.NAVBAR_ITEM_HORIZONTAL,
      '& svg': {
        width: '100%',
        height: '100%'
      }
    }
  }, icon), title, children && /*#__PURE__*/React.createElement(Iconify, {
    icon: subItem ? 'eva:chevron-right-fill' : 'eva:chevron-down-fill',
    sx: {
      ml: 0.5,
      width: ICON.NAVBAR_ITEM_HORIZONTAL,
      height: ICON.NAVBAR_ITEM_HORIZONTAL
    }
  }));
}