import React from 'react';
import PropTypes from 'prop-types';
import { memo } from 'react'; // @mui

import { Stack } from '@mui/material'; //

import { NavListRoot } from './NavList'; // ----------------------------------------------------------------------

const hideScrollbar = {
  msOverflowStyle: 'none',
  scrollbarWidth: 'none',
  overflowY: 'scroll',
  '&::-webkit-scrollbar': {
    display: 'none'
  }
};
NavSectionHorizontal.propTypes = {
  navConfig: PropTypes.array
};

function NavSectionHorizontal({
  navConfig
}) {
  return /*#__PURE__*/React.createElement(Stack, {
    direction: "row",
    justifyContent: "center",
    sx: {
      bgcolor: 'background.neutral',
      borderRadius: 1,
      px: 0.5
    }
  }, /*#__PURE__*/React.createElement(Stack, {
    direction: "row",
    sx: { ...hideScrollbar,
      py: 1
    }
  }, navConfig.map(group => /*#__PURE__*/React.createElement(Stack, {
    key: group.subheader,
    direction: "row",
    flexShrink: 0
  }, group.items.map(list => /*#__PURE__*/React.createElement(NavListRoot, {
    key: list.title,
    list: list
  }))))));
}

export default /*#__PURE__*/memo(NavSectionHorizontal);