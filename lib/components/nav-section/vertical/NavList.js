import React from 'react';
import PropTypes from 'prop-types';
import { useState } from 'react';
import { useLocation } from 'react-router-dom'; // @mui

import { List, Collapse } from '@mui/material'; //

import { NavItemRoot, NavItemSub } from './NavItem';
import { getActive } from '..'; // ----------------------------------------------------------------------

NavListRoot.propTypes = {
  isCollapse: PropTypes.bool,
  list: PropTypes.object
};
export function NavListRoot({
  list,
  isCollapse
}) {
  const {
    pathname
  } = useLocation();
  const active = getActive(list.path, pathname);
  const [open, setOpen] = useState(active);
  const hasChildren = list.children;

  if (hasChildren) {
    return /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement(NavItemRoot, {
      item: list,
      isCollapse: isCollapse,
      active: active,
      open: open,
      onOpen: () => setOpen(!open)
    }), !isCollapse && /*#__PURE__*/React.createElement(Collapse, {
      in: open,
      timeout: "auto",
      unmountOnExit: true
    }, /*#__PURE__*/React.createElement(List, {
      component: "div",
      disablePadding: true
    }, (list.children || []).map(item => /*#__PURE__*/React.createElement(NavListSub, {
      key: item.title,
      list: item
    })))));
  }

  return /*#__PURE__*/React.createElement(NavItemRoot, {
    item: list,
    active: active,
    isCollapse: isCollapse
  });
} // ----------------------------------------------------------------------

NavListSub.propTypes = {
  list: PropTypes.object
};

function NavListSub({
  list
}) {
  const {
    pathname
  } = useLocation();
  const active = getActive(list.path, pathname);
  const [open, setOpen] = useState(active);
  const hasChildren = list.children;

  if (hasChildren) {
    return /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement(NavItemSub, {
      item: list,
      onOpen: () => setOpen(!open),
      open: open,
      active: active
    }), /*#__PURE__*/React.createElement(Collapse, {
      in: open,
      timeout: "auto",
      unmountOnExit: true
    }, /*#__PURE__*/React.createElement(List, {
      component: "div",
      disablePadding: true,
      sx: {
        pl: 3
      }
    }, (list.children || []).map(item => /*#__PURE__*/React.createElement(NavItemSub, {
      key: item.title,
      item: item,
      active: getActive(item.path, pathname)
    })))));
  }

  return /*#__PURE__*/React.createElement(NavItemSub, {
    item: list,
    active: active
  });
}