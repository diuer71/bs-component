function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

import PropTypes from 'prop-types'; // @mui

import { styled } from '@mui/material/styles';
import { List, Box, ListSubheader } from '@mui/material'; //

import { NavListRoot } from './NavList'; // ----------------------------------------------------------------------

export const ListSubheaderStyle = styled(props => /*#__PURE__*/React.createElement(ListSubheader, _extends({
  disableSticky: true,
  disableGutters: true
}, props)))(({
  theme
}) => ({ ...theme.typography.overline,
  paddingTop: theme.spacing(3),
  paddingLeft: theme.spacing(2),
  paddingBottom: theme.spacing(1),
  color: theme.palette.text.primary,
  transition: theme.transitions.create('opacity', {
    duration: theme.transitions.duration.shorter
  })
})); // ----------------------------------------------------------------------

NavSectionVertical.propTypes = {
  isCollapse: PropTypes.bool,
  navConfig: PropTypes.array
};
export default function NavSectionVertical({
  navConfig,
  isCollapse = false,
  ...other
}) {
  return /*#__PURE__*/React.createElement(Box, other, navConfig.map(group => /*#__PURE__*/React.createElement(List, {
    key: group.subheader,
    disablePadding: true,
    sx: {
      px: 2
    }
  }, /*#__PURE__*/React.createElement(ListSubheaderStyle, {
    sx: { ...(isCollapse && {
        opacity: 0
      })
    }
  }, group.subheader), group.items.map(list => /*#__PURE__*/React.createElement(NavListRoot, {
    key: list.title,
    list: list,
    isCollapse: isCollapse
  })))));
}