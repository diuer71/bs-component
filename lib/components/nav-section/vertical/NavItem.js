import React from 'react';
import PropTypes from 'prop-types';
import { NavLink as RouterLink } from 'react-router-dom'; // @mui

import { Box, Link, ListItemText } from '@mui/material'; //

import Iconify from '../../Iconify';
import { ListItemStyle, ListItemTextStyle, ListItemIconStyle } from './style';
import { isExternalLink } from '..'; // ----------------------------------------------------------------------

NavItemRoot.propTypes = {
  active: PropTypes.bool,
  open: PropTypes.bool,
  isCollapse: PropTypes.bool,
  onOpen: PropTypes.func,
  item: PropTypes.shape({
    children: PropTypes.array,
    icon: PropTypes.any,
    info: PropTypes.any,
    path: PropTypes.string,
    title: PropTypes.string
  })
};
export function NavItemRoot({
  item,
  isCollapse,
  open = false,
  active,
  onOpen
}) {
  const {
    title,
    path,
    icon,
    info,
    children
  } = item;
  const renderContent = /*#__PURE__*/React.createElement(React.Fragment, null, icon && /*#__PURE__*/React.createElement(ListItemIconStyle, null, icon), /*#__PURE__*/React.createElement(ListItemTextStyle, {
    disableTypography: true,
    primary: title,
    isCollapse: isCollapse
  }), !isCollapse && /*#__PURE__*/React.createElement(React.Fragment, null, info && info, children && /*#__PURE__*/React.createElement(ArrowIcon, {
    open: open
  })));

  if (children) {
    return /*#__PURE__*/React.createElement(ListItemStyle, {
      onClick: onOpen,
      activeRoot: active
    }, renderContent);
  }

  return isExternalLink(path) ? /*#__PURE__*/React.createElement(ListItemStyle, {
    component: Link,
    href: path,
    target: "_blank",
    rel: "noopener"
  }, renderContent) : /*#__PURE__*/React.createElement(ListItemStyle, {
    component: RouterLink,
    to: path,
    activeRoot: active
  }, renderContent);
} // ----------------------------------------------------------------------

NavItemSub.propTypes = {
  active: PropTypes.bool,
  open: PropTypes.bool,
  onOpen: PropTypes.func,
  item: PropTypes.shape({
    children: PropTypes.array,
    info: PropTypes.any,
    path: PropTypes.string,
    title: PropTypes.string
  })
};
export function NavItemSub({
  item,
  open = false,
  active = false,
  onOpen
}) {
  const {
    title,
    path,
    info,
    children
  } = item;
  const renderContent = /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement(DotIcon, {
    active: active
  }), /*#__PURE__*/React.createElement(ListItemText, {
    disableTypography: true,
    primary: title
  }), info && info, children && /*#__PURE__*/React.createElement(ArrowIcon, {
    open: open
  }));

  if (children) {
    return /*#__PURE__*/React.createElement(ListItemStyle, {
      onClick: onOpen,
      activeSub: active,
      subItem: true
    }, renderContent);
  }

  return isExternalLink(path) ? /*#__PURE__*/React.createElement(ListItemStyle, {
    component: Link,
    href: path,
    target: "_blank",
    rel: "noopener",
    subItem: true
  }, renderContent) : /*#__PURE__*/React.createElement(ListItemStyle, {
    component: RouterLink,
    to: path,
    activeSub: active,
    subItem: true
  }, renderContent);
} // ----------------------------------------------------------------------

DotIcon.propTypes = {
  active: PropTypes.bool
};
export function DotIcon({
  active
}) {
  return /*#__PURE__*/React.createElement(ListItemIconStyle, null, /*#__PURE__*/React.createElement(Box, {
    component: "span",
    sx: {
      width: 4,
      height: 4,
      borderRadius: '50%',
      bgcolor: 'text.disabled',
      transition: theme => theme.transitions.create('transform', {
        duration: theme.transitions.duration.shorter
      }),
      ...(active && {
        transform: 'scale(2)',
        bgcolor: 'primary.main'
      })
    }
  }));
} // ----------------------------------------------------------------------

ArrowIcon.propTypes = {
  open: PropTypes.bool
};
export function ArrowIcon({
  open
}) {
  return /*#__PURE__*/React.createElement(Iconify, {
    icon: open ? 'eva:arrow-ios-downward-fill' : 'eva:arrow-ios-forward-fill',
    sx: {
      width: 16,
      height: 16,
      ml: 1
    }
  });
}