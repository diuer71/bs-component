function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

import React from 'react';
import PropTypes from 'prop-types'; // form

import { useFormContext, Controller } from 'react-hook-form'; // @mui

import { Radio, RadioGroup, FormHelperText, FormControlLabel } from '@mui/material'; // ----------------------------------------------------------------------

RHFRadioGroup.propTypes = {
  name: PropTypes.string,
  options: PropTypes.arrayOf(PropTypes.string),
  getOptionLabel: PropTypes.arrayOf(PropTypes.string)
};
export default function RHFRadioGroup({
  name,
  options,
  getOptionLabel,
  ...other
}) {
  const {
    control
  } = useFormContext();
  return /*#__PURE__*/React.createElement(Controller, {
    name: name,
    control: control,
    render: ({
      field,
      fieldState: {
        error
      }
    }) => /*#__PURE__*/React.createElement("div", null, /*#__PURE__*/React.createElement(RadioGroup, _extends({}, field, {
      row: true
    }, other), options.map((option, index) => /*#__PURE__*/React.createElement(FormControlLabel, {
      key: option,
      value: option,
      control: /*#__PURE__*/React.createElement(Radio, null),
      label: getOptionLabel?.length ? getOptionLabel[index] : option
    }))), !!error && /*#__PURE__*/React.createElement(FormHelperText, {
      error: true,
      sx: {
        px: 2
      }
    }, error.message))
  });
}