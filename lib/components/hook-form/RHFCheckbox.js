function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

import React from 'react';
import PropTypes from 'prop-types'; // form

import { useFormContext, Controller } from 'react-hook-form'; // @mui

import { Checkbox, FormGroup, FormControlLabel } from '@mui/material'; // ----------------------------------------------------------------------

RHFCheckbox.propTypes = {
  name: PropTypes.string
};
export function RHFCheckbox({
  name,
  ...other
}) {
  const {
    control
  } = useFormContext();
  return /*#__PURE__*/React.createElement(FormControlLabel, _extends({
    control: /*#__PURE__*/React.createElement(Controller, {
      name: name,
      control: control,
      render: ({
        field
      }) => /*#__PURE__*/React.createElement(Checkbox, _extends({}, field, {
        checked: field.value
      }))
    })
  }, other));
} // ----------------------------------------------------------------------

RHFMultiCheckbox.propTypes = {
  name: PropTypes.string,
  options: PropTypes.arrayOf(PropTypes.string)
};
export function RHFMultiCheckbox({
  name,
  options,
  ...other
}) {
  const {
    control
  } = useFormContext();
  return /*#__PURE__*/React.createElement(Controller, {
    name: name,
    control: control,
    render: ({
      field
    }) => {
      const onSelected = option => field.value.includes(option) ? field.value.filter(value => value !== option) : [...field.value, option];

      return /*#__PURE__*/React.createElement(FormGroup, null, options.map(option => /*#__PURE__*/React.createElement(FormControlLabel, _extends({
        key: option,
        control: /*#__PURE__*/React.createElement(Checkbox, {
          checked: field.value.includes(option),
          onChange: () => field.onChange(onSelected(option))
        }),
        label: option
      }, other))));
    }
  });
}