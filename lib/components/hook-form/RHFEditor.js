function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

import React from 'react';
import PropTypes from 'prop-types'; // form

import { useFormContext, Controller } from 'react-hook-form'; // @mui

import { FormHelperText } from '@mui/material'; //

import Editor from '../editor'; // ----------------------------------------------------------------------

RHFEditor.propTypes = {
  name: PropTypes.string
};
export default function RHFEditor({
  name,
  ...other
}) {
  const {
    control
  } = useFormContext();
  return /*#__PURE__*/React.createElement(Controller, {
    name: name,
    control: control,
    render: ({
      field,
      fieldState: {
        error
      }
    }) => /*#__PURE__*/React.createElement(Editor, _extends({
      id: name,
      value: field.value,
      onChange: field.onChange,
      error: !!error,
      helperText: /*#__PURE__*/React.createElement(FormHelperText, {
        error: true,
        sx: {
          px: 2,
          textTransform: 'capitalize'
        }
      }, error?.message)
    }, other))
  });
}