function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

import React from 'react';
import PropTypes from 'prop-types'; // form

import { useFormContext, Controller } from 'react-hook-form'; // @mui

import { TextField } from '@mui/material'; // ----------------------------------------------------------------------

RHFTextField.propTypes = {
  name: PropTypes.string
};
export default function RHFTextField({
  name,
  ...other
}) {
  const {
    control
  } = useFormContext();
  return /*#__PURE__*/React.createElement(Controller, {
    name: name,
    control: control,
    render: ({
      field,
      fieldState: {
        error
      }
    }) => /*#__PURE__*/React.createElement(TextField, _extends({}, field, {
      fullWidth: true,
      error: !!error,
      helperText: error?.message
    }, other))
  });
}