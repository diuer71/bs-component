function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

import React from 'react';
import PropTypes from 'prop-types'; // form

import { useFormContext, Controller } from 'react-hook-form'; // @mui

import { FormHelperText } from '@mui/material'; // type

import { UploadAvatar, UploadMultiFile, UploadSingleFile } from '../upload'; // ----------------------------------------------------------------------

RHFUploadAvatar.propTypes = {
  name: PropTypes.string
};
export function RHFUploadAvatar({
  name,
  ...other
}) {
  const {
    control
  } = useFormContext();
  return /*#__PURE__*/React.createElement(Controller, {
    name: name,
    control: control,
    render: ({
      field,
      fieldState: {
        error
      }
    }) => {
      const checkError = !!error && !field.value;
      return /*#__PURE__*/React.createElement("div", null, /*#__PURE__*/React.createElement(UploadAvatar, _extends({
        error: checkError
      }, other, {
        file: field.value
      })), checkError && /*#__PURE__*/React.createElement(FormHelperText, {
        error: true,
        sx: {
          px: 2,
          textAlign: 'center'
        }
      }, error.message));
    }
  });
} // ----------------------------------------------------------------------

RHFUploadSingleFile.propTypes = {
  name: PropTypes.string
};
export function RHFUploadSingleFile({
  name,
  ...other
}) {
  const {
    control
  } = useFormContext();
  return /*#__PURE__*/React.createElement(Controller, {
    name: name,
    control: control,
    render: ({
      field,
      fieldState: {
        error
      }
    }) => {
      const checkError = !!error && !field.value;
      return /*#__PURE__*/React.createElement(UploadSingleFile, _extends({
        accept: "image/*",
        file: field.value,
        error: checkError,
        helperText: checkError && /*#__PURE__*/React.createElement(FormHelperText, {
          error: true,
          sx: {
            px: 2
          }
        }, error.message)
      }, other));
    }
  });
} // ----------------------------------------------------------------------

RHFUploadMultiFile.propTypes = {
  name: PropTypes.string
};
export function RHFUploadMultiFile({
  name,
  ...other
}) {
  const {
    control
  } = useFormContext();
  return /*#__PURE__*/React.createElement(Controller, {
    name: name,
    control: control,
    render: ({
      field,
      fieldState: {
        error
      }
    }) => {
      const checkError = !!error && field.value?.length === 0;
      return /*#__PURE__*/React.createElement(UploadMultiFile, _extends({
        accept: "image/*",
        files: field.value,
        error: checkError,
        helperText: checkError && /*#__PURE__*/React.createElement(FormHelperText, {
          error: true,
          sx: {
            px: 2
          }
        }, error?.message)
      }, other));
    }
  });
}