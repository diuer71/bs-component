import React from 'react';
import PropTypes from 'prop-types'; // form

import { FormProvider as Form } from 'react-hook-form'; // ----------------------------------------------------------------------

FormProvider.propTypes = {
  children: PropTypes.node.isRequired,
  methods: PropTypes.object.isRequired,
  onSubmit: PropTypes.func
};
export default function FormProvider({
  children,
  onSubmit,
  methods
}) {
  return /*#__PURE__*/React.createElement(Form, methods, /*#__PURE__*/React.createElement("form", {
    onSubmit: onSubmit
  }, children));
}