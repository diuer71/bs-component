import React from 'react'; // @mui

import { Stack, Skeleton, Box, Paper } from '@mui/material'; // ----------------------------------------------------------------------

export default function SkeletonKanbanColumn() {
  return /*#__PURE__*/React.createElement(Box, {
    sx: {
      display: 'grid',
      gap: 3,
      gridTemplateColumns: 'repeat(4, 1fr)'
    }
  }, [...Array(3)].map((_, index) => /*#__PURE__*/React.createElement(Paper, {
    variant: "outlined",
    key: index,
    sx: {
      p: 2.5,
      width: 310
    }
  }, /*#__PURE__*/React.createElement(Stack, {
    spacing: 2
  }, /*#__PURE__*/React.createElement(Skeleton, {
    variant: "rectangular",
    sx: {
      paddingTop: '75%',
      borderRadius: 1.5
    }
  }), index === 0 && /*#__PURE__*/React.createElement(Skeleton, {
    variant: "rectangular",
    sx: {
      paddingTop: '25%',
      borderRadius: 1.5
    }
  }), index !== 2 && /*#__PURE__*/React.createElement(Skeleton, {
    variant: "rectangular",
    sx: {
      paddingTop: '25%',
      borderRadius: 1.5
    }
  })))));
}