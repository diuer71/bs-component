import React from 'react'; // @mui

import { Stack, Skeleton } from '@mui/material'; // ----------------------------------------------------------------------

export default function SkeletonMailSidebarItem() {
  return /*#__PURE__*/React.createElement(Stack, {
    spacing: 1,
    direction: "row",
    alignItems: "center",
    sx: {
      px: 3,
      py: 1
    }
  }, /*#__PURE__*/React.createElement(Skeleton, {
    variant: "circular",
    width: 32,
    height: 32
  }), /*#__PURE__*/React.createElement(Skeleton, {
    variant: "text",
    sx: {
      width: 0.25,
      height: 16
    }
  }));
}