import React from 'react'; // @mui

import { Box, Skeleton } from '@mui/material'; // ----------------------------------------------------------------------

export default function SkeletonPost() {
  return /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement(Skeleton, {
    width: "100%",
    height: 560,
    variant: "rectangular",
    sx: {
      borderRadius: 2
    }
  }), /*#__PURE__*/React.createElement(Box, {
    sx: {
      mt: 3,
      display: 'flex',
      alignItems: 'center'
    }
  }, /*#__PURE__*/React.createElement(Skeleton, {
    variant: "circular",
    width: 64,
    height: 64
  }), /*#__PURE__*/React.createElement(Box, {
    sx: {
      flexGrow: 1,
      ml: 2
    }
  }, /*#__PURE__*/React.createElement(Skeleton, {
    variant: "text",
    height: 20
  }), /*#__PURE__*/React.createElement(Skeleton, {
    variant: "text",
    height: 20
  }), /*#__PURE__*/React.createElement(Skeleton, {
    variant: "text",
    height: 20
  }))));
}