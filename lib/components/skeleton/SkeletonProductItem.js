import React from 'react'; // @mui

import { Card, Skeleton, Stack } from '@mui/material'; // ----------------------------------------------------------------------

export default function SkeletonProductItem() {
  return /*#__PURE__*/React.createElement(Card, null, /*#__PURE__*/React.createElement(Skeleton, {
    variant: "rectangular",
    sx: {
      paddingTop: '100%'
    }
  }), /*#__PURE__*/React.createElement(Stack, {
    spacing: 2,
    sx: {
      p: 3
    }
  }, /*#__PURE__*/React.createElement(Skeleton, {
    variant: "text",
    sx: {
      width: 0.5
    }
  }), /*#__PURE__*/React.createElement(Stack, {
    direction: "row",
    justifyContent: "space-between",
    alignItems: "center"
  }, /*#__PURE__*/React.createElement(Stack, {
    direction: "row"
  }, /*#__PURE__*/React.createElement(Skeleton, {
    variant: "circular",
    sx: {
      width: 16,
      height: 16
    }
  }), /*#__PURE__*/React.createElement(Skeleton, {
    variant: "circular",
    sx: {
      width: 16,
      height: 16
    }
  }), /*#__PURE__*/React.createElement(Skeleton, {
    variant: "circular",
    sx: {
      width: 16,
      height: 16
    }
  })), /*#__PURE__*/React.createElement(Skeleton, {
    variant: "text",
    sx: {
      width: 40
    }
  }))));
}