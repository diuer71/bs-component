import React from 'react'; // @mui

import { Grid, Skeleton } from '@mui/material'; // ----------------------------------------------------------------------

export default function SkeletonProduct() {
  return /*#__PURE__*/React.createElement(Grid, {
    container: true,
    spacing: 3
  }, /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: 12,
    md: 6,
    lg: 7
  }, /*#__PURE__*/React.createElement(Skeleton, {
    variant: "rectangular",
    width: "100%",
    sx: {
      paddingTop: '100%',
      borderRadius: 2
    }
  })), /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: 12,
    md: 6,
    lg: 5
  }, /*#__PURE__*/React.createElement(Skeleton, {
    variant: "circular",
    width: 80,
    height: 80
  }), /*#__PURE__*/React.createElement(Skeleton, {
    variant: "text",
    height: 240
  }), /*#__PURE__*/React.createElement(Skeleton, {
    variant: "text",
    height: 40
  }), /*#__PURE__*/React.createElement(Skeleton, {
    variant: "text",
    height: 40
  }), /*#__PURE__*/React.createElement(Skeleton, {
    variant: "text",
    height: 40
  })));
}