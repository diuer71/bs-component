import React from 'react'; // @mui

import { Box, Skeleton, Grid } from '@mui/material'; // ----------------------------------------------------------------------

export default function SkeletonPostItem() {
  return /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: 12,
    sm: 6,
    md: 3
  }, /*#__PURE__*/React.createElement(Skeleton, {
    variant: "rectangular",
    width: "100%",
    sx: {
      height: 200,
      borderRadius: 2
    }
  }), /*#__PURE__*/React.createElement(Box, {
    sx: {
      display: 'flex',
      mt: 1.5
    }
  }, /*#__PURE__*/React.createElement(Skeleton, {
    variant: "circular",
    sx: {
      width: 40,
      height: 40
    }
  }), /*#__PURE__*/React.createElement(Skeleton, {
    variant: "text",
    sx: {
      mx: 1,
      flexGrow: 1
    }
  })));
}