import React from 'react'; // @mui

import { Stack, Skeleton } from '@mui/material'; // ----------------------------------------------------------------------

export default function SkeletonMap() {
  return /*#__PURE__*/React.createElement(Stack, {
    spacing: 8
  }, [...Array(5)].map((_, index) => /*#__PURE__*/React.createElement(Skeleton, {
    key: index,
    variant: "rectangular",
    sx: {
      width: 1,
      height: 560,
      borderRadius: 2
    }
  })));
}