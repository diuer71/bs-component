import React from 'react'; // @mui

import { Stack, Skeleton } from '@mui/material'; // ----------------------------------------------------------------------

export default function SkeletonConversationItem() {
  return /*#__PURE__*/React.createElement(Stack, {
    spacing: 1,
    direction: "row",
    alignItems: "center",
    sx: {
      px: 3,
      py: 1.5
    }
  }, /*#__PURE__*/React.createElement(Skeleton, {
    variant: "circular",
    width: 48,
    height: 48
  }), /*#__PURE__*/React.createElement(Stack, {
    spacing: 0.5,
    sx: {
      flexGrow: 1
    }
  }, /*#__PURE__*/React.createElement(Skeleton, {
    variant: "text",
    sx: {
      width: 0.5,
      height: 16
    }
  }), /*#__PURE__*/React.createElement(Skeleton, {
    variant: "text",
    sx: {
      width: 0.25,
      height: 12
    }
  })));
}