function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

import React from 'react';
import PropTypes from 'prop-types';
import isString from 'lodash/isString';
import { useDropzone } from 'react-dropzone'; // @mui

import { Typography } from '@mui/material';
import { styled } from '@mui/material/styles'; //

import Image from '../Image';
import Iconify from '../Iconify';
import RejectionFiles from './RejectionFiles'; // ----------------------------------------------------------------------

const RootStyle = styled('div')(({
  theme
}) => ({
  width: 144,
  height: 144,
  margin: 'auto',
  borderRadius: '50%',
  padding: theme.spacing(1),
  border: `1px dashed ${theme.palette.grey[500_32]}`
}));
const DropZoneStyle = styled('div')({
  zIndex: 0,
  width: '100%',
  height: '100%',
  outline: 'none',
  display: 'flex',
  overflow: 'hidden',
  borderRadius: '50%',
  position: 'relative',
  alignItems: 'center',
  justifyContent: 'center',
  '& > *': {
    width: '100%',
    height: '100%'
  },
  '&:hover': {
    cursor: 'pointer',
    '& .placeholder': {
      zIndex: 9
    }
  }
});
const PlaceholderStyle = styled('div')(({
  theme
}) => ({
  display: 'flex',
  position: 'absolute',
  alignItems: 'center',
  flexDirection: 'column',
  justifyContent: 'center',
  color: theme.palette.text.secondary,
  backgroundColor: theme.palette.background.neutral,
  transition: theme.transitions.create('opacity', {
    easing: theme.transitions.easing.easeInOut,
    duration: theme.transitions.duration.shorter
  }),
  '&:hover': {
    opacity: 0.72
  }
})); // ----------------------------------------------------------------------

UploadAvatar.propTypes = {
  error: PropTypes.bool,
  file: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
  helperText: PropTypes.node,
  sx: PropTypes.object
};
export default function UploadAvatar({
  error,
  file,
  helperText,
  sx,
  ...other
}) {
  const {
    getRootProps,
    getInputProps,
    isDragActive,
    isDragReject,
    fileRejections
  } = useDropzone({
    multiple: false,
    ...other
  });
  return /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement(RootStyle, {
    sx: { ...((isDragReject || error) && {
        borderColor: 'error.light'
      }),
      ...sx
    }
  }, /*#__PURE__*/React.createElement(DropZoneStyle, _extends({}, getRootProps(), {
    sx: { ...(isDragActive && {
        opacity: 0.72
      })
    }
  }), /*#__PURE__*/React.createElement("input", getInputProps()), file && /*#__PURE__*/React.createElement(Image, {
    alt: "avatar",
    src: isString(file) ? file : file.preview,
    sx: {
      zIndex: 8
    }
  }), /*#__PURE__*/React.createElement(PlaceholderStyle, {
    className: "placeholder",
    sx: { ...(file && {
        opacity: 0,
        color: 'common.white',
        bgcolor: 'grey.900',
        '&:hover': {
          opacity: 0.72
        }
      }),
      ...((isDragReject || error) && {
        bgcolor: 'error.lighter'
      })
    }
  }, /*#__PURE__*/React.createElement(Iconify, {
    icon: 'ic:round-add-a-photo',
    sx: {
      width: 24,
      height: 24,
      mb: 1
    }
  }), /*#__PURE__*/React.createElement(Typography, {
    variant: "caption"
  }, file ? 'Update photo' : 'Upload photo')))), helperText && helperText, fileRejections.length > 0 && /*#__PURE__*/React.createElement(RejectionFiles, {
    fileRejections: fileRejections
  }));
}