function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

import React from 'react';
import PropTypes from 'prop-types';
import { useDropzone } from 'react-dropzone'; // @mui

import { styled } from '@mui/material/styles';
import { Box } from '@mui/material'; //

import BlockContent from './BlockContent';
import RejectionFiles from './RejectionFiles';
import MultiFilePreview from './MultiFilePreview'; // ----------------------------------------------------------------------

const DropZoneStyle = styled('div')(({
  theme
}) => ({
  outline: 'none',
  padding: theme.spacing(5, 1),
  borderRadius: theme.shape.borderRadius,
  backgroundColor: theme.palette.background.neutral,
  border: `1px dashed ${theme.palette.grey[500_32]}`,
  '&:hover': {
    opacity: 0.72,
    cursor: 'pointer'
  }
})); // ----------------------------------------------------------------------

UploadMultiFile.propTypes = {
  error: PropTypes.bool,
  showPreview: PropTypes.bool,
  files: PropTypes.array,
  onRemove: PropTypes.func,
  onRemoveAll: PropTypes.func,
  helperText: PropTypes.node,
  sx: PropTypes.object
};
export default function UploadMultiFile({
  error,
  showPreview = false,
  files,
  onRemove,
  onRemoveAll,
  helperText,
  sx,
  ...other
}) {
  const {
    getRootProps,
    getInputProps,
    isDragActive,
    isDragReject,
    fileRejections
  } = useDropzone({ ...other
  });
  return /*#__PURE__*/React.createElement(Box, {
    sx: {
      width: '100%',
      ...sx
    }
  }, /*#__PURE__*/React.createElement(DropZoneStyle, _extends({}, getRootProps(), {
    sx: { ...(isDragActive && {
        opacity: 0.72
      }),
      ...((isDragReject || error) && {
        color: 'error.main',
        borderColor: 'error.light',
        bgcolor: 'error.lighter'
      })
    }
  }), /*#__PURE__*/React.createElement("input", getInputProps()), /*#__PURE__*/React.createElement(BlockContent, null)), fileRejections.length > 0 && /*#__PURE__*/React.createElement(RejectionFiles, {
    fileRejections: fileRejections
  }), /*#__PURE__*/React.createElement(MultiFilePreview, {
    files: files,
    showPreview: showPreview,
    onRemove: onRemove,
    onRemoveAll: onRemoveAll
  }), helperText && helperText);
}