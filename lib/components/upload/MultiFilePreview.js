function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

import React from 'react';
import PropTypes from 'prop-types';
import isString from 'lodash/isString';
import { m, AnimatePresence } from 'framer-motion'; // @mui

import { alpha } from '@mui/material/styles';
import { List, Stack, Button, IconButton, ListItemText, ListItem } from '@mui/material'; // utils

import { fData } from '../../utils/formatNumber'; //

import Image from '../Image';
import Iconify from '../Iconify';
import { varFade } from '../animate'; // ----------------------------------------------------------------------

const getFileData = file => {
  if (typeof file === 'string') {
    return {
      key: file
    };
  }

  return {
    key: file.name,
    name: file.name,
    size: file.size,
    preview: file.preview
  };
}; // ----------------------------------------------------------------------


MultiFilePreview.propTypes = {
  files: PropTypes.array,
  showPreview: PropTypes.bool,
  onRemove: PropTypes.func,
  onRemoveAll: PropTypes.func
};
export default function MultiFilePreview({
  showPreview = false,
  files,
  onRemove,
  onRemoveAll
}) {
  const hasFile = files.length > 0;
  return /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement(List, {
    disablePadding: true,
    sx: { ...(hasFile && {
        my: 3
      })
    }
  }, /*#__PURE__*/React.createElement(AnimatePresence, null, files.map(file => {
    const {
      key,
      name,
      size,
      preview
    } = getFileData(file);

    if (showPreview) {
      return /*#__PURE__*/React.createElement(ListItem, _extends({
        key: key,
        component: m.div
      }, varFade().inRight, {
        sx: {
          p: 0,
          m: 0.5,
          width: 80,
          height: 80,
          borderRadius: 1.25,
          overflow: 'hidden',
          position: 'relative',
          display: 'inline-flex',
          border: theme => `solid 1px ${theme.palette.divider}`
        }
      }), /*#__PURE__*/React.createElement(Image, {
        alt: "preview",
        src: isString(file) ? file : preview,
        ratio: "1/1"
      }), /*#__PURE__*/React.createElement(IconButton, {
        size: "small",
        onClick: () => onRemove(file),
        sx: {
          top: 6,
          p: '2px',
          right: 6,
          position: 'absolute',
          color: 'common.white',
          bgcolor: theme => alpha(theme.palette.grey[900], 0.72),
          '&:hover': {
            bgcolor: theme => alpha(theme.palette.grey[900], 0.48)
          }
        }
      }, /*#__PURE__*/React.createElement(Iconify, {
        icon: 'eva:close-fill'
      })));
    }

    return /*#__PURE__*/React.createElement(ListItem, _extends({
      key: key,
      component: m.div
    }, varFade().inRight, {
      sx: {
        my: 1,
        px: 2,
        py: 0.75,
        borderRadius: 0.75,
        border: theme => `solid 1px ${theme.palette.divider}`
      }
    }), /*#__PURE__*/React.createElement(Iconify, {
      icon: 'eva:file-fill',
      sx: {
        width: 28,
        height: 28,
        color: 'text.secondary',
        mr: 2
      }
    }), /*#__PURE__*/React.createElement(ListItemText, {
      primary: isString(file) ? file : name,
      secondary: isString(file) ? '' : fData(size || 0),
      primaryTypographyProps: {
        variant: 'subtitle2'
      },
      secondaryTypographyProps: {
        variant: 'caption'
      }
    }), /*#__PURE__*/React.createElement(IconButton, {
      edge: "end",
      size: "small",
      onClick: () => onRemove(file)
    }, /*#__PURE__*/React.createElement(Iconify, {
      icon: 'eva:close-fill'
    })));
  }))), hasFile && /*#__PURE__*/React.createElement(Stack, {
    direction: "row",
    justifyContent: "flex-end",
    spacing: 1.5
  }, /*#__PURE__*/React.createElement(Button, {
    color: "inherit",
    size: "small",
    onClick: onRemoveAll
  }, "Remove all"), /*#__PURE__*/React.createElement(Button, {
    size: "small",
    variant: "contained"
  }, "Upload files")));
}