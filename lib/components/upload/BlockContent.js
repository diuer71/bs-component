import React from 'react'; // @mui

import { Box, Typography, Stack } from '@mui/material'; // assets

import { UploadIllustration } from '../../assets'; // ----------------------------------------------------------------------

export default function BlockContent() {
  return /*#__PURE__*/React.createElement(Stack, {
    spacing: 2,
    alignItems: "center",
    justifyContent: "center",
    direction: {
      xs: 'column',
      md: 'row'
    },
    sx: {
      width: 1,
      textAlign: {
        xs: 'center',
        md: 'left'
      }
    }
  }, /*#__PURE__*/React.createElement(UploadIllustration, {
    sx: {
      width: 220
    }
  }), /*#__PURE__*/React.createElement(Box, {
    sx: {
      p: 3
    }
  }, /*#__PURE__*/React.createElement(Typography, {
    gutterBottom: true,
    variant: "h5"
  }, "Drop or Select file"), /*#__PURE__*/React.createElement(Typography, {
    variant: "body2",
    sx: {
      color: 'text.secondary'
    }
  }, "Drop files here or click\xA0", /*#__PURE__*/React.createElement(Typography, {
    variant: "body2",
    component: "span",
    sx: {
      color: 'primary.main',
      textDecoration: 'underline'
    }
  }, "browse"), "\xA0thorough your machine")));
}