function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

import React from 'react';
import PropTypes from 'prop-types';
import isString from 'lodash/isString';
import { useDropzone } from 'react-dropzone'; // @mui

import { styled } from '@mui/material/styles';
import { Box } from '@mui/material'; //

import Image from '../Image';
import RejectionFiles from './RejectionFiles';
import BlockContent from './BlockContent'; // ----------------------------------------------------------------------

const DropZoneStyle = styled('div')(({
  theme
}) => ({
  outline: 'none',
  overflow: 'hidden',
  position: 'relative',
  padding: theme.spacing(5, 1),
  borderRadius: theme.shape.borderRadius,
  transition: theme.transitions.create('padding'),
  backgroundColor: theme.palette.background.neutral,
  border: `1px dashed ${theme.palette.grey[500_32]}`,
  '&:hover': {
    opacity: 0.72,
    cursor: 'pointer'
  }
})); // ----------------------------------------------------------------------

UploadSingleFile.propTypes = {
  error: PropTypes.bool,
  file: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
  helperText: PropTypes.node,
  sx: PropTypes.object
};
export default function UploadSingleFile({
  error = false,
  file,
  helperText,
  sx,
  ...other
}) {
  const {
    getRootProps,
    getInputProps,
    isDragActive,
    isDragReject,
    fileRejections
  } = useDropzone({
    multiple: false,
    ...other
  });
  return /*#__PURE__*/React.createElement(Box, {
    sx: {
      width: '100%',
      ...sx
    }
  }, /*#__PURE__*/React.createElement(DropZoneStyle, _extends({}, getRootProps(), {
    sx: { ...(isDragActive && {
        opacity: 0.72
      }),
      ...((isDragReject || error) && {
        color: 'error.main',
        borderColor: 'error.light',
        bgcolor: 'error.lighter'
      }),
      ...(file && {
        padding: '12% 0'
      })
    }
  }), /*#__PURE__*/React.createElement("input", getInputProps()), /*#__PURE__*/React.createElement(BlockContent, null), file && /*#__PURE__*/React.createElement(Image, {
    alt: "file preview",
    src: isString(file) ? file : file.preview,
    sx: {
      top: 8,
      left: 8,
      borderRadius: 1,
      position: 'absolute',
      width: 'calc(100% - 16px)',
      height: 'calc(100% - 16px)'
    }
  })), fileRejections.length > 0 && /*#__PURE__*/React.createElement(RejectionFiles, {
    fileRejections: fileRejections
  }), helperText && helperText);
}