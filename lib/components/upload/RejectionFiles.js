import React from 'react'; // @mui

import PropTypes from 'prop-types';
import { alpha } from '@mui/material/styles';
import { Box, Paper, Typography } from '@mui/material'; // utils

import { fData } from '../../utils/formatNumber'; // ----------------------------------------------------------------------

RejectionFiles.propTypes = {
  fileRejections: PropTypes.array
};
export default function RejectionFiles({
  fileRejections
}) {
  return /*#__PURE__*/React.createElement(Paper, {
    variant: "outlined",
    sx: {
      py: 1,
      px: 2,
      mt: 3,
      borderColor: 'error.light',
      bgcolor: theme => alpha(theme.palette.error.main, 0.08)
    }
  }, fileRejections.map(({
    file,
    errors
  }) => {
    const {
      path,
      size
    } = file;
    return /*#__PURE__*/React.createElement(Box, {
      key: path,
      sx: {
        my: 1
      }
    }, /*#__PURE__*/React.createElement(Typography, {
      variant: "subtitle2",
      noWrap: true
    }, path, " - ", fData(size)), errors.map(error => /*#__PURE__*/React.createElement(Typography, {
      key: error.code,
      variant: "caption",
      component: "p"
    }, "- ", error.message)));
  }));
}