function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

import React from 'react';
import PropTypes from 'prop-types';
import { forwardRef } from 'react'; // @mui

import { Typography, Link } from '@mui/material'; // utils

import GetFontValue from '../utils/getFontValue'; // ----------------------------------------------------------------------

const TextMaxLine = /*#__PURE__*/forwardRef(({
  asLink,
  variant = 'body1',
  line = 2,
  persistent = false,
  children,
  sx,
  ...other
}, ref) => {
  const {
    lineHeight
  } = GetFontValue(variant);
  const style = {
    overflow: 'hidden',
    textOverflow: 'ellipsis',
    display: '-webkit-box',
    WebkitLineClamp: line,
    WebkitBoxOrient: 'vertical',
    ...(persistent && {
      height: lineHeight * line
    }),
    ...sx
  };

  if (asLink) {
    return /*#__PURE__*/React.createElement(Link, _extends({
      color: "inherit",
      ref: ref,
      variant: variant,
      sx: { ...style
      }
    }, other), children);
  }

  return /*#__PURE__*/React.createElement(Typography, _extends({
    ref: ref,
    variant: variant,
    sx: { ...style
    }
  }, other), children);
});
TextMaxLine.propTypes = {
  asLink: PropTypes.bool,
  children: PropTypes.node.isRequired,
  line: PropTypes.number,
  persistent: PropTypes.bool,
  sx: PropTypes.object,
  variant: PropTypes.oneOf(['body1', 'body2', 'button', 'caption', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'inherit', 'overline', 'subtitle1', 'subtitle2'])
};
export default TextMaxLine;