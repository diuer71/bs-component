function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

import React from 'react';
import PropTypes from 'prop-types'; // icons

import { Icon } from '@iconify/react'; // @mui

import { Box } from '@mui/material'; // ----------------------------------------------------------------------

Iconify.propTypes = {
  icon: PropTypes.oneOfType([PropTypes.element, PropTypes.string]),
  sx: PropTypes.object
};
export default function Iconify({
  icon,
  sx,
  ...other
}) {
  return /*#__PURE__*/React.createElement(Box, _extends({
    component: Icon,
    icon: icon,
    sx: { ...sx
    }
  }, other));
}