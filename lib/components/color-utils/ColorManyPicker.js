function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

import React from 'react';
import PropTypes from 'prop-types'; // @mui

import { Box, Checkbox } from '@mui/material'; //

import Iconify from '../Iconify'; // ----------------------------------------------------------------------

ColorManyPicker.propTypes = {
  colors: PropTypes.arrayOf(PropTypes.string),
  onChangeColor: PropTypes.func,
  sx: PropTypes.object
};
export default function ColorManyPicker({
  colors,
  onChangeColor,
  sx,
  ...other
}) {
  return /*#__PURE__*/React.createElement(Box, {
    sx: sx
  }, colors.map(color => {
    const isWhite = color === '#FFFFFF' || color === 'white';
    return /*#__PURE__*/React.createElement(Checkbox, _extends({
      key: color,
      size: "small",
      value: color,
      color: "default",
      onChange: () => onChangeColor(color),
      icon: /*#__PURE__*/React.createElement(IconColor, {
        sx: { ...(isWhite && {
            border: theme => `solid 1px ${theme.palette.divider}`
          })
        }
      }),
      checkedIcon: /*#__PURE__*/React.createElement(IconColor, {
        sx: {
          transform: 'scale(1.4)',
          '&:before': {
            opacity: 0.48,
            width: '100%',
            content: "''",
            height: '100%',
            borderRadius: '50%',
            position: 'absolute',
            boxShadow: '4px 4px 8px 0 currentColor'
          },
          '& svg': {
            width: 12,
            height: 12,
            color: 'common.white'
          },
          ...(isWhite && {
            border: theme => `solid 1px ${theme.palette.divider}`,
            boxShadow: theme => `4px 4px 8px 0 ${theme.palette.grey[500_24]}`,
            '& svg': {
              width: 12,
              height: 12,
              color: 'common.black'
            }
          })
        }
      }),
      sx: {
        color,
        '&:hover': {
          opacity: 0.72
        }
      }
    }, other));
  }));
} // ----------------------------------------------------------------------

IconColor.propTypes = {
  sx: PropTypes.object
};

function IconColor({
  sx,
  ...other
}) {
  return /*#__PURE__*/React.createElement(Box, _extends({
    sx: {
      width: 20,
      height: 20,
      display: 'flex',
      borderRadius: '50%',
      position: 'relative',
      alignItems: 'center',
      justifyContent: 'center',
      bgcolor: 'currentColor',
      transition: theme => theme.transitions.create('all', {
        duration: theme.transitions.duration.shortest
      }),
      ...sx
    }
  }, other), /*#__PURE__*/React.createElement(Iconify, {
    icon: 'eva:checkmark-fill'
  }));
}