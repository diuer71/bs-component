function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

import React from 'react';
import isString from 'lodash/isString';
import PropTypes from 'prop-types'; // @mui

import { Box, Typography, Link } from '@mui/material'; //

import Breadcrumbs from './Breadcrumbs'; // ----------------------------------------------------------------------

HeaderBreadcrumbs.propTypes = {
  links: PropTypes.array,
  action: PropTypes.node,
  heading: PropTypes.string.isRequired,
  moreLink: PropTypes.oneOfType([PropTypes.string, PropTypes.array]),
  sx: PropTypes.object
};
export default function HeaderBreadcrumbs({
  links,
  action,
  heading,
  moreLink = '' || [],
  sx,
  ...other
}) {
  return /*#__PURE__*/React.createElement(Box, {
    sx: {
      mb: 5,
      ...sx
    }
  }, /*#__PURE__*/React.createElement(Box, {
    sx: {
      display: 'flex',
      alignItems: 'center'
    }
  }, /*#__PURE__*/React.createElement(Box, {
    sx: {
      flexGrow: 1
    }
  }, /*#__PURE__*/React.createElement(Typography, {
    variant: "h4",
    gutterBottom: true
  }, heading), /*#__PURE__*/React.createElement(Breadcrumbs, _extends({
    links: links
  }, other))), action && /*#__PURE__*/React.createElement(Box, {
    sx: {
      flexShrink: 0
    }
  }, action)), /*#__PURE__*/React.createElement(Box, {
    sx: {
      mt: 2
    }
  }, isString(moreLink) ? /*#__PURE__*/React.createElement(Link, {
    href: moreLink,
    target: "_blank",
    rel: "noopener",
    variant: "body2"
  }, moreLink) : moreLink.map(href => /*#__PURE__*/React.createElement(Link, {
    noWrap: true,
    key: href,
    href: href,
    variant: "body2",
    target: "_blank",
    rel: "noopener",
    sx: {
      display: 'table'
    }
  }, href))));
}