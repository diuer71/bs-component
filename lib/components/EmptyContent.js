import React from 'react';
import PropTypes from 'prop-types'; // @mui

import { styled } from '@mui/material/styles';
import { Typography } from '@mui/material'; //

import Image from './Image'; // ----------------------------------------------------------------------

const RootStyle = styled('div')(({
  theme
}) => ({
  height: '100%',
  display: 'flex',
  textAlign: 'center',
  alignItems: 'center',
  flexDirection: 'column',
  justifyContent: 'center',
  padding: theme.spacing(8, 2)
})); // ----------------------------------------------------------------------

EmptyContent.propTypes = {
  title: PropTypes.string.isRequired,
  img: PropTypes.string,
  description: PropTypes.string
};
export default function EmptyContent({
  title,
  description,
  img,
  ...other
}) {
  return /*#__PURE__*/React.createElement(RootStyle, other, /*#__PURE__*/React.createElement(Image, {
    disabledEffect: true,
    visibleByDefault: true,
    alt: "empty content",
    src: img || 'https://minimal-assets-api.vercel.app/assets/illustrations/illustration_empty_content.svg',
    sx: {
      height: 240,
      mb: 3
    }
  }), /*#__PURE__*/React.createElement(Typography, {
    variant: "h5",
    gutterBottom: true
  }, title), description && /*#__PURE__*/React.createElement(Typography, {
    variant: "body2",
    sx: {
      color: 'text.secondary'
    }
  }, description));
}