function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

import React from 'react';
import PropTypes from 'prop-types';
import { Link as RouterLink } from 'react-router-dom'; // @mui

import { Box, Link, Typography, Breadcrumbs as MUIBreadcrumbs } from '@mui/material'; // ----------------------------------------------------------------------

Breadcrumbs.propTypes = {
  activeLast: PropTypes.bool,
  links: PropTypes.array.isRequired
};
export default function Breadcrumbs({
  links,
  activeLast = false,
  ...other
}) {
  const currentLink = links[links.length - 1].name;
  const listDefault = links.map(link => /*#__PURE__*/React.createElement(LinkItem, {
    key: link.name,
    link: link
  }));
  const listActiveLast = links.map(link => /*#__PURE__*/React.createElement("div", {
    key: link.name
  }, link.name !== currentLink ? /*#__PURE__*/React.createElement(LinkItem, {
    link: link
  }) : /*#__PURE__*/React.createElement(Typography, {
    variant: "body2",
    sx: {
      maxWidth: 260,
      overflow: 'hidden',
      whiteSpace: 'nowrap',
      color: 'text.disabled',
      textOverflow: 'ellipsis'
    }
  }, currentLink)));
  return /*#__PURE__*/React.createElement(MUIBreadcrumbs, _extends({
    separator: /*#__PURE__*/React.createElement(Box, {
      component: "span",
      sx: {
        width: 4,
        height: 4,
        borderRadius: '50%',
        bgcolor: 'text.disabled'
      }
    })
  }, other), activeLast ? listDefault : listActiveLast);
} // ----------------------------------------------------------------------

LinkItem.propTypes = {
  link: PropTypes.shape({
    href: PropTypes.string,
    icon: PropTypes.any,
    name: PropTypes.string
  })
};

function LinkItem({
  link
}) {
  const {
    href,
    name,
    icon
  } = link;
  return /*#__PURE__*/React.createElement(Link, {
    key: name,
    variant: "body2",
    component: RouterLink,
    to: href || '#',
    sx: {
      lineHeight: 2,
      display: 'flex',
      alignItems: 'center',
      color: 'text.primary',
      '& > div': {
        display: 'inherit'
      }
    }
  }, icon && /*#__PURE__*/React.createElement(Box, {
    sx: {
      mr: 1,
      '& svg': {
        width: 20,
        height: 20
      }
    }
  }, icon), name);
}