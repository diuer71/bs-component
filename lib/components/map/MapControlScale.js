function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

import React from 'react';
import PropTypes from 'prop-types';
import { ScaleControl } from 'react-map-gl'; // @mui

import { styled } from '@mui/material/styles'; // ----------------------------------------------------------------------

const RootStyle = styled('div')(({
  theme
}) => ({
  zIndex: 99,
  position: 'absolute',
  left: theme.spacing(1.5),
  bottom: theme.spacing(3.5),
  boxShadow: theme.customShadows.z8,
  '& .mapboxgl-ctrl': {
    border: 'none',
    borderRadius: 4,
    lineHeight: '14px',
    color: theme.palette.common.white,
    backgroundImage: `linear-gradient(to right, #8a2387, #e94057, #f27121)`
  }
})); // ----------------------------------------------------------------------

MapControlScale.propTypes = {
  sx: PropTypes.object
};
export default function MapControlScale({
  sx,
  ...other
}) {
  return /*#__PURE__*/React.createElement(RootStyle, {
    sx: sx
  }, /*#__PURE__*/React.createElement(ScaleControl, _extends({
    maxWidth: 100,
    unit: "imperial"
  }, other)));
}