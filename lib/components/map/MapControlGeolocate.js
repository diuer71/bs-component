function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

import React from 'react';
import { GeolocateControl } from 'react-map-gl'; // @mui

import { styled } from '@mui/material/styles'; // ----------------------------------------------------------------------

const GeolocateControlStyle = styled(GeolocateControl)(({
  theme
}) => ({
  zIndex: 99,
  borderRadius: 8,
  overflow: 'hidden',
  top: theme.spacing(6),
  left: theme.spacing(1.5),
  boxShadow: theme.customShadows.z8
})); // ----------------------------------------------------------------------

export default function MapControlGeolocate({ ...other
}) {
  return /*#__PURE__*/React.createElement(GeolocateControlStyle, _extends({
    positionOptions: {
      enableHighAccuracy: true
    },
    trackUserLocation: true
  }, other));
}