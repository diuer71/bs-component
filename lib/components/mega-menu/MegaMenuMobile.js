function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

import React from 'react';
import PropTypes from 'prop-types';
import { useState, useEffect } from 'react';
import { NavLink as RouterLink, useLocation } from 'react-router-dom'; // @mui

import { Box, List, Stack, Drawer, Button, Divider, Typography, IconButton, ListItemText, ListItemIcon, ListItemButton } from '@mui/material'; // config

import { NAVBAR, ICON } from '../../config'; //

import Logo from '../Logo';
import Iconify from '../Iconify';
import Scrollbar from '../Scrollbar'; // ----------------------------------------------------------------------

MegaMenuMobile.propTypes = {
  navConfig: PropTypes.array
};
export default function MegaMenuMobile({
  navConfig
}) {
  const {
    pathname
  } = useLocation();
  const [openDrawer, setOpenDrawer] = useState(false);
  useEffect(() => {
    if (openDrawer) {
      handleDrawerClose();
    } // eslint-disable-next-line react-hooks/exhaustive-deps

  }, [pathname]);

  const handleDrawerOpen = () => {
    setOpenDrawer(true);
  };

  const handleDrawerClose = () => {
    setOpenDrawer(false);
  };

  return /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement(Button, {
    variant: "contained",
    onClick: handleDrawerOpen,
    startIcon: /*#__PURE__*/React.createElement(Iconify, {
      icon: 'eva:menu-2-fill'
    })
  }, "Menu Mobile"), /*#__PURE__*/React.createElement(Drawer, {
    open: openDrawer,
    onClose: handleDrawerClose,
    ModalProps: {
      keepMounted: true
    },
    PaperProps: {
      sx: {
        pb: 5,
        width: NAVBAR.DASHBOARD_WIDTH
      }
    }
  }, /*#__PURE__*/React.createElement(Scrollbar, null, /*#__PURE__*/React.createElement(Logo, {
    sx: {
      mx: 2.5,
      my: 3
    }
  }), /*#__PURE__*/React.createElement(Typography, {
    variant: "h6",
    sx: {
      px: 2,
      mb: 2,
      display: 'flex',
      alignItems: 'center'
    }
  }, /*#__PURE__*/React.createElement(Box, {
    component: Iconify,
    icon: 'eva:list-fill',
    sx: {
      mr: 1,
      width: 24,
      height: 24
    }
  }), " Categories"), navConfig.map(parent => /*#__PURE__*/React.createElement(SubMenu, {
    key: parent.title,
    parent: parent,
    pathname: pathname
  })))));
} // ----------------------------------------------------------------------

ParentItem.propTypes = {
  hasSub: PropTypes.bool,
  icon: PropTypes.any,
  title: PropTypes.string
};

function ParentItem({
  icon,
  title,
  hasSub,
  ...other
}) {
  return /*#__PURE__*/React.createElement(ListItemButton, _extends({
    sx: {
      textTransform: 'capitalize',
      height: 44
    }
  }, other), /*#__PURE__*/React.createElement(ListItemIcon, {
    sx: {
      width: 22,
      height: 22
    }
  }, icon), /*#__PURE__*/React.createElement(ListItemText, {
    primaryTypographyProps: {
      typography: 'body2'
    }
  }, title), hasSub && /*#__PURE__*/React.createElement(Box, {
    component: Iconify,
    icon: 'eva:arrow-ios-forward-fill'
  }));
} // ----------------------------------------------------------------------


SubMenu.propTypes = {
  parent: PropTypes.shape({
    title: PropTypes.string,
    icon: PropTypes.any,
    path: PropTypes.string,
    children: PropTypes.array
  }),
  pathname: PropTypes.string
};

function SubMenu({
  parent,
  pathname
}) {
  const [open, setOpen] = useState(false);
  const {
    title,
    icon,
    path,
    children
  } = parent;
  useEffect(() => {
    if (open) {
      handleClose();
    } // eslint-disable-next-line react-hooks/exhaustive-deps

  }, [pathname]);

  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  if (children) {
    return /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement(ParentItem, {
      title: title,
      icon: icon,
      onClick: handleOpen,
      hasSub: true
    }), /*#__PURE__*/React.createElement(Drawer, {
      open: open,
      onClose: handleClose,
      ModalProps: {
        keepMounted: true
      },
      PaperProps: {
        sx: {
          width: NAVBAR.DASHBOARD_WIDTH - 12
        }
      }
    }, /*#__PURE__*/React.createElement(Stack, {
      direction: "row",
      alignItems: "center",
      px: 1,
      py: 1.5
    }, /*#__PURE__*/React.createElement(IconButton, {
      onClick: handleClose
    }, /*#__PURE__*/React.createElement(Iconify, {
      icon: 'eva:arrow-ios-back-fill',
      width: 20,
      height: 20
    })), /*#__PURE__*/React.createElement(Typography, {
      noWrap: true,
      variant: "subtitle1",
      sx: {
        ml: 1,
        textTransform: 'capitalize'
      }
    }, title)), /*#__PURE__*/React.createElement(Divider, null), /*#__PURE__*/React.createElement(Scrollbar, null, /*#__PURE__*/React.createElement(Stack, {
      spacing: 5,
      py: 3
    }, children.map(list => {
      const {
        subheader,
        items
      } = list;
      return /*#__PURE__*/React.createElement(List, {
        key: subheader,
        disablePadding: true
      }, /*#__PURE__*/React.createElement(Typography, {
        component: "div",
        variant: "overline",
        sx: {
          px: 2.5,
          mb: 1,
          color: 'text.secondary'
        },
        noWrap: true
      }, subheader), items.map(link => /*#__PURE__*/React.createElement(ListItemButton, {
        key: link.title,
        component: RouterLink,
        to: link.path,
        sx: {
          px: 1.5
        }
      }, /*#__PURE__*/React.createElement(ListItemIcon, {
        sx: {
          mr: 0.5,
          width: ICON.NAVBAR_ITEM,
          height: ICON.NAVBAR_ITEM,
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'center'
        }
      }, /*#__PURE__*/React.createElement(Box, {
        sx: {
          width: 4,
          height: 4,
          bgcolor: 'currentColor',
          borderRadius: '50%'
        }
      })), /*#__PURE__*/React.createElement(ListItemText, {
        primary: link.title,
        primaryTypographyProps: {
          typography: 'body2',
          noWrap: true
        }
      }))));
    })))));
  }

  return /*#__PURE__*/React.createElement(ParentItem, {
    component: RouterLink,
    title: title,
    icon: icon,
    to: path
  });
}