import React from 'react';
import PropTypes from 'prop-types';
import { Link as RouterLink } from 'react-router-dom'; // @mui

import { Link, Typography, Box } from '@mui/material'; // ----------------------------------------------------------------------

MenuHotProducts.propTypes = {
  tags: PropTypes.array.isRequired
};
export default function MenuHotProducts({
  tags,
  ...other
}) {
  return /*#__PURE__*/React.createElement(Box, other, /*#__PURE__*/React.createElement(Typography, {
    variant: "caption",
    fontWeight: "fontWeightBold"
  }, "Hot Products:"), "\xA0", tags.map((tag, index) => /*#__PURE__*/React.createElement(Link, {
    component: RouterLink,
    key: tag.name,
    to: tag.path,
    underline: "none",
    variant: "caption",
    sx: {
      color: 'text.secondary',
      transition: theme => theme.transitions.create('all'),
      '&:hover': {
        color: 'primary.main'
      }
    }
  }, index === 0 ? tag.name : `, ${tag.name} `)));
}