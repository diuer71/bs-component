function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

import React from 'react';
import PropTypes from 'prop-types';
import { useRef } from 'react';
import Slider from 'react-slick';
import { NavLink as RouterLink } from 'react-router-dom'; // @mui

import { useTheme } from '@mui/material/styles';
import { Box, Link } from '@mui/material'; //

import Image from '../Image';
import TextMaxLine from '../TextMaxLine';
import { CarouselDots, CarouselArrows } from '../carousel'; // ----------------------------------------------------------------------

MenuCarousel.propTypes = {
  numberShow: PropTypes.number,
  products: PropTypes.array,
  sx: PropTypes.object
};
export default function MenuCarousel({
  products,
  numberShow,
  sx
}) {
  const theme = useTheme();
  const carouselRef = useRef(null);
  const settings = {
    dots: true,
    arrows: false,
    slidesToShow: numberShow,
    slidesToScroll: numberShow,
    rtl: Boolean(theme.direction === 'rtl'),
    ...CarouselDots()
  };

  const handlePrevious = () => {
    carouselRef.current?.slickPrev();
  };

  const handleNext = () => {
    carouselRef.current?.slickNext();
  };

  return /*#__PURE__*/React.createElement(Box, {
    sx: {
      position: 'relative',
      ...sx
    }
  }, /*#__PURE__*/React.createElement(CarouselArrows, {
    filled: true,
    onNext: handleNext,
    onPrevious: handlePrevious,
    sx: {
      '& .arrow button': {
        p: 0,
        width: 24,
        height: 24,
        top: -20
      }
    }
  }, /*#__PURE__*/React.createElement(Slider, _extends({
    ref: carouselRef
  }, settings), products.map(product => /*#__PURE__*/React.createElement(Box, {
    key: product.name,
    sx: {
      px: 1,
      textAlign: 'center'
    }
  }, /*#__PURE__*/React.createElement(Link, {
    component: RouterLink,
    color: "inherit",
    underline: "none",
    to: product.path,
    sx: {
      display: 'block',
      transition: theme => theme.transitions.create('all'),
      '&:hover': {
        color: 'primary.main'
      }
    }
  }, /*#__PURE__*/React.createElement(Image, {
    src: product.image,
    ratio: "1/1",
    disabledEffect: true,
    sx: {
      borderRadius: 1,
      mb: 1
    }
  }), /*#__PURE__*/React.createElement(TextMaxLine, {
    line: 2,
    variant: "caption",
    sx: {
      fontWeight: 'fontWeightMedium'
    }
  }, product.name)))))));
}