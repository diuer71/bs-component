function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

import React from 'react';
import PropTypes from 'prop-types';
import { m, AnimatePresence } from 'framer-motion'; // @mui

import { Dialog, Box, Paper } from '@mui/material'; //

import { varFade } from './variants'; // ----------------------------------------------------------------------

DialogAnimate.propTypes = {
  children: PropTypes.node.isRequired,
  onClose: PropTypes.func,
  open: PropTypes.bool.isRequired,
  sx: PropTypes.object,
  variants: PropTypes.object
};
export default function DialogAnimate({
  open = false,
  variants,
  onClose,
  children,
  sx,
  ...other
}) {
  return /*#__PURE__*/React.createElement(AnimatePresence, null, open && /*#__PURE__*/React.createElement(Dialog, _extends({
    fullWidth: true,
    maxWidth: "xs",
    open: open,
    onClose: onClose,
    PaperComponent: props => /*#__PURE__*/React.createElement(Box, _extends({
      component: m.div
    }, variants || varFade({
      distance: 120,
      durationIn: 0.32,
      durationOut: 0.24,
      easeIn: 'easeInOut'
    }).inUp, {
      sx: {
        width: '100%',
        height: '100%',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center'
      }
    }), /*#__PURE__*/React.createElement(Box, {
      onClick: onClose,
      sx: {
        width: '100%',
        height: '100%',
        position: 'fixed'
      }
    }), /*#__PURE__*/React.createElement(Paper, _extends({
      sx: sx
    }, props), props.children))
  }, other), children));
}