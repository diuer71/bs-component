function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

import PropTypes from 'prop-types';
import { m } from 'framer-motion'; // @mui

import { Box } from '@mui/material'; //

import { varFade } from './variants'; // ----------------------------------------------------------------------

TextAnimate.propTypes = {
  text: PropTypes.string.isRequired,
  variants: PropTypes.object,
  sx: PropTypes.object
};
export default function TextAnimate({
  text,
  variants,
  sx,
  ...other
}) {
  return /*#__PURE__*/React.createElement(Box, _extends({
    component: m.h1,
    sx: {
      typography: 'h1',
      overflow: 'hidden',
      display: 'inline-flex',
      ...sx
    }
  }, other), text.split('').map((letter, index) => /*#__PURE__*/React.createElement(m.span, {
    key: index,
    variants: variants || varFade().inUp
  }, letter)));
}