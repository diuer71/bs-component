function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

import React from 'react';
import PropTypes from 'prop-types';
import { m } from 'framer-motion';
import { forwardRef } from 'react'; // @mui

import { Box, IconButton } from '@mui/material'; // ----------------------------------------------------------------------

const IconButtonAnimate = /*#__PURE__*/forwardRef(({
  children,
  size = 'medium',
  ...other
}, ref) => /*#__PURE__*/React.createElement(AnimateWrap, {
  size: size
}, /*#__PURE__*/React.createElement(IconButton, _extends({
  size: size,
  ref: ref
}, other), children)));
IconButtonAnimate.propTypes = {
  children: PropTypes.node.isRequired,
  color: PropTypes.oneOf(['inherit', 'default', 'primary', 'secondary', 'info', 'success', 'warning', 'error']),
  size: PropTypes.oneOf(['small', 'medium', 'large'])
};
export default IconButtonAnimate; // ----------------------------------------------------------------------

const varSmall = {
  hover: {
    scale: 1.1
  },
  tap: {
    scale: 0.95
  }
};
const varMedium = {
  hover: {
    scale: 1.09
  },
  tap: {
    scale: 0.97
  }
};
const varLarge = {
  hover: {
    scale: 1.08
  },
  tap: {
    scale: 0.99
  }
};
AnimateWrap.propTypes = {
  children: PropTypes.node.isRequired,
  size: PropTypes.oneOf(['small', 'medium', 'large'])
};

function AnimateWrap({
  size,
  children
}) {
  const isSmall = size === 'small';
  const isLarge = size === 'large';
  return /*#__PURE__*/React.createElement(Box, {
    component: m.div,
    whileTap: "tap",
    whileHover: "hover",
    variants: isSmall && varSmall || isLarge && varLarge || varMedium,
    sx: {
      display: 'inline-flex'
    }
  }, children);
}