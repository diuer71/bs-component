function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

import PropTypes from 'prop-types';
import { m } from 'framer-motion'; // @mui

import { Box } from '@mui/material'; //

import { varContainer } from './variants'; // ----------------------------------------------------------------------

MotionContainer.propTypes = {
  action: PropTypes.bool,
  animate: PropTypes.bool,
  children: PropTypes.node.isRequired
};
export default function MotionContainer({
  animate,
  action = false,
  children,
  ...other
}) {
  if (action) {
    return /*#__PURE__*/React.createElement(Box, _extends({
      component: m.div,
      initial: false,
      animate: animate ? 'animate' : 'exit',
      variants: varContainer()
    }, other), children);
  }

  return /*#__PURE__*/React.createElement(Box, _extends({
    component: m.div,
    initial: "initial",
    animate: "animate",
    exit: "exit",
    variants: varContainer()
  }, other), children);
}