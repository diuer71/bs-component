function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

import React from 'react';
import PropTypes from 'prop-types'; // @mui

import { Stack } from '@mui/material'; // ----------------------------------------------------------------------

TextIconLabel.propTypes = {
  endIcon: PropTypes.bool,
  icon: PropTypes.any,
  sx: PropTypes.object,
  value: PropTypes.any
};
export default function TextIconLabel({
  icon,
  value,
  endIcon = false,
  sx,
  ...other
}) {
  return /*#__PURE__*/React.createElement(Stack, _extends({
    direction: "row",
    alignItems: "center",
    sx: {
      typography: 'body2',
      ...sx
    }
  }, other), !endIcon && icon, value, endIcon && icon);
}