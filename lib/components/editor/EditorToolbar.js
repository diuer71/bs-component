import React from 'react';
import PropTypes from 'prop-types';
import { Quill } from 'react-quill'; // components

import Iconify from '../Iconify'; //

import EditorToolbarStyle from './EditorToolbarStyle'; // ----------------------------------------------------------------------

const FONT_FAMILY = ['Arial', 'Tahoma', 'Georgia', 'Impact', 'Verdana'];
const FONT_SIZE = ['8px', '9px', '10px', '12px', '14px', '16px', '20px', '24px', '32px', '42px', '54px', '68px', '84px', '98px'];
const HEADINGS = ['Heading 1', 'Heading 2', 'Heading 3', 'Heading 4', 'Heading 5', 'Heading 6'];
export function undoChange() {
  this.quill.history.undo();
}
export function redoChange() {
  this.quill.history.redo();
}
const Size = Quill.import('attributors/style/size');
Size.whitelist = FONT_SIZE;
Quill.register(Size, true);
const Font = Quill.import('attributors/style/font');
Font.whitelist = FONT_FAMILY;
Quill.register(Font, true);
export const formats = ['align', 'background', 'blockquote', 'bold', 'bullet', 'code', 'code-block', 'color', 'direction', 'font', 'formula', 'header', 'image', 'indent', 'italic', 'link', 'list', 'script', 'size', 'strike', 'table', 'underline', 'video'];
EditorToolbar.propTypes = {
  id: PropTypes.string.isRequired,
  isSimple: PropTypes.bool
};
export default function EditorToolbar({
  id,
  isSimple,
  ...other
}) {
  return /*#__PURE__*/React.createElement(EditorToolbarStyle, other, /*#__PURE__*/React.createElement("div", {
    id: id
  }, /*#__PURE__*/React.createElement("div", {
    className: "ql-formats"
  }, !isSimple && /*#__PURE__*/React.createElement("select", {
    className: "ql-font",
    defaultValue: ""
  }, /*#__PURE__*/React.createElement("option", {
    value: ""
  }, "Font"), FONT_FAMILY.map(font => /*#__PURE__*/React.createElement("option", {
    key: font,
    value: font
  }, font))), !isSimple && /*#__PURE__*/React.createElement("select", {
    className: "ql-size",
    defaultValue: "16px"
  }, FONT_SIZE.map(size => /*#__PURE__*/React.createElement("option", {
    key: size,
    value: size
  }, size))), /*#__PURE__*/React.createElement("select", {
    className: "ql-header",
    defaultValue: ""
  }, HEADINGS.map((heading, index) => /*#__PURE__*/React.createElement("option", {
    key: heading,
    value: index + 1
  }, heading)), /*#__PURE__*/React.createElement("option", {
    value: ""
  }, "Normal"))), /*#__PURE__*/React.createElement("div", {
    className: "ql-formats"
  }, /*#__PURE__*/React.createElement("button", {
    type: "button",
    className: "ql-bold"
  }), /*#__PURE__*/React.createElement("button", {
    type: "button",
    className: "ql-italic"
  }), /*#__PURE__*/React.createElement("button", {
    type: "button",
    className: "ql-underline"
  }), /*#__PURE__*/React.createElement("button", {
    type: "button",
    className: "ql-strike"
  })), !isSimple && /*#__PURE__*/React.createElement("div", {
    className: "ql-formats"
  }, /*#__PURE__*/React.createElement("select", {
    className: "ql-color"
  }), /*#__PURE__*/React.createElement("select", {
    className: "ql-background"
  })), /*#__PURE__*/React.createElement("div", {
    className: "ql-formats"
  }, /*#__PURE__*/React.createElement("button", {
    type: "button",
    className: "ql-list",
    value: "ordered"
  }), /*#__PURE__*/React.createElement("button", {
    type: "button",
    className: "ql-list",
    value: "bullet"
  }), !isSimple && /*#__PURE__*/React.createElement("button", {
    type: "button",
    className: "ql-indent",
    value: "-1"
  }), !isSimple && /*#__PURE__*/React.createElement("button", {
    type: "button",
    className: "ql-indent",
    value: "+1"
  })), !isSimple && /*#__PURE__*/React.createElement("div", {
    className: "ql-formats"
  }, /*#__PURE__*/React.createElement("button", {
    type: "button",
    className: "ql-script",
    value: "super"
  }), /*#__PURE__*/React.createElement("button", {
    type: "button",
    className: "ql-script",
    value: "sub"
  })), !isSimple && /*#__PURE__*/React.createElement("div", {
    className: "ql-formats"
  }, /*#__PURE__*/React.createElement("button", {
    type: "button",
    className: "ql-code-block"
  }), /*#__PURE__*/React.createElement("button", {
    type: "button",
    className: "ql-blockquote"
  })), /*#__PURE__*/React.createElement("div", {
    className: "ql-formats"
  }, /*#__PURE__*/React.createElement("button", {
    type: "button",
    className: "ql-direction",
    value: "rtl"
  }), /*#__PURE__*/React.createElement("select", {
    className: "ql-align"
  })), /*#__PURE__*/React.createElement("div", {
    className: "ql-formats"
  }, /*#__PURE__*/React.createElement("button", {
    type: "button",
    className: "ql-link"
  }), /*#__PURE__*/React.createElement("button", {
    type: "button",
    className: "ql-image"
  }), /*#__PURE__*/React.createElement("button", {
    type: "button",
    className: "ql-video"
  })), /*#__PURE__*/React.createElement("div", {
    className: "ql-formats"
  }, !isSimple && /*#__PURE__*/React.createElement("button", {
    type: "button",
    className: "ql-formula"
  }), /*#__PURE__*/React.createElement("button", {
    type: "button",
    className: "ql-clean"
  })), !isSimple && /*#__PURE__*/React.createElement("div", {
    className: "ql-formats"
  }, /*#__PURE__*/React.createElement("button", {
    type: "button",
    className: "ql-undo"
  }, /*#__PURE__*/React.createElement(Iconify, {
    icon: 'ic:round-undo',
    width: 18,
    height: 18
  })), /*#__PURE__*/React.createElement("button", {
    type: "button",
    className: "ql-redo"
  }, /*#__PURE__*/React.createElement(Iconify, {
    icon: 'ic:round-redo',
    width: 18,
    height: 18
  })))));
}