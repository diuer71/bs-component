function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

import React from 'react';
import { useState } from 'react';
import PropTypes from 'prop-types';
import { useSnackbar } from 'notistack';
import { CopyToClipboard } from 'react-copy-to-clipboard'; // @mui

import { Tooltip, TextField, IconButton, InputAdornment } from '@mui/material'; //

import Iconify from './Iconify'; // ----------------------------------------------------------------------

CopyClipboard.propTypes = {
  value: PropTypes.string
};
export default function CopyClipboard({
  value,
  ...other
}) {
  const {
    enqueueSnackbar
  } = useSnackbar();
  const [state, setState] = useState({
    value,
    copied: false
  });

  const handleChange = event => {
    setState({
      value: event.target.value,
      copied: false
    });
  };

  const onCopy = () => {
    setState({ ...state,
      copied: true
    });

    if (state.value) {
      enqueueSnackbar('Copied!');
    }
  };

  return /*#__PURE__*/React.createElement(TextField, _extends({
    fullWidth: true,
    value: state.value,
    onChange: handleChange,
    InputProps: {
      endAdornment: /*#__PURE__*/React.createElement(InputAdornment, {
        position: "end"
      }, /*#__PURE__*/React.createElement(CopyToClipboard, {
        text: state.value,
        onCopy: onCopy
      }, /*#__PURE__*/React.createElement(Tooltip, {
        title: "Copy"
      }, /*#__PURE__*/React.createElement(IconButton, null, /*#__PURE__*/React.createElement(Iconify, {
        icon: 'eva:copy-fill',
        width: 24,
        height: 24
      })))))
    }
  }, other));
}