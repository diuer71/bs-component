import React from 'react';
import PropTypes from 'prop-types';
import { Paper, Typography } from '@mui/material'; // ----------------------------------------------------------------------

SearchNotFound.propTypes = {
  searchQuery: PropTypes.string
};
export default function SearchNotFound({
  searchQuery = '',
  ...other
}) {
  return searchQuery ? /*#__PURE__*/React.createElement(Paper, other, /*#__PURE__*/React.createElement(Typography, {
    gutterBottom: true,
    align: "center",
    variant: "subtitle1"
  }, "Not found"), /*#__PURE__*/React.createElement(Typography, {
    variant: "body2",
    align: "center"
  }, "No results found for \xA0", /*#__PURE__*/React.createElement("strong", null, "\"", searchQuery, "\""), ". Try checking for typos or using complete words.")) : /*#__PURE__*/React.createElement(Typography, {
    variant: "body2"
  }, " Please enter keywords");
}