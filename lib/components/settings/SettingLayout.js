import React from 'react';
import PropTypes from 'prop-types'; // @mui

import { styled, alpha } from '@mui/material/styles';
import { Grid, RadioGroup, CardActionArea, Box, Stack } from '@mui/material'; // hooks

import useSettings from '../../hooks/useSettings'; //

import { BoxMask } from '.'; // ----------------------------------------------------------------------

const BoxStyle = styled(CardActionArea)(({
  theme
}) => ({
  display: 'flex',
  flexDirection: 'column',
  padding: theme.spacing(1.5),
  color: theme.palette.text.disabled,
  border: `solid 1px ${theme.palette.grey[500_12]}`,
  borderRadius: Number(theme.shape.borderRadius) * 1.25
})); // ----------------------------------------------------------------------

export default function SettingLayout() {
  const {
    themeLayout,
    onChangeLayout
  } = useSettings();
  return /*#__PURE__*/React.createElement(RadioGroup, {
    name: "themeLayout",
    value: themeLayout,
    onChange: onChangeLayout
  }, /*#__PURE__*/React.createElement(Grid, {
    dir: "ltr",
    container: true,
    spacing: 2.5
  }, ['horizontal', 'vertical'].map(layout => {
    const isSelected = themeLayout === layout;
    const isVertical = layout === 'vertical';
    return /*#__PURE__*/React.createElement(Grid, {
      key: layout,
      item: true,
      xs: 6
    }, /*#__PURE__*/React.createElement(BoxStyle, {
      sx: { ...(isSelected && {
          color: 'primary.main',
          boxShadow: theme => theme.customShadows.z20
        })
      }
    }, isVertical ? /*#__PURE__*/React.createElement(VerticalBox, {
      isSelected: isSelected
    }) : /*#__PURE__*/React.createElement(HorizontalBox, {
      isSelected: isSelected
    }), /*#__PURE__*/React.createElement(BoxMask, {
      value: layout
    })));
  })));
} // ----------------------------------------------------------------------

const style = {
  width: 1,
  height: 32,
  borderRadius: 0.5
};
VerticalBox.propTypes = {
  isSelected: PropTypes.bool
};

function VerticalBox({
  isSelected
}) {
  return /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement(Box, {
    sx: { ...style,
      mb: 0.75,
      height: 12,
      bgcolor: theme => alpha(theme.palette.text.disabled, 0.72),
      ...(isSelected && {
        bgcolor: theme => alpha(theme.palette.primary.main, 0.72)
      })
    }
  }), /*#__PURE__*/React.createElement(Box, {
    sx: { ...style,
      border: theme => `dashed 1px ${theme.palette.divider}`,
      bgcolor: theme => alpha(theme.palette.text.disabled, 0.08),
      ...(isSelected && {
        border: theme => `dashed 1px ${theme.palette.primary.main}`,
        bgcolor: theme => alpha(theme.palette.primary.main, 0.16)
      })
    }
  }));
}

HorizontalBox.propTypes = {
  isSelected: PropTypes.bool
};

function HorizontalBox({
  isSelected
}) {
  return /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement(Box, {
    sx: { ...style,
      mb: 0.75,
      height: 12,
      bgcolor: theme => alpha(theme.palette.text.disabled, 0.72),
      ...(isSelected && {
        bgcolor: theme => alpha(theme.palette.primary.main, 0.72)
      })
    }
  }), /*#__PURE__*/React.createElement(Stack, {
    width: 1,
    direction: "row",
    justifyContent: "space-between"
  }, /*#__PURE__*/React.createElement(Box, {
    sx: { ...style,
      width: 20,
      bgcolor: theme => alpha(theme.palette.text.disabled, 0.32),
      ...(isSelected && {
        bgcolor: theme => alpha(theme.palette.primary.main, 0.32)
      })
    }
  }), /*#__PURE__*/React.createElement(Box, {
    sx: { ...style,
      width: `calc(100% - 26px)`,
      border: theme => `dashed 1px ${theme.palette.divider}`,
      bgcolor: theme => alpha(theme.palette.text.disabled, 0.08),
      ...(isSelected && {
        border: theme => `dashed 1px ${theme.palette.primary.main}`,
        bgcolor: theme => alpha(theme.palette.primary.main, 0.16)
      })
    }
  })));
}