import React from 'react'; // @mui

import { styled } from '@mui/material/styles';
import { Grid, RadioGroup, CardActionArea } from '@mui/material'; // hooks

import useSettings from '../../hooks/useSettings'; //

import Iconify from '../Iconify';
import { BoxMask } from '.'; // ----------------------------------------------------------------------

const BoxStyle = styled(CardActionArea)(({
  theme
}) => ({
  height: 72,
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
  color: theme.palette.text.disabled,
  border: `solid 1px ${theme.palette.grey[500_12]}`,
  borderRadius: Number(theme.shape.borderRadius) * 1.25
})); // ----------------------------------------------------------------------

export default function SettingDirection() {
  const {
    themeDirection,
    onChangeDirection
  } = useSettings();
  return /*#__PURE__*/React.createElement(RadioGroup, {
    name: "themeDirection",
    value: themeDirection,
    onChange: onChangeDirection
  }, /*#__PURE__*/React.createElement(Grid, {
    dir: "ltr",
    container: true,
    spacing: 2.5
  }, ['ltr', 'rtl'].map((direction, index) => {
    const isSelected = themeDirection === direction;
    return /*#__PURE__*/React.createElement(Grid, {
      key: direction,
      item: true,
      xs: 6
    }, /*#__PURE__*/React.createElement(BoxStyle, {
      sx: { ...(isSelected && {
          color: 'primary.main',
          boxShadow: theme => theme.customShadows.z20
        })
      }
    }, /*#__PURE__*/React.createElement(Iconify, {
      icon: index === 0 ? 'ph:align-left-duotone' : 'ph:align-right-duotone',
      width: 28,
      height: 28
    }), /*#__PURE__*/React.createElement(BoxMask, {
      value: direction
    })));
  })));
}