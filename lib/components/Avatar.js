function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

import React from 'react';
import PropTypes from 'prop-types';
import { forwardRef } from 'react';
import { useTheme } from '@mui/material/styles';
import { Avatar as MUIAvatar } from '@mui/material'; // ----------------------------------------------------------------------

const Avatar = /*#__PURE__*/forwardRef(({
  color = 'default',
  children,
  sx,
  ...other
}, ref) => {
  const theme = useTheme();

  if (color === 'default') {
    return /*#__PURE__*/React.createElement(MUIAvatar, _extends({
      ref: ref,
      sx: sx
    }, other), children);
  }

  return /*#__PURE__*/React.createElement(MUIAvatar, _extends({
    ref: ref,
    sx: {
      fontWeight: theme.typography.fontWeightMedium,
      color: theme.palette[color].contrastText,
      backgroundColor: theme.palette[color].main,
      ...sx
    }
  }, other), children);
});
Avatar.propTypes = {
  children: PropTypes.node,
  sx: PropTypes.object,
  color: PropTypes.oneOf(['default', 'primary', 'secondary', 'info', 'success', 'warning', 'error'])
};
export default Avatar;