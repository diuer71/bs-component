function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

import React from 'react'; // hooks

import useAuth from '../hooks/useAuth'; // utils

import createAvatar from '../utils/createAvatar'; //

import Avatar from './Avatar'; // ----------------------------------------------------------------------

export default function MyAvatar({ ...other
}) {
  const {
    user
  } = useAuth();
  return /*#__PURE__*/React.createElement(Avatar, _extends({
    src: user?.photoURL,
    alt: user?.displayName,
    color: user?.photoURL ? 'default' : createAvatar(user?.displayName).color
  }, other), createAvatar(user?.displayName).name);
}