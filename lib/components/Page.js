function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

import React from 'react';
import PropTypes from 'prop-types';
import { Helmet } from 'react-helmet-async';
import { forwardRef } from 'react'; // @mui

import { Box } from '@mui/material'; // ----------------------------------------------------------------------

const Page = /*#__PURE__*/forwardRef(({
  children,
  title = '',
  meta,
  ...other
}, ref) => /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement(Helmet, null, /*#__PURE__*/React.createElement("title", null, `${title} | Minimal-UI`), meta), /*#__PURE__*/React.createElement(Box, _extends({
  ref: ref
}, other), children)));
Page.propTypes = {
  children: PropTypes.node.isRequired,
  title: PropTypes.string,
  meta: PropTypes.node
};
export default Page;