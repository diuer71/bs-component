import { Suspense, lazy } from 'react';
import { Navigate, useRoutes, useLocation } from 'react-router-dom'; // layouts

import MainLayout from '../layouts/main';
import DashboardLayout from '../layouts/dashboard';
import LogoOnlyLayout from '../layouts/LogoOnlyLayout'; // guards

import GuestGuard from '../guards/GuestGuard';
import AuthGuard from '../guards/AuthGuard'; // import RoleBasedGuard from '../guards/RoleBasedGuard';
// config

import { PATH_AFTER_LOGIN } from '../config'; // components

import LoadingScreen from '../components/LoadingScreen'; // ----------------------------------------------------------------------

const Loadable = Component => props => {
  // eslint-disable-next-line react-hooks/rules-of-hooks
  const {
    pathname
  } = useLocation();
  return /*#__PURE__*/React.createElement(Suspense, {
    fallback: /*#__PURE__*/React.createElement(LoadingScreen, {
      isDashboard: pathname.includes('/dashboard')
    })
  }, /*#__PURE__*/React.createElement(Component, props));
};

export default function Router() {
  return useRoutes([{
    path: 'auth',
    children: [{
      path: 'login',
      element: /*#__PURE__*/React.createElement(GuestGuard, null, /*#__PURE__*/React.createElement(Login, null))
    }, {
      path: 'register',
      element: /*#__PURE__*/React.createElement(GuestGuard, null, /*#__PURE__*/React.createElement(Register, null))
    }, {
      path: 'login-unprotected',
      element: /*#__PURE__*/React.createElement(Login, null)
    }, {
      path: 'register-unprotected',
      element: /*#__PURE__*/React.createElement(Register, null)
    }, {
      path: 'reset-password',
      element: /*#__PURE__*/React.createElement(ResetPassword, null)
    }, {
      path: 'verify',
      element: /*#__PURE__*/React.createElement(VerifyCode, null)
    }]
  }, // Dashboard Routes
  {
    path: 'dashboard',
    element: /*#__PURE__*/React.createElement(AuthGuard, null, /*#__PURE__*/React.createElement(DashboardLayout, null)),
    children: [{
      element: /*#__PURE__*/React.createElement(Navigate, {
        to: PATH_AFTER_LOGIN,
        replace: true
      }),
      index: true
    }, {
      path: 'app',
      element: /*#__PURE__*/React.createElement(GeneralApp, null)
    }, {
      path: 'ecommerce',
      element: /*#__PURE__*/React.createElement(GeneralEcommerce, null)
    }, {
      path: 'analytics',
      element: /*#__PURE__*/React.createElement(GeneralAnalytics, null)
    }, {
      path: 'banking',
      element: /*#__PURE__*/React.createElement(GeneralBanking, null)
    }, {
      path: 'booking',
      element: /*#__PURE__*/React.createElement(GeneralBooking, null)
    }, {
      path: 'e-commerce',
      children: [{
        element: /*#__PURE__*/React.createElement(Navigate, {
          to: "/dashboard/e-commerce/shop",
          replace: true
        }),
        index: true
      }, {
        path: 'shop',
        element: /*#__PURE__*/React.createElement(EcommerceShop, null)
      }, {
        path: 'product/:name',
        element: /*#__PURE__*/React.createElement(EcommerceProductDetails, null)
      }, {
        path: 'list',
        element: /*#__PURE__*/React.createElement(EcommerceProductList, null)
      }, {
        path: 'product/new',
        element: /*#__PURE__*/React.createElement(EcommerceProductCreate, null)
      }, {
        path: 'product/:name/edit',
        element: /*#__PURE__*/React.createElement(EcommerceProductCreate, null)
      }, {
        path: 'checkout',
        element: /*#__PURE__*/React.createElement(EcommerceCheckout, null)
      }, {
        path: 'invoice',
        element: /*#__PURE__*/React.createElement(EcommerceInvoice, null)
      }]
    }, {
      path: 'user',
      children: [{
        element: /*#__PURE__*/React.createElement(Navigate, {
          to: "/dashboard/user/profile",
          replace: true
        }),
        index: true
      }, {
        path: 'profile',
        element: /*#__PURE__*/React.createElement(UserProfile, null)
      }, {
        path: 'cards',
        element: /*#__PURE__*/React.createElement(UserCards, null)
      }, {
        path: 'list',
        element: /*#__PURE__*/React.createElement(UserList, null)
      }, {
        path: 'new',
        element: /*#__PURE__*/React.createElement(UserCreate, null)
      }, {
        path: ':name/edit',
        element: /*#__PURE__*/React.createElement(UserCreate, null)
      }, {
        path: 'account',
        element: /*#__PURE__*/React.createElement(UserAccount, null)
      }]
    }, {
      path: 'blog',
      children: [{
        element: /*#__PURE__*/React.createElement(Navigate, {
          to: "/dashboard/blog/posts",
          replace: true
        }),
        index: true
      }, {
        path: 'posts',
        element: /*#__PURE__*/React.createElement(BlogPosts, null)
      }, {
        path: 'post/:title',
        element: /*#__PURE__*/React.createElement(BlogPost, null)
      }, {
        path: 'new-post',
        element: /*#__PURE__*/React.createElement(BlogNewPost, null)
      }]
    }, {
      path: 'mail',
      children: [{
        element: /*#__PURE__*/React.createElement(Navigate, {
          to: "/dashboard/mail/all",
          replace: true
        }),
        index: true
      }, {
        path: 'label/:customLabel',
        element: /*#__PURE__*/React.createElement(Mail, null)
      }, {
        path: 'label/:customLabel/:mailId',
        element: /*#__PURE__*/React.createElement(Mail, null)
      }, {
        path: ':systemLabel',
        element: /*#__PURE__*/React.createElement(Mail, null)
      }, {
        path: ':systemLabel/:mailId',
        element: /*#__PURE__*/React.createElement(Mail, null)
      }]
    }, {
      path: 'chat',
      children: [{
        element: /*#__PURE__*/React.createElement(Chat, null),
        index: true
      }, {
        path: 'new',
        element: /*#__PURE__*/React.createElement(Chat, null)
      }, {
        path: ':conversationKey',
        element: /*#__PURE__*/React.createElement(Chat, null)
      }]
    }, {
      path: 'calendar',
      element: /*#__PURE__*/React.createElement(Calendar, null)
    }, {
      path: 'kanban',
      element: /*#__PURE__*/React.createElement(Kanban, null)
    }]
  }, // Main Routes
  {
    path: '*',
    element: /*#__PURE__*/React.createElement(LogoOnlyLayout, null),
    children: [{
      path: 'coming-soon',
      element: /*#__PURE__*/React.createElement(ComingSoon, null)
    }, {
      path: 'maintenance',
      element: /*#__PURE__*/React.createElement(Maintenance, null)
    }, {
      path: 'pricing',
      element: /*#__PURE__*/React.createElement(Pricing, null)
    }, {
      path: 'payment',
      element: /*#__PURE__*/React.createElement(Payment, null)
    }, {
      path: '500',
      element: /*#__PURE__*/React.createElement(Page500, null)
    }, {
      path: '404',
      element: /*#__PURE__*/React.createElement(NotFound, null)
    }, {
      path: '*',
      element: /*#__PURE__*/React.createElement(Navigate, {
        to: "/404",
        replace: true
      })
    }]
  }, {
    path: '/',
    element: /*#__PURE__*/React.createElement(MainLayout, null),
    children: [{
      element: /*#__PURE__*/React.createElement(HomePage, null),
      index: true
    }, {
      path: 'about-us',
      element: /*#__PURE__*/React.createElement(About, null)
    }, {
      path: 'contact-us',
      element: /*#__PURE__*/React.createElement(Contact, null)
    }, {
      path: 'faqs',
      element: /*#__PURE__*/React.createElement(Faqs, null)
    }]
  }, {
    path: '*',
    element: /*#__PURE__*/React.createElement(Navigate, {
      to: "/404",
      replace: true
    })
  }]);
} // IMPORT COMPONENTS
// Authentication

const Login = Loadable( /*#__PURE__*/lazy(() => import('../pages/auth/Login')));
const Register = Loadable( /*#__PURE__*/lazy(() => import('../pages/auth/Register')));
const ResetPassword = Loadable( /*#__PURE__*/lazy(() => import('../pages/auth/ResetPassword')));
const VerifyCode = Loadable( /*#__PURE__*/lazy(() => import('../pages/auth/VerifyCode'))); // Dashboard

const GeneralApp = Loadable( /*#__PURE__*/lazy(() => import('../pages/dashboard/GeneralApp')));
const GeneralEcommerce = Loadable( /*#__PURE__*/lazy(() => import('../pages/dashboard/GeneralEcommerce')));
const GeneralAnalytics = Loadable( /*#__PURE__*/lazy(() => import('../pages/dashboard/GeneralAnalytics')));
const GeneralBanking = Loadable( /*#__PURE__*/lazy(() => import('../pages/dashboard/GeneralBanking')));
const GeneralBooking = Loadable( /*#__PURE__*/lazy(() => import('../pages/dashboard/GeneralBooking')));
const EcommerceShop = Loadable( /*#__PURE__*/lazy(() => import('../pages/dashboard/EcommerceShop')));
const EcommerceProductDetails = Loadable( /*#__PURE__*/lazy(() => import('../pages/dashboard/EcommerceProductDetails')));
const EcommerceProductList = Loadable( /*#__PURE__*/lazy(() => import('../pages/dashboard/EcommerceProductList')));
const EcommerceProductCreate = Loadable( /*#__PURE__*/lazy(() => import('../pages/dashboard/EcommerceProductCreate')));
const EcommerceCheckout = Loadable( /*#__PURE__*/lazy(() => import('../pages/dashboard/EcommerceCheckout')));
const EcommerceInvoice = Loadable( /*#__PURE__*/lazy(() => import('../pages/dashboard/EcommerceInvoice')));
const BlogPosts = Loadable( /*#__PURE__*/lazy(() => import('../pages/dashboard/BlogPosts')));
const BlogPost = Loadable( /*#__PURE__*/lazy(() => import('../pages/dashboard/BlogPost')));
const BlogNewPost = Loadable( /*#__PURE__*/lazy(() => import('../pages/dashboard/BlogNewPost')));
const UserProfile = Loadable( /*#__PURE__*/lazy(() => import('../pages/dashboard/UserProfile')));
const UserCards = Loadable( /*#__PURE__*/lazy(() => import('../pages/dashboard/UserCards')));
const UserList = Loadable( /*#__PURE__*/lazy(() => import('../pages/dashboard/UserList')));
const UserAccount = Loadable( /*#__PURE__*/lazy(() => import('../pages/dashboard/UserAccount')));
const UserCreate = Loadable( /*#__PURE__*/lazy(() => import('../pages/dashboard/UserCreate')));
const Chat = Loadable( /*#__PURE__*/lazy(() => import('../pages/dashboard/Chat')));
const Mail = Loadable( /*#__PURE__*/lazy(() => import('../pages/dashboard/Mail')));
const Calendar = Loadable( /*#__PURE__*/lazy(() => import('../pages/dashboard/Calendar')));
const Kanban = Loadable( /*#__PURE__*/lazy(() => import('../pages/dashboard/Kanban'))); // Main

const HomePage = Loadable( /*#__PURE__*/lazy(() => import('../pages/Home')));
const About = Loadable( /*#__PURE__*/lazy(() => import('../pages/About')));
const Contact = Loadable( /*#__PURE__*/lazy(() => import('../pages/Contact')));
const Faqs = Loadable( /*#__PURE__*/lazy(() => import('../pages/Faqs')));
const ComingSoon = Loadable( /*#__PURE__*/lazy(() => import('../pages/ComingSoon')));
const Maintenance = Loadable( /*#__PURE__*/lazy(() => import('../pages/Maintenance')));
const Pricing = Loadable( /*#__PURE__*/lazy(() => import('../pages/Pricing')));
const Payment = Loadable( /*#__PURE__*/lazy(() => import('../pages/Payment')));
const Page500 = Loadable( /*#__PURE__*/lazy(() => import('../pages/Page500')));
const NotFound = Loadable( /*#__PURE__*/lazy(() => import('../pages/Page404')));