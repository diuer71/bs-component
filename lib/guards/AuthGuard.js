import React from 'react';
import PropTypes from 'prop-types';
import { useState } from 'react';
import { Navigate, useLocation } from 'react-router-dom'; // hooks

import useAuth from '../hooks/useAuth'; // pages

import Login from '../pages/auth/Login'; // components

import LoadingScreen from '../components/LoadingScreen'; // ----------------------------------------------------------------------

AuthGuard.propTypes = {
  children: PropTypes.node
};
export default function AuthGuard({
  children
}) {
  const {
    isAuthenticated,
    isInitialized
  } = useAuth();
  const {
    pathname
  } = useLocation();
  const [requestedLocation, setRequestedLocation] = useState(null);

  if (!isInitialized) {
    return /*#__PURE__*/React.createElement(LoadingScreen, null);
  }

  if (!isAuthenticated) {
    if (pathname !== requestedLocation) {
      setRequestedLocation(pathname);
    }

    return /*#__PURE__*/React.createElement(Login, null);
  }

  if (requestedLocation && pathname !== requestedLocation) {
    setRequestedLocation(null);
    return /*#__PURE__*/React.createElement(Navigate, {
      to: requestedLocation
    });
  }

  return /*#__PURE__*/React.createElement(React.Fragment, null, children);
}